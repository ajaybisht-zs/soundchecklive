<?php
use Migrations\AbstractSeed;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users seed.
 */
class StUsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'role_id' => '1',
                'email' => 'admin@soundchecklive.com',
                'password' => (new DefaultPasswordHasher)->hash('admin@123'),
                'status' => '1',
                'forgot_token' => NULL,
                'created' => '2019-07-09 00:00:00',
                'modified' => '2019-08-02 09:30:30',
                'step_completed' => '0',
                'stripe_customer_id' => '',
                'is_deleted' => '0',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
