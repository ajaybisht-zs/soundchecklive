<?php
use Migrations\AbstractSeed;

/**
 * Colors seed.
 */
class ColorsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'hex_code' => '#00FF00',
                'name' => 'Lime',
                'created' => '2019-08-01 09:21:27',
                'modified' => '2019-08-01 09:21:27',
            ],
            [
                'id' => '2',
                'hex_code' => '#008000',
                'name' => 'Green',
                'created' => '2019-08-01 09:21:52',
                'modified' => '2019-08-01 09:21:52',
            ],
            [
                'id' => '3',
                'hex_code' => '#00FFFF',
                'name' => 'Aqua',
                'created' => '2019-08-01 09:22:23',
                'modified' => '2019-08-01 09:22:23',
            ],
            [
                'id' => '4',
                'hex_code' => '#008080',
                'name' => 'Teal',
                'created' => '2019-08-01 09:23:01',
                'modified' => '2019-08-01 09:23:01',
            ],
        ];

        $table = $this->table('colors');
        $table->insert($data)->save();
    }
}
