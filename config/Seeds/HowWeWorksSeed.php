<?php
use Migrations\AbstractSeed;

/**
 * HowWeWorks seed.
 */
class HowWeWorksSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '9',
                'user_id' => '0',
                'title' => 'How we wrok1',
                'video_name' => 'SampleVideo_1280x720_1mb.mp4',
                'video_dir' => 'webroot/files/HowWeWorks/video_name/',
                'created' => '2019-09-17 09:02:12',
                'modified' => '2019-09-17 09:35:19',
            ],
        ];

        $table = $this->table('how_we_works');
        $table->insert($data)->save();
    }
}
