<?php
use Migrations\AbstractSeed;

/**
 * Plans seed.
 */
class PlansSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '3',
                'title' => 'Free',
                'stripe_plan_id' => '',
                'price' => '0',
                'frequency' => 'monthly',
                'currency' => 'USD',
                'description' => '<li class="list-group-item bg-transparent">50 Mobile Short Codes</li>
<li class="list-group-item bg-transparent">Access to the platform</li>',
                'is_activated' => '1',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '4',
                'title' => 'Sound',
                'stripe_plan_id' => '',
                'price' => '10000',
                'frequency' => 'monthly',
                'currency' => 'USD',
                'description' => '<li class="list-group-item bg-transparent">250 Mobile Short Codes Monthly
</li>
<li class="list-group-item bg-transparent">Distribution to Listener Pool</li>
<li class="list-group-item bg-transparent">Detailed User Reporting</li>',
                'is_activated' => '1',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '5',
                'title' => 'Sound Check
',
                'stripe_plan_id' => '',
                'price' => '25000',
                'frequency' => 'monthly',
                'currency' => 'USD',
                'description' => '<li class="list-group-item bg-transparent">$250/Month</li>
<li class="list-group-item bg-transparent">500 Mobile Short Codes Monthly</li>
<li class="list-group-item bg-transparent">Distribution to Listener Pool</li>
<li class="list-group-item bg-transparent">Detailed User Reporting</li>
<li class="list-group-item bg-transparent">Ground Promotion</li>
<li class="list-group-item bg-transparent">3 Contracted Live Performances Annually</li>',
                'is_activated' => '1',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '6',
                'title' => 'Sound Check Live',
                'stripe_plan_id' => '',
                'price' => '75000',
                'frequency' => 'monthly',
                'currency' => 'USD',
                'description' => '<li class="list-group-item bg-transparent">$750/Month)</li>
<li class="list-group-item bg-transparent">Dedicated Representative assigned to record</li>
<li class="list-group-item bg-transparent">1,000 Mobile Short Codes Monthly</li>
<li class="list-group-item bg-transparent">Distribution to Listener Pool</li>
<li class="list-group-item bg-transparent">Detailed User Reporting</li>
<li class="list-group-item bg-transparent">Ground Promotion</li>
<li class="list-group-item bg-transparent">Pitched to top Investor Pool</li>
<li class="list-group-item bg-transparent">Invitation to annual SoundCheckLive private dinner</li>
<li class="list-group-item bg-transparent">Distribution to DJ pool</li>
<li class="list-group-item bg-transparent">Artist Billboard Rotation</li>
<li class="list-group-item bg-transparent">Weekly Focus Meeting</li>',
                'is_activated' => '1',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
        ];

        $table = $this->table('plans');
        $table->insert($data)->save();
    }
}
