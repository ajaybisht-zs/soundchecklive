<?php
use Migrations\AbstractSeed;

/**
 * Genres seed.
 */
class GenresSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '20',
                'name' => 'Dubstep',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '1',
                'name' => 'Alternative',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '2',
                'name' => 'Country',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '3',
                'name' => 'Dance',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '4',
                'name' => 'EDM',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '5',
                'name' => 'Electronic',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '6',
                'name' => 'Hip Hop',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '7',
                'name' => 'Hip-Hop/Rap',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '8',
                'name' => 'Indie',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '9',
                'name' => 'R&amp;B',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '10',
                'name' => 'Rap',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '11',
                'name' => 'Rock',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '12',
                'name' => 'Soul',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '13',
                'name' => 'Club',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '14',
                'name' => 'Funk',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '15',
                'name' => 'Hip/Hop',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '16',
                'name' => 'Jazz',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '17',
                'name' => 'Reggae',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '18',
                'name' => 'Reggea',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'id' => '19',
                'name' => 'Hip-Hop',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
        ];

        $table = $this->table('genres');
        $table->insert($data)->save();
    }
}
