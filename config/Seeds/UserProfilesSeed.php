<?php
use Migrations\AbstractSeed;

/**
 * UserProfiles seed.
 */
class UserProfilesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => '1',
                'genre_id' => NULL,
                'first_name' => 'Joel',
                'last_name' => 'Nelson',
                'phone_number' => '45345',
                'zipcode' => 'fgsdfg',
                'is_signed' => '0',
                'signed_by' => NULL,
                'biography' => NULL,
                'is_performance_rights_affiliated' => '0',
                'performance_rights_organization' => NULL,
                'avatar' => NULL,
                'avatar_dir' => NULL,
                'capital_goals' => NULL,
                'budget_usage' => NULL,
                'is_ready_for_release' => '0',
                'birth_year' => '1',
                'birth_month' => '2',
                'birth_day' => '2000',
                'created' => '2019-07-25 07:43:59',
                'modified' => '2019-07-25 07:43:59',
                'banner' => '',
                'banner_dir' => '',
                'artist_name' => '',
                'smart_link' => '',
            ],
        ];

        $table = $this->table('user_profiles');
        $table->insert($data)->save();
    }
}
