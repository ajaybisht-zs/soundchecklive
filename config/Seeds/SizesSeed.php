<?php
use Migrations\AbstractSeed;

/**
 * Sizes seed.
 */
class SizesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'size' => 'XL',
                'created' => '2019-08-05 06:41:51',
                'modified' => '2019-08-05 06:41:51',
            ],
            [
                'id' => '2',
                'size' => 'L',
                'created' => '2019-08-05 06:42:00',
                'modified' => '2019-08-05 06:42:00',
            ],
            [
                'id' => '3',
                'size' => 'S',
                'created' => '2019-08-05 06:42:32',
                'modified' => '2019-08-05 06:42:32',
            ],
        ];

        $table = $this->table('sizes');
        $table->insert($data)->save();
    }
}
