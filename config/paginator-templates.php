<?php
return [
    'number' => '<li  class="page-item"><a class="page-link text-blue-dark" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a class="page-link text-blue-dark" href="{{url}}">{{text}}</a></li>',
    'prevActive' => '<li  class="page-item"><a class="page-link text-blue-dark" href="{{url}}" aria-label="Previous"><span aria-hidden="true">Prev</span></a></li>',
    'prevDisabled' => '<li class="page-item disabled"><a class="page-link text-blue-dark" href="javascript:void(0);" aria-label="Previous"><span aria-hidden="true">Prev</span></a></li>',
    'nextActive' => '<li class="page-item"><a class="page-link text-blue-dark" href="{{url}}" aria-label="Next"><span aria-hidden="true">Next</span></a></li>',
    'nextDisabled' => '<li class=" page-item disabled"><a class="page-link text-blue-dark" href="javascript:void(0);" aria-label="Next"><span aria-hidden="true">Next</span></a></li>',
];