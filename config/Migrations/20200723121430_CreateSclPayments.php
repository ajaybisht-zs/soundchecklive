<?php
use Migrations\AbstractMigration;

class CreateSclPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */

    public function change()
    {
        $table = $this->table('scl_payments')

        ->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'comment' => 'artist id'
        ])
        ->addColumn('transition_id', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])
        ->addColumn('amount', 'decimal', [
            'default' => null,
            'null' => false,
            'precision' => 10,
            'scale' => 2,
        ])
        ->addColumn('card_holder_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])

        ->addColumn('status', 'boolean', [
            'comment' => '1 => success , 2 => failure',
            'default' => '0',
            'limit' => 4,
            'null' => false,
        ])
        ->addColumn('failure_reason', 'text', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ])
        ->create();
    }
}
