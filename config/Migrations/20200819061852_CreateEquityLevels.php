<?php
use Migrations\AbstractMigration;

class CreateEquityLevels extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('equity_levels');
        $table->addColumn('level_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('follower', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('stack', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => true,
        ]);
        $table->addColumn('roster_slot', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ]);
        $table->addColumn('advance_notic', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);
        $table->create();
        $table->insert([
            [
                'level_name' => 'E',
                'follower' => '25,000 - 49,999',
                'stack' => 1,
                'roster_slot' => 3,
                'advance_notic' => '',
            ],
            [
                'level_name' => 'Q',
                'follower' =>  '50,000 - 249,999',
                'stack' =>  3,
                'roster_slot' =>  5,
                'advance_notic' => '',
            ],
            [
                'level_name' => 'U',
                'follower' => '250,000 - 999,999',
                'stack' => 5,
                'roster_slot' => 8,
                'advance_notic' => '',
            ],
            [
                'level_name' => 'I',
                'follower' => '1 Million - 2,999,999',
                'stack' => 8,
                'roster_slot' => 10,
                'advance_notic' => 24,
            ],
            [
                'level_name' => 'T',
                'follower' => '3 Million - 4,999,999',
                'stack' => 11,
                'roster_slot' => 12,
                'advance_notic' => 12,
            ],
            [
                'level_name' => 'Y',
                'follower' => '5 Million+',
                'stack' => 15,
                'roster_slot' => 20,
                'advance_notic' => 8,
            ]
        ])->save();
    }
}
