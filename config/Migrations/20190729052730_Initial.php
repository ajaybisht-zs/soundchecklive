<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Initial extends AbstractMigration
{
    public function up()
    {

        $this->table('genres')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('plans')
            ->addColumn('title', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('stripe_plan_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('price', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('frequency', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('currency', 'string', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('is_activated', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('roles')
            ->addColumn('type', 'text', [
                'comment' => '1 => SuperAdmin, 2 => Admin, 3=>User',
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $this->table('user_bank_details')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bank_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ])
            ->addColumn('name_on_account', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('account_number', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ])
            ->addColumn('routing_number', 'string', [
                'default' => null,
                'limit' => 40,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_events')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('city', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('event_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('event_time', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('link_for_ticket', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('image', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('image_dir', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_media_handles')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('facebook', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('twitter', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('instagram', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_merchandises')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ])
            ->addColumn('price', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_plans')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('plan_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('status', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('user_profiles')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('genre_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('first_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ])
            ->addColumn('last_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ])
            ->addColumn('phone_number', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('zipcode', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('is_signed', 'boolean', [
                'comment' => '0 -> independent
1 -> signed',
                'default' => '0',
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('signed_by', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('biography', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('is_performance_rights_affiliated', 'boolean', [
                'comment' => '0->none
1-> I don\'t know',
                'default' => '0',
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('performance_rights_organization', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('avatar', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('avatar_dir', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('capital_goals', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('budget_usage', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('is_ready_for_release', 'boolean', [
                'comment' => '0-> still need work
1-> ready',
                'default' => '0',
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('birth_year', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('birth_month', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('birth_day', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_record_keys')
            ->addColumn('user_record_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('keyword', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_record_id',
                ]
            )
            ->create();

        $this->table('user_records')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('record_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('record_written_by', 'boolean', [
                'comment' => '0->Yes, I wrote the record myself
1->Yes, I wrote the record along with oter writers
2->No, somebody else wrote the record for me',
                'default' => '0',
                'limit' => 3,
                'null' => true,
            ])
            ->addColumn('is_already_distributed', 'boolean', [
                'comment' => '0->No
1->Yes',
                'default' => '0',
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('cover_image', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('cover_image_dir', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('artist_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('features', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('producer', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('writer', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('record_file', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('record_dir', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('video_pitch_file', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('video_pitch_path', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('users')
            ->addColumn('role_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('status', 'boolean', [
                'comment' => '1 => approved , => disapproved',
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('forgot_token', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('stripe_customer_id', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ])
            ->addColumn('step_completed', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('user_bank_details')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_events')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_media_handles')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_merchandises')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_profiles')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_record_keys')
            ->addForeignKey(
                'user_record_id',
                'user_records',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_records')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('user_bank_details')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('user_events')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('user_media_handles')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('user_merchandises')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('user_profiles')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('user_record_keys')
            ->dropForeignKey(
                'user_record_id'
            )->save();

        $this->table('user_records')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('genres')->drop()->save();
        $this->table('plans')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('user_bank_details')->drop()->save();
        $this->table('user_events')->drop()->save();
        $this->table('user_media_handles')->drop()->save();
        $this->table('user_merchandises')->drop()->save();
        $this->table('user_plans')->drop()->save();
        $this->table('user_profiles')->drop()->save();
        $this->table('user_record_keys')->drop()->save();
        $this->table('user_records')->drop()->save();
        $this->table('users')->drop()->save();
    }
}
