<?php
use Migrations\AbstractMigration;

class AddSmartlinkToUserProfile extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_profiles');
        $table->addColumn('smart_link', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]);
        $table->update();
    }
}
