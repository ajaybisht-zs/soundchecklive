<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;


class AddExtraStepToUserProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_profiles');

        $table->addColumn('gender', 'integer', [
                'comment' => '1 => male , => 2 => female',
                'default' => null,
                'limit' => 4,
                'null' => true,
        ]);

        $table->addColumn('ethnicity', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => true
        ]);

        $table->addColumn('is_booking_agent', 'boolean', [
            'limit' => MysqlAdapter::INT_TINY,
            'length' => 1,
            'default' => 0,
            'comment' => '0-> No, 1-> Yes',
            'null' => true
        ]);

        $table->addColumn('is_performed_overseas', 'boolean', [
            'limit' => MysqlAdapter::INT_TINY,
            'length' => 1,
            'default' => 0,
            'comment' => '0-> No, 1-> Yes',
            'null' => true
        ]);

        $table->addColumn('is_merchandise_sale', 'boolean', [
            'limit' => MysqlAdapter::INT_TINY,
            'length' => 1,
            'default' => 0,
            'comment' => '0-> No, 1-> Yes',
            'null' => true
        ]);

        $table->addColumn('country_id', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => true
        ]);

        $table->addColumn('state_id', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => true
        ]);

        $table->addColumn('city', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('scl_score', 'decimal', [
            'null' => true,
            'precision' => 10,
            'scale' => 2,
            'default' => 0
        ]);

        $table->update();
    }
}
