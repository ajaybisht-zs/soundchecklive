<?php
use Migrations\AbstractMigration;

class AddYoutubeToUserMediaHandles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_media_handles');
        $table->addColumn('youtube', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->update();
    }
}
