<?php
use Migrations\AbstractMigration;

class CreateProofOfConcepts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('proof_of_concepts');

        $table->addColumn('amount', 'decimal', [
            'default' => null,
            'null' => false,
            'precision' => 10,
            'scale' => 2,
        ]);

        $table->addColumn('people_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);

        $table->addColumn('email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('charge_id', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        
        $table->addColumn('first_name', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);

        $table->addColumn('last_name', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('address', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => false,
        ]);

        $table->addColumn('zip_code', 'string', [
            'default' => null,
            'limit' => 14,
            'null' => false,
        ]);

        $table->addColumn('city', 'string', [
            'default' => null,
            'limit' => 150,
            'null' => false,
        ]);

        $table->addColumn('country_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);

        $table->addColumn('phone_number', 'string', [
            'default' => null,
            'limit' => 15,
            'null' => false,
        ]);

        $table->addColumn('status', 'integer', [
            'comment' => '1 => success , 2 => failure',
            'default' => '0',
            'limit' => 4,
            'null' => false,
        ]);

        $table->addColumn('failure_reason', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);

        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
