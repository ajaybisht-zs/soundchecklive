<?php
use Migrations\AbstractMigration;

class AddBrandNameToFashionDesignerProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fashion_designer_profiles');
        $table->addColumn('brand_name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
        ]);
        $table->update();
    }
}
