<?php
use Migrations\AbstractMigration;

class AddExtraMediaToUserMediaHandles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_media_handles');

        $table->addColumn('tiktok', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->addColumn('twitch', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->addColumn('snapchat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->update();
    }
}
