<?php
use Migrations\AbstractMigration;

class AddBannerToUserProfiled extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_profiles');
        
        $table->addColumn('banner', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('banner_dir', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        
        $table->update();
    }
}
