<?php
use Migrations\AbstractMigration;

class AddColumnTypeToUserRecordPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_record_payments');

        $table->addColumn('type', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => false,
            'comment' => '0 => Record Partner, 1 => Pitch Partner'
        ]);

        $table->update();
    }
}
