<?php
use Migrations\AbstractMigration;

class AddChatSessionIdToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('chat_session_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
        ]);
        $table->update();
    }
}
