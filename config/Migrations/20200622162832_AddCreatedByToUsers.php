<?php
use Migrations\AbstractMigration;

class AddCreatedByToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('created_by', 'boolean', [
                'comment' => '1 => created by admin ,0 => disapproved',
                'default' => '0',
                'limit' => 2,
                'null' => true,
            ]);
        $table->update();
    }
}
