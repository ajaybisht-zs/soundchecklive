<?php
use Migrations\AbstractMigration;

class CreateFilmGenres extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('filmgenres');
        $table->addColumn('name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ]);
        $table->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);
        $table->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);
        $table->create();

        $table->insert([
            [
                'name' => 'Action',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Adventure',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Classic',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Comedy',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Crime',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Dance',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Epics',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Gangster',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Historical',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Horror',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Musicals',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Sci-Fi',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'War',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Westerns',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
        ])->save();
    }
}
