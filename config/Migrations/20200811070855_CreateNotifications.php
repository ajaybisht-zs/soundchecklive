<?php
use Migrations\AbstractMigration;

class CreateNotifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('notifications');
        $table->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]);
        $table->addColumn('is_readed', 'integer', [
            'comment' => '1 => accepted , 0 => declined',
            'default' => null,
            'limit' => 4,
            'null' => true,
        ]);
        $table->addColumn('call_durantion', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
        ]);
        $table->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
        ]);
        $table->create();
    }
}
