<?php
use Migrations\AbstractMigration;

class CreateFilmDirectorMediaHandles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('film_director_media_handles');
        $table->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]);
        $table->addColumn('facebook', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ]);
        $table->addColumn('twitter', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ]);
        $table->addColumn('instagram', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ]);
        $table->addColumn('youtube', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
            ]);
        $table->addColumn('tiktok', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->addColumn('twitch', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->addColumn('snapchat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->create();   
    }
}
