<?php
use Migrations\AbstractMigration;

class CreateUserSupporters extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_supporters');

        $table->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
        ]);

        $table->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
        ]);

        $table->addColumn('phone_number', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
        ]);

        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);

        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);

        $table->create();
    }
}
