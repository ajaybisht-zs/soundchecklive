<?php
use Migrations\AbstractMigration;

class CreateInfluencerProfile extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('influencer_profile');
        $table->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]);
        $table->addColumn('equity_level_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);
        $table->addColumn('first_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ]);
        $table->addColumn('last_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ]);

        $table->addColumn('phone_number', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]);
        $table->addColumn('zipcode', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]);

        $table->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
        ]);

        $table->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
        ]);
        $table->create();
    }
}
