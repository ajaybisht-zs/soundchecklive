<?php
use Migrations\AbstractMigration;

class AlterAssignDeafaultValueToUserProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_profiles');
        $table->changeColumn('banner', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->changeColumn('banner_dir', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->changeColumn('artist_name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->changeColumn('smart_link', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);
        $table->update();
    }
}
