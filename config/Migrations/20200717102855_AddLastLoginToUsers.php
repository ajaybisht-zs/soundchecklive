<?php
use Migrations\AbstractMigration;

class AddLastLoginToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('last_login', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);
        $table->update();
    }
}
