<?php
use Migrations\AbstractMigration;

class AddStatusToUserMerchandises extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_merchandises');
        $table->addColumn('status', 'boolean', [
            'comment' => '1 => approved , 0 => disapproved',
            'default' => null,
            'limit' => 4,
            'null' => false,
        ]);
        $table->update();
    }
}
