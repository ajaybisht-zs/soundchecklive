<?php
use Migrations\AbstractMigration;

class CreateStyles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('styles');
        $table->addColumn('name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ]);
        $table->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);
        $table->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);
        $table->create();

        $table->insert([
            [
                'name' => 'Casual',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Office Wear',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Sports Wear',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Classic',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Exotic',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Street',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Footwear',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Glasses',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Vintage',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Chic',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Arty',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Preppy',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Bohemian',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Goth',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Grunge',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Flamboyant',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Punk',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Rocker',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Tomboy',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Jewelry',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
            [
                'name' => 'Accessories',
                'created' => '2019-07-23 12:40:08',
                'modified' => '2019-07-23 12:40:08',
            ],
        ])->save();
    }
}
