<?php
use Migrations\AbstractMigration;

class AddCoverImageToFilmDirectorProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('film_director_profiles');
        $table->addColumn('video_pitch_image', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]);

        $table->addColumn('video_pitch_image_dir', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
        ]);
        $table->update();
    }
}
