<?php
use Migrations\AbstractMigration;

class CreateSiteDowns extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('site_downs');
        $table->addColumn('status', 'boolean', [
                'comment' => '0->No 1->Yes',
                'default' => '0',
                'null' => false,
                'limit' => 2
        ]);
        $table->create();

        $table->insert([
            [
                'id' => 1,
                'status' => 0
            ]
        ])->save();
    }
}
