<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateFashionDesignerProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fashion_designer_profiles');

        $table->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]);
        $table->addColumn('style_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);
        $table->addColumn('first_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ]);
        $table->addColumn('last_name', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => false,
            ]);
        $table->addColumn('phone_number', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]);
        $table->addColumn('zipcode', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]);
        $table->addColumn('is_licensed_brand_or_independent_line', 'boolean', [
                'comment' => '0 -> licensed,1 -> independent line',
                'default' => '0',
                'limit' => 2,
                'null' => true,
            ]);

        $table->addColumn('parent_company', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
        ]);

        $table->addColumn('biography', 'text', [
                'default' => null,
                'limit' => 500,
                'null' => true,
        ]);

        $table->addColumn('avatar', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('avatar_dir', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('what_type_budget_seeking', 'integer', [
                'default' => null,
                'limit' => 8,
                'null' => true,
        ]);

        $table->addColumn('buget_used_for_roi', 'text', [
                'default' => null,
                'limit' => 500,
                'null' => true,
        ]);

        $table->addColumn('garment_pic', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('garment_dir', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('process_video', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('process_video_dir', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('pitch_video', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);

        $table->addColumn('pitch_video_dir', 'string', [
                'default' => null,
                'limit' => 245,
                'null' => true,
        ]);


        $table->addColumn('birth_year', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);

        $table->addColumn('birth_month', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);

        $table->addColumn('birth_day', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]);


        $table->addColumn('keyword_1', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
        ]);

        $table->addColumn('keyword_2', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
        ]);

        $table->addColumn('keyword_3', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
        ]);

        $table->addColumn('keyword_4', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
        ]);

        $table->addColumn('keyword_5', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
        ]);

        $table->addColumn('gender', 'integer', [
                'comment' => '1 => male , => 2 => female',
                'default' => null,
                'limit' => 4,
                'null' => true,
        ]);

        $table->addColumn('ethnicity', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => true
        ]);

        $table->addColumn('is_brand_manager_agent', 'boolean', [
            'limit' => MysqlAdapter::INT_TINY,
            'length' => 1,
            'default' => 0,
            'comment' => '0-> No, 1-> Yes',
            'null' => true
        ]);

        $table->addColumn('is_performed_overseas', 'boolean', [
            'limit' => MysqlAdapter::INT_TINY,
            'length' => 1,
            'default' => 0,
            'comment' => '0-> No, 1-> Yes',
            'null' => true
        ]);

        $table->addColumn('is_merchandise_sale', 'boolean', [
            'limit' => MysqlAdapter::INT_TINY,
            'length' => 1,
            'default' => 0,
            'comment' => '0-> No, 1-> Yes',
            'null' => true
        ]);

        $table->addColumn('country_id', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => true
        ]);

        $table->addColumn('state_id', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => true
        ]);

        $table->addColumn('city', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('scl_score', 'decimal', [
            'null' => true,
            'precision' => 10,
            'scale' => 2,
            'default' => 0
        ]);

        $table->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);

         $table->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);
        $table->create();
    }
}
