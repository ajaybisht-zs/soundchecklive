<?php
use Migrations\AbstractMigration;

class CreateLevels extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('levels');
        $table->addColumn('level_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('price', 'decimal', [
            'default' => null,
            'null' => false,
            'precision' => 10,
            'scale' => 2,
        ]);
        $table->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]);
        $table->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
        ]);
        $table->create();

        $table->insert([
            [
                'level_name' => 1,
                'price' => 0.99,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 2,
                'price' => 1.06,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 3,
                'price' => 4.24,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 4,
                'price' => 16.96,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 5,
                'price' => 67.82,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 6,
                 'price' => 271.72,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 7,
                'price' => 542.54,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 8,
                 'price' => 1085.07,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 9,
                 'price' => 2170.14,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
            [
                'level_name' => 10,
                 'price' => 4340.28,
                'created' => '2020-07-23 12:40:08',
                'modified' => '2020-07-23 12:40:08',
            ],
        ])->save();
    }
}
