<?php 
define('ROLE_ADMIN', 1);
define('ROLE_ARTIST', 2);
define('ROLE_FAN', 3);
define('ROLE_FASHION_DESIGINER', 4);
define('ROLE_FILM_DIRECTOR', 5);
define('INFLUENCER', 6);

return [
    'Roles' => [
        'admin' => ROLE_ADMIN,
        'artist' => ROLE_ARTIST,
        'fan' => ROLE_FAN,
        'designer' => ROLE_FASHION_DESIGINER,
        'director' => ROLE_FILM_DIRECTOR,
        'influencer' => INFLUENCER,
    ]
];
