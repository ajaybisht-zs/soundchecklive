
# CakePHP Application Skeleton

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

 **Requirements**
		- PHP 5.6.0 or greater (including PHP 7.3)
		- mbstring PHP extension
		- intl PHP extension
		- simplexml PHP extension
		- PDO PHP extension
		- GD2 php extension
		- BC-Math php extension
		- Set post_max_size=200M
		- Set upload_max_filesize=200M

1.  Clone project repository from git and merge development branch to master.
2. Run composer update.
3. Run command to create Logs and tmp folder
```bash
mkdir logs tmp
```
4. Give permission to logs and tmp folder by running following command
```bash
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:${HTTPDUSER}:rwx tmp
setfacl -R -d -m u:${HTTPDUSER}:rwx tmp
setfacl -R -m u:${HTTPDUSER}:rwx logs
setfacl -R -d -m u:${HTTPDUSER}:rwx logs
```
5. create .env file in config folder and set database and smtp settings in keys DATABASE_URL, EMAIL_TRANSPORT_DEFAULT_URL
6. Add Admin email address in .env file ```export ADMIN_EMAIL_ADDRESS="soundchecklive@outlook.com"```
7. Run migrations to create table . ```bin/cake migrations migrate ```
8. Run seed command. ```bin/cake migrations seed```
 





