<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Time;

class commonHelper extends Helper
{
	/**
	* Check if mp3 exist
	* @param mp3 directory and file name
	* @return true|false
	*/
	public function checkMp3($dirName, $fileName)
	{
		$mp3Path = null;
		if (!empty($dirName) && !empty($fileName)) {
			$fullPath = str_replace('webroot', '', __('{0}{1}', $dirName, $fileName));

			if (file_exists(WWW_ROOT. $fullPath) ) {
				$mp3Path = $fullPath;
			}
		}
		return $mp3Path;
	}

	/**
	* Check if coverimage exist
	* @param coverimage directory and file name
	* @return true|false
	*/
	public function checkCoverImage($dirName, $fileName)
	{
		$coverImagePath = null;
		if (!empty($dirName) && !empty($fileName)) {
			$fullPath = str_replace('webroot', '', __('{0}{1}', $dirName, $fileName));
			if (file_exists(WWW_ROOT. $fullPath)) {
				$coverImagePath = $fullPath;
			}
		}
		return $coverImagePath;
	}

	public function showMerchandiseImages($images) {
		$res = [];
		foreach($images as $val) {
			$res[] = $this->checkCoverImage($val->image_dir,$val->image);
		}
		return $res;
	}

	public function showThumbNailImage($images) {
		$res = [];
		foreach($images as $val) {
			$res[] = $this->checkCoverImage($val->image_dir,'thumbnail-'.$val->image);
		}
		return $res;
	}

	public function getBuget($id) {
		if($id == 0) {
			return '$25,000 - $50,000';
		} elseif($id == 1) {
			return '$50,001 - $100,000';
		} elseif($id == 2) {
			return '$100,001 - $250,000';
		} elseif($id == 3) {
			return '$250,001 - $500,000';
		} else {
			return '$500,001 - $1,000,000';
		}
	}
	
	public function setRadioButton() {
		return $radioTemplate =  	[
							'radioContainer' => '{{content}}',
							'radioWrapper' => '<div class="form-check form-check-inline mr-3">{{label}}</div>'
		];
	}


	public function getAllCountry()
	{
		$countriesTable = TableRegistry::get('Countries');
		$country = $countriesTable->find('list')->toArray();
		return $country;
	}


	/*
	* fetch all states
	*/
	public function getAll()
	{
		$statesTable = TableRegistry::get('States');
		$states = $statesTable->find('list')->toArray();
		return $states;
	}

	/*
	* Fetch particular state
	*
	*/
	public function findByCountryId($countryId)
	{
		$statesTable = TableRegistry::get('States');
		$states = $statesTable
				->find('list')
				->where([
					'country_id' => $countryId
				])
				->toArray();
		return $states;
	}
}
