<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Filesystem\File;

/**
 * Images helper
 */
class ImagesHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    public $helpers = ['Url'];
    protected $_defaultConfig = [];

    public function artistAvatar($artist) {
    	if(empty($artist->avatar) || empty($artist->avatar_dir) || is_array($artist->avatar)) {
            return null;
        }

        $filePath = str_replace('webroot', '', $artist->avatar_dir.$artist->avatar);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
        	return null;
        }
        
        return $filePath;
    }

    public function userProfilePic($artist) {
        if(empty($artist['avatar']) || empty($artist['avatar_dir']) || is_array($artist['avatar'])) {
            return null;
        }

        $filePath = str_replace('webroot', '', $artist['avatar_dir'].$artist['avatar']);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

    public function artistBanner($artist) {
        if(empty($artist->banner) || empty($artist->banner_dir) || is_array($artist->banner) ) {
            return null;
        }

        $filePath = str_replace('webroot', '', $artist->banner_dir.$artist->banner);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

    public function getVideoPitch($record) {
        if(empty($record->video_pitch_file) || empty($record->video_pitch_path) || is_array($record->video_pitch_file) ) {
            return null;
        }

        $filePath = str_replace('webroot', '', $record->video_pitch_path.$record->video_pitch_file);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

    public function getRecordCover($record, $thumb = null) {
        if(empty($record->cover_image) || empty($record->cover_image_dir) || is_array($record->cover_image) ) {
            return null;
        }

        $coverimage = $record->cover_image;
        
        if($thumb) {
            $coverimage = trim($thumb). $coverimage;
        }

        $filePath = str_replace('webroot', '', $record->cover_image_dir.$coverimage);

        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        return $filePath;
    }

    public function getRecordMp3($record) {
    	if(empty($record->record_file) || empty($record->record_dir) || is_array($record->record_file) ) {
            return null;
        }

        $filePath = str_replace('webroot', '', $record->record_dir.$record->record_file);

        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
        	return null;
        }
        return $filePath;
    }



    public function getSongObject($record) {      
        $mp3 = $this->getRecordMp3($record);
        $cover = $this->getRecordCover($record, 'thumbnail-');
        
        if(!$mp3 ) {
            return null;
        }

        if(!$cover ) {
            $cover = '/img/default-audio.png';
        }

        $mp3Url = $this->Url->build($mp3,['fullBase' => true]);
        $coverUrl = $this->Url->build($cover,['fullBase' => true]);

        return base64_encode(json_encode([
            "name"  => $record->record_name,
            "artist"    => $record->artist_name,
            "album" => "SoundCheckLive",
            "url"   => $mp3Url,
            "cover_art_url" => $coverUrl
        ]));
    }

    public function checkCoverImage($dirName, $fileName)
    {
        $coverImagePath = null;
        if (!empty($dirName) && !empty($fileName)) {
            $fullPath = str_replace('webroot', '', __('{0}{1}', $dirName, $fileName));
            if (file_exists(WWW_ROOT. $fullPath)) {
                $coverImagePath = $fullPath;
            }
        }
        return $coverImagePath;
    }

    public function getArtistMerchandiseImage($images) {
        //'thumbnail-'.
        $res = [];
        foreach($images as $val) {
            $res = $this->checkCoverImage($val->image_dir,'thumbnail-'.$val->image);
        }
        return $res;
    }

    public function getUserVideoPitch($record) {
        if(empty($record->video_pitch_file) || empty($record->video_pitch_path) || is_array($record->video_pitch_file) ) {
            return null;
        }

        $filePath = str_replace('webroot', '', $record->video_pitch_path.$record->video_pitch_file);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getUserVideoPitchPoster($video) {
       if(empty($video->poster_name) || empty($video->poster_dir) || is_array($video->video_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $video->poster_dir.$video->poster_name);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getHowWeWorkVideo($video) {
       if(empty($video->video_name) || empty($video->video_dir) || is_array($video->video_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $video->video_dir.$video->video_name);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getHowWeWorkVideoPoster($video) {
       if(empty($video->poster_name) || empty($video->poster_dir) || is_array($video->video_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $video->poster_dir.$video->poster_name);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getFilmDirectorRecentlyVideo($video) {
       if(empty($video->movie_reel) || empty($video->movie_reel_dir) || is_array($video->movie_reel_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $video->movie_reel_dir.$video->movie_reel);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getFilmDirectorRecentlyVideoPoster($video) {
       if(empty($video->poster_pic) || empty($video->poster_dir) || is_array($video->poster_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $video->poster_dir.$video->poster_pic);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function filmMakerPitchVideo($artist) {
        if(empty($artist['pitch_video']) || empty($artist['pitch_video_dir']) || is_array($artist['pitch_video'])) {
            return null;
        }

        $filePath = str_replace('webroot', '', $artist['pitch_video_dir'].$artist['pitch_video']);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

    public function filmDirectorPitchVideo($artist) {
        if(empty($artist->pitch_video) || empty($artist->pitch_video_dir) || is_array($artist->pitch_video)) {
            return null;
        }

        $filePath = str_replace('webroot', '', $artist->pitch_video_dir.$artist->pitch_video);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

     public function getFashionDesignerLastestGarment($garment) {
       if(empty($garment->garment_pic) || empty($garment->garment_dir) || is_array($garment->garment_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $garment->garment_dir.$garment->garment_pic);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getArtistPitchVideo($video) {
       if(empty($video->video_pitch_image) || empty($video->video_pitch_image_dir) || is_array($video->video_pitch_image_dir) ) {
            return null;
        }
        $filePath = str_replace('webroot', '', $video->video_pitch_image_dir.$video->video_pitch_image);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        } 
        return $filePath;
    }

    public function getMovieReel($record) {
        if(empty($record->movie_reel) || empty($record->movie_reel_dir) || is_array($record->movie_reel) ) {
            return null;
        }

        $filePath = str_replace('webroot', '', $record->movie_reel_dir.$record->movie_reel);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

    public function getProcessVideo($record) {
        if(empty($record->process_video) || empty($record->process_video_dir) || is_array($record->process_video) ) {
            return null;
        }

        $filePath = str_replace('webroot', '', $record->process_video_dir.$record->process_video);
        $file = new File(WWW_ROOT.$filePath);

        if(!$file->exists() ) {
            return null;
        }
        
        return $filePath;
    }

}
