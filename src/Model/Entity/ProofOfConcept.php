<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProofOfConcept Entity
 *
 * @property int $id
 * @property float $amount
 * @property int $people_id
 * @property string $email
 * @property string $charge_id
 * @property string $first_name
 * @property string|null $last_name
 * @property string $address
 * @property string $zip_code
 * @property string $city
 * @property int $country_id
 * @property string $phone_number
 * @property int $status
 * @property string|null $failure_reason
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\Charge $charge
 */
class ProofOfConcept extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'people_id' => true,
        'email' => true,
        'charge_id' => true,
        'first_name' => true,
        'last_name' => true,
        'address' => true,
        'zip_code' => true,
        'city' => true,
        'country_id' => true,
        'phone_number' => true,
        'status' => true,
        'failure_reason' => true,
        'created' => true,
        'modified' => true,
        'person' => true,
        'charge' => true,
        'state_id' => true,
    ];
}
