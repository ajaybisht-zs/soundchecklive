<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserEvent Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $city
 * @property \Cake\I18n\FrozenDate $event_date
 * @property string $event_time
 * @property string|null $link_for_ticket
 * @property string|null $image
 * @property string|null $image_dir
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserEvent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'city' => true,
        'event_date' => true,
        'event_time' => true,
        'link_for_ticket' => true,
        'image' => true,
        'image_dir' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'user_event_images' => true
    ];
}
