<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FilmDirectorProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $filmgenre_id
 * @property string $first_name
 * @property string $last_name
 * @property string $production_company
 * @property string $phone_number
 * @property string $zipcode
 * @property bool|null $are_you_affiliated_any
 * @property string|null $affiliated_with_name
 * @property bool|null $have_distributor
 * @property string|null $distributor_name
 * @property string|null $biography
 * @property string|null $avatar
 * @property string|null $avatar_dir
 * @property int|null $what_type_budget_seeking
 * @property string|null $buget_used_for_roi
 * @property string|null $poster_pic
 * @property string|null $poster_dir
 * @property string|null $movie_reel
 * @property string|null $movie_reel_dir
 * @property string|null $pitch_video
 * @property string|null $pitch_video_dir
 * @property int|null $birth_year
 * @property int|null $birth_month
 * @property int|null $birth_day
 * @property string|null $keyword_1
 * @property string|null $keyword_2
 * @property string|null $keyword_3
 * @property string|null $keyword_4
 * @property string|null $keyword_5
 * @property int|null $gender
 * @property int|null $ethnicity
 * @property bool|null $is_booking_manager
 * @property bool|null $is_marketed_overseas
 * @property bool|null $is_merchandise_sale
 * @property int|null $country_id
 * @property int|null $state_id
 * @property string|null $city
 * @property float|null $scl_score
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Filmgenre $filmgenre
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\State $state
 */
class FilmDirectorProfile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'filmgenre_id' => true,
        'first_name' => true,
        'last_name' => true,
        'production_company' => true,
        'phone_number' => true,
        'zipcode' => true,
        'are_you_affiliated_any' => true,
        'affiliated_with_name' => true,
        'have_distributor' => true,
        'distributor_name' => true,
        'biography' => true,
        'avatar' => true,
        'avatar_dir' => true,
        'what_type_budget_seeking' => true,
        'buget_used_for_roi' => true,
        'poster_pic' => true,
        'poster_dir' => true,
        'movie_reel' => true,
        'movie_reel_dir' => true,
        'pitch_video' => true,
        'pitch_video_dir' => true,
        'birth_year' => true,
        'birth_month' => true,
        'birth_day' => true,
        'keyword_1' => true,
        'keyword_2' => true,
        'keyword_3' => true,
        'keyword_4' => true,
        'keyword_5' => true,
        'gender' => true,
        'ethnicity' => true,
        'is_booking_manager' => true,
        'is_marketed_overseas' => true,
        'is_merchandise_sale' => true,
        'country_id' => true,
        'state_id' => true,
        'city' => true,
        'scl_score' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'filmgenre' => true,
        'country' => true,
        'state' => true,
        'video_pitch_image' => true,
        'video_pitch_image_dir' => true,
    ];
}
