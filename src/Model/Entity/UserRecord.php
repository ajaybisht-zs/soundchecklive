<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserRecord Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $record_name
 * @property bool|null $record_written_by
 * @property bool|null $is_already_distributed
 * @property string $cover_image
 * @property string $cover_image_dir
 * @property string $artist_name
 * @property string|null $features
 * @property string $producer
 * @property string $writer
 * @property string $record_file
 * @property string $record_dir
 * @property string|null $video_pitch_file
 * @property string|null $video_pitch_path
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\UserRecordKey[] $user_record_keys
 */
class UserRecord extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'record_name' => true,
        'record_written_by' => true,
        'is_already_distributed' => true,
        'cover_image' => true,
        'cover_image_dir' => true,
        'artist_name' => true,
        'features' => true,
        'producer' => true,
        'writer' => true,
        'record_file' => true,
        'record_dir' => true,
        'video_pitch_file' => true,
        'video_pitch_path' => true,
        'no_of_partners' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'user_record_keys' => true,
        'pitch_partners' => true,
        'user_pitch_payments' => true,
        'video_pitch_image' => true,
        'video_pitch_image_dir' => true,
    ];

    protected function _getTotalFans() {
        if(!empty($this->user_record_payments)) {
            return count($this->user_record_payments);
        }else {
            return 0;
        }
    }

    public function _getPartner() {
        $partners = 0;
        if( !empty($this->user_record_payments)) {
            $partners = $partners + count($this->user_record_payments);
        }
        return ($this->no_of_partners) ? ($this->no_of_partners + $partners) : $partners;
    }

    public function _getPitchPartners() {
        if(!empty($this->user_pitch_payments)) {
            return count($this->user_pitch_payments);
        }
        return 0;
    }
}
