<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $genre_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $zipcode
 * @property int|null $is_signed
 * @property string|null $signed_by
 * @property string|null $biography
 * @property int|null $is_performance_rights_affiliated
 * @property string|null $performance_rights_organization
 * @property string|null $avatar
 * @property string|null $avatar_dir
 * @property string|null $capital_goals
 * @property string|null $budget_usage
 * @property int|null $is_ready_for_release
 * @property int|null $birth_year
 * @property int|null $birth_month
 * @property int|null $birth_day
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Genre $genre
 */
class UserProfile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'genre_id' => true,
        'first_name' => true,
        'last_name' => true,
        'phone_number' => true,
        'zipcode' => true,
        'is_signed' => true,
        'signed_by' => true,
        'biography' => true,
        'is_performance_rights_affiliated' => true,
        'performance_rights_organization' => true,
        'avatar' => true,
        'avatar_dir' => true,
        'capital_goals' => true,
        'budget_usage' => true,
        'is_ready_for_release' => true,
        'birth_year' => true,
        'birth_month' => true,
        'birth_day' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'banner' => true,
        'banner_dir' => true,
        'genre' => true,
        'artist_name' => true,
        'smart_link' => true,
        'gender' => true,
        'ethnicity' => true,
        'is_booking_agent' => true,
        'is_performed_overseas' => true,
        'is_merchandise_sale' => true,
        'country_id' => true,
        'state_id' => true,
        'city' => true,
        'scl_score' => true,
    ];
}
