<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserRecordKey Entity
 *
 * @property int $id
 * @property int $user_record_id
 * @property string $keyword
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\UserRecord $user_record
 */
class UserRecordKey extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_record_id' => true,
        'keyword' => true,
        'created' => true,
        'modified' => true,
        'user_record' => true
    ];
}
