<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PartnerProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $equity_level_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $zipcode
 * @property int|null $birth_year
 * @property int|null $birth_month
 * @property int|null $birth_day
 * @property string|null $ig_user_name
 * @property int|null $ig_follower
 * @property string|null $t_user_name
 * @property int|null $t_follower
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\EquityLevel $equity_level
 */
class PartnerProfile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'equity_level_id' => true,
        'first_name' => true,
        'last_name' => true,
        'phone_number' => true,
        'zipcode' => true,
        'birth_year' => true,
        'birth_month' => true,
        'birth_day' => true,
        'ig_user_name' => true,
        'ig_follower' => true,
        't_user_name' => true,
        't_follower' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'equity_level' => true,
    ];
}
