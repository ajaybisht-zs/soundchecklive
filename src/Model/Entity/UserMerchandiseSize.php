<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserMerchandiseSize Entity
 *
 * @property int $id
 * @property int $user_merchandise_id
 * @property string $size_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\UserMerchandise $user_merchandise
 * @property \App\Model\Entity\Size $size
 */
class UserMerchandiseSize extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_merchandise_id' => true,
        'size_id' => true,
        'created' => true,
        'modified' => true,
        'user_merchandise' => true,
        'size' => true
    ];
}
