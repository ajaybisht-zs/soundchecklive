<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FashionDesignerProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $style_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $zipcode
 * @property bool|null $is_licensed_brand_or_independent_line
 * @property string|null $parent_company
 * @property string $biography
 * @property string|null $avatar
 * @property string|null $avatar_dir
 * @property int|null $what_type_budget_seeking
 * @property string $buget_used_for_roi
 * @property string|null $garment_pic
 * @property string|null $garment_dir
 * @property string|null $process_video
 * @property string|null $process_video_dir
 * @property string|null $pitch_video
 * @property string|null $pitch_video_dir
 * @property int|null $birth_year
 * @property int|null $birth_month
 * @property int|null $birth_day
 * @property string|null $keyword_1
 * @property string|null $keyword_2
 * @property string|null $keyword_3
 * @property string|null $keyword_4
 * @property string|null $keyword_5
 * @property int|null $gender
 * @property int|null $ethnicity
 * @property bool|null $is_brand_manager_agent
 * @property bool|null $is_performed_overseas
 * @property bool|null $is_merchandise_sale
 * @property int|null $country_id
 * @property int|null $state_id
 * @property string|null $city
 * @property float|null $scl_score
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Style $style
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\State $state
 */
class FashionDesignerProfile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'style_id' => true,
        'first_name' => true,
        'last_name' => true,
        'phone_number' => true,
        'zipcode' => true,
        'is_licensed_brand_or_independent_line' => true,
        'parent_company' => true,
        'biography' => true,
        'avatar' => true,
        'avatar_dir' => true,
        'what_type_budget_seeking' => true,
        'buget_used_for_roi' => true,
        'garment_pic' => true,
        'garment_dir' => true,
        'process_video' => true,
        'process_video_dir' => true,
        'pitch_video' => true,
        'pitch_video_dir' => true,
        'birth_year' => true,
        'birth_month' => true,
        'birth_day' => true,
        'keyword_1' => true,
        'keyword_2' => true,
        'keyword_3' => true,
        'keyword_4' => true,
        'keyword_5' => true,
        'gender' => true,
        'ethnicity' => true,
        'is_brand_manager_agent' => true,
        'is_performed_overseas' => true,
        'is_merchandise_sale' => true,
        'country_id' => true,
        'state_id' => true,
        'city' => true,
        'scl_score' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'style' => true,
        'country' => true,
        'state' => true,
        'brand_name' => true
    ];
}
