<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SclPayment Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $transition_id
 * @property float $amount
 * @property string|null $card_holder_name
 * @property bool $status
 * @property string|null $failure_reason
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\UserRecord $user_record
 * @property \App\Model\Entity\Transition $transition
 */
class SclPayment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'transition_id' => true,
        'amount' => true,
        'card_holder_name' => true,
        'status' => true,
        'failure_reason' => true,
        'created' => true,
        'modified' => true,
        'user_record' => true,
        'transition' => true
    ];
}
