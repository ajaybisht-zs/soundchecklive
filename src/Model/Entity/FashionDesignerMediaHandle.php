<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FashionDesignerMediaHandle Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $instagram
 * @property string|null $youtube
 * @property string|null $tiktok
 * @property string|null $twitch
 * @property string|null $snapchat
 *
 * @property \App\Model\Entity\User $user
 */
class FashionDesignerMediaHandle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'facebook' => true,
        'twitter' => true,
        'instagram' => true,
        'youtube' => true,
        'tiktok' => true,
        'twitch' => true,
        'snapchat' => true,
        'user' => true
    ];
}
