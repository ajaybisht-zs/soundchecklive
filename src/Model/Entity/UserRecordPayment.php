<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserRecordPayment Entity
 *
 * @property int $id
 * @property int $user_record_id
 * @property string $transition_id
 * @property string $name
 * @property string $email
 * @property int $status
 * @property string $failure_reason
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\UserRecord $user_record
 * @property \App\Model\Entity\Transition $transition
 */
class UserRecordPayment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_record_id' => true,
        'transition_id' => true,
        'name' => true,
        'email' => true,
        'status' => true,
        'failure_reason' => true,
        'created' => true,
        'modified' => true,
        'user_record' => true,
        'transition' => true,
        'amount' => true,
        'type' => true,
    ];
}
