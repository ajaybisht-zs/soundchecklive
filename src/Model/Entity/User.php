<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property int $role_id
 * @property string $email
 * @property string $password
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\UserProfile[] $user_profiles
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'role_id' => true,
        'email' => true,
        'password' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'role' => true,
        'user_profile' => true,
        'user_media_handle' => true,
        'user_records' => true,
        'user_record' => true,
        'user_bank_detail' => true,
        'user_plan' => true,
        'step_completed' => true,
        'user_plans' =>true,
        'fashion_designer_profile' => true,
        'fashion_designer_media_handle' => true,
        'film_director_profile' => true,
        'film_director_media_handle' => true,
        'created_by' => true,
        'level_id' => true,
        'last_login' => true,
        'levels' => true,
        'chat_session_id' => true,
        'influencer_profile' => true,
        'partner_profile' => true,
        'user_supporters' => true,
        
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password) {
        if (strlen($password) > 0) {
          return (new DefaultPasswordHasher)->hash($password);
        }
    }

    protected function _getFullName() {   
        if(!empty($this->user_profile)) {
            return $this->user_profile->first_name . '  ' . $this->user_profile->last_name;
        }
        return $this->email;
    }
}
