<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserMerchandiseColors Model
 *
 * @property \App\Model\Table\UserMerchandisesTable|\Cake\ORM\Association\BelongsTo $UserMerchandises
 * @property \App\Model\Table\ColorsTable|\Cake\ORM\Association\BelongsTo $Colors
 *
 * @method \App\Model\Entity\UserMerchandiseColor get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseColor findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserMerchandiseColorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_merchandise_colors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserMerchandises', [
            'foreignKey' => 'user_merchandise_id',
            'joinType' => 'INNER'
        ]);
        /*$this->belongsTo('Colors', [
            'foreignKey' => 'color_id',
            'joinType' => 'INNER'
        ]);*/

        $this->belongsToMany('UserMerchandises', [
            'joinTable' => 'user_merchandise_colors',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_merchandise_id'], 'UserMerchandises'));
        $rules->add($rules->existsIn(['color_id'], 'Colors'));

        return $rules;
    }
}
