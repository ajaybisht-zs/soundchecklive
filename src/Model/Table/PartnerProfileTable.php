<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PartnerProfile Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\EquityLevelsTable&\Cake\ORM\Association\BelongsTo $EquityLevels
 *
 * @method \App\Model\Entity\PartnerProfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\PartnerProfile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PartnerProfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PartnerProfile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PartnerProfile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PartnerProfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PartnerProfile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PartnerProfile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PartnerProfileTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('partner_profile');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('EquityLevels', [
            'foreignKey' => 'equity_level_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 245)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 245)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('phone_number')
            ->maxLength('phone_number', 20)
            ->requirePresence('phone_number', 'create')
            ->notEmptyString('phone_number');

        $validator
            ->scalar('zipcode')
            ->maxLength('zipcode', 10)
            ->requirePresence('zipcode', 'create')
            ->notEmptyString('zipcode');

        $validator
            ->integer('birth_year')
            ->allowEmptyString('birth_year');

        $validator
            ->integer('birth_month')
            ->allowEmptyString('birth_month');

        $validator
            ->integer('birth_day')
            ->allowEmptyString('birth_day');

        $validator
            ->scalar('ig_user_name')
            ->maxLength('ig_user_name', 255)
            ->allowEmptyString('ig_user_name');

        $validator
            ->integer('ig_follower')
            ->allowEmptyString('ig_follower');

        $validator
            ->scalar('t_user_name')
            ->maxLength('t_user_name', 255)
            ->allowEmptyString('t_user_name');

        $validator
            ->integer('t_follower')
            ->allowEmptyString('t_follower');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['equity_level_id'], 'EquityLevels'));

        return $rules;
    }
}
