<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SclPayments Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TransitionsTable&\Cake\ORM\Association\BelongsTo $Transitions
 *
 * @method \App\Model\Entity\SclPayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\SclPayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SclPayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SclPayment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SclPayment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SclPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SclPayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SclPayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SclPaymentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('scl_payments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Transitions', [
            'foreignKey' => 'transition_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->decimal('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->scalar('card_holder_name')
            ->maxLength('card_holder_name', 255)
            ->allowEmptyString('card_holder_name');

        $validator
            ->boolean('status')
            ->notEmptyString('status');

        $validator
            ->scalar('failure_reason')
            ->allowEmptyString('failure_reason');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
       /* $rules->add($rules->existsIn(['transition_id'], 'Transitions'));*/

        return $rules;
    }
}
