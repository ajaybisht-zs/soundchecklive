<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Filmgenres Model
 *
 * @property \App\Model\Table\FilmDirectorProfilesTable&\Cake\ORM\Association\HasMany $FilmDirectorProfiles
 *
 * @method \App\Model\Entity\Filmgenre get($primaryKey, $options = [])
 * @method \App\Model\Entity\Filmgenre newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Filmgenre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Filmgenre|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Filmgenre saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Filmgenre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Filmgenre[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Filmgenre findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FilmgenresTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('filmgenres');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('FilmDirectorProfiles', [
            'foreignKey' => 'filmgenre_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 245)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
