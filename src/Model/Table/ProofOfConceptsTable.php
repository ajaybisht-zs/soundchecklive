<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProofOfConcepts Model
 *
 * @property \App\Model\Table\PeopleTable&\Cake\ORM\Association\BelongsTo $People
 * @property \App\Model\Table\ChargesTable&\Cake\ORM\Association\BelongsTo $Charges
 * @property &\Cake\ORM\Association\BelongsTo $Countries
 *
 * @method \App\Model\Entity\ProofOfConcept get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProofOfConcept newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProofOfConcept[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProofOfConcept|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProofOfConcept saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProofOfConcept patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProofOfConcept[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProofOfConcept findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProofOfConceptsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('proof_of_concepts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Peoples', [
            'foreignKey' => 'people_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Charges', [
            'foreignKey' => 'charge_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER',
        ]);
         $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            //'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->decimal('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 100)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 100)
            ->allowEmptyString('last_name');

        $validator
            ->scalar('address')
            ->maxLength('address', 1000)
            ->requirePresence('address', 'create')
            ->notEmptyString('address');

        $validator
            ->scalar('zip_code')
            ->maxLength('zip_code', 14)
            ->requirePresence('zip_code', 'create')
            ->notEmptyString('zip_code');

        $validator
            ->scalar('city')
            ->maxLength('city', 150)
            ->requirePresence('city', 'create')
            ->notEmptyString('city');

        $validator
            ->scalar('phone_number')
            ->maxLength('phone_number', 15)
            ->requirePresence('phone_number', 'create')
            ->notEmptyString('phone_number');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        $validator
            ->scalar('failure_reason')
            ->allowEmptyString('failure_reason');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['email']));
        //$rules->add($rules->existsIn(['people_id'], 'People'));
        //$rules->add($rules->existsIn(['charge_id'], 'Charges'));
       // $rules->add($rules->existsIn(['country_id'], 'Countries'));

        return $rules;
    }
}
