<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * FashionDesignerProfiles Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\StylesTable&\Cake\ORM\Association\BelongsTo $Styles
 * @property \App\Model\Table\CountriesTable&\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\StatesTable&\Cake\ORM\Association\BelongsTo $States
 *
 * @method \App\Model\Entity\FashionDesignerProfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FashionDesignerProfile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FashionDesignerProfilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fashion_designer_profiles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            // You can configure as many upload fields as possible,
            // where the pattern is `field` => `config`
            //
            // Keep in mind that while this plugin does not have any limits in terms of
            // number of files uploaded per request, you should keep this down in order
            // to decrease the ability of your users to block other requests.
            'avatar' => [
                'fields' => [
                    'dir' => 'avatar_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ],
            'pitch_video' => [
                'fields' => [
                    'dir' => 'pitch_video_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)?strtolower($ext): 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ],
            'garment_pic' => [
                'fields' => [
                    'dir' => 'garment_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ],
            'process_video' => [
                'fields' => [
                    'dir' => 'process_video_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)?strtolower($ext): 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ]
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Styles', [
            'foreignKey' => 'style_id'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 245)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 245)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('phone_number')
            ->maxLength('phone_number', 20)
            ->requirePresence('phone_number', 'create')
            ->notEmptyString('phone_number');

        $validator
            ->scalar('zipcode')
            ->maxLength('zipcode', 10)
            ->requirePresence('zipcode', 'create')
            ->notEmptyString('zipcode');

        $validator
            ->boolean('is_licensed_brand_or_independent_line')
            ->allowEmptyString('is_licensed_brand_or_independent_line');

        $validator
            ->scalar('parent_company')
            ->maxLength('parent_company', 255)
            ->allowEmptyString('parent_company');

        $validator
            ->scalar('biography')
            ->maxLength('biography', 500)
            ->allowEmptyString('biography');

        $validator
            //->scalar('avatar')
            //->maxLength('avatar', 245)
            ->allowEmptyString('avatar');

        $validator
            //->scalar('avatar_dir')
            //->maxLength('avatar_dir', 245)
            ->allowEmptyString('avatar_dir');

        $validator
            ->integer('what_type_budget_seeking')
            ->allowEmptyString('what_type_budget_seeking');

        $validator
            ->scalar('buget_used_for_roi')
            ->maxLength('buget_used_for_roi', 500)
            ->allowEmptyString('buget_used_for_roi');

        $validator
            //->scalar('garment_pic')
           // ->maxLength('garment_pic', 245)
            ->allowEmptyString('garment_pic');

        $validator
           // ->scalar('garment_dir')
            //->maxLength('garment_dir', 245)
            ->allowEmptyString('garment_dir');

        $validator
            //->scalar('process_video')
           // ->maxLength('process_video', 245)
            ->allowEmptyString('process_video');

        $validator
           // ->scalar('process_video_dir')
           // ->maxLength('process_video_dir', 245)
            ->allowEmptyString('process_video_dir');

        $validator
            //->scalar('pitch_video')
           // ->maxLength('pitch_video', 245)
            ->allowEmptyString('pitch_video');

        $validator
            //->scalar('pitch_video_dir')
            //->maxLength('pitch_video_dir', 245)
            ->allowEmptyString('pitch_video_dir');

        $validator
            ->integer('birth_year')
            ->allowEmptyString('birth_year');

        $validator
            ->integer('birth_month')
            ->allowEmptyString('birth_month');

        $validator
            ->integer('birth_day')
            ->allowEmptyString('birth_day');

        $validator
            ->scalar('keyword_1')
            ->maxLength('keyword_1', 100)
            ->allowEmptyString('keyword_1');

        $validator
            ->scalar('keyword_2')
            ->maxLength('keyword_2', 100)
            ->allowEmptyString('keyword_2');

        $validator
            ->scalar('keyword_3')
            ->maxLength('keyword_3', 100)
            ->allowEmptyString('keyword_3');

        $validator
            ->scalar('keyword_4')
            ->maxLength('keyword_4', 100)
            ->allowEmptyString('keyword_4');

        $validator
            ->scalar('keyword_5')
            ->maxLength('keyword_5', 100)
            ->allowEmptyString('keyword_5');

        $validator
            ->integer('gender')
            ->allowEmptyString('gender');

        $validator
            ->integer('ethnicity')
            ->allowEmptyString('ethnicity');

        $validator
            ->boolean('is_brand_manager_agent')
            ->allowEmptyString('is_brand_manager_agent');

        $validator
            ->boolean('is_performed_overseas')
            ->allowEmptyString('is_performed_overseas');

        $validator
            ->boolean('is_merchandise_sale')
            ->allowEmptyString('is_merchandise_sale');

        $validator
            ->scalar('city')
            ->maxLength('city', 255)
            ->allowEmptyString('city');

        $validator
            ->decimal('scl_score')
            ->allowEmptyString('scl_score');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['style_id'], 'Styles'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        $rules->add($rules->existsIn(['state_id'], 'States'));

        return $rules;
    }
}
