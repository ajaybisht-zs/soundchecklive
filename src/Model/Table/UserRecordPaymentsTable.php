<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserRecordPayments Model
 *
 * @property \App\Model\Table\UserRecordsTable|\Cake\ORM\Association\BelongsTo $UserRecords
 * @property \App\Model\Table\TransitionsTable|\Cake\ORM\Association\BelongsTo $Transitions
 *
 * @method \App\Model\Entity\UserRecordPayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserRecordPayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserRecordPayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserRecordPayment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserRecordPayment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserRecordPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserRecordPayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserRecordPayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserRecordPaymentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_record_payments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserRecords', [
            'foreignKey' => 'user_record_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Transitions', [
            'foreignKey' => 'transition_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->notEmptyString('status');

        /*$validator
            ->scalar('failure_reason')
            ->requirePresence('failure_reason', 'create')
            ->notEmptyString('failure_reason');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_record_id'], 'UserRecords'));
       /* $rules->add($rules->existsIn(['transition_id'], 'Transitions'));*/

        return $rules;
    }
}
