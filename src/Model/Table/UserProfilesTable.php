<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
/**
 * UserProfiles Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\GenresTable|\Cake\ORM\Association\BelongsTo $Genres
 *
 * @method \App\Model\Entity\UserProfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserProfile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserProfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserProfile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserProfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserProfilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_profiles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Genres', [
            'foreignKey' => 'genre_id'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            // You can configure as many upload fields as possible,
            // where the pattern is `field` => `config`
            //
            // Keep in mind that while this plugin does not have any limits in terms of
            // number of files uploaded per request, you should keep this down in order
            // to decrease the ability of your users to block other requests.
            'avatar' => [
                'fields' => [
                    'dir' => 'avatar_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ],
            'banner' => [
                'fields' => [
                    'dir' => 'banner_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 245)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 245)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('phone_number')
            ->maxLength('phone_number', 20)
            ->requirePresence('phone_number', 'create')
            ->notEmptyString('phone_number');

        $validator
            ->scalar('zipcode')
            ->maxLength('zipcode', 10)
            ->requirePresence('zipcode', 'create')
            ->notEmptyString('zipcode');

        $validator
            ->allowEmptyString('is_signed');

        $validator
            ->scalar('signed_by')
            ->maxLength('signed_by', 245)
            ->allowEmptyString('signed_by');

        $validator
            ->scalar('biography')
            ->allowEmptyString('biography');

        $validator
            ->allowEmptyString('is_performance_rights_affiliated');

        $validator
            ->scalar('performance_rights_organization')
            ->maxLength('performance_rights_organization', 245)
            ->allowEmptyString('performance_rights_organization');

        $validator
            // ->scalar('avatar')
            // ->maxLength('avatar', 245)
            ->allowEmptyString('avatar');

        $validator
            ->scalar('avatar_dir')
            ->maxLength('avatar_dir', 245)
            ->allowEmptyString('avatar_dir');


        $validator
            // ->scalar('avatar')
            // ->maxLength('avatar', 245)
            ->allowEmptyString('banner');

        $validator
            ->scalar('banner_dir')
            ->maxLength('banner_dir', 245)
            ->allowEmptyString('banner_dir');

        $validator
            ->scalar('capital_goals')
            ->maxLength('capital_goals', 245)
            ->allowEmptyString('capital_goals');

        $validator
            ->scalar('budget_usage')
            ->allowEmptyString('budget_usage');

        $validator
            ->allowEmptyString('is_ready_for_release');

        $validator
            ->integer('birth_year')
            ->allowEmptyString('birth_year');

        $validator
            ->integer('birth_month')
            ->allowEmptyString('birth_month');

        $validator
            ->integer('birth_day')
            ->allowEmptyString('birth_day');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['genre_id'], 'Genres'));

        return $rules;
    }

    
}
