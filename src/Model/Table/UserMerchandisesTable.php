<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserMerchandises Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UserMerchandiseSizesTable|\Cake\ORM\Association\HasMany $UserMerchandiseSizes
 *
 * @method \App\Model\Entity\UserMerchandise get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserMerchandise newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserMerchandise[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandise|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMerchandise saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMerchandise patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandise[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandise findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserMerchandisesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_merchandises');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsToMany('Sizes',
            [
                'joinTable' => 'user_merchandise_sizes',
            ]
        );

        $this->hasMany('UserMerchandiseImages', [
            'foreignKey' => 'user_merchandise_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->belongsToMany('Colors',
            [
                'joinTable' => 'user_merchandise_colors',
            ]
        );

       /* $this->hasMany('UserMerchandiseColors', [
            'foreignKey' => 'user_merchandise_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);*/
 
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 245)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('price')
            ->maxLength('price', 45)
            ->requirePresence('price', 'create')
            ->notEmptyString('price');

        /*$validator
            ->scalar('image')
            ->maxLength('image', 245)
            ->allowEmptyFile('image');

        $validator
            ->scalar('image_dir')
            ->allowEmptyFile('image_dir');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getArtistMerchandise() {
        return $this->find()
            ->where()
            ->contain(['UserMerchandiseImages'])
            ->matching('Users.UserProfiles', function ($q) {
                return $q->where(['Users.status' => 1]);
            })
            ->order(['UserMerchandises.created' => 'DESC']);
    }

    public function getMerchandise() {
        return $this->find()
            ->where()
            ->contain(['UserMerchandiseImages'])
            ->matching('Users.UserProfiles', function ($q) {
                return $q->where(['Users.id' => 1]);
            })
            ->order(['UserMerchandises.created' => 'DESC']);
    }
}
