<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Intervention\Image\ImageManagerStatic as Image;
/**
 * UserRecords Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UserRecordKeysTable|\Cake\ORM\Association\HasMany $UserRecordKeys
 *
 * @method \App\Model\Entity\UserRecord get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserRecord newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserRecord[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserRecord|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserRecord saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserRecord patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserRecord[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserRecord findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserRecordsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_records');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('UserRecordKeys', [
            'foreignKey' => 'user_record_id',
            'saveStrategy' => 'replace',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('UserRecordPayments', [
            'foreignKey' => 'user_record_id',
            'saveStrategy' => 'replace',
            // 'conditions' => ['type' => 0]
        ]);

        $this->hasMany('UserPitchPayments', [
            'className' => 'UserRecordPayments',
            'foreignKey' => 'user_record_id',
            'saveStrategy' => 'replace',
            // 'conditions' => ['type' => 1]
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
           'cover_image' => [
                'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'cover_image_dir', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    // configure with favored image driver (gd by default)
                    Image::configure(array('driver' => 'gd'));
                    $img = Image::make($data['tmp_name'])
                        ->fit(300)
                        ->save($tmp);
                    // Now return the original *and* the thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },
               'keepFilesOnDelete' => false
           ],
           'record_file' => [
               'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'record_dir', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
               ],
               'keepFilesOnDelete' => false
           ],
           'video_pitch_file' => [
               'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'video_pitch_path', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
               ],
               'keepFilesOnDelete' => false
           ],
           'video_pitch_image' => [
                'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'video_pitch_image_dir', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    // configure with favored image driver (gd by default)
                    Image::configure(array('driver' => 'gd'));
                    $img = Image::make($data['tmp_name'])
                        ->fit(300)
                        ->save($tmp);
                    // Now return the original *and* the thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },
               'keepFilesOnDelete' => false
           ],
       ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('record_name')
            ->maxLength('record_name', 245)
            ->allowEmptyString('record_name', null );

        $validator
            ->allowEmptyString('record_written_by', null);

        $validator
            ->allowEmptyString('is_already_distributed', null);

        $validator
            ->allowEmptyFile('cover_image', null);

        $validator
            ->scalar('cover_image_dir')
            ->allowEmptyFile('cover_image_dir', null);

        $validator
            ->scalar('artist_name')
            ->maxLength('artist_name', 245)
            ->allowEmptyString('artist_name', null);

        $validator
            ->scalar('features')
            ->allowEmptyString('features', null);

        $validator
            ->scalar('producer')
            ->maxLength('producer', 245)
            ->allowEmptyString('producer', null);

        $validator
            ->scalar('writer')
            ->maxLength('writer', 245)
            ->allowEmptyString('writer', null);

        $validator            
            ->allowEmptyFile('record_file', null);

        $validator
            ->scalar('record_dir')
            ->allowEmptyString('record_dir', null);

        $validator
            ->allowEmptyFile('video_pitch_file',null);

        $validator
            ->allowEmptyString('video_pitch_path',null);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getRecentRecords() {
        return $this->find()
            ->where()
            ->contain(['UserRecordPayments'])
            ->matching('Users', function ($q) {
                return $q->where(['Users.status' => 1, 'Users.is_deleted' => 0]);
            })
            ->order(['UserRecords.created' => 'DESC']);
    }

    public function getHighScl() {
        return $this->find()
            ->where()
            ->contain(['UserRecordPayments'])
            ->matching('Users', function ($q) {
                return $q->where(['Users.status' => 1, 'Users.is_deleted' => 0]);
            })
            ->order(['UserRecords.created' => 'DESC']);
    }

    public function getTrendingRecords() {
        return $this->find()
            ->where()
            ->contain(['UserRecordPayments'])
            ->matching('Users', function ($q) {
                return $q->where(['Users.status' => 1, 'Users.is_deleted' => 0]);
            })
            ->order(['UserRecords.created' => 'DESC']);
    }

    public function getBestSellerRecords() {
        return $this->find()
            ->where()
            ->contain(['UserRecordPayments'])
            ->matching('Users', function ($q) {
                return $q->where(['Users.status' => 1, 'Users.is_deleted' => 0]);
            })
            ->order(['UserRecords.created' => 'DESC']);
    }

    public function getSoundCheckRecords() {
        return $this->find()
            ->where()
            ->contain(['UserRecordPayments'])
            ->matching('Users', function ($q) {
                return $q->where(['Users.role_id' => 1]);
            })
            ->order(['UserRecords.created' => 'DESC']);
    }
}
