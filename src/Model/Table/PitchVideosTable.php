<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * PitchVideos Model
 *
 * @method \App\Model\Entity\PitchVideo get($primaryKey, $options = [])
 * @method \App\Model\Entity\PitchVideo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PitchVideo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PitchVideo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PitchVideo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PitchVideo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PitchVideo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PitchVideo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PitchVideosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pitch_videos');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator->allowEmptyString('video_name', 'custom', [
            'rule' => function ($value, $context) {
                $ext = $ext = pathinfo($context['data']['video_name']['name'], PATHINFO_EXTENSION);
                if($ext == 'mp4') {
                    return true;
                } else {
                    return false;
                }
            },
            'message' => 'Please upload mp4 only'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            // You can configure as many upload fields as possible,
            // where the pattern is `field` => `config`
            //
            // Keep in mind that while this plugin does not have any limits in terms of
            // number of files uploaded per request, you should keep this down in order
            // to decrease the ability of your users to block other requests.
            'video_name' => [
                'fields' => [
                    'dir' => 'video_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ],

            'poster_name' => [
                'fields' => [
                    'dir' => 'poster_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ]
        ]);

        return $validator;
    }

    public function getpitchVideo() {
        return $this->find()
            ->order(['created' => 'ASC'])
            ->first();
    }
}
