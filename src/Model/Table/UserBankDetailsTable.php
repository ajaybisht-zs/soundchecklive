<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserBankDetails Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserBankDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserBankDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserBankDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserBankDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserBankDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserBankDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserBankDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserBankDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserBankDetailsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_bank_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('bank_name')
            ->maxLength('bank_name', 245)
            ->requirePresence('bank_name', 'create')
            ->allowEmptyString('bank_name');

        $validator
            ->scalar('name_on_account')
            ->maxLength('name_on_account', 245)
            ->allowEmptyString('name_on_account');

        $validator
            ->scalar('account_number')
            ->maxLength('account_number', 245)
            ->requirePresence('account_number', 'create')
            ->allowEmptyString('account_number');

        $validator
            ->integer('routing_number')
            ->requirePresence('routing_number', 'create')
            ->allowEmptyString('routing_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
