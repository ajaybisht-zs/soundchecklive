<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * UserEvents Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserEvent get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserEvent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserEvent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserEvent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserEvent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserEvent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserEvent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserEvent findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserEventsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_events');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('UserEventImages', [
            'foreignKey' => 'user_event_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            // You can configure as many upload fields as possible,
            // where the pattern is `field` => `config`
            //
            // Keep in mind that while this plugin does not have any limits in terms of
            // number of files uploaded per request, you should keep this down in order
            // to decrease the ability of your users to block other requests.
            'image' => [
                'fields' => [
                    'dir' => 'image_dir',            
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'keepFilesOnDelete' => false
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('city')
            ->maxLength('city', 45)
            ->requirePresence('city', 'create')
            ->notEmptyString('city');

        $validator
            ->date('event_date')
            ->requirePresence('event_date', 'create')
            ->notEmptyDate('event_date');

        $validator
            ->scalar('event_time')
            ->maxLength('event_time', 45)
            ->requirePresence('event_time', 'create')
            ->notEmptyString('event_time');

        $validator
            ->scalar('link_for_ticket')
            ->maxLength('link_for_ticket', 245)
            ->allowEmptyString('link_for_ticket');

        $validator
            ->allowEmptyFile('image');

        $validator
            ->scalar('image_dir')
            ->maxLength('image_dir', 45)
            ->allowEmptyFile('image_dir');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
