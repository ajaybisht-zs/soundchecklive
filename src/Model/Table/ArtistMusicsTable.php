<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtistMusics Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ArtistMusic get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtistMusic newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtistMusic[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtistMusic|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtistMusic saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtistMusic patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtistMusic[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtistMusic findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArtistMusicsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artist_musics');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
           'video_name' => [
               'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'video_dir', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
               ],
               'keepFilesOnDelete' => false
           ],
       ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        /*$validator
            ->scalar('video_name')
            ->maxLength('video_name', 255)
            ->requirePresence('video_name', 'create')
            ->notEmptyString('video_name');

        $validator
            ->scalar('video_dir')
            ->maxLength('video_dir', 255)
            ->requirePresence('video_dir', 'create')
            ->notEmptyString('video_dir');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getMusicVideo() {

        return $this->find()
            ->contain([
                'Users'=> [
                    'UserProfiles',
                    'UserRecords'=> ['UserRecordPayments']
                ]
            ])
            ->matching('Users',function($q){
                return $q->where(['Users.status' => 1,'Users.is_deleted' => 0]);
            })
            ->order(['ArtistMusics.created' => 'DESC'])->toArray();
    }
}
