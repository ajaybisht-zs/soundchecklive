<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FashionDesignerMediaHandles Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\FashionDesignerMediaHandle get($primaryKey, $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FashionDesignerMediaHandle findOrCreate($search, callable $callback = null, $options = [])
 */
class FashionDesignerMediaHandlesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fashion_designer_media_handles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('facebook')
            ->maxLength('facebook', 245)
            ->allowEmptyString('facebook');

        $validator
            ->scalar('twitter')
            ->maxLength('twitter', 245)
            ->allowEmptyString('twitter');

        $validator
            ->scalar('instagram')
            ->maxLength('instagram', 245)
            ->allowEmptyString('instagram');

        $validator
            ->scalar('youtube')
            ->maxLength('youtube', 245)
            ->allowEmptyString('youtube');

        $validator
            ->scalar('tiktok')
            ->maxLength('tiktok', 255)
            ->allowEmptyString('tiktok');

        $validator
            ->scalar('twitch')
            ->maxLength('twitch', 255)
            ->allowEmptyString('twitch');

        $validator
            ->scalar('snapchat')
            ->maxLength('snapchat', 255)
            ->allowEmptyString('snapchat');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
