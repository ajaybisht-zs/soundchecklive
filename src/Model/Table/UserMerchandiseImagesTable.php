<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * UserMerchandiseImages Model
 *
 * @property \App\Model\Table\UserMerchandisesTable|\Cake\ORM\Association\BelongsTo $UserMerchandises
 *
 * @method \App\Model\Entity\UserMerchandiseImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserMerchandiseImage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserMerchandiseImagesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_merchandise_images');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserMerchandises', [
            'foreignKey' => 'user_merchandise_id',
            'joinType' => 'INNER'
        ]);

         $this->addBehavior('Josegonzalez/Upload.Upload', [
           'image' => [
               'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'image_dir', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
               ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $ext = $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $ext = ($ext)? $ext: 'jpg';
                    return Text::uuid().".$ext";
                },
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    // configure with favored image driver (gd by default)
                    Image::configure(array('driver' => 'gd'));
                    $img = Image::make($data['tmp_name'])
                        ->fit(300)
                        ->save($tmp);
                    // Now return the original *and* the thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },
               'keepFilesOnDelete' => false
           ],
       ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

       /* $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmptyFile('image');

        $validator
            ->scalar('image_dir')
            ->maxLength('image_dir', 255)
            ->requirePresence('image_dir', 'create')
            ->notEmptyFile('image_dir');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_merchandise_id'], 'UserMerchandises'));

        return $rules;
    }
}
