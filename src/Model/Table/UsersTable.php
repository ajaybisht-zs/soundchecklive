<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\UserProfilesTable|\Cake\ORM\Association\HasMany $UserProfiles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('UserProfiles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('UserMediaHandles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('UserBankDetails', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('UserPlans', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('UserRecords', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('UserEvents', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('UserMerchandises', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('FashionDesignerProfiles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('FashionDesignerMediaHandles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

         $this->hasOne('FilmDirectorProfiles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('FilmDirectorMediaHandles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

         $this->belongsTo('Levels', [
            'foreignKey' => 'level_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('InfluencerProfile', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('PartnerProfile', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('UserSupporters', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'saveStrategy' => 'replace',
        ]);
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        //$rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }

    public function checkPassword($password, $hashedPassword) {
        return (new DefaultPasswordHasher)->check($password, $hashedPassword);
    }

    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $query
            ->contain([
                'UserProfiles'
            ]);

        return $query;
    }

    public function getArtistPitchVideo() {

        $this->hasOne('UserRecords', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        return $this->find()
            ->where(['Users.status' => 1])
            ->matching('UserRecords')
            ->contain([
                'UserRecords' => ['UserPitchPayments', 'UserRecordPayments']
            ])
            ->order(['Users.created' => 'DESC']);
    }

    public function getFilmDirectorLatestRecord() {
       $res =  $this->find()
            ->where()
            ->contain(['FilmDirectorProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 5,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->toArray();
        return $res;
    }

    public function getFilmDirectorTrendingRecords() {
        return $this->find()
            ->where()
            ->contain(['FilmDirectorProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 5,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->order(['FilmDirectorProfiles.created' => 'DESC'])
            ->toArray();
            return $res;
    }


    public function getFilmDirectorBestSellerRecords() {
        return $this->find()
            ->where()
            ->contain(['FilmDirectorProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 5,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->order(['FilmDirectorProfiles.created' => 'DESC'])
            ->toArray();
            return $res;
    }

    public function filmDirectorPitchVideo() {
        return $this->find()
            ->where()
            ->contain(['FilmDirectorProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 5,
                        'is_deleted' => 0,
                         'created_by' => 0

                    ])
            ->order(['FilmDirectorProfiles.created' => 'DESC'])
            ->toArray();
            return $res;
    }

    public function getFilmDirectorSclOriginal() {
       $res =  $this->find()
            ->where()
            ->contain(['FilmDirectorProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 5,
                        'is_deleted' => 0,
                        'created_by' => 1

                    ])
            ->toArray();
        return $res;
    }

    public function getFilmDirectorHightestSclSore() {
       $res =  $this->find()
            ->where()
            ->contain(['FilmDirectorProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 5,
                        'is_deleted' => 0,
                         'created_by' => 0

                    ])
            ->order(['FilmDirectorProfiles.scl_score' => 'DESC'])
            ->toArray();
        return $res;
    }


    public function getFashionDesignerLatestRecord() {
       $res =  $this->find()
            ->where()
            ->contain(['FashionDesignerProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 4,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->toArray();
        return $res;
    }

    public function getFashionDesignerTrendingRecords() {
       $res =  $this->find()
            ->where()
            ->contain(['FashionDesignerProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 4,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->order(['FashionDesignerProfiles.created' => 'DESC'])
            ->toArray();
        return $res;
    }

    public function getBestFashionDesignerSeller() {
       $res =  $this->find()
            ->where()
            ->contain(['FashionDesignerProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 4,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->order(['FashionDesignerProfiles.created' => 'DESC'])
            ->toArray();
        return $res;
    }

    public function getFashionDesignerSclOriginal() {
       $res =  $this->find()
            ->where()
            ->contain(['FashionDesignerProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 4,
                        'is_deleted' => 0,
                        'created_by' => 1

                    ])
            ->order(['FashionDesignerProfiles.created' => 'DESC'])
            ->toArray();
        return $res;
    }

    public function getFashionDesignerHightestSclSore() {
       $res =  $this->find()
            ->where()
            ->contain(['FashionDesignerProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 4,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->order(['FashionDesignerProfiles.scl_score' => 'DESC'])
            ->toArray();
        return $res;
    }

    public function getFashionDesignerPitchVideo() {
        return $this->find()
            ->where()
            ->contain(['FashionDesignerProfiles'])
            ->where([
                        'status' => 1,
                        'role_id' => 4,
                        'is_deleted' => 0,
                        'created_by' => 0

                    ])
            ->order(['FashionDesignerProfiles.created' => 'DESC'])
            ->toArray();
            return $res;
    }

    public function getOnlineUsers() {
        $time=time();
        $res =  $this->find()
            ->contain(['UserProfiles','Levels'])
            ->where([
                        'status' => 1,
                        'is_deleted' => 0,
                        'last_login >' => $time

                    ])
            ->toArray();
            //pr( $res);die;
       return $res;
    }

}

