<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserEventImages Model
 *
 * @property \App\Model\Table\UserEventsTable|\Cake\ORM\Association\BelongsTo $UserEvents
 *
 * @method \App\Model\Entity\UserEventImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserEventImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserEventImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserEventImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserEventImage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserEventImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserEventImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserEventImage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserEventImagesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_event_images');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserEvents', [
            'foreignKey' => 'user_event_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
           'image' => [
               'fields' => [
                   // if these fields or their defaults exist
                   // the values will be set.
                   'dir' => 'image_dir', // defaults to dir
                   //'size' => 'photo_size', // defaults to size
                   //'type' => 'photo_type', // defaults to type
               ],
               'keepFilesOnDelete' => false
           ],
       ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        /*$validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmptyFile('image');

        $validator
            ->scalar('image_dir')
            ->maxLength('image_dir', 255)
            ->requirePresence('image_dir', 'create')
            ->notEmptyFile('image_dir');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_event_id'], 'UserEvents'));

        return $rules;
    }
}
