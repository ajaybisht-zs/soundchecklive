<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserRecordKeys Model
 *
 * @property \App\Model\Table\UserRecordsTable|\Cake\ORM\Association\BelongsTo $UserRecords
 *
 * @method \App\Model\Entity\UserRecordKey get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserRecordKey newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserRecordKey[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserRecordKey|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserRecordKey saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserRecordKey patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserRecordKey[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserRecordKey findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserRecordKeysTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_record_keys');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserRecords', [
            'foreignKey' => 'user_record_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        /*$validator
            ->scalar('keyword')
            ->maxLength('keyword', 10)
            ->requirePresence('keyword', 'create')
            ->notEmptyString('keyword');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_record_id'], 'UserRecords'));

        return $rules;
    }
}
