<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;
use Cake\Event\Event;
use Cake\DataSource\EntityInterface;
use ArrayObject;
use Cake\Core\Configure;


/**
 * User mailer.
 */
class UsersMailer extends Mailer
{
    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'User';
      /**
     * Mailer's name.
     *
     * @var string
     */

    public function welcome($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Welcome to Sound Check Live') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('welcome'); // By default template with same name as method name is used.
    }

    public function newRegistraion($user) {
        $this
            ->setTo(env('ADMIN_EMAIL_ADDRESS'))
            ->setEmailFormat('html')
            ->setSubject(sprintf('Welcome to Sound Check Live') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('new_registration'); // By default template with same name as method name is used.
    }

    public function newFilmmakerRegistraion($user) {
        $this
            ->setTo(env('ADMIN_EMAIL_ADDRESS'))
            ->setEmailFormat('html')
            ->setSubject(sprintf('A New Fashion Designer Just 
              Sign up') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('new_filmmaker_registraion'); // By default template with same name as method name is used.
    }

    public function newFilmDirectorRegistraion($user) {
        $this
            ->setTo(env('ADMIN_EMAIL_ADDRESS'))
            ->setEmailFormat('html')
            ->setSubject(sprintf('A New Film Director Just Sign up') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('new_film_director_registraion'); // By default template with same name as method name is used.
    }


    public function adminWelcome($user) {
        $this
            ->setTo(env('ADMIN_EMAIL_ADDRESS'))
            ->setEmailFormat('html')
            ->setSubject(sprintf('New User Registered to Sound Check Live') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('admin_welcome'); // By default template with same name as method name is used.
    }

    public function forgot($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Sound Check Live Reset Password') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('forgot'); // By default template with same name as method name is used.
    }

    public function approveFilmDirector($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Your Film Director Account has been Approved') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('approve_film_director'); // By default template with same name as method name is used.
    }

    public function approveFashionDesigner($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Your Fashion Designer Account has been Approved') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('approve_fashion_designer'); // By default template with same name as method name is used.
    }

    public function welcomeEmail($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Welcome To SooundChecklive.co') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('welcome_email'); // By default template with same name as method name is used.
    }

    public function approveFilmDirectorNew($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Your Film Director Account has been Approved') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('approve_film_director_new'); // By default template with same name as method name is used.
    }

    public function approveFashionDesignerNew($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Your Fashion Designer Account has been Approved') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('approve_fashion_designer_new'); // By default template with same name as method name is used.
    }

    public function influencerProfile($user) {
         $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Welcome to Sound Check Live') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('welcome_influencer'); // By default template with same name as method name is used.
    }

    public function influencerSucess($user) {
        $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Welcome to Sound Check Live') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('influencer_sucess'); // By default template with same name as method name 

    }

    public function partnerProfile($user) {
         $this
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Welcome to Sound Check Live') )
            ->setViewVars(['user' => $user])
            ->viewBuilder()->setTemplate('partner_profile'); // By default template with same name as method name is used.
    }

    public function contactUs($data) {
        $this
            ->setTo('soundchecklive@outlook.com')
            //->setFrom([env('Admin_Email_Address') => 'Parkingninja'])
            // ->setTransport('')
            ->setEmailFormat('html')
            ->setSubject(sprintf('New contact email received') )
            ->setViewVars(['data' => $data])
            ->viewBuilder()->setTemplate('contact_us'); // By default template with same name as method name is used.
    }

     public function donation($proofOfConcept) {
         $this
            ->setTo($proofOfConcept->email)
            ->setEmailFormat('html')
            ->setSubject(sprintf('Thanks for investing in soundchecklive.co') )
            ->setViewVars(['proofOfConcept' => $proofOfConcept])
            ->viewBuilder()->setTemplate('donation'); // By default template with same name as method name is used.
    }
}
