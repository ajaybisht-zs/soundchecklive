<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;

/**
 * PoweredByElevens Controller
 *
 *
 * @method \App\Model\Entity\PoweredByEleven[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PoweredByElevensController extends AppController
{

    use MailerAwareTrait;

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Stripe');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('influencer');
        $this->loadModel('Peoples');
        $poweredByEleven = $this->Peoples->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if(empty($data['amount']) && empty($data['custom_price'])) {
                $this->Flash->error(__('The select at least one amount or enter amount you wish to want donate'));
                return $this->redirect(['action' => 'index']);
            } else {
                $selectPrice = !empty($data['amount'])?$data['amount']:0;
                $customPrice = !empty($data['custom_price'])?$data['custom_price']:0;
                $finalAmount = $selectPrice+$customPrice;
                $people = $data['people_id'];
                $session = $this->request->getSession();
                $session->write('step1.amount',$finalAmount);
                $session->write('step1.people_id',$people);
                return $this->redirect(['action' => 'details']);
            }
            
        }
        $people = $this->Peoples->find('list', ['limit' => 200]);
        $this->set(compact('people','poweredByEleven'));
    }

    public function share()
    {
        $this->viewBuilder()->setLayout('influencer');
       
    }


    public function details()
    {
        $this->viewBuilder()->setLayout('influencer');
         $this->loadModel('States');
        $session = $this->request->getSession();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $session->write('step2', $data);
            return $this->redirect(['action' => 'payment']);
        }
        $country = $this->States->find('list')->where(['country_id' => 231])->toArray();
         $this->set(compact('country'));
    }

    /*public function payment()
    {
        $this->viewBuilder()->setLayout('influencer');
    }*/

    /**
     * View method
     *
     * @param string|null $id Powered By Eleven id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $poweredByEleven = $this->PoweredByElevens->get($id, [
            'contain' => [],
        ]);

        $this->set('poweredByEleven', $poweredByEleven);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $poweredByEleven = $this->PoweredByElevens->newEntity();
        if ($this->request->is('post')) {
            $poweredByEleven = $this->PoweredByElevens->patchEntity($poweredByEleven, $this->request->getData());
            if ($this->PoweredByElevens->save($poweredByEleven)) {
                $this->Flash->success(__('The powered by eleven has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The powered by eleven could not be saved. Please, try again.'));
        }
        $this->set(compact('poweredByEleven'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Powered By Eleven id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $poweredByEleven = $this->PoweredByElevens->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $poweredByEleven = $this->PoweredByElevens->patchEntity($poweredByEleven, $this->request->getData());
            if ($this->PoweredByElevens->save($poweredByEleven)) {
                $this->Flash->success(__('The powered by eleven has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The powered by eleven could not be saved. Please, try again.'));
        }
        $this->set(compact('poweredByEleven'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Powered By Eleven id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $poweredByEleven = $this->PoweredByElevens->get($id);
        if ($this->PoweredByElevens->delete($poweredByEleven)) {
            $this->Flash->success(__('The powered by eleven has been deleted.'));
        } else {
            $this->Flash->error(__('The powered by eleven could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function payment() {
        $this->viewBuilder()->setLayout('influencer');
        $this->loadModel('ProofOfConcepts');
        $response = false;
        $proofOfConcept = $this->ProofOfConcepts->newEntity();
        $session = $this->request->getSession();
        if($this->request->is('ajax')) {
            $data = $this->request->getData();
            $sessionData = $session->read();

            $data['stripeToken'] =  $data['stripeToken'];
            $data['first_name'] = $sessionData['step2']['first_name'];
            $data['last_name'] =  $sessionData['step2']['last_name'];
            $data['email'] =   $sessionData['step2']['email'];
            $data['address'] =   $sessionData['step2']['address'];
            $data['zip_code'] =   $sessionData['step2']['zip_code'];
            $data['city'] =   $sessionData['step2']['city'];
            $data['country_id'] =   231;
            $data['state_id'] =   $sessionData['step2']['state_id'];
            $data['phone_number'] =   $sessionData['step2']['phone_number'];
            $data['amount'] = $sessionData['step1']['amount'];
            $data['people_id'] = $sessionData['step1']['people_id'];
            $res = $this->Stripe->createDonationCharge($data);
            if($res['status'] == "succeeded") {
                $data['status'] = 1;
                $data['charge_id'] = $res->id;
                $proofOfConcept = $this->ProofOfConcepts->patchEntity($proofOfConcept,$data);
                if ($this->ProofOfConcepts->save($proofOfConcept)) {
                    $this->getMailer('Users')->send('donation', [$proofOfConcept]);
                    $session->delete('step1');
                    $session->delete('step2');
                    $response = ['status' => true, 'message' =>'Your payment has been successfully processed','name' => ucwords($proofOfConcept->first_name. ' '.$proofOfConcept->last_name)]; 
                } else {
                   $response = ['status' => false, 'message' =>'your payment has been declined. please try again']; 
                }
            } else {
                $response = ['status' => false, 'message' =>$res['message']];
            }
        }
        $this->set([
                'response'=> $response,
                '_serialize'=> 'response'
        ]);
    }
}
