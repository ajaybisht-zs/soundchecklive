<?php
namespace App\Controller\Artist;

use App\Controller\AppController;

/**
 * UserEvents Controller
 *
 *
 * @method \App\Model\Entity\UserEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserEventsController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/artist_dashboard');
        $this->loadComponent('ManageProductImages');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userEvents = $this->paginate($this->UserEvents);

        $this->set(compact('userEvents'));
    }

    /**
     * View method
     *
     * @param string|null $id User Event id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userEvent = $this->UserEvents->get($id, [
            'contain' => []
        ]);

        $this->set('userEvent', $userEvent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->loadModel('Users');
        $userEvent = $this->UserEvents->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if(!empty($data['jfiler-items-exclude-user_event_images-0'])) {
                $deleteFile = $data['jfiler-items-exclude-user_event_images-0'];
                $explode = explode(',',$deleteFile);
                foreach ($explode as $key => $value) {
                    $exp = explode('//',$value);
                    $deleteFileName =  str_replace(['"', ']'], "", $exp[1]);
                    unset($data['user_event_images'][array_search($deleteFileName, array_column($data['user_event_images'], 'name'))]);
                }
            }
            $data['user_id'] = $this->Auth->user('id');
            $data = $this->ManageProductImages->manageUpcomingShowImage($data);
            $userEvent = $this->UserEvents->patchEntity($userEvent, $data,[
                                        'associated' => [
                                                        'UserEventImages',
                                                    ]
                                    ]);
            if ($this->UserEvents->save($userEvent)) {
                $this->Flash->success(__('The user event has been saved.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The user event could not be saved. Please, try again.'));
            return $this->redirect(['action' => 'add']);
        }
        $upcomingShow = $this->UserEvents
                            ->find()
                            ->where(['user_id' => $this->Auth->user('id')])
                            ->contain(['UserEventImages'])
                            ->order(['created' => 'DESC'])
                            ->limit(3)
                            ->toArray();
        try {
            $userProfile = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['UserProfiles','UserMediaHandles','userRecords']
            ]);
        } catch (RecordNotFoundException $e) {
            return $this->redirect('/registration');
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'UserRecords','action' => 'view', 'prefix'=> 'artist']);
        }

        $this->set(compact('userEvent','upcomingShow','userProfile'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userEvent = $this->UserEvents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userEvent = $this->UserEvents->patchEntity($userEvent, $this->request->getData());
            if ($this->UserEvents->save($userEvent)) {
                $this->Flash->success(__('The user event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user event could not be saved. Please, try again.'));
        }
        $this->set(compact('userEvent'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userEvent = $this->UserEvents->get($id);
        if ($this->UserEvents->delete($userEvent)) {
            $this->Flash->success(__('The user event has been deleted.'));
        } else {
            $this->Flash->error(__('The user event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
