<?php
namespace App\Controller\Artist;

use App\Controller\AppController;

/**
 * UserMediaHandles Controller
 *
 * @property \App\Model\Table\UserMediaHandlesTable $UserMediaHandles
 *
 * @method \App\Model\Entity\UserMediaHandle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserMediaHandlesController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/artist_dashboard');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userMediaHandles = $this->paginate($this->UserMediaHandles);

        $this->set(compact('userMediaHandles'));
    }

    /**
     * View method
     *
     * @param string|null $id User Media Handle id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userMediaHandle = $this->UserMediaHandles->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userMediaHandle', $userMediaHandle);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $userMediaHandle = $this->UserMediaHandles->newEntity();
        if ($this->request->is('post')) {
            $userMediaHandle = $this->UserMediaHandles->patchEntity($userMediaHandle, $this->request->getData());
            if ($this->UserMediaHandles->save($userMediaHandle)) {
                $this->Flash->success(__('The user media handle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user media handle could not be saved. Please, try again.'));
        }
        $users = $this->UserMediaHandles->Users->find('list', ['limit' => 200]);
        $this->set(compact('userMediaHandle', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Media Handle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->loadModel('Users');
        $userProfile = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UserMediaHandles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userProfile = $this->Users->patchEntity($userProfile, $this->request->getData());
            if ($this->Users->save($userProfile)) {
                $this->Flash->success(__('Media Handles has been updated.'));
            }else {
                $this->Flash->error(__('Media Handles could not be updated. Please, try again.'));
            }
        }

        $this->set(compact('userProfile'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Media Handle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userMediaHandle = $this->UserMediaHandles->get($id);
        if ($this->UserMediaHandles->delete($userMediaHandle)) {
            $this->Flash->success(__('The user media handle has been deleted.'));
        } else {
            $this->Flash->error(__('The user media handle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
