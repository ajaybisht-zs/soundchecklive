<?php
namespace App\Controller\Artist;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Mailer\MailerAwareTrait;


/**
 * UserProfiles Controller
 *
 * @property \App\Model\Table\UserProfilesTable $UserProfiles
 *
 * @method \App\Model\Entity\UserProfile[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserProfilesController extends AppController {
    use MailerAwareTrait;
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/login');
        $this->loadComponent('Twillio');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'Genres']
        ];
        $userProfiles = $this->paginate($this->UserProfiles);

        $this->set(compact('userProfiles'));
    }

    /**
     * View method
     *
     * @param string|null $id User Profile id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $userProfile = $this->UserProfiles->get($id, [
            'contain' => ['Users', 'Genres']
        ]);

        $this->set('userProfile', $userProfile);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->loadModel('Users');
        $this->loadModel('UserRecords');
        $this->loadModel('Plans');
        $userRecords = $this->UserRecords->newEntity();

        $session = $this->getRequest()->getSession();
        if (!$session->check('register.id')) {
            // Config.language exists and is not null.
        }

        $id = $session->read('register.id');
        $plans = $this->Plans->find();

        try {

            $user = $this->Users->get($id, [
                'contain' => [
                    'UserProfiles',
                    'UserBankDetails',
                    'UserMediaHandles',
                    'UserRecords' => ['UserRecordKeys'],
                    'UserPlans'
                ]
            ]);
            if(!empty($user->user_records)) {
                $userRecords = $user->user_records[0];
            }
        } catch (RecordNotFoundException $e) {
            return $this->redirect('/registration');
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'Logins','action' => 'add', 'prefix'=> 'artist']);
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            //pr($this->request->getData());
            //die;
            
            $data = $this->formatBirthDay($this->request->getData());
            if(!empty($data['next'])) {
                $data['step_completed'] = $data['next'] -1;
            }

            $data= $this->formatSupporters($this->request->getData());

            $user = $this->Users->patchEntity($user, $data, [
                'assosiated' => [
                    'UserProfiles', 
                    'UserMediaHandles', 
                    'UserRecords' => [
                        'associated' => ['UserRecordKeys']
                    ],
                    'UserBankDetails',
                    'UserSupporters'
                ]
            ]);


            if ($this->Users->save($user)) {
                if($data['next'] == 8) {
                    $score = $this->calculateSclScore($user->id);
                        if($score) {
                            $saveScore = $this->UserProfiles->get($user->user_profile->id);
                            $saveScore->scl_score = $score ;
                            if ($this->UserProfiles->save($saveScore)) {
                            }
                        }
                    $text = 'The New Artist '.ucwords($user->user_profile->first_name.' '.$user->user_profile->last_name). ' just register on soundchecklive.co. Please login to admin penel to approve.';
                     $text2 = 'Welcome to https://www.soundchecklive.com ! You will be notified when your account has been approved. Get your campaign ready by deciding who your core fanbase is. This could be as simple as a list of friends and family.';

                    $this->getMailer('Users')->send('newRegistraion', [$user]);
                    $this->getMailer('Users')->send('welcomeEmail', [$user]);
                    $t = $this->Twillio->sendSms(env('ADMIN_PHONE'), $text);
                    $z = $this->Twillio->sendSms($user->user_profile->phone_number, $text2);
                    return $this->redirect(['action' => 'thankyou',$score]);
                }
                return $this->redirect(['action' => 'add', '#' => $data['next']]);
            }
            
            $this->Flash->error(__('The user profile could not be saved. Please, try again.'));
        }

        $genres = $this->UserProfiles->Genres->find('list', ['limit' => 200]);
        $this->set(compact('user', 'genres', 'userRecords', 'plans'));
    }

    public function formatSupporters($data) {
        if(!empty($data['user_supporters'])) {
            $tagsName = $data['user_supporters'];

            foreach($data['user_supporters'] as $key => $val) {
                if(empty($val['name']) || empty($val['phone_number'])) {
                    unset($data['user_supporters'][$key]);
                } 
            }
        }
        /*if(!empty($tags)) {
            $data['tags'] = $tags;
        }*/
        return $data;
    }


    /**
     * Edit method
     *
     * @param string|null $id User Profile id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->viewBuilder()->setLayout('/User/artist_dashboard');
        $this->loadModel('Users');

        try {
            $userProfile = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['UserProfiles','UserMediaHandles','userRecords']
            ]);
        } catch (RecordNotFoundException $e) {
            return $this->redirect('/registration');
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'Logins','action' => 'add', 'prefix'=> 'artist']);
        }

       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userProfile = $this->Users->patchEntity($userProfile, $this->request->getData(),['associated' => ['UserRecords','UserProfiles','UserMediaHandles']]);
            if ($this->Users->save($userProfile)) {
                //pr($userProfile);die;
                $updateUserProfile = $this->Users->get($this->Auth->user('id'), [
                                'contain' => ['UserProfiles','UserMediaHandles','UserRecords']
                            ])->toArray();
                $this->Auth->setUser($updateUserProfile);
                //$this->Flash->success(__('The user profile has been saved.'));
                return $this->redirect(['controller'=> 'userProfiles','action' => 'videoPitch', 'prefix'=> 'artist']);
            }else {
                $this->Flash->error(__('The user profile could not be saved. Please, try again.'));
                return $this->redirect(['controller'=> 'userProfiles','action' => 'videoPitch', 'prefix'=> 'artist']);
            }

        }
        $users = $this->UserProfiles->Users->find('list', ['limit' => 200]);
        $genres = $this->UserProfiles->Genres->find('list', ['limit' => 200]);
        $this->set(compact('userProfile', 'users', 'genres'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Profile id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userProfile = $this->UserProfiles->get($id);
        if ($this->UserProfiles->delete($userProfile)) {
            $this->Flash->success(__('The user profile has been deleted.'));
        } else {
            $this->Flash->error(__('The user profile could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function videoPitch() {
        $this->viewBuilder()->setLayout('/User/artist_dashboard');
        $this->loadModel('UserRecords');
        $this->loadModel('Users');
        try {
            $record = $this->UserRecords->find()
                ->where(['user_id' => $this->Auth->user('id')])
                ->firstOrFail();
        } catch (RecordNotFoundException $e) {
            $record = $this->UserRecords->newEntity();
            // return $this->redirect('/registration');
        } catch(InvalidPrimaryKeyException $e) {
            $record = $this->UserRecords->newEntity();
            // return $this->redirect(['controller'=> 'Logins','action' => 'add', 'prefix'=> 'artist']);
        }

        try {
            $userProfile = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['UserProfiles','UserMediaHandles','userRecords']
            ]);
        } catch (RecordNotFoundException $e) {
            return $this->redirect('/registration');
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'Logins','action' => 'add', 'prefix'=> 'artist']);
        }

       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $record = $this->UserRecords->patchEntity($record, $this->request->getData());
            $record->user_id = $this->Auth->user('id');
            if ($this->UserRecords->save($record)) {
                //$this->Flash->success(__('The user profile has been saved.'));
            } else {
                $record->errors();
            }

        }
        $users = $this->UserProfiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('record', 'users','userProfile'));
    }


    protected function formatBirthDay ($data) {
        if(!empty($data['birth'])) {
            $data['user_profile']['birth_day'] = $data['birth']['day'];
            $data['user_profile']['birth_month']  = $data['birth']['month'];
            $data['user_profile']['birth_year']  = $data['birth']['year'];
        }
        return $data;
    }

   
    public function saveRecords() {
        $this->loadModel('UserRecords');
        $session = $this->getRequest()->getSession();
        if (!$session->check('register.id')) {
            // Config.language exists and is not null.
        }

        $id = $session->read('register.id');

        $data = $this->request->getData();
        $data['user_id'] = $id; 
        
        $record = $this->UserRecords->newEntity();
        
        if(!empty($data['id'])) {
            $record->id = $data['id'];
        }
        
        $record = $this->UserRecords->patchEntity($record, $data, [
            'assosiated' => ['UserRecordKeys']
        ]);
        //pr($record);die;
        if ($this->UserRecords->save($record)) {
           // $this->Flash->success(__('The user profile has been saved.'));
            return $this->redirect(['action' => 'add', '#' => $data['next']]);
        }
        $this->Flash->success(__('Something went wrong.'));
        return $this->redirect(['action' => 'add', '#' => $data['next'] - 1 ]);
    }

    public function pageUnderConstruction() {
        $this->viewBuilder()->setLayout('/User/artist_dashboard');
    }

    public function filmDirector() {
        // $this->viewBuilder()->setLayout('/User/artist_dashboard');
    }

    public function fashionDesigner() {
        // $this->viewBuilder()->setLayout('/User/artist_dashboard');
    }

    public function thankyou($score) {
        // $this->viewBuilder()->setLayout('/User/artist_dashboard');
        $this->set(compact('score'));
    }

    protected function calculateSclScore($id) {
        $this->loadModel('Users');
        $this->loadModel('UserRecords');
        $this->loadModel('Plans');

        $user = $this->Users->get($id, [
                'contain' => [
                    'UserProfiles',
                    'UserBankDetails',
                    'UserMediaHandles',
                    'UserRecords' => ['UserRecordKeys'],
                    'UserPlans'
                ]
        ]);
            $point= 0;
            $ethnicityPoint = 0;
            $genre = 0;
            $pro = 0;
            $merchandiseSalePoint = 0;
            $isBookingAgentPoint = 0;
            $isPerformedOverseasPoint = 0;
            $countryPoint = 0;
            $currentlyLivePoint = 0;
            $agePoint = 0;
        if(!empty($user->user_profile->birth_year)) {
            $then = \DateTime::createFromFormat("Y/m/d", $user->user_profile->birth_year.'/'.$user->user_profile->birth_month.'/'.$user->user_profile->birth_day);
            $diff = $then->diff(new \DateTime());
            $age = $diff->format("%y");
            $ethnicity = $user->user_profile->ethnicity;
            if($age >= 18 && $age <= 25) {
                $agePoint = 19.5;
            } elseif($age >= 26 && $age <= 30) {
                $agePoint = 14.6;
            } elseif($age >= 31 && $age <= 35) {
                $agePoint = 9.8;
            } elseif($age > 36) {
                $agePoint = 4.9;
            }else {
                $agePoint = 0;
            }          
        }
        $point = $point+$agePoint;

       if(!empty($user->user_profile->ethnicity)) {
            if($user->user_profile->ethnicity == 1 || $user->user_profile->ethnicity == 6 || $user->user_profile->ethnicity == 7) {
                $ethnicityPoint+= 2;
            } elseif($user->user_profile->ethnicity == 2 || $user->user_profile->ethnicity == 4 || $user->user_profile->ethnicity == 5){
                $ethnicityPoint+= 3;
            } else {
                $ethnicityPoint+= 1;
            }
        }

        $point= $point+$ethnicityPoint;

       if(!empty($user->user_profile->genre_id)) {
            if($user->user_profile->genre_id == 10 || $user->user_profile->genre_id == 6 || $user->user_profile->genre_id == 9) {
                $genre = 4.5;
            } elseif($user->user_profile->genre_id == 2 || $user->user_profile->genre_id == 11) {
                $genre = 3;
            }else {
               $genre = 1.5; 
            }
        }

        $point = $point+$genre;


        if(!empty($user->user_profile->performance_rights_organization)) {
            $pro = 3; // if input filled
        }

        $point = $point+$pro;

        // merchandise sale
        if($user->user_profile->is_merchandise_sale == 1) {
            $merchandiseSalePoint = 4; // if input filled
        }

        $point = $point+$merchandiseSalePoint;

        // booking agent point
        if($user->user_profile->is_booking_agent == 1) {
            $isBookingAgentPoint = 4; // if input filled
        }

        $point = $point+$isBookingAgentPoint;

        //perform overseas point
        if($user->user_profile->is_performed_overseas == 1) {
            $isPerformedOverseasPoint = 0.6; // if input filled
        }

        $point = $point+$isPerformedOverseasPoint;

        //Where are you from
        if(!empty($user->user_profile->country_id)) {
            $countryPoint = 7.5; // if input filled
        }

        $point = $point+$countryPoint;

        //Where are you from
        if(!empty($user->user_profile->state_id)) {
            $currentlyLivePoint = 7.5; // if input filled
        }

        $point = $point+$currentlyLivePoint;

        return $point;
    } 

}
