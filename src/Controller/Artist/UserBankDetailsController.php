<?php
namespace App\Controller\Artist;

use App\Controller\AppController;

/**
 * UserBankDetails Controller
 *
 *
 * @method \App\Model\Entity\UserBankDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserBankDetailsController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/artist_dashboard');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userBankDetails = $this->paginate($this->UserBankDetails);

        $this->set(compact('userBankDetails'));
    }

    /**
     * View method
     *
     * @param string|null $id User Bank Detail id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userBankDetail = $this->UserBankDetails->get($id, [
            'contain' => []
        ]);

        $this->set('userBankDetail', $userBankDetail);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userBankDetail = $this->UserBankDetails->newEntity();
        if ($this->request->is('post')) {
            $userBankDetail = $this->UserBankDetails->patchEntity($userBankDetail, $this->request->getData());
            if ($this->UserBankDetails->save($userBankDetail)) {
                $this->Flash->success(__('The user bank detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user bank detail could not be saved. Please, try again.'));
        }
        $this->set(compact('userBankDetail'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Bank Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
       $this->loadModel('Users');
        $userBankDetail = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UserBankDetails']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userBankDetail = $this->Users->patchEntity($userBankDetail, $this->request->getData());
            if ($this->Users->save($userBankDetail)) {
                $this->Flash->success(__('Banking information has been updated successfully.'));
            }else {
                $this->Flash->error(__('Banking information could not be updated. Please, try again.'));
            }
        }

        $this->set(compact('userBankDetail'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Bank Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userBankDetail = $this->UserBankDetails->get($id);
        if ($this->UserBankDetails->delete($userBankDetail)) {
            $this->Flash->success(__('The user bank detail has been deleted.'));
        } else {
            $this->Flash->error(__('The user bank detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
