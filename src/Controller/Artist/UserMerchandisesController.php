<?php
namespace App\Controller\Artist;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * UserMerchandises Controller
 *
 * @property \App\Model\Table\UserMerchandisesTable $UserMerchandises
 *
 * @method \App\Model\Entity\UserMerchandise[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserMerchandisesController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/artist_dashboard');
        $this->loadComponent('ManageProductImages');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userMerchandises = $this->paginate($this->UserMerchandises);

        $this->set(compact('userMerchandises'));
    }

    /**
     * View method
     *
     * @param string|null $id User Merchandise id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userMerchandise = $this->UserMerchandises->get($id, [
            'contain' => ['Users', 'Sizes', 'Colors', 'UserMerchandiseImages']
        ]);

        $this->set('userMerchandise', $userMerchandise);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Sizes');
        $this->loadModel('Colors');
        $this->loadModel('Users');
        $userMerchandise = $this->UserMerchandises->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user('id');
            $data = $this->ManageProductImages->setDefaultData($data);

            $userMerchandise = $this->UserMerchandises->patchEntity($userMerchandise, $data,
                                    [
                                        'associated' => [
                                                        'Sizes',
                                                        'UserMerchandiseImages',
                                                        'Colors'
                                                    ]
                                    ]);
            if ($this->UserMerchandises->save($userMerchandise)) {
                $this->Flash->success(__('The user merchandise has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The user merchandise could not be saved. Please, try again.'));
            return $this->redirect(['action' => 'add']);
        }
        $sizes = $this->Sizes->find('list',['valueField' => 'size'])->toArray();
        $colors = $this->Colors->find('list',['valueField' => 'hex_code'])->toArray();
        $merchandiseImage = $this->UserMerchandises
                            ->find()
                            ->where(['user_id' => $this->Auth->user('id')])
                            ->contain(['UserMerchandiseImages'])
                            ->order(['created' => 'DESC'])
                            ->limit(3)
                            ->toArray();
        try {
            $userProfile = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['UserProfiles','UserMediaHandles','userRecords']
            ]);
        } catch (RecordNotFoundException $e) {
           return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }

        $this->set(compact('userMerchandise', 'sizes', 'colors','merchandiseImage','userProfile'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Merchandise id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userMerchandise = $this->UserMerchandises->get($id, [
            'contain' => ['Sizes', 'Colors']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userMerchandise = $this->UserMerchandises->patchEntity($userMerchandise, $this->request->getData());
            if ($this->UserMerchandises->save($userMerchandise)) {
                $this->Flash->success(__('The user merchandise has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user merchandise could not be saved. Please, try again.'));
        }
        $users = $this->UserMerchandises->Users->find('list', ['limit' => 200]);
        $sizes = $this->UserMerchandises->Sizes->find('list', ['limit' => 200]);
        $colors = $this->UserMerchandises->Colors->find('list', ['limit' => 200]);
        $this->set(compact('userMerchandise', 'users', 'sizes', 'colors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Merchandise id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userMerchandise = $this->UserMerchandises->get($id);
        if ($this->UserMerchandises->delete($userMerchandise)) {
            $this->Flash->success(__('The user merchandise has been deleted.'));
        } else {
            $this->Flash->error(__('The user merchandise could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
