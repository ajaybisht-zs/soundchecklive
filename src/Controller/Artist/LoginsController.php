<?php
namespace App\Controller\Artist;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

/**
 * Logins Controller
 *
 *
 * @method \App\Model\Entity\Login[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LoginsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $logins = $this->paginate($this->Logins);

        $this->set(compact('logins'));
    }

    /**
     * View method
     *
     * @param string|null $id Login id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $login = $this->Logins->get($id, [
            'contain' => []
        ]);

        $this->set('login', $login);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->viewBuilder()->setLayout('User/login');
        $this->loadModel('Users');
        $session = $this->getRequest()->getSession();
        $title = 'Login';
        
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if($user['role_id'] == 1) {
                $this->Flash->error('Username or password is incorrect.');
                return $this->redirect($this->referer());
            }
            if ($user) {
                return $this->redirect(['controller' => 'Catalogues','action' => 'index','prefix' => false]);
                if($user['step_completed'] != 13) {                  

                    $session->write('register.id', $user['id']);
                    return $this->redirect(['controller' => 'UserProfiles', 'action' => 'add', '#'=> $user['step_completed']]);
                }
                if(!$user['status']) {
                    $this->Flash->error('Your account is not activated yet.');
                    return $this->redirect($this->referer());
                }
                
                if($user['is_deleted']) {
                    $this->Flash->error('Your account has been deleted.');
                    return $this->redirect($this->referer());
                }
                

                $this->Auth->setUser($user);

                if(!$user['chat_session_id']) {
                    $dataSessionId = $this->createSessionId();
                }

                $data = [];
                $user = $this->Users->get($user['id'], [
                        'contain' => [],
                    ]
                );
                $data['last_login'] = time()+10;
                $data['chat_session_id'] = isset($dataSessionId)?$dataSessionId:$user->chat_session_id;
                $user = $this->Users->patchEntity($user, $data);
                if ($this->Users->save($user)) {
                  
                }

                return $this->redirect(['controller' => 'UserProfiles', 'action' => 'videoPitch']);
                // return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Username or password is incorrect.');
        } 

        if(!empty($this->Auth->user() ) ) {
            if($this->Auth->user('role_id') == 1 ) {
                return $this->redirect('/'); 
            }
            if($this->Auth->user('step_completed') != 13) {
                $session->write('register.id', $this->Auth->user('id') );
                return $this->redirect(['controller' => 'UserProfiles', 'action' => 'add', '#'=> $this->Auth->user('step_completed') ]);
            }

            return $this->redirect('/');
        }
        $this->set(compact('title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Login id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->viewBuilder()->setLayout('User/artist_dashboard');
            $this->loadModel('Users');
        if($this->request->is('post')) {
            $data = $this->request->getData();
            
            try {
                $user = $this->Users->get($this->Auth->user('id'));
            }catch(RecordNotFoundException $e) {                
                $this->Flash->error('Account not found.');
                return $this->redirect($this->referer());
            }
            // echo $user->password;
            // pr($data['current_password']);
           
            if(! $this->Users->checkPassword($data['current_password'], $user->password) ) {
                $this->Flash->error('Provided current password did not matched with you current password');
                return $this->redirect($this->referer());
            }

            if($data['password'] !== $data['confirm_password']) {
                $this->Flash->error('Password did not matched with Re-Entered password' );
                return $this->redirect($this->referer());
            }

            $user = $this->Users->patchEntity($user, $data);
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Password updated successfully.'));
            } else {
                $this->Flash->error(__('Password could not be updated. Please, try again.'));
            }

            return $this->redirect($this->referer());
        }

        try {
            $userProfile = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['UserProfiles','UserMediaHandles','userRecords']
            ]);
        } catch (RecordNotFoundException $e) {
            return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }
         $this->set(compact('userProfile'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Login id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
    public function aboutUs() {
        $this->viewBuilder()->setLayout('User/login');
    }

    public function createSessionId() {

        $apiKey = env('TOK_API_KEY');
        $apiSecret = env('TOK_SECRET_KEY');
        $opentok = new OpenTok($apiKey, $apiSecret);

        $sessionOptions = array(
            'mediaMode' => MediaMode::ROUTED
        );

        $session = $opentok->createSession($sessionOptions);
        $sessionId = $session->getSessionId();
        return $sessionId;

        /*$apiKey = env('TOK_API_KEY');
        $apiSecret = env('TOK_SECRET_KEY');
        $opentok = new OpenTok($apiKey, $apiSecret);
        $session = $opentok->createSession();
        $session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));
        // An automatically archived session:
        $sessionOptions = array(
            'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED
        );
        $session = $opentok->createSession($sessionOptions);
        $sessionId = $session->getSessionId();
        return $sessionId;*/
    }


}
