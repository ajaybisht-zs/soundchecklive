<?php
namespace App\Controller\Artist;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * UserRecords Controller
 *
 * @property \App\Model\Table\UserRecordsTable $UserRecords
 *
 * @method \App\Model\Entity\UserRecord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserRecordsController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/login');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userRecords = $this->paginate($this->UserRecords);

        $this->set(compact('userRecords'));
    }

    /**
     * View method
     *
     * @param string|null $id User Record id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Users');
        $this->viewBuilder()->setLayout('/User/artist_dashboard');
        try {
            $userRecord = $this->UserRecords->find()
                            ->where(['UserRecords.user_id' => $this->Auth->user('id')])
                            ->contain([
                                'Users'=>[
                                            'UserProfiles'
                                        ],
                                'UserRecordKeys'
                            ])->firstOrFail();
        } catch (RecordNotFoundException $e) {
            $this->Flash->error(__('The user record not found.'));
            return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'UserRecords','action' => 'view', 'prefix'=> 'artist']);
        }

        try {
            $userProfile = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['UserProfiles','UserMediaHandles','userRecords']
            ]);
        } catch (RecordNotFoundException $e) {
            return $this->redirect('/registration');
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'UserRecords','action' => 'view', 'prefix'=> 'artist']);
        }

        $this->set('userRecord', $userRecord);
        $this->set('userProfile', $userProfile );
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userRecord = $this->UserRecords->newEntity();
        if ($this->request->is('post')) {
            $userRecord = $this->UserRecords->patchEntity($userRecord, $this->request->getData());
            if ($this->UserRecords->save($userRecord)) {
                $this->Flash->success(__('The user record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user record could not be saved. Please, try again.'));
        }
        $users = $this->UserRecords->Users->find('list', ['limit' => 200]);
        $this->set(compact('userRecord', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Record id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userRecord = $this->UserRecords->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userRecord = $this->UserRecords->patchEntity($userRecord, $this->request->getData());
            if ($this->UserRecords->save($userRecord)) {
                $this->Flash->success(__('The user record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user record could not be saved. Please, try again.'));
        }
        $users = $this->UserRecords->Users->find('list', ['limit' => 200]);
        $this->set(compact('userRecord', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Record id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userRecord = $this->UserRecords->get($id);
        if ($this->UserRecords->delete($userRecord)) {
            $this->Flash->success(__('The user record has been deleted.'));
        } else {
            $this->Flash->error(__('The user record could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
