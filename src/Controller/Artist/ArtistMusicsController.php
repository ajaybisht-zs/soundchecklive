<?php
namespace App\Controller\Artist;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * ArtistMusics Controller
 *
 * @property \App\Model\Table\ArtistMusicsTable $ArtistMusics
 *
 * @method \App\Model\Entity\ArtistMusic[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtistMusicsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $artistMusics = $this->paginate($this->ArtistMusics);

        $this->set(compact('artistMusics'));
    }

    /**
     * View method
     *
     * @param string|null $id Artist Music id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artistMusic = $this->ArtistMusics->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('artistMusic', $artistMusic);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->viewBuilder()->setLayout('/User/artist_dashboard');        
        
        try {
            $artistVideo = $this->ArtistMusics->find()
                ->where([
                    'ArtistMusics.user_id' => $this->Auth->user('id')
                ])
                ->firstOrFail();

        } catch (RecordNotFoundException $e) {
            $artistVideo = $this->ArtistMusics->newEntity();
        } catch(InvalidPrimaryKeyException $e) {
            $artistVideo = $this->ArtistMusics->newEntity();
        }
        
        if ($this->request->is('post')) {

            $userId = $this->Auth->user('id');
            $data = $this->request->getData();
            $data['user_id'] = $userId;
            $artistVideo = $this->ArtistMusics->patchEntity($artistVideo, $data);
            if ($this->ArtistMusics->save($artistVideo)) {
                $this->Flash->success(__('The video has been uploaded successfuly.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The video could not be uploaded. Please, try again.'));           
        
        }
        
        $users = $this->ArtistMusics->Users->find('list', ['limit' => 200]);

        $this->set(compact('users','artistVideo'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artist Music id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artistMusic = $this->ArtistMusics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artistMusic = $this->ArtistMusics->patchEntity($artistMusic, $this->request->getData());
            if ($this->ArtistMusics->save($artistMusic)) {
                $this->Flash->success(__('The artist music has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artist music could not be saved. Please, try again.'));
        }
        $users = $this->ArtistMusics->Users->find('list', ['limit' => 200]);
        $this->set(compact('artistMusic', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artist Music id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artistMusic = $this->ArtistMusics->get($id);
        if ($this->ArtistMusics->delete($artistMusic)) {
            $this->Flash->success(__('The artist music has been deleted.'));
        } else {
            $this->Flash->error(__('The artist music could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
