<?php
namespace App\Controller\Artist;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Mailer\MailerAwareTrait;
/**
 * ForgotPasswords Controller
 *
 *
 * @method \App\Model\Entity\ForgotPassword[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ForgotPasswordsController extends AppController
{
    use MailerAwareTrait;
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $forgotPasswords = $this->paginate($this->ForgotPasswords);

        $this->set(compact('forgotPasswords'));
    }

    /**
     * View method
     *
     * @param string|null $id Forgot Password id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $forgotPassword = $this->ForgotPasswords->get($id, [
            'contain' => []
        ]);

        $this->set('forgotPassword', $forgotPassword);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
       
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            try {
                $user = $this->Users->find()
                    ->where(['email' => $this->request->getData('email')])
                    ->contain(['UserProfiles'])
                    ->firstOrFail();
                $user->forgot_token = Text::uuid();
                if($this->Users->save($user)) {
                    $this->getMailer('Users')->send('forgot', [$user]);  
                    $this->Flash->success(__('A password reset email has been sent. Please check your inbox and follow the instructions.'));
                }
            } catch(RecordNotFoundException $e) {
                $this->Flash->error(__('Email not found. Please try again!'));
            }
            return $this->redirect($this->referer());
            
        }
        $this->set(compact('forgotPassword'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Forgot Password id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->viewBuilder()->setLayout('User/login');
        $this->loadModel('Users');
        try{

            $user = $this->Users->find()
                ->where([
                    'forgot_token' => $id
                ]) 
                ->firstOrFail();

        }catch(RecordNotFoundException $e){
            $this->Flash->error('Invalid Link.');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            if($this->request->getData('password') != $this->request->getData('confirm_password')) {
                $this->Flash->success(__('The password and confirm password couldn\'t matched.'));
                return $this->redirect($this->referer());
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->forgot_token = '';
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The password has been reset.'));
                return $this->redirect(['controller' => 'Users','action' => 'login', 'prefix' => 'artist']);
            }
            
            $this->Flash->error(__('The password could not be saved. Please, try again.'));
            return $this->redirect($this->referer());
        }

        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Forgot Password id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $forgotPassword = $this->ForgotPasswords->get($id);
        if ($this->ForgotPasswords->delete($forgotPassword)) {
            $this->Flash->success(__('The forgot password has been deleted.'));
        } else {
            $this->Flash->error(__('The forgot password could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
