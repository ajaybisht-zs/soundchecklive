<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\ServerRequest;
use Cake\Validation\Validator;
use Cake\Http\Exception\NotFoundException;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;

/**
 * Homes Controller
 *
 *
 * @method \App\Model\Entity\Home[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomesController extends AppController
{
    use MailerAwareTrait;
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout(false);
        $this->loadModel('HowWeWorks');
        $this->loadModel('Peoples');
        $howWeWorks = $this->HowWeWorks->getHowWeWork();
        $mob = new ServerRequest();
        $isMobile = $mob->is('mobile');
        $people = $this->Peoples->find('list', ['limit' => 200]);
        $this->set(compact('howWeWorks','isMobile','people'));
    }

    /**
     * View method
     *
     * @param string|null $id Home id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $home = $this->Homes->get($id, [
            'contain' => []
        ]);

        $this->set('home', $home);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $home = $this->Homes->newEntity();
        if ($this->request->is('post')) {
            $home = $this->Homes->patchEntity($home, $this->request->getData());
            if ($this->Homes->save($home)) {
                $this->Flash->success(__('The home has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The home could not be saved. Please, try again.'));
        }
        $this->set(compact('home'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Home id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $home = $this->Homes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $home = $this->Homes->patchEntity($home, $this->request->getData());
            if ($this->Homes->save($home)) {
                $this->Flash->success(__('The home has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The home could not be saved. Please, try again.'));
        }
        $this->set(compact('home'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Home id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $home = $this->Homes->get($id);
        if ($this->Homes->delete($home)) {
            $this->Flash->success(__('The home has been deleted.'));
        } else {
            $this->Flash->error(__('The home could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function mobileSlide() {
        $this->viewBuilder()->setLayout(false);

    }

    public function checkDevice() {
        $mob = new ServerRequest();
        $isMobile = $mob->is('mobile');
        if(!$isMobile) {
            return $this->redirect(['controller' => 'Homes', 'action' => 'index']);
        } else { 
            return $this->redirect(['controller' => 'Homes', 'action' => 'mobileSlide']);
        }
    }

    public function getToken() {
        /*if ($this->request->is('ajax')) {*/
            $id = $this->request->getQuery('user_id');
            $beamsClient = new \Pusher\PushNotifications\PushNotifications(array(
              "instanceId" => "e7c7ffca-f2cc-4718-8da2-eba3e64d98ee",
              "secretKey" => "7CC84A5BA4129F4353A8A9DEC4E1CE2471CB6E531A0A5CD2D80531EC1EF25D60",
            ));
            $beamsToken = $beamsClient->generateToken($id);
            //$token = $beamsToken['token'];
            $response = $beamsToken;
            echo(json_encode($response));die;
       /* }*/
        /*$this->set([
            'response'=> $response,
            '_serialize'=> 'response'
        ]);*/
    }

        public function contactUs() {
        if ($this->request->is('post')) {
            $validator = new Validator();
            $validator
                ->requirePresence('first_name')
                ->notEmptyString('first_name', 'First name cannot be empty')
                ->add('first_name', [
                    'length' => [
                        'rule' => ['minLength', 3],
                        'message' => 'First name need to be at least 3 characters long',
                    ],
                    'maxLength' => [
                        'rule' => ['maxLength', 40],
                        'message' => 'First name cannot be too long.'
                    ]                    
                ])

                ->add('last_name', [
                    'length' => [
                        'rule' => ['minLength', 3],
                        'message' => 'Last name need to be at least 3 characters long',
                    ],
                    'maxLength' => [
                        'rule' => ['maxLength', 40],
                        'message' => 'Last name cannot be too long.'
                    ]                    
                ])

                ->notEmptyString('phone', 'phone number cannot be empty')
            
                /*->notEmptyString('subject', 'Subject name cannot be empty')
                ->add('subject', [
                    'length' => [
                        'rule' => ['minLength', 1],
                        'message' => 'subject need to be at least 1 characters long',
                    ],
                    'maxLength' => [
                        'rule' => ['maxLength', 255],
                        'message' => 'Subject cannot be too long.'
                    ]
                ])*/
                ->notEmptyString('message', 'Message cannot be empty')
                ->notEmptyString('message','Please enter your message')
                ->add('message', [
                    'maxLength' => [
                        'rule' => ['maxLength', 1000],
                        'message' => 'Message cannot be too long.'
                    ]
                ])

                ->requirePresence('email')
                ->notEmptyString('email', 'email cannot be empty')
                ->add('email', [
                    'validFormat' => [
                        'rule' => ['email'],
                        'message' => 'Please enter valid email address',
                    ]
                ])
            ;
                
            $err = $validator->errors($this->request->getData());

            if(!$err) {
                    $data = $this->request->getData();
                   // pr($data);die;

                    try {
                        $t = $this->getMailer('Users')->send('contactUs', [$data]);
                        $this->Flash->success('Thanks for contacting us. We will get back to you soon.');
                        return $this->redirect($this->referer());
                    } catch (\Exception $e) {
                       $this->Flash->error('Something went going worng. Please try agian');
                       return $this->redirect($this->referer());
                    }
                    //$this->_contactUs($data);
                    //return $this->redirect($this->referer());

            } else {
                $err = $this->_setValidationError($err);
                $this->Flash->error('Please fix the errors'.$err);
                return $this->redirect($this->referer());
            }
        }
    }
}