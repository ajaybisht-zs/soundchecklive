<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserEvents Controller
 *
 *
 * @method \App\Model\Entity\UserEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserEventsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userEvents = $this->paginate($this->UserEvents);

        $this->set(compact('userEvents'));
    }

    /**
     * View method
     *
     * @param string|null $id User Event id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userEvent = $this->UserEvents->get($id, [
            'contain' => []
        ]);

        $this->set('userEvent', $userEvent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userEvent = $this->UserEvents->newEntity();
        if ($this->request->is('post')) {
            $userEvent = $this->UserEvents->patchEntity($userEvent, $this->request->getData());
            if ($this->UserEvents->save($userEvent)) {
                $this->Flash->success(__('The user event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user event could not be saved. Please, try again.'));
        }
        $this->set(compact('userEvent'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userEvent = $this->UserEvents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userEvent = $this->UserEvents->patchEntity($userEvent, $this->request->getData());
            if ($this->UserEvents->save($userEvent)) {
                $this->Flash->success(__('The user event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user event could not be saved. Please, try again.'));
        }
        $this->set(compact('userEvent'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userEvent = $this->UserEvents->get($id);
        if ($this->UserEvents->delete($userEvent)) {
            $this->Flash->success(__('The user event has been deleted.'));
        } else {
            $this->Flash->error(__('The user event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
