<?php
namespace App\Controller;

use App\Controller\AppController;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;
use Pusher\Pusher;
/**
 * Catalogues Controller
 *
 *
 * @method \App\Model\Entity\Catalogue[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CataloguesController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Stripe');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($type = null) {
        $this->viewBuilder()->setLayout(false);
        $this->loadModel('UserRecords');
        $this->loadModel('UserMerchandises');
        $this->loadModel('Users');
        $this->loadModel('HowWeWorks');
        $this->loadModel('ArtistMusics');
        $this->loadModel('PitchVideos');

        $this->loadModel('SiteDowns');
        $disableLayout = $this->SiteDowns->get(1);

        $getOnlineArtist = $this->Users->getOnlineUsers();
       
        $recentRecords = $this->UserRecords->getRecentRecords();
        $trendingRecords = $this->UserRecords->getTrendingRecords();
        $bestSellerRecords = $this->UserRecords->getBestSellerRecords();
        $soundOriginalRecords = $this->UserRecords->getSoundCheckRecords();
        $artistMerchandise = $this->UserMerchandises->getArtistMerchandise();
        $artistPitchVideo = $this->Users->getArtistPitchVideo();
        $howWeWorks = $this->HowWeWorks->getHowWeWork();
        $pitchVideos = $this->PitchVideos->getpitchVideo();
        $musicVideo = $this->Users->getArtistPitchVideo();
        //pr($musicVideo->toArray());die;

        $films = $this->Users->getFilmDirectorLatestRecord();
        $fashion = $this->Users->getFashionDesignerLatestRecord();
        $highSclMusic = $this->UserRecords->getHighScl();
        //pr($films);
        //pr($fashion);
        //die;
        if($type == 'film') {
            $filmDirectorRecentRecords = $this->Users->getFilmDirectorLatestRecord();
            $trendingFilmDirectorRecords = $this->Users->getFilmDirectorTrendingRecords();
            $bestFilmDirectorBestSellerRecords = $this->Users->getFilmDirectorBestSellerRecords();
            $filmDirectorPitchVideo = $this->Users->filmDirectorPitchVideo();
            $filmDirectorFashion = $this->Users->getFashionDesignerLatestRecord();
            $filmDirectorSclOriginals = $this->Users->getFilmDirectorSclOriginal();
            $filmDirectorHightestSclScore = $this->Users->getFilmDirectorHightestSclSore();
            $limitedEditionApparel = $this->UserMerchandises->getMerchandise();
        }

       //pr($limitedEditionApparel);die;
    

        if($type == 'fashion') {
            $fashionDesignerRecentRecords = $this->Users->getFashionDesignerLatestRecord();
            $trendingFashionDesignerRecords = $this->Users->getFashionDesignerTrendingRecords();
            $bestFashionDesignerSeller = $this->Users->getBestFashionDesignerSeller();
            $fashionDesignerSclOriginals = $this->Users->getFashionDesignerSclOriginal();
            $fashionDesignerHightestSclScore = $this->Users->getFashionDesignerHightestSclSore();
            $fashionDesignerPitchVideo = $this->Users->getFashionDesignerPitchVideo();
            $fashionDesignermusic = $this->UserRecords->getRecentRecords();
            $fashionDesignerFilm = $this->Users->getFilmDirectorLatestRecord();
            $limitedEditionApparel = $this->UserMerchandises->getMerchandise();
           //pr($fashionDesignerRecentRecords);die;
        }

      //pr($fashionDesignerHightestSclScore);die;


        $this->set(compact('recentRecords', 'trendingRecords', 'bestSellerRecords', 'soundOriginalRecords', 'artistMerchandise','artistPitchVideo','howWeWorks','musicVideo','pitchVideos','filmDirectorRecentRecords','fashionDesignerRecentRecords','trendingFilmDirectorRecords','bestFilmDirectorBestSellerRecords','filmDirectorPitchVideo','filmDirectorPitchVideo','filmDirectorFashion','filmDirectorSclOriginals','filmDirectorHightestSclScore','disableLayout','limitedEditionApparel','trendingFashionDesignerRecords','bestFashionDesignerSeller','fashionDesignerSclOriginals','fashionDesignerHightestSclScore','fashionDesignerPitchVideo','fashionDesignermusic','fashionDesignerFilm','films','fashion','highSclMusic','getOnlineArtist'));
    }

    /**
     * View method
     *
     * @param string|null $id Catalogue id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catalogue = $this->Catalogues->get($id, [
            'contain' => []
        ]);

        $this->set('catalogue', $catalogue);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catalogue = $this->Catalogues->newEntity();
        if ($this->request->is('post')) {
            $catalogue = $this->Catalogues->patchEntity($catalogue, $this->request->getData());
            if ($this->Catalogues->save($catalogue)) {
                $this->Flash->success(__('The catalogue has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The catalogue could not be saved. Please, try again.'));
        }
        $this->set(compact('catalogue'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Catalogue id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catalogue = $this->Catalogues->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catalogue = $this->Catalogues->patchEntity($catalogue, $this->request->getData());
            if ($this->Catalogues->save($catalogue)) {
                $this->Flash->success(__('The catalogue has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The catalogue could not be saved. Please, try again.'));
        }
        $this->set(compact('catalogue'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Catalogue id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catalogue = $this->Catalogues->get($id);
        if ($this->Catalogues->delete($catalogue)) {
            $this->Flash->success(__('The catalogue has been deleted.'));
        } else {
            $this->Flash->error(__('The catalogue could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function payment() {
        $this->viewBuilder()->setLayout(false);
        $this->loadModel('UserRecordPayments');
        if($this->request->is('ajax')) {
            $data = $this->request->getData();
            $amount =  $data['amount'];
            if($amount < 10) {
               $data['amount'] = 10;
            } 
            $res = $this->Stripe->createCharge($data);
            if($res['status'] == "succeeded") {
                $userRecordPayment = $this->UserRecordPayments->newEntity();
                $data['status'] = 1;
                $data['transition_id'] = $res->id;
                $userRecordPayment = $this->UserRecordPayments->patchEntity($userRecordPayment,$data);
                if ($this->UserRecordPayments->save($userRecordPayment)) {
                    $response = ['status' => true, 'message' =>'Your payment has been successfully processed']; 
                } else {
                   $response = ['status' => false, 'message' =>'your payment has been declined. please try again']; 
                }
            } else {
                $response = ['status' => false, 'message' =>$res['message']];
            }
        }
        $this->set([
                'response'=> $response,
                '_serialize'=> 'response'
        ]);
    }

    public function artistDetails($id = null) {
        $this->viewBuilder()->setLayout('User/login');
        $this->loadModel('UserRecords');
        $this->loadModel('ArtistMusics');
        $this->loadModel('Users');
        $records = $this->UserRecords->get($id, [
            'contain' => [
                'Users' => ['UserProfiles','UserEvents.UserEventImages','UserMerchandises.UserMerchandiseImages'],
                'UserRecordKeys',
                'UserRecordPayments',
                'UserPitchPayments'
            ]
        ]);
        //pr($records->user);die;
        try {
            $artistVideo = $this->ArtistMusics->find()->where(['ArtistMusics.user_id' => $records->user_id])->first();
        } catch (RecordNotFoundException $e) {
           return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }

        try {
            $similerArtist = $this->Users
                ->find()
                ->contain(['UserProfiles'])
                ->where([
                        'UserProfiles.genre_id' => $records->user->user_profile->genre_id,
                        'Users.status' => 1,
                        'Users.is_deleted' => 0
                    ])
                ->toArray();
        } catch (RecordNotFoundException $e) {
           return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }
        //pr(count($similerArtist));die;
    
        $this->set('record', $records);
        $this->set('upcomingShow');
        $this->set('artistVideo',$artistVideo);
        $this->set('similerArtist',$similerArtist);
    }

    public function productDescriptions($id) {
      $this->viewBuilder()->setLayout('User/login');
      $this->loadModel('UserMerchandises');
      $userMerchandise = $this->UserMerchandises->get($id, [
            'contain' => [
                            'Users',
                            'Colors',
                            'Sizes',
                            'UserMerchandiseImages',
                        ]
        ]);
        $this->set('userMerchandise', $userMerchandise);  
    }

    public function filmDirectorProfile($id = null) {
        $this->viewBuilder()->setLayout('User/login');
        $this->loadModel('Users');
        try {
            $filmDirectorProfile = $this->Users->get($id, [
            'contain' => ['filmDirectorProfiles']
            ]);
            //pr($filmDirectorProfile->FilmDirectorProfiles);die;
        } catch (RecordNotFoundException $e) {
           return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }
    $this->set('filmDirectorProfiles', $filmDirectorProfile);    
    }

    public function fashionDesignerProfile($id = null) {
        $this->viewBuilder()->setLayout('User/login');
        $this->loadModel('Users');
        try {
            $fashionDesignerProfile = $this->Users->get($id, [
            'contain' => ['fashionDesignerProfiles']
            ]);
            //pr($filmDirectorProfile->FilmDirectorProfiles);die;
        } catch (RecordNotFoundException $e) {
           return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }
    $this->set('fashionDesignerProfile', $fashionDesignerProfile);    
    }

    public function updatestatus() {
        if($this->request->is('ajax')) {
            $this->loadModel('Users');
            $user = $this->Auth->user('id');
            $data = [];
            $user = $this->Users->get($user, [
                    'contain' => [],
                ]
            );
            $data['last_login'] = time()+10;
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
              $response = 'updated';
            }
        }
        $this->set([
                'response'=> $response,
                '_serialize'=> 'response'
        ]);
    }

    public function chatPayment() {
       $this->viewBuilder()->setLayout(false);
        $this->loadModel('SclPayments');
        if($this->request->is('ajax')) {
            $data = $this->request->getData();
            $res = $this->Stripe->createSclCharge($data);
            if($res['status'] == "succeeded") {
                $sclLivePayment = $this->SclPayments->newEntity();
                $data['status'] = 1;
                $data['transition_id'] = $res->id;
                $sclLivePayment = $this->SclPayments->patchEntity($sclLivePayment,$data);
                if ($this->SclPayments->save($sclLivePayment)) {
                    $this->loadModel('Notifications');
                    $notification = $this->Notifications->newEntity();
                    $noti['user_id'] = $sclLivePayment->user_id;
                    $noti['fan_name'] = $sclLivePayment->card_holder_name;
                    $notification = $this->Notifications->patchEntity($notification, $noti);
                    if ($this->Notifications->save($notification)) {
                        $pusherResponse = $this->pusherApi($notification);
                        $this->sendPushNotification('user-'.$sclLivePayment->user_id);
                    }
                    $response = ['status' => true, 'message' =>'Your payment has been successfully processed and you will be redirect to video chat page.','user_id' => $sclLivePayment->user_id]; 
                } else {
                   $response = ['status' => false, 'message' =>'your payment has been declined. please try again']; 
                }
            } else {
                $response = ['status' => false, 'message' =>$res['message']];
            }
        }
        $this->set([
                'response'=> $response,
                '_serialize'=> 'response'
        ]);
    }


    public function videoChat($userId = null,$sessionId = null) {
       $this->viewBuilder()->setLayout(false); 
       $apiKey = env('TOK_API_KEY');
       $apiSecret = env('TOK_SECRET_KEY');
       $opentok = new OpenTok($apiKey, $apiSecret);
       
       //$session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

       /*// Create a session that attempts to use peer-to-peer streaming:
        $session = $opentok->createSession();

        // A session that uses the OpenTok Media Router, which is required for archiving:
        $session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

        // A session with a location hint:
       // $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

        // An automatically archived session:
        $sessionOptions = array(
            'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED
        );
        $session = $opentok->createSession($sessionOptions);


        // Store this sessionId in the database for later use
        $sessionId = $session->getSessionId();
*/
        $this->loadModel('Users');
        $user = $this->Users->get($userId, [
                        'contain' => ['UserProfiles'],
                    ]
                );

        $sessionId  = $user->chat_session_id;

        $token = $opentok->generateToken($sessionId);
        //pr( $token);die;
        // Generate a Token by calling the method on the Session (returned from createSession)
       // $token = $session->generateToken();

        // Set some options in a token
        /*$token = $sessionId->generateToken(array(
            'role'       => Role::MODERATOR,
            'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            'data'       => 'name=Johnny',
            'initialLayoutClassList' => array('focus')
        ));*/

        //return $this->redirect(['action' => 'videoChat',['sessionId' => $sessionId,'token' => $token,'apiKey' => $apiKey]]);
        $this->set(compact('sessionId','token','apiKey','userId','user'));

    }
}
