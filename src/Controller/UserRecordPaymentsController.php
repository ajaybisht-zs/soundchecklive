<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserRecordPayments Controller
 *
 *
 * @method \App\Model\Entity\UserRecordPayment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserRecordPaymentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userRecordPayments = $this->paginate($this->UserRecordPayments);

        $this->set(compact('userRecordPayments'));
    }

    /**
     * View method
     *
     * @param string|null $id User Record Payment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userRecordPayment = $this->UserRecordPayments->get($id, [
            'contain' => []
        ]);

        $this->set('userRecordPayment', $userRecordPayment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userRecordPayment = $this->UserRecordPayments->newEntity();
        if ($this->request->is('post')) {
            $userRecordPayment = $this->UserRecordPayments->patchEntity($userRecordPayment, $this->request->getData());
            if ($this->UserRecordPayments->save($userRecordPayment)) {
                $this->Flash->success(__('The user record payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user record payment could not be saved. Please, try again.'));
        }
        $this->set(compact('userRecordPayment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Record Payment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userRecordPayment = $this->UserRecordPayments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userRecordPayment = $this->UserRecordPayments->patchEntity($userRecordPayment, $this->request->getData());
            if ($this->UserRecordPayments->save($userRecordPayment)) {
                $this->Flash->success(__('The user record payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user record payment could not be saved. Please, try again.'));
        }
        $this->set(compact('userRecordPayment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Record Payment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userRecordPayment = $this->UserRecordPayments->get($id);
        if ($this->UserRecordPayments->delete($userRecordPayment)) {
            $this->Flash->success(__('The user record payment has been deleted.'));
        } else {
            $this->Flash->error(__('The user record payment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
