<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;
use Pusher\Pusher;
use Cake\Utility\Hash;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        if($this->request->getParam('prefix') == 'admin') { 
            $action = 'logout';
            $controller = "Users";
            $prefix = 'admin';
           
        } else {
            $action = 'add';
            $controller = "Logins";
            $prefix = 'artist'; 
        }

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        $this->loadComponent('TinyAuth.Auth', [
            'autoClearCache' => true,
            'authorize' => [
                'TinyAuth.Tiny' => [
                   'roleColumn' => 'role_id',
                   'rolesTable' => 'Roles',
                ]
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password'],
                    'finder' => 'auth'
                ],
            ],
            'loginAction' => [
                'controller' => 'Logins',
                'action' => 'add',
                'prefix'    => 'artist'
            ],
            /*'loginRedirect' => [
                'controller' => 'Browses',
                'action' => 'index'
            ],*/
            'logoutRedirect' => [
                'controller' => $controller,
                'action' => $action,
                'prefix'    => $prefix
            ],
            'storage' => [
                'className' => 'Session',
            ],
            'unauthorizedRedirect' => $this->referer() 
        ]); 
        $this->loadComponent('TinyAuth.AuthUser');

        $action = $this->request->getParam('action');
        if(!in_array($action, ['siteUnderConstruction','siteDown'])) {
            $this->_checkSiteDown();  
        }
       //$this->sendPushNotification();
    }

    private function _checkSiteDown()
    {
        $this->loadModel('SiteDowns');
        try {

           $siteDown = $this->SiteDowns->get(1);
           if($siteDown->status == 1){
                return $this->redirect('/site-down');
           } 
        }catch(\NotFoundException $e) {
           
        }
    }

    public function pusherApi($data) {
        $app_id = "1053188";
        $key = "9d0c4f7191c7c641a4ee";
        $secret = "9dc26e1a043c8a5bea4b";
        $cluster = "mt1";
        $pusher = new Pusher($key,$secret,$app_id, array('cluster' => $cluster,'encrypted' => true) );
        $pusherResponse = $pusher->trigger('soundechecklive', 'my-event', $data);
        if($pusherResponse) {
            return true;
        }
        return false;
    }

    public function sendPushNotification($userId) {
       $beamsClient = new \Pusher\PushNotifications\PushNotifications(array(
          "instanceId" => "e7c7ffca-f2cc-4718-8da2-eba3e64d98ee",
          "secretKey" => "7CC84A5BA4129F4353A8A9DEC4E1CE2471CB6E531A0A5CD2D80531EC1EF25D60",
        ));

        $publishResponse = $beamsClient->publishToUsers(
              [$userId],
              [
                "web" => array(
                "notification" => array(
                  "title" => "Hi!",
                  "body" => "Someone calling you."
                )
              ),
              ]
            );

            //echo("Published with Publish ID: " . $publishResponse->publishId . "\n");
            //die;
    }

     /**
     * Method _setValidationErrorForInvoice to set validation errors for invoice
     *
     * @param $errors array containing the error list
     * @return $str string containg the list of errors as string
     */
    protected function _setValidationError($errors = array()) {
        $allErrors = Hash::flatten($errors);
        $allErrors = array_unique($allErrors);
        $str = null;
        if (!empty($allErrors)) {
            foreach ($allErrors as $key => $val):
                $str.= $val;
            endforeach;
        }
        return $str;
    }
}
