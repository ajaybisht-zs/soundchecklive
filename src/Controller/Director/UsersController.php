<?php
namespace App\Controller\Director;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Mailer\MailerAwareTrait;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    use MailerAwareTrait;
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('User/login');
        $this->loadComponent('Twillio');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'UserProfiles', 'UserMediaHandles', 'UserBankDetails', 'UserPlans', 'FashionDesignerProfiles', 'FashionDesignerMediaHandles', 'UserRecords', 'UserEvents', 'UserMerchandises']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
   public function add() {
        $this->loadModel('Filmgenres');
        $this->loadModel('FilmDirectorProfiles');

        $session = $this->getRequest()->getSession();
        if (!$session->check('register.id')) {
            // Config.language exists and is not null.
        }

        $id = $session->read('register.id');

        try {

            $user = $this->Users->get($id, [
                'contain' => [
                    'FilmDirectorProfiles',
                   'FilmDirectorMediaHandles',
                   'UserSupporters'
                ]
            ]);
        } catch (RecordNotFoundException $e) {
            return $this->redirect('/filmDirector-signUp');
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect(['controller'=> 'Logins','action' => 'add', 'prefix'=> 'artist']);
        }

        if ($this->request->is('post') || $this->request->is('put')) {
        
            
            $data = $this->formatBirthDay($this->request->getData());

            if(!empty($data['next'])) {
                $data['step_completed'] = $data['next'] -1;
            }
            $data= $this->formatSupporters($this->request->getData());
            $user = $this->Users->patchEntity($user, $data, [
                'assosiated' => [
                   'FilmDirectorProfiles',
                   'FilmDirectorMediaHandles',
                   'UserSupporters'
                ]
            ]);
            /*pr($user);
            die;*/

            if ($this->Users->save($user)) {
                if($data['next'] == 15) {

                    $score = $this->calculateSclScore($user->id);
                    if($score) {
                        $saveScore = $this->FilmDirectorProfiles->get($user->film_director_profile->id);
                        $saveScore->scl_score = $score ;
                        if ($this->FilmDirectorProfiles->save($saveScore)) {

                        }
                    }
                    $text = 'The New Film Director '.ucwords($user->film_director_profile->first_name.' '.$user->film_director_profile->last_name). ' just register on soundchecklive.co. Please login to admin panel to approve.';
                    $text2 = 'Welcome to https://www.soundchecklive.com ! You will be notified when your account has been approved. Get your campaign ready by deciding who your core fanbase is. This could be as simple as a list of friends and family.';
                    $this->getMailer('Users')->send('newFilmDirectorRegistraion', [$user]);
                    $this->getMailer('Users')->send('welcomeEmail', [$user]);
                    $this->Twillio->sendSms(env('ADMIN_PHONE'), $text);
                    $this->Twillio->sendSms($user->film_director_profile->phone_number, $text2);
                    return $this->redirect(['action' => 'thankyou',$score]);
                }
                return $this->redirect(['action' => 'add', '#' => $data['next']]);
            }
            //pr($user->errors());die;
            
            $this->Flash->error(__('The film director profile could not be saved. Please, try again.'));
        }

        $genre = $this->Filmgenres->find('list', ['limit' => 200]);
        $this->set(compact('user', 'genre'));
    }

    protected function formatBirthDay($data) {
        if(!empty($data['film_director_profile']['birth']['month'])) {
            $data['film_director_profile']['birth_day'] = $data['film_director_profile']['birth']['day'];
            $data['film_director_profile']['birth_month']  = $data['film_director_profile']['birth']['month'];;
            $data['film_director_profile']['birth_year']  = $data['film_director_profile']['birth']['year'];;
        }
        //pr($data);die;
        return $data;
    }

    public function formatSupporters($data) {
        if(!empty($data['user_supporters'])) {
            $tagsName = $data['user_supporters'];

            foreach($data['user_supporters'] as $key => $val) {
                if(empty($val['name']) || empty($val['phone_number'])) {
                    unset($data['user_supporters'][$key]);
                } 
            }
        }
        /*if(!empty($tags)) {
            $data['tags'] = $tags;
        }*/
        return $data;
    }

    public function thankyou($score = null) {
        $this->set(compact('score'));
        // $this->viewBuilder()->setLayout('/User/artist_dashboard');
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /* Register method
     *
     * @return \Cake\Http\Response|null
     */
    public function register()
    {
        $title = 'Registraion';
        $session = $this->getRequest()->getSession();
        if ($this->request->is('post')) {
            $user = $this->Users->newEntity();
            $data =  $this->request->getData();
            if($data['over18'] == 0) {
                $this->Flash->error(__('You cannot access soudchecklive.co because you are not above 18 year'));
                return $this->redirect(['action' => 'register']);
            }
            $data['film_director_profile']['phone_number'] = $data['film_director_profile']['full_phone'];
            $data['role_id'] = 5; //filmDirector
            $data = $this->Users->patchEntity($user,$data,['associated' => ['FilmDirectorProfiles']]);
            if ($this->Users->save($data)) {
                $session->write('register.id', $user->id);
                // $this->Flash->success(__('Thank you for registering with sound check live. Your account is currently under review. You will be notified via email upon approval'));
                // $this->getMailer('Users')->send('newRegistraion', [$user]);  

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Your Registraion process failed. Please, try again.'));
            return $this->redirect(['action' => 'register']);
        }
        $this->set(compact('title'));
    }

    protected function calculateSclScore($id = null) {
        $this->loadModel('Users');
        $this->loadModel('UserRecords');
        $this->loadModel('Plans');

       $user = $this->Users->get($id, [
                'contain' => [
                    'FilmDirectorProfiles',
                   'FilmDirectorMediaHandles'
                ]
        ]);


            $point= 0;
            $ethnicityPoint = 0;
            $genre = 0;
            $pro = 0;
            $merchandiseSalePoint = 0;
            $isBookingAgentPoint = 0;
            $isPerformedOverseasPoint = 0;
            $countryPoint = 0;
            $currentlyLivePoint = 0;

        if(!empty($user->film_director_profile)) {
            $then = \DateTime::createFromFormat("Y/m/d", $user->film_director_profile->birth_year.'/'.$user->film_director_profile->birth_month.'/'.$user->film_director_profile->birth_day);
            $diff = $then->diff(new \DateTime());
            $age = $diff->format("%y");
            $ethnicity = $user->film_director_profile->ethnicity;

            if($age >= 18 && $age <= 25) {
                $agePoint = 19.5;
            } elseif($age >= 26 && $age <= 30) {
                $agePoint = 14.6;
            } elseif($age >= 31 && $age <= 35) {
                $agePoint = 9.8;
            } elseif($age > 36) {
                $agePoint = 4.9;
            }else {
                $agePoint = 0;
            }          
        }
        $point = $point+$agePoint;

       if(!empty($user->film_director_profile->ethnicity)) {
            if($user->film_director_profile->ethnicity == 1 || $user->film_director_profile->ethnicity == 6 || $user->film_director_profile->ethnicity == 7) {
                $ethnicityPoint+= 2;
            } elseif($user->film_director_profile->ethnicity == 2 || $user->film_director_profile->ethnicity == 4 || $user->film_director_profile->ethnicity == 5){
                $ethnicityPoint+= 3;
            } else {
                $ethnicityPoint+= 1;
            }
        }

        $point= $point+$ethnicityPoint;

       if(!empty($user->film_director_profile->filmgenre_id)) {
            $genre = 4.5; 
        }

        $point = $point+$genre;

        //production company

        if(!empty($user->film_director_profile->affiliated_with_name)) {
            $pro = 3; // if input filled
        }

        $point = $point+$pro;

        // merchandise sale
        if($user->film_director_profile->is_merchandise_sale == 1) {
            $merchandiseSalePoint = 4; // if input filled
        }

        $point = $point+$merchandiseSalePoint;

        // booking manager
        if($user->film_director_profile->is_booking_manager == 1) {
            $isBookingAgentPoint = 4; // if input filled
        }

        $point = $point+$isBookingAgentPoint;

        //perform overseas point
        if($user->film_director_profile->is_marketed_overseas == 1) {
            $isPerformedOverseasPoint = 0.6; // if input filled
        }

        $point = $point+$isPerformedOverseasPoint;

        //Where are you from
        if(!empty($user->film_director_profile->country_id)) {
            $countryPoint = 7.5; // if input filled
        }

        $point = $point+$countryPoint;

        //Where are you from
        if(!empty($user->film_director_profile->state_id)) {
            $currentlyLivePoint = 7.5; // if input filled
        }

        $point = $point+$currentlyLivePoint;

        return $point;
    } 
}
