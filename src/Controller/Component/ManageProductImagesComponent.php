<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Products component
 */
class ManageProductImagesComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function noop(array $array)
    {
        // Do stuff to array and return the result
        return ['image' => $array];
    }


    public function setDefaultData($data) {
        if(!empty($data['user_merchandise_images']) ) {            
            $data['user_merchandise_images'] = Hash::map($data['user_merchandise_images'], "{n}", [$this, 'noop']);
            foreach ($data['user_merchandise_images'] as $key => $value) {
                if(empty($value)) {
                    unset($data['user_merchandise_images'][$key]);
                }
            }
        }
        return $data;
    }

    public function manageUpcomingShowImage($data) {
        if(!empty($data['user_event_images']) ) {            
            $data['user_event_images'] = Hash::map($data['user_event_images'], "{n}", [$this, 'noop']);
            foreach ($data['user_event_images'] as $key => $value) {
                if(empty($value)) {
                    unset($data['user_event_images'][$key]);
                }
            }
        }
        return $data;
    }
}
