<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class StripeComponent extends Component
{
	public function initialize(array $config)
	{
		//\Stripe\Stripe::setApiKey(Configure::read('Stripe.Secret_key'));
		/*\Stripe\Stripe::setApiKey('sk_test_51H5vzNLbnU9rp6EU6VFPX1oZ7uVzRGZcgKcoe5Ieg73hkfvNLCdq9gqnMkXCJYxdILfFCD0yW5WlcMEmZSCHgNBZ00VaZK98v2');*/
		\Stripe\Stripe::setApiKey(env('STRIPE_CLIENT_SECRET'));
	}

	public function createCharge($data) {
		try {
			$charge = \Stripe\Charge::create([
			    'amount' => number_format($data['amount'], 2, ".", "")*100,
			    'currency' => 'usd',
			    'description' => 'Become partner fee',
			    'source' => $data['stripeToken'],
			    'metadata' => [
			    				"record_id" => $data['user_record_id'],
			    				'email' => $data['email'],
			    				'name' => $data['name'],
			    				'amount' => number_format($data['amount'], 2, ".", "")*100
			    			]
			]);
			$response = $charge;
		} catch (\Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$message = $err['message'];
			$errorType = $err['type'];
			$customErrorMessage = $errorType." $message";
			$response = ['status' => false, 'message' => $customErrorMessage];
		}
		return $response;
	}

	public function createSclCharge($data) {
		try {
			$charge = \Stripe\Charge::create([
			    'amount' => number_format($data['amount'], 2, ".", "")*100,
			    'currency' => 'usd',
			    'description' => 'scl live chat fee',
			    'source' => $data['stripeToken'],
			    'metadata' => [
			    				"scl_artist_id" => $data['user_id'],
			    				'card_holder_name' => $data['card_holder_name'],
			    				'amount' => number_format($data['amount'], 2, ".", "")*100
			    			]
			]);
			$response = $charge;
		} catch (\Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$message = $err['message'];
			$errorType = $err['type'];
			$customErrorMessage = $errorType." $message";
			$response = ['status' => false, 'message' => $customErrorMessage];
		}
		return $response;
	}

	public function createDonationCharge($data) {
		try {
			$charge = \Stripe\Charge::create([
			    'amount' => number_format($data['amount'], 2, ".", "")*100,
			    'currency' => 'usd',
			    'description' => 'Concept of proof Charge',
			    'source' => $data['stripeToken'],
			    'metadata' => [
			    				"first_name" => $data['first_name'],
			    				'last_name' => $data['last_name'],
			    				'email' => $data['email'],
			    				'amount' => number_format($data['amount'], 2, ".", "")*100
			    			]
			]);
			$response = $charge;
		} catch (\Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$message = $err['message'];
			$errorType = $err['type'];
			$customErrorMessage = $errorType." $message";
			$response = ['status' => false, 'message' => $customErrorMessage];
		}
		return $response;
	}

}