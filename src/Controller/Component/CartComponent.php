<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class CartComponent extends Component
{

	public function getcart()
    {
        return $this->request->getSession()->read('cart');
    }
    
	public function add($id, $quantity = 1, $product = null)
    {
    	$session = $this->request->getSession();

        if(!is_numeric($quantity) || $quantity < 1) {
            $quantity = 1;
        }

        if(!empty($product->user_merchandise_images)) {
            $productImage =  $product->user_merchandise_images;
        } else {
            $productImage =  [];
        }
        
        $quantity = abs($quantity);
        $data = [];

        $data = [
            'record_id' => $product->id,
            'name' => $product->name,
            'user_id' => $product->id,
            'image' => $productImage,
            'price' => sprintf('%01.2f', $product->price),
            'quantity' => $quantity,
            'subtotal' => sprintf('%01.2f',$product->price * $quantity),
        ];
       $session->write('cart.Orderproducts'.$product->id, $data);
       $count = count($session->read('cart')); 
       $sessionData = $session->read();
       return $sessionData;
    }

    public function remove($id) {
        if($this->request->getSession()->read('cart.'.$id)) {
            $product = $this->request->getSession()->read('cart.'.$id);
            $this->request->getSession()->delete('cart.'.$id);
            return true;
        }
        return false;
    }

    public function cartUpdate($id, $quantity, $product = null)
    {
    	$session = $this->request->getSession();
        if(!is_numeric($quantity) || $quantity < 1) {
            $quantity = 1;
        }
        $quantity = abs($quantity);
        $data = [];
        $data = [
            'record_id' => $id,
            'name' => $product['name'],
            'user_id' => $product['user_id'],
            'image' => $product['image'],
            'price' => sprintf('%01.2f', $product['price']),
            'quantity' => $quantity,
            'subtotal' => sprintf('%01.2f',$product['price'] * $quantity),
        ];
       $session->write('cart.Orderproducts'.$id, $data);
       $count = count($session->read('cart')); 
       $sessionData = $session->read();
       return $sessionData;
    }

    public function clear()
    {
        $session = $this->request->getSession();
        $session->delete('cart');
    }
}