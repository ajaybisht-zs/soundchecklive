<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;

/**
 * Twilio component
 */
class TwillioComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    //AC51091e668de8ffd28daa5737af514814
    //23252414ac1438ac5caf5e872baf5d93
    //+14159805510

    //local development:
    //AC52357f3256dc45c12910cc4cbafb5af9
    //d045e45f22084f9fee570cf3d244ecb5
    //+14243221366

    
    public function sendSms($number, $message) {
        try {
        $account_sid = env('TWILLO_ACCOUNT_SID');
        $auth_token = env('AUTH_TOKEN');
        // A Twilio number you own with SMS capabilities
        $twilio_number = env('TWILLO_NUMBER');

        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
            // Where to send a text message (your cell phone?)
            $number,
            array(
                'from' => $twilio_number,
                'body' => $message
            )
        );

        } catch(RestException $e) {
            //pr($e);die;
            //return $e;
        }
    }
}

