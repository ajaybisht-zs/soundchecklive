<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * UserMerchandises Controller
 *
 * @property \App\Model\Table\UserMerchandisesTable $UserMerchandises
 *
 * @method \App\Model\Entity\UserMerchandise[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserMerchandisesController extends AppController
{

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadComponent('ManageProductImages');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
         $this->paginate = [
            'contain' => [
                'Users' =>['UserProfiles'],
                'UserMerchandiseImages'
            ],
            'limit' => 10
        ];
        $userMerchandises = $this->paginate($this->UserMerchandises);

        $this->set(compact('userMerchandises'));
    }

    /**
     * View method
     *
     * @param string|null $id User Merchandise id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userMerchandise = $this->UserMerchandises->get($id, [
            'contain' => [
                            'Users',
                            'Colors',
                            'Sizes',
                            'UserMerchandiseImages',
                        ]
        ]);

        $this->set('userMerchandise', $userMerchandise);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Sizes');
        $this->loadModel('Colors');
        $userMerchandise = $this->UserMerchandises->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user('id');
            $data = $this->ManageProductImages->setDefaultData($data);

            $userMerchandise = $this->UserMerchandises->patchEntity($userMerchandise, $data,
                                    [
                                        'associated' => [
                                                        'Sizes',
                                                        'UserMerchandiseImages',
                                                        'Colors'
                                                    ]
                                    ]);
            if ($this->UserMerchandises->save($userMerchandise)) {
                $this->Flash->success(__('The user merchandise has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user merchandise could not be saved. Please, try again.'));
            return $this->redirect(['action' => 'add']);
        }
        $sizes = $this->Sizes->find('list',['valueField' => 'size'])->toArray();
        $colors = $this->Colors->find('list',['valueField' => 'hex_code'])->toArray();
        $this->set(compact('userMerchandise','sizes','colors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Merchandise id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userMerchandise = $this->UserMerchandises->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userMerchandise = $this->UserMerchandises->patchEntity($userMerchandise, $this->request->getData());
            if ($this->UserMerchandises->save($userMerchandise)) {
                $this->Flash->success(__('The user merchandise has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user merchandise could not be saved. Please, try again.'));
        }
        $users = $this->UserMerchandises->Users->find('list', ['limit' => 200]);
        $this->set(compact('userMerchandise', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Merchandise id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userMerchandise = $this->UserMerchandises->get($id);
        if ($this->UserMerchandises->delete($userMerchandise)) {
            $this->Flash->success(__('The user merchandise has been deleted.'));
        } else {
            $this->Flash->error(__('The user merchandise could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function approveMarchandise($id = null,$val = null) {
        $this->viewBuilder()->layout('ajax');
        if ($this->request->is(['ajax'])) {
            $userMerchandise = $this->UserMerchandises->get($id);
            $userMerchandise->status = $val;
            $flag   = false;
            if ($this->UserMerchandises->save($userMerchandise)) {
                $flag   = true; 
                //$this->getMailer('Users')->send('welcome', [$user]);  
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }
}
