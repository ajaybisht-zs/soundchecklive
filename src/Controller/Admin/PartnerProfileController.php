<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;

/**
 * PartnerProfile Controller
 *
 * @property \App\Model\Table\PartnerProfileTable $PartnerProfile
 *
 * @method \App\Model\Entity\PartnerProfile[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PartnerProfileController extends AppController
{
    use MailerAwareTrait;
     public $paginate = [
           'limit' => 10
    ];
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin/admin');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'EquityLevels'],
        ];
        $partnerProfile = $this->paginate($this->PartnerProfile);

        $this->set(compact('partnerProfile'));
    }

    /**
     * View method
     *
     * @param string|null $id Partner Profile id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Users');
        $partnerProfile = $this->Users->get($id, [
            'contain' => ['PartnerProfile.EquityLevels'],
        ]);
        //pr($partnerProfile);die;

        $this->set('influencer', $partnerProfile);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $partnerProfile = $this->PartnerProfile->newEntity();
        if ($this->request->is('post')) {
            $partnerProfile = $this->PartnerProfile->patchEntity($partnerProfile, $this->request->getData());
            if ($this->PartnerProfile->save($partnerProfile)) {
                $this->Flash->success(__('The partner profile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The partner profile could not be saved. Please, try again.'));
        }
        $users = $this->PartnerProfile->Users->find('list', ['limit' => 200]);
        $equityLevels = $this->PartnerProfile->EquityLevels->find('list', ['limit' => 200]);
        $this->set(compact('partnerProfile', 'users', 'equityLevels'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Partner Profile id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $this->loadModel('Users');
        $influencer = $this->Users->get($id, [
            'contain' => ['PartnerProfile.EquityLevels'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->formatBirthDay($this->request->getData());
            $influencer = $this->Users->patchEntity($influencer, $data,['associated' => ['PartnerProfile']]);
            if ($this->Users->save($influencer,['associated' => ['PartnerProfile']])) {
                $this->Flash->success(__('The Partner profile has been updated.'));
                return $this->redirect(['action' => 'edit',$id]);
            } else {
                $this->Flash->error(__('The Partner could not be saved. Please, try again.')); 
            }
        }
        $this->set(compact('influencer'));
    }

    protected function formatBirthDay ($data) {
        if(!empty($data['partner_profile']['birth_month'])) {
            $data['partner_profile']['birth_day'] = $data['partner_profile']['birth_day']['day'];
            $data['partner_profile']['birth_month']  = $data['partner_profile']['birth_month']['month'];;
            $data['partner_profile']['birth_year']  = $data['partner_profile']['birth_year']['year'];;
        }
        return $data;
    }

    /**
     * Delete method
     *
     * @param string|null $id Partner Profile id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Users');
        $partnerProfile = $this->Users->get($id);
        if ($this->Users->delete($partnerProfile)) {
            $this->Flash->success(__('The partner profile has been deleted.'));
        } else {
            $this->Flash->error(__('The partner profile could not be deleted. Please, try again.'));
        }

      return $this->redirect($this->referer());
    }

       public function pendingPartners() {
        $userRoleId = [3];
        $this->loadModel('Users');
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'PartnerProfile',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 0, 'Users.is_deleted' => 0])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function approvedPartners() {
        $userRoleId = [3];
        $this->loadModel('Users');
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'PartnerProfile',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 1, 'Users.is_deleted' => 0])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

     public function approveUser($id = null,$val = null) {
        $this->loadModel('Users');
        $this->viewBuilder()->layout('ajax');
        if ($this->request->is(['ajax'])) {
             $user = $this->Users->get($id,[
                        'contain' => ['PartnerProfile']
                    ]);
            $userEmail =  $user->email;
            $user->status = $val;
            $flag   = false;
            if ($this->Users->save($user)) {
                $flag   = true; 
               $this->getMailer('Users')->send('partnerProfile', [$user]);  
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }
}
