<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;

/**
 * Influencers Controller
 *
 *
 * @method \App\Model\Entity\Influencer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InfluencersController extends AppController
{
     use MailerAwareTrait;
     public $paginate = [
           'limit' => 10
    ];
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin/admin');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $influencers = $this->paginate($this->Influencers);

        $this->set(compact('influencers'));
    }

    /**
     * View method
     *
     * @param string|null $id Influencer id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Users');
        $influencer = $this->Users->get($id, [
            'contain' => ['InfluencerProfile.EquityLevels'],
        ]);

        $this->set('influencer', $influencer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $influencer = $this->Influencers->newEntity();
        if ($this->request->is('post')) {
            $influencer = $this->Influencers->patchEntity($influencer, $this->request->getData());
            if ($this->Influencers->save($influencer)) {
                $this->Flash->success(__('The influencer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The influencer could not be saved. Please, try again.'));
        }
        $this->set(compact('influencer'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Influencer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Users');
        $influencer = $this->Users->get($id, [
            'contain' => ['InfluencerProfile.EquityLevels'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->formatBirthDay($this->request->getData());
            $influencer = $this->Users->patchEntity($influencer, $data,['associated' => ['InfluencerProfile']]);
            if ($this->Users->save($influencer,['associated' => ['InfluencerProfile']])) {
                $this->Flash->success(__('The influencer profile has been updated.'));
                return $this->redirect(['action' => 'edit',$id]);
            } else {
                $this->Flash->error(__('The influencer could not be saved. Please, try again.')); 
            }
        }
        $this->set(compact('influencer'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Influencer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Users');
        $influencer = $this->Users->get($id);
        if ($this->Users->delete($influencer)) {
            $this->Flash->success(__('The influencer has been deleted.'));
             return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__('The influencer could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

    public function pendingInfluencer() {
        $userRoleId = [6];
        $this->loadModel('Users');
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'InfluencerProfile',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 0, 'Users.is_deleted' => 0])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function approvedInfluencers() {
        $userRoleId = [6];
        $this->loadModel('Users');
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'InfluencerProfile',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 1, 'Users.is_deleted' => 0])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function approved($id = null,$val = null) {
        $this->loadModel('Users');
        try {
            $approvedInfluencers = $this->Users->get(base64_decode($id), [
                'contain' => [],
            ]);

            $approvedInfluencers->status = 1;

            if ($this->request->is(['patch', 'post', 'put'])) {
                if ($this->Users->save($approvedInfluencers)) {
                    $this->Flash->success(__('The Influencers has been approved successfully.'));
                    try {
                           // $this->getMailer('Users')->send('adminApprovedParkingSpot', [$approvedParkingSpace]);
                        } catch (\Exception $e) {
                            // error
                        }
                    return $this->redirect(['controller' => 'Influencers','action' => 'approved_influencers']);
                }
                $this->Flash->error(__('The Influencers could not be approved. Please, try again.'));
            }
        } catch (\Exception $error) {
            $error = $error->getMessage();
            $this->Flash->error(__($error));
            return $this->redirect($this->referer());
        }
    }

    public function getEquity($follwer) {
        if($follwer > 25000 && $follwer <= 49999) {
            $level = 1;
        } elseif ($follwer > 50000 && $follwer <= 249999) {
            $level = 2;
        } elseif ($follwer > 250000 && $follwer <= 999999) {
            $level = 3;
        } elseif ($follwer > 1000000 && $follwer <= 2999999) {
            $level = 4;
        } elseif ($follwer > 3000000 && $follwer <= 4999999) {
            $level = 5;
        } elseif ($follwer > 5000000) {
            $level = 6;
        } else {
            $level = '';
        }
        return $level;
    }

    protected function formatBirthDay ($data) {
        if(!empty($data['influencer_profile']['birth_month'])) {
            $data['influencer_profile']['birth_day'] = $data['influencer_profile']['birth_day']['day'];
            $data['influencer_profile']['birth_month']  = $data['influencer_profile']['birth_month']['month'];;
            $data['influencer_profile']['birth_year']  = $data['influencer_profile']['birth_year']['year'];;
        }
        return $data;
    }

     public function approveUser($id = null,$val = null) {
        $this->loadModel('Users');
        $this->viewBuilder()->layout('ajax');
        if ($this->request->is(['ajax'])) {
             $user = $this->Users->get($id,[
                        'contain' => ['InfluencerProfile']
                    ]);
            $userEmail =  $user->email;
            $user->status = $val;
            $flag   = false;
            if ($this->Users->save($user)) {
                $flag   = true; 
                $this->getMailer('Users')->send('influencerProfile', [$user]);  
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }
}
