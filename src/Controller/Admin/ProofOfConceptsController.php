<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ProofOfConcepts Controller
 *
 * @property \App\Model\Table\ProofOfConceptsTable $ProofOfConcepts
 *
 * @method \App\Model\Entity\ProofOfConcept[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProofOfConceptsController extends AppController
{
    public $paginate = [
           'limit' => 10
    ];
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin/admin');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Peoples'],
        ];
        $proofOfConcepts = $this->paginate($this->ProofOfConcepts);
       // pr($proofOfConcepts);die;

        $this->set(compact('proofOfConcepts'));
    }

    /**
     * View method
     *
     * @param string|null $id Proof Of Concept id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $proofOfConcept = $this->ProofOfConcepts->get($id, [
            'contain' => ['Peoples','Countries','States'],
        ]);
        //pr($proofOfConcept);die;

        $this->set('proofOfConcept', $proofOfConcept);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $proofOfConcept = $this->ProofOfConcepts->newEntity();
        if ($this->request->is('post')) {
            $proofOfConcept = $this->ProofOfConcepts->patchEntity($proofOfConcept, $this->request->getData());
            if ($this->ProofOfConcepts->save($proofOfConcept)) {
                $this->Flash->success(__('The proof of concept has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The proof of concept could not be saved. Please, try again.'));
        }
        $people = $this->ProofOfConcepts->People->find('list', ['limit' => 200]);
        $charges = $this->ProofOfConcepts->Charges->find('list', ['limit' => 200]);
        $countries = $this->ProofOfConcepts->Countries->find('list', ['limit' => 200]);
        $this->set(compact('proofOfConcept', 'people', 'charges', 'countries'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Proof Of Concept id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $proofOfConcept = $this->ProofOfConcepts->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $proofOfConcept = $this->ProofOfConcepts->patchEntity($proofOfConcept, $this->request->getData());
            if ($this->ProofOfConcepts->save($proofOfConcept)) {
                $this->Flash->success(__('The proof of concept has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The proof of concept could not be saved. Please, try again.'));
        }
        $people = $this->ProofOfConcepts->People->find('list', ['limit' => 200]);
        $charges = $this->ProofOfConcepts->Charges->find('list', ['limit' => 200]);
        $countries = $this->ProofOfConcepts->Countries->find('list', ['limit' => 200]);
        $this->set(compact('proofOfConcept', 'people', 'charges', 'countries'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Proof Of Concept id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $proofOfConcept = $this->ProofOfConcepts->get($id);
        if ($this->ProofOfConcepts->delete($proofOfConcept)) {
            $this->Flash->success(__('The proof of concept has been deleted.'));
        } else {
            $this->Flash->error(__('The proof of concept could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
