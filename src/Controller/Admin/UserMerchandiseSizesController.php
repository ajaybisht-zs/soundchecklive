<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * UserMerchandiseSizes Controller
 *
 * @property \App\Model\Table\UserMerchandiseSizesTable $UserMerchandiseSizes
 *
 * @method \App\Model\Entity\UserMerchandiseSize[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserMerchandiseSizesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userMerchandiseSizes = $this->paginate($this->UserMerchandiseSizes);

        $this->set(compact('userMerchandiseSizes'));
    }

    /**
     * View method
     *
     * @param string|null $id User Merchandise Size id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userMerchandiseSize = $this->UserMerchandiseSizes->get($id, [
            'contain' => ['UserMerchandises']
        ]);

        $this->set('userMerchandiseSize', $userMerchandiseSize);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userMerchandiseSize = $this->UserMerchandiseSizes->newEntity();
        if ($this->request->is('post')) {
            $userMerchandiseSize = $this->UserMerchandiseSizes->patchEntity($userMerchandiseSize, $this->request->getData());
            if ($this->UserMerchandiseSizes->save($userMerchandiseSize)) {
                $this->Flash->success(__('The user merchandise size has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user merchandise size could not be saved. Please, try again.'));
        }
        $userMerchandises = $this->UserMerchandiseSizes->UserMerchandises->find('list', ['limit' => 200]);
        $this->set(compact('userMerchandiseSize', 'userMerchandises'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Merchandise Size id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userMerchandiseSize = $this->UserMerchandiseSizes->get($id, [
            'contain' => ['UserMerchandises']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userMerchandiseSize = $this->UserMerchandiseSizes->patchEntity($userMerchandiseSize, $this->request->getData());
            if ($this->UserMerchandiseSizes->save($userMerchandiseSize)) {
                $this->Flash->success(__('The user merchandise size has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user merchandise size could not be saved. Please, try again.'));
        }
        $userMerchandises = $this->UserMerchandiseSizes->UserMerchandises->find('list', ['limit' => 200]);
        $this->set(compact('userMerchandiseSize', 'userMerchandises'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Merchandise Size id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userMerchandiseSize = $this->UserMerchandiseSizes->get($id);
        if ($this->UserMerchandiseSizes->delete($userMerchandiseSize)) {
            $this->Flash->success(__('The user merchandise size has been deleted.'));
        } else {
            $this->Flash->error(__('The user merchandise size could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
