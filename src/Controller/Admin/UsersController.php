<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Network\Session\DatabaseSession;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Exception\Exception;
use Cake\Mailer\MailerAwareTrait;
use Cake\I18n\Number;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    use MailerAwareTrait;

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Twillio');
    }

    public $paginate = [
           'limit' => 10
    ];
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function login()
    {
        $this->viewBuilder()->setLayout(false);
        if ($this->request->is('post')) {
         $user = $this->Auth->identify(); 
         if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Users', 'action' => 'index', 'prefix' => 'admin']);
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
    }

    public function index() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('UserRecords');
        $this->loadModel('ProofOfConcepts');

        $approvedUser = $this->Users->find()
                    ->where(['status' => 1,'role_id' => 2, 'is_deleted' => 0])
                    ->count();

        $pendingForApproval = $this->Users->find()
                    ->where(['status' => 0,'role_id' => 2, 'is_deleted' => 0])
                    ->count();

        $soundCheckLiveOriginal = $this->UserRecords->find()
                    ->where(['user_id' => 1])
                    ->count();

        $approvedFilmDirector = $this->Users->find()
                    ->where(['status' => 1,'role_id' => 5, 'is_deleted' => 0])
                    ->count();

        $pendingFilmDirector = $this->Users->find()
                    ->where(['status' => 0,'role_id' => 5, 'is_deleted' => 0])
                    ->count();

        $approvedFashionDesigner = $this->Users->find()
                    ->where(['status' => 1,'role_id' => 4, 'is_deleted' => 0])
                    ->count();

        $pendingFashionDesigner = $this->Users->find()
                    ->where(['status' => 0,'role_id' => 4, 'is_deleted' => 0])
                    ->count();

        $approvedInfluencer = $this->Users->find()
                    ->where(['status' => 1,'role_id' => 6, 'is_deleted' => 0])
                    ->count();

        $pendingInfluencer = $this->Users->find()
                    ->where(['status' => 0,'role_id' => 6, 'is_deleted' => 0])
                    ->count();


        $totalEarning =  $this->ProofOfConcepts->find();
        $totalEarning = $totalEarning->select([
                        'total_earning' => $totalEarning->func()->sum('amount')])->first();
        //pr($totalEarning);die;

        $this->set(compact('approvedUser','pendingForApproval','soundCheckLiveOriginal','approvedFilmDirector','pendingFilmDirector','totalEarning','approvedFashionDesigner','pendingFashionDesigner','approvedInfluencer','pendingInfluencer'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $user = $this->Users->get($id, [
            'contain' => [ 
                'UserProfiles',
                'UserRecords.UserRecordKeys',
                'UserMediaHandles',
                'UserSupporters'
            ]
        ]);

        $this->set(compact('user'));
    }


    public function logout() {
        $this->Auth->logout();
        $this->Flash->success(__('You have successfully logged out.'));
        return $this->redirect(['controller' => 'Users', 'action' => 'login', 'prefix' => 'admin']);
    }

   
    public function approvedUser()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [2];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'UserProfiles',
                            'UserPlans.Plans',
                            'UserRecords.UserRecordPayments'
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 1, 'Users.is_deleted' => 0])
                        ->andWhere($condition['users'])
                        ->order(['Users.id' => 'ASC']);
                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    /**
    * set searching condition
    * @param searching parameter
    * @return searching conditions as per query
    **/
    protected function _setCondition($search)
    {
        $condition = [];
        $condition['users'] = [];

        if (!empty($search)) {
            $tag_search = trim($search);
            if (filter_var($tag_search, FILTER_VALIDATE_EMAIL)) {
                $condition['users'] = ['Users.email' => $tag_search];
            } else {
                $condition['users'][] = [
                    'OR' => [
                        ['UserProfiles.first_name LIKE' => '%'.$tag_search.'%'],
                        ['UserProfiles.last_name LIKE' => '%'.$tag_search.'%']
                    ]
                ];
            }
        }

        return $condition;
    }

    public function pendingForApproval()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [2];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'UserProfiles',
                            'UserPlans.Plans',
                            'UserRecords.UserRecordPayments'
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 0, 'Users.is_deleted' => 0])
                        ->andWhere($condition['users'])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function trashedUsers()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [2];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                $condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'UserProfiles',
                            'UserPlans.Plans',
                            'UserRecords.UserRecordPayments'
                        ])
                        ->where(['Users.role_id IN' => $userRoleId, 'Users.is_deleted' => 1])
                        ->andWhere($condition['users'])
                        ->order(['Users.id' => 'ASC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function approveUser($id = null,$val = null) {
        $this->viewBuilder()->layout('ajax');
        if ($this->request->is(['ajax'])) {
             $user = $this->Users->get($id,[
                        'contain' => ['UserProfiles','UserSupporters']
                    ]);
            $userEmail =  $user->email;
            $user->status = $val;
            $flag   = false;
            if ($this->Users->save($user)) {
                $flag   = true; 
                $this->getMailer('Users')->send('welcome', [$user]);
                $userName = $user->user_profile->first_name.' '.$user->user_profile->last_name;
                $text = "Hi, A new user $userName register in soundchecklive.co with your reference";
                if($user->status == 1) {
                    if(!empty($user->user_supporters)) {
                        foreach ($user->user_supporters as $key => $value) {
                           if(!empty($value->phone_number)) {
                                //pr($value->phone_number);die;
                                $this->Twillio->sendSms('+1'.$value->phone_number, $text);
                           }
                        }
                    }

                }
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }

    public function changePassword() {      
        if ($this->request->is('post')  && !empty($this->request->getData('password'))) {          
            if ($this->request->getData('password') !== $this->request->getData('confirm_password')) {
                $this->Flash->error('Password and confirm password did not matched');
                return $this->redirect($this->referer());
            }
            $user = $this->Users->get($this->Auth->user('id'));      
            if (!(new DefaultPasswordHasher)->check(
                    $this->request->getData('old_password'),
                    $user->password
                )) {                
                    $this->Flash->error('Old Password did not matched');
                    return $this->redirect($this->referer());
            }
            //Update Password
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->id = $this->Auth->user('id');
            if ($this->Users->save($user)) {
                $this->Flash->success('Password has been updated succesfully');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Password could not be updated. Please, try again');                        
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->is_deleted = 1;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The Users has been deleted.'));
        } else {
            $this->Flash->error(__('The Users could not be deleted. Please, try again.'));
            return $this->redirect($this->referer());
        }
        return $this->redirect($this->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->is_deleted = 0;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The Users has been restored.'));
        } else {
            $this->Flash->error(__('The Users could not be restored. Please, try again.'));
            return $this->redirect($this->referer());
        }
        return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('UserProfiles');
        $this->loadModel('Levels');
        $this->Users->hasOne('UserRecords', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $user = $this->Users->get($id, [
            'contain' => [ 
                'UserProfiles',
                'UserBankDetails',
                'UserRecords.UserRecordKeys',
                'UserMediaHandles',
                'UserSupporters'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data = $this->formatBirthDay($data);
            $data= $this->formatSupporters($data);
            $user = $this->Users->patchEntity($user, $data,
                [
                    'associated' => 
                    [
                        'UserRecords.UserRecordKeys',
                        'UserMediaHandles',
                        'UserProfiles',
                        'UserBankDetails',
                        'UserSupporters'
                    ]
                ]);
            //pr($user);die;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user information edited.'));
               return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The user information could not be edited. Please, try again.'));
        }
        $genres = $this->UserProfiles->Genres->find('list', ['limit' => 200]);
        $levels = $this->Levels->find('list', [
            'keyField' => 'id',
            'valueField' => function ($row) {
                                return 'Level '.$row['level_name'] . ' ' .'('. Number::currency($row['price'],'USD').')';
                            }
        ])->toArray();
        $this->set(compact('user','genres','levels'));
    }

    protected function formatBirthDay ($data) {
        if(!empty($data['user_profile']['birth_month']['month'])) {
            $data['user_profile']['birth_day'] = $data['user_profile']['birth_day']['day'];
            $data['user_profile']['birth_month']  = $data['user_profile']['birth_month']['month'];;
            $data['user_profile']['birth_year']  = $data['user_profile']['birth_year']['year'];;
        }
        return $data;
    }

    public function howWeWorks() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('HowWeWorks');
        $findCurrentVideo = $this->HowWeWorks->getHowWeWork();
        
        if(empty($findCurrentVideo)) {
            $findCurrentVideo = $this->HowWeWorks->newEntity();
        }
        
        if($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user('id');
            $howWeworkVideo = $this->HowWeWorks->patchEntity($findCurrentVideo, $data);
            if ($this->HowWeWorks->save($howWeworkVideo)) {
                $this->Flash->success(__('The user record has been saved.'));

                return $this->redirect(['action' => 'howWeWorks']);
            }
            $this->Flash->error(__('The user record could not be saved. Please, try again.'));
        }
        $this->set(compact('findCurrentVideo'));
    }

    public function addSmartLink() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('UserProfiles'); 
        $data = $this->request->getData();
        $id= $data['id'];
        try {
            $user = $this->UserProfiles->find()->where(['user_id' => $id])->first();     
        } catch (RecordNotFoundException $e) {
           return $this->redirect($this->referer());
        } catch(InvalidPrimaryKeyException $e) {
            return $this->redirect($this->referer());
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->UserProfiles->patchEntity($user, $data);
            if ($this->UserProfiles->save($user)) {
                $this->Flash->success(__('The Smart link added successfully'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The Smart link could not be saved. Please, try again.'));
            return $this->redirect($this->referer());
        }
    }

    public function addUser() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('UserProfiles');
        $this->loadModel('Plans');

        $this->Users->hasOne('UserRecords', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        if ($this->request->is('post')) {
            $user = $this->Users->newEntity();
            $data = $this->formatBirthDay($this->request->getData());
            $data['role_id'] = 2;
            $user = $this->Users->patchEntity($user, $data, [
                'associated' => [
                    'UserProfiles', 
                    'UserMediaHandles', 
                    'UserRecords',
                    'UserRecords.UserRecordKeys' ,
                    'UserPlans',
                ]
            ]);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved successfully'));
               return $this->redirect(['action' => 'pendingForApproval']);
            } else {
                $this->Flash->error(__('The user could not be edited. Please, try again.'));
                return $this->redirect($this->referer());  
            }
        }
        $genres = $this->UserProfiles->Genres->find('list', ['limit' => 200]);
        $plan = $this->Plans->find('list', ['limit' => 200]);
        $this->set(compact('genres','plan'));
    }


    public function pendingFilmDirector()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [5];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                //$condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'FilmDirectorProfiles',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 0, 'Users.is_deleted' => 0])
                        //->andWhere($condition['users'])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }


    public function approvedFilmDirector()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [5];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                //$condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'FilmDirectorProfiles',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 1, 'Users.is_deleted' => 0])
                        //->andWhere($condition['users'])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function pendingFashionDesigner()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [4];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                //$condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'FashionDesignerProfiles',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 0, 'Users.is_deleted' => 0])
                        //->andWhere($condition['users'])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }


    public function approvedFashionDesigner()
    {
        $this->viewBuilder()->setLayout('admin/admin');
        $userRoleId = [4];
        try {
            if ($this->request->is('get')) {
                $search = $this->request->getQuery('search_name');
                //$condition = $this->_setCondition($search);

                $users = $this->Users
                        ->find('all')
                        ->contain([
                            'FashionDesignerProfiles',
                        ])
                        ->where(['Users.role_id IN' => $userRoleId,'Users.status' => 1, 'Users.is_deleted' => 0])
                        //->andWhere($condition['users'])
                        ->order(['Users.created' => 'DESC']);

                $users = $this->paginate($users);
            }

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } catch (\Exception $e) {
            $this->Flash->error($e);
            return $this->redirect($this->referer());
        }
    }

    public function approveFilmDirector($id = null,$val = null) {
        $this->viewBuilder()->layout('ajax');
        $text= 'Your Film Director account has been approved! Please login to your account for more details at https://www.soundchecklive.co/login. Verify your account to have access to all of our features.';
        if ($this->request->is(['ajax'])) {
             $user = $this->Users->get($id,[
                        'contain' => ['FilmDirectorProfiles','UserSupporters']
                    ]);
            $userEmail =  $user->email;
            $user->status = $val;
            $flag   = false;
            if ($this->Users->save($user)) {
                $this->getMailer('Users')->send('approveFilmDirectorNew', [$user]);
                $this->Twillio->sendSms($user->film_director_profile->phone_number, $text);

                $userName = $user->film_director_profile->first_name.' '.$user->film_director_profile->last_name;
                $text2 = "Hi, A new Film director $userName register in soundchecklive.co with your reference";
                if($user->status == 1) {
                    if(!empty($user->user_supporters)) {
                        foreach ($user->user_supporters as $key => $value) {
                           if(!empty($value->phone_number)) {
                                //pr($value->phone_number);die;
                                $this->Twillio->sendSms('+1'.$value->phone_number, $text2);
                           }
                        }
                    }
                }
                $flag   = true; 
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }

    public function approveFashionDesigner($id = null,$val = null) {
        $this->viewBuilder()->layout('ajax');
        $text= 'Your Fashion Designer account has been approved! Please login to your account for more details at https://www.soundchecklive.co/login. Verify your account to have access to all of our features.';
        if ($this->request->is(['ajax'])) {
             $user = $this->Users->get($id,[
                        'contain' => ['FashionDesignerProfiles','UserSupporters']
                    ]);
            $userEmail =  $user->email;
            $user->status = $val;
            $flag   = false;
            if ($this->Users->save($user)) {
                $this->getMailer('Users')->send('approveFashionDesignerNew', [$user]);
                $this->Twillio->sendSms($user->fashion_designer_profile->phone_number, $text);

                $userName = $user->fashion_designer_profile->first_name.' '.$user->fashion_designer_profile->last_name;
                $text2 = "Hi, A new Fashion Designer $userName register in soundchecklive.co with your reference";
                if($user->status == 1) {
                    if(!empty($user->user_supporters)) {
                        foreach ($user->user_supporters as $key => $value) {
                           if(!empty($value->phone_number)) {
                                //pr($value->phone_number);die;
                                $this->Twillio->sendSms('+1'.$value->phone_number, $text2);
                           }
                        }
                    }
                }

                $flag   = true; 
                //$this->getMailer('Users')->send('approveFashionDesigner', [$user]);  
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }

    public function addFilmDirector() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('Filmgenres');
        $user = $this->Users->newEntity();
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->_formatBirthDayFilmDirector($this->request->getData());
            $data['status'] = 1;
            $data['role_id'] = 5;
            $data['created_by'] = 1; //created by admin or SCL original MEMBER
            $data['film_director_profile']['phone_number'] = $data['film_director_profile']['full_phone'];
            $user = $this->Users->patchEntity($user, $data, [
                'assosiated' => [
                   'FilmDirectorProfiles',
                   'FilmDirectorMediaHandles'
                ]
            ]);
            if ($this->Users->save($user)) {
                 $this->Flash->success(__('The film director has been added successfully.'));
                return $this->redirect(['action' => 'approvedFilmDirector']);
            }

            $this->Flash->error(__('The film director profile could not be saved. Please, try again.'));
        }

        $genres = $this->Filmgenres->find('list', ['limit' => 200]);
         $this->set(compact('genres','user'));

    }

    protected function _formatBirthDayFilmDirector($data) {
        if(!empty($data['film_director_profile']['birth_month']['month'])) {
            $data['film_director_profile']['birth_day'] = $data['film_director_profile']['birth_day']['day'];
            $data['film_director_profile']['birth_month']  = $data['film_director_profile']['birth_month']['month'];;
            $data['film_director_profile']['birth_year']  = $data['film_director_profile']['birth_year']['year'];;
        }
        return $data;
    }

    public function addFashionDesigner() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('Styles');
        $user = $this->Users->newEntity();
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->_formatBirthDayFashionDesigner($this->request->getData());
            $data['status'] = 1;
            $data['role_id'] = 4;
            $data['created_by'] = 1; //created by admin or SCL MEMBER
            $data['fashion_designer_profile']['phone_number'] = $data['fashion_designer_profile']['full_phone'];
            $user = $this->Users->patchEntity($user, $data, [
                'assosiated' => [
                   'FashionDesignerProfiles',
                   'FashionDesignerMediaHandles'
                ]
            ]);
            if ($this->Users->save($user)) {
                 $this->Flash->success(__('The fashion Designer has been added successfully.'));
                return $this->redirect(['action' => 'approvedFashionDesigner']);
            }

            $this->Flash->error(__('The film director profile could not be saved. Please, try again.'));
        }

        $genres = $this->Styles->find('list', ['limit' => 200]);
         $this->set(compact('genres','user'));

    }

    protected function _formatBirthDayFashionDesigner($data) {
        if(!empty($data['fashion_designer_profile']['birth_month']['month'])) {
            $data['fashion_designer_profile']['birth_day'] = $data['fashion_designer_profile']['birth_day']['day'];
            $data['fashion_designer_profile']['birth_month']  = $data['fashion_designer_profile']['birth_month']['month'];;
            $data['fashion_designer_profile']['birth_year']  = $data['fashion_designer_profile']['birth_year']['year'];;
        }
        return $data;
    }

    public function editFilmDirector($id = null) {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('Filmgenres');
        $this->loadModel('Levels');
        $user = $this->Users->get($id, [
            'contain' => [ 
                'FilmDirectorProfiles',
                'FilmDirectorMediaHandles',
                'UserSupporters'
            ]
        ]);
        //pr($user);die;
        if ($this->request->is('post') || $this->request->is('put')) {
            $reqData = $this->request->getData();
            $reqData['film_director_profile']['filmgenre_id'] = $reqData['film_director_profile']['genre_id'];
            $reqData = $this->_formatBirthDayFilmDirector($reqData);
            $data= $this->formatSupporters($this->request->getData());
            //pr($data);die;
            $user = $this->Users->patchEntity($user, $reqData, [
                'assosiated' => [
                   'FilmDirectorProfiles',
                   'FilmDirectorMediaHandles'
                ]
            ]);
            if ($this->Users->save($user)) {
                 $this->Flash->success(__('The film director has been updated successfully.'));
                return $this->redirect(['action' => 'editFilmDirector',$id]);
            }

            $this->Flash->error(__('The film director profile could not be saved. Please, try again.'));
        }

        $genres = $this->Filmgenres->find('list', ['limit' => 200]);
        $levels = $this->Levels->find('list', [
            'keyField' => 'id',
            'valueField' => function ($row) {
                                return 'Level '.$row['level_name'] . ' ' .'('. Number::currency($row['price'],'USD').')';
                            }
        ])->toArray();
         $this->set(compact('genres','user','levels'));

    }

    public function formatSupporters($data) {
        if(!empty($data['user_supporters'])) {
            $tagsName = $data['user_supporters'];

            foreach($data['user_supporters'] as $key => $val) {
                if(empty($val['name']) || empty($val['phone_number'])) {
                    unset($data['user_supporters'][$key]);
                } 
            }
        }
        /*if(!empty($tags)) {
            $data['tags'] = $tags;
        }*/
        return $data;
    }

     public function editFashionDesigner($id =null) {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('Styles');
        $this->loadModel('Levels');
        $user = $this->Users->get($id, [
            'contain' => [ 
                'FashionDesignerProfiles',
                'FashionDesignerMediaHandles',
                'UserSupporters'
            ]
        ]);
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->getData();
            $data = $this->_formatBirthDayFashionDesigner($data);
            $data= $this->formatSupporters($data);
            //$data['fashion_designer_profile']['phone_number'] = $data['fashion_designer_profile']['full_phone'];
            $user = $this->Users->patchEntity($user, $data, [
                'assosiated' => [
                   'FashionDesignerProfiles',
                   'FashionDesignerMediaHandles',
                   'UserSupporters'
                ]
            ]);
            if ($this->Users->save($user)) {
                 $this->Flash->success(__('The fashion Designer has been updated successfully.'));
                return $this->redirect(['action' => 'editFashionDesigner',$id]);
            }

            $this->Flash->error(__('The film director profile could not be saved. Please, try again.'));
        }

        $genres = $this->Styles->find('list', ['limit' => 200]);
        $levels = $this->Levels->find('list', [
            'keyField' => 'id',
            'valueField' => function ($row) {
                                return 'Level '.$row['level_name'] . ' ' .'('. Number::currency($row['price'],'USD').')';
                            }
        ])->toArray();
         $this->set(compact('genres','user','levels'));

    }
}


