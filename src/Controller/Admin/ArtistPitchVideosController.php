<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtistPitchVideos Controller
 *
 *
 * @method \App\Model\Entity\ArtistPitchVideo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtistPitchVideosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $artistPitchVideos = $this->paginate($this->ArtistPitchVideos);

        $this->set(compact('artistPitchVideos'));
    }

    /**
     * View method
     *
     * @param string|null $id Artist Pitch Video id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artistPitchVideo = $this->ArtistPitchVideos->get($id, [
            'contain' => []
        ]);

        $this->set('artistPitchVideo', $artistPitchVideo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artistPitchVideo = $this->ArtistPitchVideos->newEntity();
        if ($this->request->is('post')) {
            $artistPitchVideo = $this->ArtistPitchVideos->patchEntity($artistPitchVideo, $this->request->getData());
            if ($this->ArtistPitchVideos->save($artistPitchVideo)) {
                $this->Flash->success(__('The artist pitch video has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artist pitch video could not be saved. Please, try again.'));
        }
        $this->set(compact('artistPitchVideo'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artist Pitch Video id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artistPitchVideo = $this->ArtistPitchVideos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artistPitchVideo = $this->ArtistPitchVideos->patchEntity($artistPitchVideo, $this->request->getData());
            if ($this->ArtistPitchVideos->save($artistPitchVideo)) {
                $this->Flash->success(__('The artist pitch video has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artist pitch video could not be saved. Please, try again.'));
        }
        $this->set(compact('artistPitchVideo'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artist Pitch Video id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artistPitchVideo = $this->ArtistPitchVideos->get($id);
        if ($this->ArtistPitchVideos->delete($artistPitchVideo)) {
            $this->Flash->success(__('The artist pitch video has been deleted.'));
        } else {
            $this->Flash->error(__('The artist pitch video could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * ArtistPitch video
     *
     * @param string|null $id Artist Pitch Video id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function pitchVideo() {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('PitchVideos');
        $findCurrentVideo = $this->PitchVideos->getpitchVideo();
        
        if(empty($findCurrentVideo)) {
            $findCurrentVideo = $this->PitchVideos->newEntity();
        }
        
        if($this->request->is('post')) {
            $data = $this->request->getData();
            $howWeworkVideo = $this->PitchVideos->patchEntity($findCurrentVideo, $data);
            if ($this->PitchVideos->save($howWeworkVideo)) {
                $this->Flash->success(__('The artist pitch video uploaded successfully.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('The video cannot be uploaded. Please, try again.'));
                return $this->redirect($this->referer()); 
            }
           
        }
        $this->set(compact('findCurrentVideo'));
    }

    public function disableLayout($val = null) {
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('SiteDowns');
        $siteDown = $this->SiteDowns->get(1);
        if ($this->request->is(['ajax'])) {
            $siteDown->disable_layout = $val;
            $flag   = false;
            if ($this->SiteDowns->save($siteDown)) {
                $flag   = true; 
            }
        }
        $this->set(compact('flag'));
        $this->set('_serialize', ['flag']);
        $this->set(compact('siteDown'));
    }
}
