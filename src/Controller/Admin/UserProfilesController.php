<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * UserProfiles Controller
 *
 * @property \App\Model\Table\UserProfilesTable $UserProfiles
 *
 * @method \App\Model\Entity\UserProfile[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserProfilesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin/admin');
        $this->loadModel('UserRecords');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'conditions' => ['UserRecords.user_id' => $this->Auth->user('id')],
            'contain' => ['UserRecordKeys'],
            'limit' => 10
        ];
        $userRecords = $this->paginate($this->UserRecords);
        $this->set(compact('userRecords'));
    }

    /**
     * View method
     *
     * @param string|null $id User Profile id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userRecords = $this->UserRecords->get($id, [
            'contain' => ['UserRecordKeys']
        ]);

        $this->set('userRecords', $userRecords);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userProfile = $this->UserRecords->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user('id');
            $data = $this->UserRecords->patchEntity($userProfile, $data,['associated' => ['UserRecordKeys']]);
            if ($this->UserRecords->save($data)) {
                $this->Flash->success(__('The Record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Record could not be saved. Please, try again.'));
            return $this->redirect(['action' => 'add']);
        }
      /*  $users = $this->UserProfiles->Users->find('list', ['limit' => 200]);
        $genres = $this->UserProfiles->Genres->find('list', ['limit' => 200]);*/
        //$this->set(compact('userProfile', 'users', 'genres'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Profile id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userProfile = $this->UserProfiles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userProfile = $this->UserProfiles->patchEntity($userProfile, $this->request->getData());
            if ($this->UserProfiles->save($userProfile)) {
                $this->Flash->success(__('The user profile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user profile could not be saved. Please, try again.'));
        }
        $users = $this->UserProfiles->Users->find('list', ['limit' => 200]);
        $genres = $this->UserProfiles->Genres->find('list', ['limit' => 200]);
        $this->set(compact('userProfile', 'users', 'genres'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Profile id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userProfile = $this->UserProfiles->get($id);
        if ($this->UserProfiles->delete($userProfile)) {
            $this->Flash->success(__('The user profile has been deleted.'));
        } else {
            $this->Flash->error(__('The user profile could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
