<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Carts Controller
 *
 *
 * @method \App\Model\Entity\Cart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CartsController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Cart');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $carts = $this->paginate($this->Carts);

        $this->set(compact('carts'));
    }

    /**
     * View method
     *
     * @param string|null $id Cart id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cart = $this->Carts->get($id, [
            'contain' => []
        ]);

        $this->set('cart', $cart);
    }

    public function cart() {
        $this->viewBuilder()->setLayout('User/login');
        $session = $this->request->getSession();
        $sessionData = $session->read();
        $this->set('cart',$sessionData);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $this->viewBuilder()->setLayout('User/login');
        $this->loadModel('UserMerchandises');
        $this->loadModel('UserMerchandises');
        $getRecords = $this->UserMerchandises->get(base64_decode($id), [
            'contain' => ['UserMerchandiseImages']
        ]);
        $this->set(compact('getRecords'));
        /*$cart = $this->Carts->newEntity();
        if ($this->request->is('post')) {
            $cart = $this->Carts->patchEntity($cart, $this->request->getData());
            if ($this->Carts->save($cart)) {
                $this->Flash->success(__('The cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cart could not be saved. Please, try again.'));
        }
        $this->set(compact('cart'));*/

        if ($this->request->is('post')) {
            $id = base64_decode($id);
            $quantity = 1;
            $product = $this->UserMerchandises->get($id, [
                'contain' => ['UserMerchandiseImages']
            ]);
            if(empty($product)) {
                $this->Flash->error('Invalid request');
            } else {
                $res= $this->Cart->add($id, $quantity, $product);
                $this->Flash->success($product->name . ' has been added to the shopping cart');
                return $this->redirect(['action' => 'cart']);
            }
            return $this->redirect($this->referer());
        } else {
            return $this->redirect(['action' => 'add']);
        }
    }

    public function add111()
    {
        if ($this->request->is('post')) {
            $id = $this->request->data['id'];
            $quantity = 1;
            $productoptionId = isset($this->request->data['productoptionlist']) ? $this->request->data['productoptionlist'] : 0;
            $product = $this->Products->get($id, [
                'contain' => []
            ]);
            if(empty($product)) {
                $this->Flash->error('Invalid request');
            } else {
                $this->Cart->add($id, $quantity, $productoptionId);
                $this->Flash->success($product->name . ' has been added to the shopping cart');
            }
            return $this->redirect($this->referer());
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Cart id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cart = $this->Carts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cart = $this->Carts->patchEntity($cart, $this->request->getData());
            if ($this->Carts->save($cart)) {
                $this->Flash->success(__('The cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cart could not be saved. Please, try again.'));
        }
        $this->set(compact('cart'));
    }

    /**
     * Remove method
     *
     * @param string|null $id Cart id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function removeItem($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $res = $this->Cart->remove($id);
        if($res) {
            $this->Flash->success(__('The cart item has been deleted.'));
                return $this->redirect(['action' => 'cart']);
        } else {
            $this->Flash->error(__('The cart item could not be deleted. Please, try again.'));
             return $this->redirect(['action' => 'cart']);
        }
    }

    public function itemupdate() {
        $updateQuantity = $this->request->getData();
        $session = $this->request->getSession();
        $sessionData = $session->read('cart');
        if ($this->request->is('ajax')) {
            $updateQuantity = $this->request->getData();
            $id = $updateQuantity['id'];
            $quantity = isset($updateQuantity['quantity']) ? $updateQuantity['quantity'] : 1;
            foreach($sessionData as $key => $val) {
                if($key == 'Orderproducts'.$id) {
                    $val['quantity'] = $updateQuantity['quantity'];
                }
            }
            $product = $this->Cart->cartUpdate($id, $quantity, $val);
            $cart = $this->Cart->getcart();
            echo json_encode($cart);
            die;
        }
    }

    public function clearCart() {
        $this->Cart->clear();
    }

    public function checkout() {
        $this->viewBuilder()->setLayout('User/login');
        if($this->request->is('post')) {
            pr($this->request->getData());die;
        }
    }
}
