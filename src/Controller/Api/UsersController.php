<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Controller\Component\RequestHandlerComponent;
use Cake\Network\Exception\MethodNotAllowedException;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;



/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
	public function initialize()
	{
		$this->loadComponent('RequestHandler');
		$this->loadComponent('Auth');
		$this->Auth->allow([
			'checkEmailSignup', 'getStates'
		]);
	}

	/**
	* @method check email for signup
	*
	* @return true for email not exist and false for email exist
	**/
	public function checkEmailSignup() {
		$response = false;
		if ($this->request->is('ajax')) {
			try {
				$this->Users->find()
					->where([
						'email' => $this->request->getQuery('email'),
					])
					->firstOrFail();
			}catch(RecordNotFoundException $e) {
				$response = true;
			}
		}

		$this->set([
			'response'=> $response,
			'_serialize'=> 'response'
		]);
	}

	/*
	* Get states link to countries
	*
	*/
	public function getStates($countryId) {
		$this->request->allowMethod(['ajax']);
		$this->loadModel('States');
        $statesQuery = $this->States
            ->find('list')
            ->where(['country_id' => $countryId])
            ->order(['name'=> 'asc']);

        if ($statesQuery->isEmpty()) {
            throw new NotFoundException(__('States not found.'));
        }
        $this->set([
            'response' => $statesQuery,
            '_serialize' => 'response'
        ]);
	}
}