<?php
namespace App\Controller\Influencer;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Mailer\MailerAwareTrait;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    use MailerAwareTrait;
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('influencer');    
        if ($this->request->is('post')) {
            $session = $this->getRequest()->getSession();
            $data =$this->request->getData();
            $totalIg = ($data['ig_follower'])?$data['ig_follower']:0;
            $totalTwitter = ($data['t_follower'])?$data['t_follower']:0;
            $totalFollower = $totalIg+$totalTwitter;
            $getEquityLevel = $this->getEquity($totalFollower);

            if(!empty($getEquityLevel)) {
                $session->write('level.level_id', $getEquityLevel);
                $session->write('level.data', $data);
                return $this->redirect(['action' => 'success',base64_encode($getEquityLevel)]);
            }
            else {
                $session->write('level.data', $data);
               return $this->redirect(['action' => 'notEligible']);

            }
        }
    }
    

    public function success($id = null)
    {
        $this->viewBuilder()->setLayout('influencer');
        $this->loadModel('EquityLevels');  
        if ($this->request->is('get')) {
            $session = $this->getRequest()->getSession();
            $levelId = $session->read('level.level_id');
            $getLevelId = base64_decode($id);
            if($levelId = $getLevelId) {
                try{
                    $level = $this->EquityLevels->get($getLevelId);
                    $this->set('level', $level);
                }catch(RecordNotFoundException $e){
                    $this->Flash->error('Record not found');
                }
            }else {
                $this->Flash->error(__('Something went wrong. Please, try again.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function notEligible($id = null)
    {
        $this->viewBuilder()->setLayout('influencer');
        $users = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $session = $this->getRequest()->getSession();
            $sessionData = $this->formatBirthDay($session->read('level.data'));
            $data = $this->request->getData();
            $data['role_id'] = 3; // eligible for parter/fan
            $data['partner_profile']['phone_number'] = $data['partner_profile']['full_phone'];
            $data['partner_profile']['ig_user_name'] = $sessionData['ig_user_name'];
            $data['partner_profile']['ig_follower'] = $sessionData['ig_follower'];
            $data['partner_profile']['t_user_name'] = $sessionData['t_user_name'];
            $data['partner_profile']['t_follower'] = $sessionData['t_follower'];
            $data['partner_profile']['birth_month'] = $sessionData['birth_month'];
            $data['partner_profile']['birth_day'] = $sessionData['birth_day'];
            $data['partner_profile']['birth_year'] = $sessionData['birth_year'];
            $influencer = $this->Users->patchEntity($users, $data,['associated' => ['PartnerProfile']]);
            if ($this->Users->save($influencer)) {
                $session->delete('level');
                $this->getMailer('Users')->send('influencerSucess', [$influencer]);
                //$this->Flash->success(__('Your profile has been created successfully. Your account is currently under review. You will be notified via email upon approval'));
                return $this->redirect(['controller' => 'Catalogues','action' => 'index','prefix' => false]);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }    
    }

    public function claim($id = null)
    {
        $this->viewBuilder()->setLayout('influencer');
        $this->loadModel('Users');
        $users = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $session = $this->getRequest()->getSession();
            $sessionData = $this->formatBirthDay($session->read('level.data'));
            $data = $this->request->getData();
            $data['role_id'] = 6;
            $data['influencer_profile']['phone_number'] = $data['influencer_profile']['phone_number'];
            $data['influencer_profile']['equity_level_id'] = $session->read('level.level_id');
            $data['influencer_profile']['ig_user_name'] = $sessionData['ig_user_name'];
            $data['influencer_profile']['ig_follower'] = $sessionData['ig_follower'];
            $data['influencer_profile']['t_user_name'] = $sessionData['t_user_name'];
            $data['influencer_profile']['t_follower'] = $sessionData['t_follower'];
            $data['influencer_profile']['birth_month'] = $sessionData['birth_month'];
            $data['influencer_profile']['birth_day'] = $sessionData['birth_day'];
            $data['influencer_profile']['birth_year'] = $sessionData['birth_year'];
            $totalFollower =  $data['influencer_profile']['ig_follower']+$data['influencer_profile']['t_follower'];

            $influencer = $this->Users->patchEntity($users, $data,['associated' => ['InfluencerProfile']]);
            if ($this->Users->save($influencer)) {
                $session->delete('level');
                $this->getMailer('Users')->send('influencerSucess', [$influencer]);
                //$this->Flash->success(__('Your profile has been created successfully. Your account is currently under review. You will be notified via email upon approval'));
                return $this->redirect(['action' => 'congrats','follwer'=> $totalFollower]);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }  
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Levels', 'UserProfiles', 'UserMediaHandles', 'UserBankDetails', 'UserPlans', 'FashionDesignerProfiles', 'FashionDesignerMediaHandles', 'FilmDirectorProfiles', 'FilmDirectorMediaHandles', 'UserRecords', 'UserEvents', 'UserMerchandises'],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $levels = $this->Users->Levels->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'levels'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $levels = $this->Users->Levels->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'levels'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function getEquity($follwer) {
        if($follwer > 25000 && $follwer <= 49999) {
            $level = 1;
        } elseif ($follwer > 50000 && $follwer <= 249999) {
            $level = 2;
        } elseif ($follwer > 250000 && $follwer <= 999999) {
            $level = 3;
        } elseif ($follwer > 1000000 && $follwer <= 2999999) {
            $level = 4;
        } elseif ($follwer > 3000000 && $follwer <= 4999999) {
            $level = 5;
        } elseif ($follwer > 5000000) {
            $level = 6;
        } else {
            $level = '';
        }
        return $level;
    }

    protected function formatBirthDay ($data) {
        if(!empty($data['birth_day'])) {
            $data['birth_day'] = $data['birth_day']['day'];
            $data['birth_month']  = $data['birth_month']['month'];
            $data['birth_year']  = $data['birth_year']['year'];
        }
        return $data;
    }

    public function congrats() {
        $this->viewBuilder()->setLayout('influencer');
    }

    public function download() {
            $filePath = WWW_ROOT .'img'. DS .'infulencer-image.jpg';
            $this->response->file($filePath ,array('download'=> true, 'name'=> 'infulencer-image.jpg'));
            return $this->response;
    }
}
