<div class="page-title bg-dark py-5 mobile-black-header-setting">
    <div class="container">
        <div class="row">
            <div class="col-12 black-header-setting">
                <div class="play-img">
                    <?= $this->Html->image('play.png') ?>
                </div>
                <h2 class="text-center text-white">
                    Your almost done! Answer the questions to become a SCL member.
                </h2>

                <h5 class="text-center text-white mt-3">Film Director</h5>
            </div>
        </div>
    </div>
</div>
<div class="step-form-wraper">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-3 py-md-45">
                <div class="progress custom-progress ">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 5%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <section class="col-12 step-from">
                <div class="col-sm-12  text-center pt-3 question-section 
                        pb-3 border-bottom mb-5">
                    <div class="question-number">1/14</div>
                    <div class="question-text ">What's your date of birth?</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control custom-input">
                                    <option>Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control custom-input">
                                    <option>Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control custom-input">
                                    <option>Year</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button class=" black-btn custom-margin next">
                        Next
                    </button>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12  text-center pt-3 question-section 
                        pb-3 border-bottom mb-5">
                    <div class="question-number ">2/14</div>
                    <div class="question-text">Select your film genre</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <select class="form-control">
                            <option>Action</option>
                            <option>Adventure</option>
                            <option>Classic</option>
                            <option>Comedy</option>
                            <option>Crime</option>
                            <option>Dance</option>
                            <option>Epics</option>
                            <option>Gangster</option>
                            <option>Historical</option>
                            <option>Horror</option>
                            <option>Musicals</option>
                            <option>Sci-Fi</option>
                            <option>War</option>
                            <option>Westerns</option>
                        </select>
                    </div>

                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">3/20</div>
                    <div class="question-text ">Are you legally affiliated with any studios, production companies or
                        producers? </div>
                </div>
                <div class="col-md-5 m-auto ">
                    <div class="text-center mt-5">
                        <label class="custom-radio-btn">Yes
                            <input type="radio" checked="checked" value="1" name="radio7">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn">No
                            <input type="radio" value="0" name="radio7">
                            <span class="checkmark"></span>
                        </label>

                    </div>
                    <div class="form-group mt-4 ">
                        <input type="text" class="form-control custom-input" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">4/20</div>
                    <div class="question-text ">Have you had or do you currently have a distributor?
                    </div>
                </div>
                <div class="col-md-6 m-auto">

                    <div class="text-center mt-5">

                        <label class="custom-radio-btn">Yes
                            <input type="radio" value="0" name="radio7">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn">No
                            <input type="radio" value="0" name="radio7">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="form-group">

                        <input type="text" class="form-control custom-input">
                    </div>


                </div>


                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">5/20</div>
                    <div class="question-text">Tell your fans about yourself (Biography)</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <textarea class="form-control custom-input" placeholder="400 Characters Maximum"
                            rows="4"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">6/20</div>
                    <div class="question-text">Upload your Artist profile picture.</div>
                </div>
                <div class="col-md-5 m-auto text-center">
                    <img src="/img/no-profile-image.png">
                    <div class="upload-btn-wrapper">
                        <button class="btn">Upload</button>
                        <input type="file" name="myfile">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">7/20</div>
                    <div class="question-text ">What are your social media handles?</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group relative-position">
                        <img src="/img/fb-icon.png" alt="">
                        <label>Facebook</label>
                        <div class="input text"><input type="text" class="form-control custom-input"
                                placeholder="Enter your URL" maxlength="245"></div>
                    </div>
                    <div class="form-group relative-position">
                        <img src="/img/twitter.png" alt="">
                        <label>Twitter</label>
                        <div class="input text"><input type="text" class="form-control custom-input"
                                placeholder="Enter your URL" maxlength="245"></div>
                    </div>
                    <div class="form-group relative-position">
                        <img src="/img/instagram.png" alt="">
                        <label>Instagram</label>
                        <div class="input text"><input type="text" class="form-control custom-input"
                                placeholder="Enter your URL" maxlength="245">
                        </div>


                    </div>
                    <div class="form-group relative-position">
                        <img src="/img/youtube.png" alt="">
                        <label>Youtube</label>
                        <div class="input text"><input type="text" class="form-control custom-input"
                                placeholder="Enter your URL" maxlength="245">
                        </div>


                    </div>
                    <div class="form-group relative-position">
                        <img src="/img/tiktok.png">
                        <label>Add Tik Tok</label>
                        <input type="text" class="form-control custom-input" placeholder="Enter your URL">
                    </div>
                    <div class="form-group relative-position">
                        <img src="/img/twitch.png">
                        <label>Add Twitch</label>
                        <input type="text" class="form-control custom-input" placeholder="Enter your URL">
                    </div>
                    <div class="form-group relative-position">
                        <img src="/img/snapchat.png">
                        <label>Add SnapChat</label>
                        <input type="text" class="form-control custom-input" placeholder="Enter your URL">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">8/20</div>
                    <div class="question-text ">What type of budget are you realistically seeking? </div>
                </div>
                <div class="col-md-5 m-auto">

                    <div class="m-auto capital-goal">
                        <label class="custom-radio-btn d-block">$25,000 - $50,000
                            <input type="radio" checked="checked" value="1" name="radio9">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$50,001 - $100,000
                            <input type="radio" value="0" name="radio9">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$100,001 - $250,000
                            <input type="radio" value="0" name="radio9">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$250,001 - $500,000
                            <input type="radio" value="0" name="radio9">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$500,001 - $1,000,000
                            <input type="radio" value="0" name="radio9">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">9/20</div>
                    <div class="question-text">How will this budget be used to further your career and secure a ROI?
                    </div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <textarea class="form-control custom-input" rows="4"
                            placeholder="400 Characters Maximum"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">10/20</div>
                    <div class="question-text ">Upload your product media, movie trailer or director’s reel.
                    </div>
                </div>
                <div class="col-md-6 m-auto">
                    <label>Cover Art Poster</label>
                    <div class="form-group relative-position">

                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <input type="file" name="cover_image" accept="image/*" required="required">
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="" id="cover-image-target">

                    </div>

                    <label>Trailer or Director’s Movie Reel
                    </label>
                    <div class="form-group relative-position">

                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <input type="file" name="cover_image" accept="image/*" required="required">
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="" id="cover-image-target">

                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">11/20</div>
                    <div class="question-text ">Choose 5 “key words” you’d like to use to market your brand.
                        <!-- <div> <small>each word should be a maximum of 4 to 5 characters </small></div> -->
                    </div>

                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <input type="text" class="form-control custom-input" placeholder="#1">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control custom-input" placeholder="#2">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control custom-input" placeholder="#3">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control custom-input" placeholder="#4">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control custom-input" placeholder="#5">
                    </div>

                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">12/20</div>
                    <div class="question-text ">Now that you are ready upload your video pitch.
                        <br>
                        <small>(60
                            sec max)</small>
                    </div>
                </div>
                <div class="col-md-6 m-auto">
                    <div class="form-group relative-position">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <div class="input file"><input type="file" name="video_pitch_file"></div>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="Upload your video"
                            id="video-pitch-file-target">
                        <div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                next
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">13/20</div>
                </div>
                <div class="col-md-6 m-auto ">
                    <div class="row step-13">
                        <div class=" form-group col-12">
                            <label>Are you male or female?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="optradio1" checked> Male</label>
                                <label class="radio-inline"><input type="radio" name="optradio1"> Female</label>
                            </div>
                        </div>
                        <div class=" form-group col-12">
                            <label>What's your ethnicity?</label>
                            <select class="form-control">
                                <option>White or Caucasian</option>
                                <option> Black or African American</option>
                                <option>American Indian or Alaska Native</option>
                                <option>Latino or Hispanic</option>
                                <option>Asian</option>
                                <option>Pacific Islander or Hawaiian</option>
                                <option>Other</option>


                            </select>
                        </div>
                        <div class=" form-group col-12">
                            <label>Where are you originally from? </label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Country</label>
                                    <select class="form-control">
                                        <option>Select Country</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>State</label>
                                    <select class="form-control">
                                        <option>Select State</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>City</label>
                                    <select class="form-control">
                                        <option>Select City</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class=" form-group col-12">
                            <label>Do you have a booking agent or manager?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="optradio2" checked> Yes</label>
                                <label class="radio-inline"><input type="radio" name="optradio2"> No</label>
                            </div>
                        </div>

                        <div class=" form-group col-12">
                            <label>Have you ever been marketed overseas?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="optradio3" checked> Yes</label>
                                <label class="radio-inline"><input type="radio" name="optradio3"> No</label>
                            </div>
                        </div>
                        <div class=" form-group col-12">
                            <label>Do you have merchandise for sale? </label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="optradio4" checked> Yes</label>
                                <label class="radio-inline"><input type="radio" name="optradio4"> No</label>
                            </div>
                        </div>




                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'Artist/UserProfiles/add',
        'Artist/UserProfiles/step-validate',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>