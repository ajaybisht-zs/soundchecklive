<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserProfile $userProfile
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Profile'), ['action' => 'edit', $userProfile->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Profile'), ['action' => 'delete', $userProfile->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userProfile->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Profiles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Profile'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userProfiles view large-9 medium-8 columns content">
    <h3><?= h($userProfile->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $userProfile->has('user') ? $this->Html->link($userProfile->user->id, ['controller' => 'Users', 'action' => 'view', $userProfile->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Genre') ?></th>
            <td><?= $userProfile->has('genre') ? $this->Html->link($userProfile->genre->name, ['controller' => 'Genres', 'action' => 'view', $userProfile->genre->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($userProfile->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($userProfile->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone Number') ?></th>
            <td><?= h($userProfile->phone_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Zipcode') ?></th>
            <td><?= h($userProfile->zipcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Signed By') ?></th>
            <td><?= h($userProfile->signed_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Performance Rights Organization') ?></th>
            <td><?= h($userProfile->performance_rights_organization) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avatar') ?></th>
            <td><?= h($userProfile->avatar) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avatar Dir') ?></th>
            <td><?= h($userProfile->avatar_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Capital Goals') ?></th>
            <td><?= h($userProfile->capital_goals) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userProfile->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Signed') ?></th>
            <td><?= $this->Number->format($userProfile->is_signed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Performance Rights Affiliated') ?></th>
            <td><?= $this->Number->format($userProfile->is_performance_rights_affiliated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Ready For Release') ?></th>
            <td><?= $this->Number->format($userProfile->is_ready_for_release) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userProfile->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userProfile->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Biography') ?></h4>
        <?= $this->Text->autoParagraph(h($userProfile->biography)); ?>
    </div>
    <div class="row">
        <h4><?= __('Budget Usage') ?></h4>
        <?= $this->Text->autoParagraph(h($userProfile->budget_usage)); ?>
    </div>
</div>
