<div class="container thankupage">
    <div class="gray-strip">Your registration is complete.</div>
<p class="text-center mt-5">Congratulations! A Sound Check Live team member will contact you shortly for on-boarding. </p>
<div class="text-center mt-4 mb-4">
    <?php echo $this->Html->image('check-tick.png', ['alt' => 'tick']); ?>
</div>
    <!-- <p class="text-center mt-5">A Sound Check Live team Member will contact you shortly for on-boarding.</br>
        Your Sound Check live profile has generated a SCL Score of <?= $score ?>.</br>
        This is a provisional score that doesn't factor in talent and will increase over time.
    </p> -->
    <!-- <div class="progress mx-auto mt-5 mb-5" data-value='<?= $score ?>'>
        <span class="progress-left">
                      <span class="progress-bar border-primary"></span>
        </span>
        <span class="progress-right">
                      <span class="progress-bar border-primary"></span>
        </span>
        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
          <div class="h2 font-weight-bold"><?= $score ?><sup class="small"></sup></div>
        </div>
      </div> -->
    <div class="text-center"><a href="/catalog">Click here for catalog</a></div>
</div>


<?php 
    echo $this->Html->script([
        'Artist/UserProfiles/redirect_to_music_page',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>