<div class="col-lg-9">
    <div class="content h-100">
        <div class="bg-white h-100 p-4">
        <div class="row"><?= $this->element('Banner/banner');?></div>
            <div class="row">
                <?= $this->Flash->render(); ?>
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script([
        'Artist/VideoPitch/video_validate',
        'Artist/UserMediaHandles/edit'
    ], [
        'block' => 'scriptBottom'
    ]);
?>
