<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserProfile[]|\Cake\Collection\CollectionInterface $userProfiles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Profile'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userProfiles index large-9 medium-8 columns content">
    <h3><?= __('User Profiles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('genre_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('zipcode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_signed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('signed_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_performance_rights_affiliated') ?></th>
                <th scope="col"><?= $this->Paginator->sort('performance_rights_organization') ?></th>
                <th scope="col"><?= $this->Paginator->sort('avatar') ?></th>
                <th scope="col"><?= $this->Paginator->sort('avatar_dir') ?></th>
                <th scope="col"><?= $this->Paginator->sort('capital_goals') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_ready_for_release') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userProfiles as $userProfile): ?>
            <tr>
                <td><?= $this->Number->format($userProfile->id) ?></td>
                <td><?= $userProfile->has('user') ? $this->Html->link($userProfile->user->id, ['controller' => 'Users', 'action' => 'view', $userProfile->user->id]) : '' ?></td>
                <td><?= $userProfile->has('genre') ? $this->Html->link($userProfile->genre->name, ['controller' => 'Genres', 'action' => 'view', $userProfile->genre->id]) : '' ?></td>
                <td><?= h($userProfile->first_name) ?></td>
                <td><?= h($userProfile->last_name) ?></td>
                <td><?= h($userProfile->phone_number) ?></td>
                <td><?= h($userProfile->zipcode) ?></td>
                <td><?= $this->Number->format($userProfile->is_signed) ?></td>
                <td><?= h($userProfile->signed_by) ?></td>
                <td><?= $this->Number->format($userProfile->is_performance_rights_affiliated) ?></td>
                <td><?= h($userProfile->performance_rights_organization) ?></td>
                <td><?= h($userProfile->avatar) ?></td>
                <td><?= h($userProfile->avatar_dir) ?></td>
                <td><?= h($userProfile->capital_goals) ?></td>
                <td><?= $this->Number->format($userProfile->is_ready_for_release) ?></td>
                <td><?= h($userProfile->created) ?></td>
                <td><?= h($userProfile->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userProfile->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userProfile->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userProfile->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userProfile->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
