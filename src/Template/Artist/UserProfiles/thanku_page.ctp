
<div class="container thankupage">
    <div class="gray-strip">Your registration is complete.</div>

    <p class="text-center mt-5">A Sound Check Live team Member will contact you shortly for on-boarding.</br>
        Your Sound Check live profile has generated a Scl Score of 73.4.</br>
        This is a Provisional score that doesn't factor in talent and will increase over time..
    </p>

    <div class="progress mx-auto mt-5 mb-5" data-value='70'>
        <span class="progress-left">
                      <span class="progress-bar border-primary"></span>
        </span>
        <span class="progress-right">
                      <span class="progress-bar border-danger"></span>
        </span>
        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
          
          <div class="h2 font-weight-bold">73.4<sup class="small">%</sup></div>
        </div>
      </div>
</div>

