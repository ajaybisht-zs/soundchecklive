<div class="page-title bg-dark py-5 mobile-black-header-setting">
    <div class="container">
        <div class="row">
            <div class="col-12 black-header-setting">
                <div class="play-img">
                    <?= $this->Html->image('play.png') ?>
                </div>
                <h2 class="text-center text-white">
                    Your almost done! Answer the questions to become a SCL member.
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="step-form-wraper">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-3 py-md-45">
                <div class="progress custom-progress ">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 5.5%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'post','id' => 'birthDayForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 2 ]) ?>
                <div class="col-sm-12  text-center pt-3 question-section 
                    pb-3 border-bottom mb-5">
                    <div class="question-number">1/8</div>
                    <div class="question-text">What's your date of birth?</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= 
                                    $this->Form->month('birth', [
                                        'class' => 'form-control custom-input',
                                        'empty' => 'Month',
                                        'value' => (!empty($user->user_profile->birth_month))? $user->user_profile->birth_month:'1',
                                        'id' => 'birthMonth'
                                    ]); 
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= 
                                    $this->Form->day('birth', [
                                        'class' => 'form-control custom-input',
                                        'empty' => 'Date',
                                        'value' => (!empty($user->user_profile->birth_day)) ? $user->user_profile->birth_day:'1',
                                        'id' => 'birthDay'
                                    ]); 
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->year('birth', [
                                        'minYear' => 1950,
                                        'maxYear' => date('Y'),
                                        'class' => 'form-control custom-input',
                                        'empty' => 'Year',
                                        'value' => (!empty($user->user_profile->birth_year))? $user->user_profile->birth_year:2000,
                                        'id' => 'birthYear'
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button class="pc-bold black-btn custom-margin" type='submit'>
                        Next
                    </button>
                </div>
                <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <div class="col-sm-12  text-center pt-3 question-section 
                    pb-3 border-bottom mb-5">
                    <div class="question-number">2/8</div>
                    <div class="question-text">Select Your Genre</div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'genreForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 3 ]) ?>
                <div class="col-md-5 m-auto">
                    <div class="form-group d-none">
                        <select class="form-control custom-input">
                            <option>Music</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label class="custom-label sr-only">Select your genre</label>
                        <?= 
                                $this->Form->control('user_profile.genre_id', [
                                    'options' => $genres,
                                    'label' => false,
                                    'class' => 'form-control custom-input',
                                    
                                    'empty' => 'Select'
                                ])
                            ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'>Back</button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>

           <!--  <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">3/14</div>
                    <div class="question-text">Are you currently signed or independent?</div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'signedForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 4 ]) ?>
                <div class="text-center ">
                    <div class="d-inline-block">
                        <label class="custom-radio-btn d-block text-left">I’m an independent Artist
                            <input type="radio"
                                <?= ( $user->user_profile->is_signed == 1 || $user->user_profile->is_signed == '' )? 'checked':null ?>
                                value="1" name="user_profile[is_signed]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block text-left">I’m currently a signed Artist
                            <input type="radio" value="0"
                                <?= ( $user->user_profile->is_signed == 1 || $user->user_profile->is_signed == '')? null:'checked' ?>
                                name="user_profile[is_signed]">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="form-group mt-4 signed-input">
                        <?= 
                                $this->Form->control('user_profile.signed_by', [
                                    'label' => false,
                                    'class' => 'form-control custom-input',
                                    'placeholder' => 'Name of label?'
                                ])
                            ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>
 -->
            <!-- <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">4/14</div>
                    <div class="question-text"><?= __('Tell your fans about yourself (Biography)') ?></div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'biographyForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 5 ]) ?>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                                    $this->Form->control('user_profile.biography', [
                                        'type'  => 'textarea',
                                        'label' => false,
                                        'class' => 'form-control custom-input',
                                        'placeholder' => "500 Characters Maximum",
                                        'rows' => 4,
                                    ])
                                ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section> -->



            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">3/8</div>
                    <div class="question-text">Which Performance Rights Organization (PRO) are you affiliated with?
                    </div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'affilatedForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 4 ]) ?>
                <div class="col-md-6 m-auto">
                    <div class="text-center text-center-mobile mt-5">

                        <label class="custom-radio-btn">None
                            <input type="radio"
                                <?= ($user->user_profile->is_performance_rights_affiliated == 0)? 'checked':null ?>
                                value="0" name="user_profile[is_performance_rights_affiliated]">
                            <span class="checkmark"></span>
                        </label>

                        <label class="custom-radio-btn">I don't know
                            <input type="radio"
                                <?= ($user->user_profile->is_performance_rights_affiliated == 1)? 'checked':null ?>
                                value="1" name="user_profile[is_performance_rights_affiliated]">
                            <span class="checkmark"></span>
                        </label>

                        <label class="custom-radio-btn">Name of Agency
                            <input type="radio"
                                <?= ($user->user_profile->is_performance_rights_affiliated == 2)? 'checked':null ?>
                                value="2" name="user_profile[is_performance_rights_affiliated]">
                            <span class="checkmark"></span>
                        </label>

                    </div>

                    <div class="form-group">
                        <?php 
                            $hide = 'd-none';
                            if($user->user_profile->is_performance_rights_affiliated == 2) {
                                $hide = '';
                            } 
                        ?>
                        <?= 
                                $this->Form->control('user_profile.performance_rights_organization', [
                                    'label' => false,
                                    'class' => 'form-control custom-input '.$hide,
                                    'placeholder'=>'Name of Agency',
                                    'id' => 'agency-name',
                                    'required' => true
                                ])
                            ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>

                <?= $this->Form->end() ?>
            </section>


           <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">4/8</div>
                    <div class="question-text">Upload your Artist profile picture.</div>
                </div>
                <?= $this->Form->create($user,['type' => 'file','id' => 'avatarForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 5 ]) ?>
                <div class="col-md-5 m-auto text-center upcoming-show-img">
                    <?php 
                            if(!empty($user->user_profile) && !empty($user->user_profile->avatar)) {
                                echo $this->Html->image(DS.str_replace('webroot/', '', $user->user_profile->avatar_dir.DS.$user->user_profile->avatar), [
                                        'class' => 'rounded-circle',
                                        'id' => 'avatarImage',
                                        'class' => 'rounded-pill'
                                ]);
                            }else {
                                echo $this->Html->image('no-profile-image.png', [
                                    'id' => 'avatarImage',
                                    'class' => 'rounded-pill'
                                ]);
                            }
                        ?>

                    <div class="upload-btn-wrapper">
                        <button class="btn">Upload</button>
                        <?= $this->Form->control('user_profile.avatar', [
                                    'type' => 'file', 
                                    'label' => false, 
                                    'accept' =>"image/*",
                                    'onchange' => 'updateImageDisplay(this,"avatarImage")'
                                ])
                            ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>

           <!--  <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">7/14</div>
                    <div class="question-text">What are your social media handles?</div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'mediaHandlesForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 8 ]) ?>
                <div class="col-md-5 m-auto">

                    <div class="form-group relative-position">
                        <?= $this->Html->image('instagram.jpg') ?>

                        <label>Instagram</label>
                        <?= 
                                $this->Form->control('user_media_handle.instagram', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your URL'
                                ])
                            ?>
                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('twitter.jpg') ?>

                        <label>Twitter</label>
                        <?= 
                                $this->Form->control('user_media_handle.twitter', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your URL'
                                ])
                            ?>
                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('facebook-f.png') ?>

                        <label>Facebook</label>
                        <?= 
                                $this->Form->control('user_media_handle.facebook', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your URL'
                                ])
                            ?>
                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('youtube.png') ?>

                        <label>Youtube</label>
                        <?= 
                                $this->Form->control('user_media_handle.youtube', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your youtube URL'
                                ])
                            ?>

                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('sanapchat.png') ?>

                        <label>SnapChat</label>
                        <?= 
                                $this->Form->control('user_media_handle.snapchat', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your URL'
                                ])
                            ?>
                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('ticktok.jpg') ?>

                        <label>Tik Tok</label>
                        <?= 
                                $this->Form->control('user_media_handle.tiktok', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your Tik Tok URL'
                                ])
                            ?>
                    </div>
                    <div class="form-group relative-position">
                        <?= $this->Html->image('twitch-icon.png') ?>

                        <label>Twitch</label>
                        <?= 
                                $this->Form->control('user_media_handle.twitch', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your Twitch URL'
                                ])
                            ?>

                    </div>


                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section> -->

           <!--  <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="row">
                        <div class="col-md-6 m-auto">
                            <div class="question-number">8/14</div>
                            <div class="question-text">How much funding are you seeking to launch your career?</div>
                        </div>
                    </div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'capitalGoalsForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 9 ]) ?>
                <div class="col-md-3 m-auto">
                    <div class="text-center sr-only">What type of budget do you need, realistically</div>
                    <div class="capital-goal">
                        <label class="custom-radio-btn d-block ml-1">$25,000 - $50,000
                            <input type="radio" <?= ( $user->user_profile->capital_goals == 0)? 'checked':null ?>
                                value="0" name="user_profile[capital_goals]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block ml-1">$50,001 - $100,000
                            <input type="radio" <?= ( $user->user_profile->capital_goals == 1)? 'checked':null ?>
                                value="1" name="user_profile[capital_goals]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block ml-1">$100,001 - $250,000
                            <input type="radio" <?= ( $user->user_profile->capital_goals == 2)? 'checked':null ?>
                                value="2" name="user_profile[capital_goals]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block ml-1">$250,001 - $500,000
                            <input type="radio" <?= ( $user->user_profile->capital_goals == 3)? 'checked':null ?>
                                value="3" name="user_profile[capital_goals]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block ml-1">$500,001 - $1,000,000
                            <input type="radio" <?= ( $user->user_profile->capital_goals == 4)? 'checked':null ?>
                                value="4" name="user_profile[capital_goals]">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section> -->

            <!-- <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">9/14</div>
                    <div class="question-text">How will this budget be used to further your career
                        and secure a ROI?</div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'budgetHelpsForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 10 ]) ?>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                                $this->Form->control('user_profile.budget_usage', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => '500 Characters Maximum',
                                    'rows' => 5,
                                    'maxlength'=> '500'
                                ])
                            ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section> -->

            <section class="col-12 step-from d-none">
                <div id="overlay" class="d-none">
                    <div class="loader"></div>
                </div>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">5/8</div>
                    <div class="question-text">Upload your best record and fill out the information below.
                    </div>
                    <small>Be sure to choose wisely. This record will be used along with your video pitch for partnering
                        opportunities.</small>
                </div>
                <?= 
                    $this->Form->create($userRecords, [
                        'url'=> [
                            'action' => 'saveRecords'
                        ],
                        'type' => 'file',
                        'id' => 'recordInfoForm']) 
                ?>
                <?= $this->Form->hidden('next', ['value' => 6 ]) ?>
                <?= $this->Form->hidden('id', ['value' => ( !empty($userRecords->id) )? $userRecords->id: null ]) ?>

                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                                    $this->Form->control('record_name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "Record Name"
                                    ])
                                ?>
                    </div>
                    <!--   <div class="form-group"> -->
                    <?php 
                                    /*echo $this->Form->control('artist_name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "Artist Name"
                                    ])*/
                                ?>
                    <!-- </div> -->
                    <div class="form-group">
                        <?= 
                                    $this->Form->control('features', [
                                        'type' => 'text',
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "Features"
                                    ])
                                ?>
                    </div>
                    <div class="form-group">
                        <?= 
                                    $this->Form->control('producer', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "Produced by"
                                    ])
                                ?>
                    </div>
                    <div class="form-group">
                        <?= 
                                    $this->Form->control('writer', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "Written by"
                                    ])
                                ?>
                    </div>

                    <div class="form-group relative-position">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <?= 
                                        $this->Form->control('cover_image', [
                                            'type'  => 'file', 'label' => false,
                                            'accept' =>"image/*",
                                            (empty($userRecords) || empty($userRecords->cover_image) )? 'required': null,
                                            'onchange' => 'updateImageName(this, "#cover-image-target")'
                                        ]) 
                                    ?>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="Upload cover image"
                            id='cover-image-target'>
                        <label id="cover-image-error" class="error" for="cover-image"></label>
                    </div>

                    <?php if(!empty($userRecords) && !empty($userRecords->cover_image)): ?>
                    <div class="form-group">
                        <div class="upcoming-show-img">
                            <?= $this->Html->image('/'.str_replace('webroot/', '', $userRecords->cover_image_dir.DS.$userRecords->cover_image)) ?>
                        </div>
                    </div>
                    <?php endif; ?>


                    <div class="form-group relative-position">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <?= 
                                        $this->Form->control('record_file', [
                                            'type' => 'file', 'label' => false,
                                            //'accept' => 'audio/mp3',
                                            (empty($userRecords) || empty($userRecords->record_file)) ? 'required': null,
                                            'onchange' => 'updateImageName(this, "#record-file-target")'
                                        ]) 
                                    ?>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="Upload record mp3,wav and mp4"
                            id='record-file-target'>
                        <label id="record-file-error" class="error" for="record-file"></label>
                    </div>
                    <?php if(!empty($userRecords) && !empty($userRecords->record_file)): ?>
                    <div class="form-group">
                        <div class="upcoming-show-img">
                            <?php
                                       echo $this->Html->media($userRecords->record_file, [
                                           'fullBase' => true,
                                           'text' => 'Fallback text',
                                           'id' => 'audio',
                                           'width' => '100%',
                                           'pathPrefix' => str_replace('webroot/', '', $userRecords->record_dir) ,
                                           'controls',
                                       ]);
                                   ?>
                        </div>
                    </div>
                    <?php endif; ?>


                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" id="artistFormSubmit">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">6/8</div>
                    <div class="question-text">Choose up to five “key words” you’d like to use to market this record.
                        <div> <small>Each word should be a maximum of 4 to 5 characters </small></div>
                    </div>
                </div>
                <?= 
                    $this->Form->create($userRecords, [
                        'url' => [
                            'action'=> 'saveRecords'
                        ],
                        'type' => 'post',
                        'id' => 'recordKeysForm'
                    ]) 
                ?>
                <?= $this->Form->hidden('next', ['value' => 7 ]) ?>
                <?= $this->Form->hidden('id', ['value' => ( !empty($userRecords->id) )? $userRecords->id:null ]) ?>

                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                                $this->Form->control('user_record_keys..keyword', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "#1",
                                    'maxlength' =>  5,
                                    'value' => (!empty($userRecords->user_record_keys[0])) ? $userRecords->user_record_keys[0]['keyword']:null
                                ])
                            ?>
                    </div>
                    <div class="form-group">
                        <?= 
                                $this->Form->control('user_record_keys..keyword', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "#2",
                                    'maxlength' =>  5,
                                    'value' => (!empty($userRecords->user_record_keys[0])) ? $userRecords->user_record_keys[0]['keyword']:null
                                ])
                            ?>
                    </div>
                    <div class="form-group">
                        <?= 
                                $this->Form->control('user_record_keys..keyword', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "#3",
                                    'maxlength' =>  5,
                                    'value' => (!empty($userRecords->user_record_keys[0])) ? $userRecords->user_record_keys[0]['keyword']:null
                                ])
                            ?>
                    </div>
                    <div class="form-group">
                        <?= 
                                $this->Form->control('user_record_keys..keyword', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "#4",
                                    'maxlength' =>  5,
                                    'value' => (!empty($userRecords->user_record_keys[0])) ? $userRecords->user_record_keys[0]['keyword']:null
                                ])
                            ?>
                    </div>
                    <div class="form-group">
                        <?= 
                                $this->Form->control('user_record_keys..keyword', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "#5",
                                    'maxlength' =>  5,
                                    'value' => (!empty($userRecords->user_record_keys[0])) ? $userRecords->user_record_keys[0]['keyword']:null
                                ])
                            ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button"><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>

           <!--  <section class="col-12 step-from d-none">
                <div id="overlay" class="d-none video-pitch-overlay">
                    <div class="loader"></div>
                </div>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">12/14</div>
                    <div class="question-text">Now that you are ready upload your video pitch.<br><small>(60
                            sec max)</small>
                    </div>
                </div>
                <?= $this->Form->create($userRecords, [
                        'url'=> [
                            'action' => 'saveRecords'
                        ],
                        'type' => 'file',
                        'id' => 'videoPitchForm'
                    ]) 
                ?>
                <?= $this->Form->hidden('next', ['value' => 13 ]) ?>
                <?= $this->Form->hidden('id', ['value' => ( !empty($userRecords->id) )? $userRecords->id:null ]) ?>
                <div class="col-md-5 m-auto">

                    <div class="form-group relative-position">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <?= 
                                    $this->Form->control('video_pitch_file', [
                                        'type' => 'file', 'label' => false,
                                        'accept' => 'video/*',
                                        'onchange' => 'updateImageName(this,"#video-pitch-file-target")',
                                        'required' => true
                                    ]) 
                                ?>

                        </div>
                        <input type="text" class="form-control custom-input" placeholder="Upload your video"
                            id="video-pitch-file-target">
                        <label id="video-pitch-file-error" class="error" for="video-pitch-file"></label>
                        <video controls width="500px" id="vid" style="display:none"></video>
                        <div>

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit' id="videoPitchFromSubmit">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section> -->

            <!-- <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3  border-bottom mb-5">
                    <div class="question-number">13/14</div>

                </div>
                <?=$this->Form->create($user,['type' => 'post','id' => 'PlanForm']) ?>
                <?php 
                    $selectedPlan = (!empty($user) && !empty($user->user_plan))? $user->user_plan->plan_id : null
                ?>
                <?= $this->Form->hidden('next', ['value' => 14 ]) ?>
                <div class="col-md-6 m-auto">
                    <div class="row step-13">
                        <div class=" form-group col-12">
                            <label>Are you male or female?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="user_profile[gender]" checked
                                        value="1"> Male</label>
                                <label class="radio-inline"><input type="radio" name="user_profile[gender]" value="2">
                                    Female</label>
                            </div>
                        </div>
                        <div class=" form-group col-12">
                            <label>What's your ethnicity?</label>
                            <?php 
                                $ethnicity = [
                                    1 => 'White or Caucasian',
                                    2 => 'Black or African American',
                                    3 => 'American Indian or Alaska Native',
                                    4 => 'Latino or Hispanic',
                                    5 => 'Asian',
                                    6 => 'Pacific Islander or Hawaiian',
                                    7 => 'Other',
                                ]

                            ?>
                            <?= 
                                $this->Form->control('user_profile.ethnicity', [
                                    'options' => $ethnicity,
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => 'Select',
                                    'required' => true
                                ])
                            ?>

                        </div>
                        <div class=" form-group col-12">
                            <label>Where are you originally from? </label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Country</label>
                                    <?php
                                        $countries = $this->common->getAllCountry();
                                        $defaultCountry = 0;
                                        echo $this->Form->select('user_profile.country_id', $countries, [
                                            'class' => 'form-control',
                                            'label' => false,
                                            'data-select' => 'countries',
                                            'empty' => __('Please select a country'),
                                            'value' => $defaultCountry,
                                            'required' => true
                                        ]);
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <label>State</label>
                                    <?php
                                    $states = [];
                                    echo $this->Form->select('user_profile.state_id', $states, [
                                        'class' => 'form-control',
                                        'data-select' => 'states',
                                         'required' => true
                                    ]);
                                ?>
                                </div>
                                <div class="col-sm-4">
                                    <label>City</label>
                                    <?php
                                    echo $this->Form->control('user_profile.city', [
                                        'type' => 'text',
                                        'placeholder' => 'City',
                                        'class' => 'form-control',
                                        'label' => false,
                                        'value' => '',
                                        'required' => true
                                    ]);
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class=" form-group col-12">
                            <label>Do you have a booking agent?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="user_profile[is_booking_agent]"
                                        value=1 checked> Yes</label>
                                <label class="radio-inline"><input type="radio" name="user_profile[is_booking_agent]"
                                        value=0> No</label>
                            </div>
                        </div>

                        <div class=" form-group col-12">
                            <label>Have you ever performed overseas?</label>
                            <div>
                                <label class="radio-inline"><input type="radio"
                                        name="user_profile[is_performed_overseas]" checked value=1> Yes</label>
                                <label class="radio-inline"><input type="radio"
                                        name="user_profile[is_performed_overseas]" value=0> No</label>
                            </div>
                        </div>
                        <div class=" form-group col-12">
                            <label>Do you have merchandise for sale? </label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="user_profile[is_merchandise_sale]"
                                        checked value=1> Yes</label>
                                <label class="radio-inline"><input type="radio" name="user_profile[is_merchandise_sale]"
                                        value=0> No</label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section> -->


            <section class="col-12 step-from d-none powered-by-section">
                <div class="col-sm-12 text-center pt-3 question-section pb-3  border-bottom mb-5">
                 
                    <div class="question-number mb-3">7/8</div>
                    <div class="mb-2"><h5><b>Powered by 11</b></h5></div>
                    <div class="question-text">Jump start your career by entering the names and phone numbers of your 10
                        closet supporters and we’ll alert them!
                    </div>
                    <div class="skip mt-2"><a href="#">-Skip-</a></div>

                </div>
             
                 <?=$this->Form->create($user,['type' => 'post','id' => 'PlanForm']) ?>
                    <?php 
                        $selectedPlan = (!empty($user) && !empty($user->user_plan))? $user->user_plan->plan_id : null
                    ?>
                <?= $this->Form->hidden('next', ['value' => 8 ]) ?>
                    <div class="col-md-7 m-auto">
                        <div class="row">
                            <div class="col-sm-6 col-6">
                                <label><b>First Name #1</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.0.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #1</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.0.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #2</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.1.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #2</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.1.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #3</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.2.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #3</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.2.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #4</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.3.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #4</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.3.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #5</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.4.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #5</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.4.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #6</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.5.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #6</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.5.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #7</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.6.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #7</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.6.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #8</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.7.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #8</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.7.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #9</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.8.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #9</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.8.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                            <label><b>First Name #10</b></label>
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.9.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                            <label><b>Phone Number #10</b></label>
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.9.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-6">
                            <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                            </div>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </section>


            <!-- <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3  border-bottom mb-5">
                    <div class="question-number">13/14</div>
                    <div class="question-text">Lastly,select your membership plan
                    </div>
                </div>
                <?=$this->Form->create($user,['type' => 'post','id' => 'PlanForm']) ?>
                    <?php 
                        $selectedPlan = (!empty($user) && !empty($user->user_plan))? $user->user_plan->plan_id : null
                    ?>
                    <?= $this->Form->hidden('next', ['value' => 14 ]) ?>
                        <div class="col-sm-12">
                            <div class="row">
                                <?php 
                                    if(!empty($plans)):
                                        foreach ($plans as $key => $plan):
                                            if($key == 0 && !$selectedPlan){
                                                $selectedPlan = $plan->id;
                                            }
                                ?>
                                            <div class="col-sm-3 text-center">
                                                <div class="plan-box <?= ($plan->id == $selectedPlan)?'active':'bg-white rounded' ?>" data-id=<?= $plan->id ?> >
                                                    <div class="plan-title py-3 mb-2 border-bottom">
                                                        <span class="<?= strtolower(str_replace(' ', '-', $plan->title)) ?> h5">
                                                            <?= $plan->title ?>
                                                        </span>
                                                    </div>
                                                    <div class="py-5">
                                                        <?php if(!empty($plan->price) ): ?>
                                                            <b class="h3 font-weight-bold">$<?= number_format(($plan->price / 100), 2) ?></b>
                                                            <sub class="h6">/Month</sub>
                                                        <?php else: ?>
                                                            <b class="h3 font-weight-bold">Free</b>
                                                        <?php 
                                                            endif; 
                                                        ?>
                                                    </div>
                                                    <ul class="pl-3 plan-box-content list-group list-group-flush ml-0 mb-0">
                                                       <?= $plan->description ?>
                                                    </ul>
                                                </div>
                                            </div>
                                <?php
                                        endforeach;
                                    endif;
                                ?>
                            <?= $this->Form->hidden('user_plan.plan_id', ['value' => $selectedPlan, 'id' => 'selectedPlan' ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                            </div>
                            <div class="col-6">
                                <div class="text-right">
                                   <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                                </div>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
            </section> -->

        </div>
    </div>
</div>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'Artist/UserProfiles/add',
        'Artist/UserProfiles/step-validate',
        'Artist/UserProfiles/country',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>