<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMerchandise $userMerchandise
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Merchandise'), ['action' => 'edit', $userMerchandise->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Merchandise'), ['action' => 'delete', $userMerchandise->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandise->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Merchandise Images'), ['controller' => 'UserMerchandiseImages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Merchandise Image'), ['controller' => 'UserMerchandiseImages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sizes'), ['controller' => 'Sizes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Size'), ['controller' => 'Sizes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Colors'), ['controller' => 'Colors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Color'), ['controller' => 'Colors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userMerchandises view large-9 medium-8 columns content">
    <h3><?= h($userMerchandise->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $userMerchandise->has('user') ? $this->Html->link($userMerchandise->user->id, ['controller' => 'Users', 'action' => 'view', $userMerchandise->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($userMerchandise->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= h($userMerchandise->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userMerchandise->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userMerchandise->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userMerchandise->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $userMerchandise->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($userMerchandise->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sizes') ?></h4>
        <?php if (!empty($userMerchandise->sizes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Size') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userMerchandise->sizes as $sizes): ?>
            <tr>
                <td><?= h($sizes->id) ?></td>
                <td><?= h($sizes->size) ?></td>
                <td><?= h($sizes->created) ?></td>
                <td><?= h($sizes->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Sizes', 'action' => 'view', $sizes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Sizes', 'action' => 'edit', $sizes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sizes', 'action' => 'delete', $sizes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sizes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Colors') ?></h4>
        <?php if (!empty($userMerchandise->colors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Hex Code') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userMerchandise->colors as $colors): ?>
            <tr>
                <td><?= h($colors->id) ?></td>
                <td><?= h($colors->hex_code) ?></td>
                <td><?= h($colors->name) ?></td>
                <td><?= h($colors->created) ?></td>
                <td><?= h($colors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Colors', 'action' => 'view', $colors->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Colors', 'action' => 'edit', $colors->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Colors', 'action' => 'delete', $colors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $colors->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Merchandise Images') ?></h4>
        <?php if (!empty($userMerchandise->user_merchandise_images)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Merchandise Id') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Image Dir') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userMerchandise->user_merchandise_images as $userMerchandiseImages): ?>
            <tr>
                <td><?= h($userMerchandiseImages->id) ?></td>
                <td><?= h($userMerchandiseImages->user_merchandise_id) ?></td>
                <td><?= h($userMerchandiseImages->image) ?></td>
                <td><?= h($userMerchandiseImages->image_dir) ?></td>
                <td><?= h($userMerchandiseImages->created) ?></td>
                <td><?= h($userMerchandiseImages->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserMerchandiseImages', 'action' => 'view', $userMerchandiseImages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserMerchandiseImages', 'action' => 'edit', $userMerchandiseImages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserMerchandiseImages', 'action' => 'delete', $userMerchandiseImages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandiseImages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
