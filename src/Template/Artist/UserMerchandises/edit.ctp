<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMerchandise $userMerchandise
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userMerchandise->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandise->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Merchandise Images'), ['controller' => 'UserMerchandiseImages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Merchandise Image'), ['controller' => 'UserMerchandiseImages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sizes'), ['controller' => 'Sizes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Size'), ['controller' => 'Sizes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Colors'), ['controller' => 'Colors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Color'), ['controller' => 'Colors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userMerchandises form large-9 medium-8 columns content">
    <?= $this->Form->create($userMerchandise) ?>
    <fieldset>
        <legend><?= __('Edit User Merchandise') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('name');
            echo $this->Form->control('price');
            echo $this->Form->control('description');
            echo $this->Form->control('status');
            echo $this->Form->control('sizes._ids', ['options' => $sizes]);
            echo $this->Form->control('colors._ids', ['options' => $colors]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
