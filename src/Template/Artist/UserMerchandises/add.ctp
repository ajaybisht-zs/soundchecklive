<?php use Cake\I18n\Number;?>
<style type="text/css">
    .color-code input {
        visibility: hidden;
        margin-left: 0px;
        width: 30px;
        height: 30px;
        margin-top: 0;
   }
   /*.color-code input + label {
        border: 2px solid transparent;
   }*/
    .color-code input:checked + label {
      position: relative;
    }

    .color-code input:checked + label::after {
        position: absolute;
        left: -4px;
        right: 0px;
        top: -4px;
        bottom: 0;
        content: "";
        display: block;
        border: 2px solid #000;
        width: 38px;
        height: 38px;
        border-radius: 50%;
    }
</style>
<div class="col-lg-9">
    <div class="content h-100">
        <div class="bg-white p-4">
            <div class="row">
                <div class="col-12">
                <div class="row"><?= $this->element('Banner/imagebanner');?></div>
                    <h2 class="tab-title mb-3">Merchandise</h2>
                    <?= $this->Flash->render();?>
                </div>
            </div>
            <div class="row">
                <?php if(!empty($merchandiseImage)) :
                    foreach($merchandiseImage as $image) :
                    $res = $this->common->showThumbNailImage($image->user_merchandise_images);
                ?>
                <div class="col-md-4 col-sm-6 mb-3">
                    <div class="product">
                        <div class="border">
                            <?php if($res) { ?>
                            <?= $this->Html->image($res[0],['alt' => 'merchandise-image', 'class' => 'w-100'])?>
                            <?php } else {?>
                            <?= $this->Html->image('Noimage.png',['alt' => 'merchandise-image', 'class' => 'w-100'])?>
                            <?php } ?>
                        </div>
                        <div class="d-flex justify-content-between mt-2">
                            <div class="product-name"><?= isset($image->name)?ucfirst($image->name):''?></div>
                            <div class="product-price"><?= isset($image->price)?Number::currency($image->price, 'USD'):''?></div>
                        </div>
                    </div>
                </div>
                <?php
                    endforeach; 
                   else : ?>
                    <div class="col-12">
                        <div class="alert alert-info py-3 d-flex align-items-center" role="alert">
                            <h4 class="alert-heading">Alert!</h4>
                            <p class="ml-3">No merchandise found</p>
                        </div>
                    </div>
                <?php
                    endif;
                ?>
            </div>
            <?php 
                if (count($merchandiseImage) < 3 ) :
            ?>
            <div class="row">
                <div class="col-12">
                    <h2 class="tab-title border-bottom pb-2 mt-4 mb-4">Do you have merchandise for sale ?
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php
                        echo $this->Form->create(null, [
                            'url' => [
                                'controller' => 'UserMerchandises',
                                'action' => 'add',
                                'prefix' => 'artist'
                            ],
                            'id' => 'user-merchandise',
                            'class' => 'user',
                            'type' => 'file'
                        ]);
                    ?>
                        <div class="form-group mb-3">
                            <label>Upload Image</label>
                            <?php
                                echo $this->Form->control('user_merchandise_images', [
                                    'type' => 'file',
                                    'label' => false,
                                    'id' => 'filer_input2'
                                ]);
                            ?>
                        </div>
                        <div class="form-group mb-3">
                            <label>Description</label>
                            <?php
                                echo $this->Form->textarea('description', [
                                    'placeholder' => 'description',
                                    'class' => 'form-control form-control-user',
                                    'label' => false,
                                ]);
                            ?>
                        </div>
                        <div class="form-group mb-3">
                            <label>Name of item</label>
                            <?php
                                echo $this->Form->control('name', [
                                    'type' => 'text',
                                    'placeholder' => 'Merchandise Name',
                                    'class' => 'form-control',
                                    'label' => false,
                                ]);
                            ?>
                        </div>
                        <div class="form-group mb-3">
                            <label>Price</label>
                            <?php
                                echo $this->Form->control('price', [
                                    'type' => 'number',
                                    'placeholder' => 'price',
                                    'class' => 'form-control',
                                    'label' => false,
                                ]);
                            ?>
                        </div>
                        <div class="form-group mb-3">
                            <label>Select Size</label>
                            <?php
                                echo $this->Form->select(
                                    'sizes._ids',
                                        $sizes,
                                        [
                                            'multiple' => true,
                                            'class' => 'form-control',
                                            'label' => false,
                                            'id' => 'exampleFormControlSelect1'
                                        ]
                                    );
                            ?>
                        </div>
                        <div class="form-group mb-3">
                            <label>Select Colors</label>
                            <div class="form-row">
                                <?php foreach($colors as $key => $val) :?>
                                    <div class="col-auto color-code">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck<?php echo $key?>" style="background-color:#000000" name="colors[_ids][]"  value="<?php echo $key?>">
                                        <label class="form-check-label rounded-pill" for="exampleCheck<?php echo $key?>" style="background-color:<?php echo $val?>; width: 30px; height: 30px;"></label>
                                    </div> 
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="form-group text-right mt-3">
                            <button class="btn bg-red text-white px-4 py-1" type="submit">Add</button>
                        </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
            <?php else :?>
                <div class="col-12">
                    <div class="alert alert-info py-3 d-flex align-items-center" role="alert">
                        <h4 class="alert-heading">Alert!</h4>
                        <p class="ml-3">You cannot add more than three merchandise</p>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script([
            'frontend/user/user_merchandise_validate',
            'backend/Admin/custom'
            ], [
        'block' => 'scriptBottom'
    ]);
?>