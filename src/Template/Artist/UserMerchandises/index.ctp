<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMerchandise[]|\Cake\Collection\CollectionInterface $userMerchandises
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Merchandise Images'), ['controller' => 'UserMerchandiseImages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Merchandise Image'), ['controller' => 'UserMerchandiseImages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sizes'), ['controller' => 'Sizes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Size'), ['controller' => 'Sizes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Colors'), ['controller' => 'Colors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Color'), ['controller' => 'Colors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userMerchandises index large-9 medium-8 columns content">
    <h3><?= __('User Merchandises') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userMerchandises as $userMerchandise): ?>
            <tr>
                <td><?= $this->Number->format($userMerchandise->id) ?></td>
                <td><?= $userMerchandise->has('user') ? $this->Html->link($userMerchandise->user->id, ['controller' => 'Users', 'action' => 'view', $userMerchandise->user->id]) : '' ?></td>
                <td><?= h($userMerchandise->name) ?></td>
                <td><?= h($userMerchandise->price) ?></td>
                <td><?= h($userMerchandise->created) ?></td>
                <td><?= h($userMerchandise->modified) ?></td>
                <td><?= h($userMerchandise->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userMerchandise->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userMerchandise->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userMerchandise->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandise->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
