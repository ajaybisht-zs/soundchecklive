<div class="page-title bg-dark py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center text-white">
                    <?= $this->Html->image('play.png');?>
                    Reset Password
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="register-form">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12">
                <?= 
                    $this->Form->create(null, [
                        'type' => 'post',
                        'class' => 'form-width mt-5',
                        'id'    => 'resetPasswordForm'
                    ]) 
                ?>
                    <?= $this->Flash->render();?>
                    <div class="row">
                        <div class="col-12 pb-4">
                            <label>New Password</label>
                            <?= 
                                $this->Form->control('password', [
                                    'type' => 'password',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'required',
                                    'minLength' => 8
                                ]) 
                            ?>
                        </div>                   
                    </div>
                     <div class="row">
                        <div class="col-12 pb-4">
                            <label>Confirm Password</label>
                            <?= 
                                $this->Form->control('confirm_password', [
                                    'type' => 'password',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'required',
                                    'minLength' => 8
                                ]) 
                            ?>
                        </div>                   
                    </div>
                 
                    <div class="text-center mt-3 mb-4">
                        <button type='submit' class="bg-dark custom-input d-block w-100 text-uppercase text-white border-0">Reset Password</button>
                    </div>
                  
                <?= $this->Form->end() ?>
                
            </div>
        </div>
    </div>
</div>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'Users/reset',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>
