<div class="col-lg-9">
    <div class="content h-100">
        <div class="bg-white h-100 p-4">
            <div class="row">
                <div class="col-12">
                    <h2 class="tab-title mb-4">Banking Infomation</h2>
                    <?= $this->Flash->render();?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?= $this->Form->create($userBankDetail, ['type' => 'post', 'id' => 'editBankDetailForm']) ?>
                        <div class="form-group">
                            <label>Bank Name</label>
                            <?= 
                                $this->Form->control('user_bank_detail.bank_name', [
                                    'class' => 'form-control custom-input',
                                    'placeholder' => "Bank of America",
                                    'label' => false
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Bank Account Number</label>
                            <?= 
                                $this->Form->control('user_bank_detail.account_number', [
                                    'class' => 'form-control custom-input',
                                    'placeholder' => "252012547895",
                                    'label' => false
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Bank Routing Number</label>
                            <?= 
                                $this->Form->control('user_bank_detail.routing_number', [
                                    'class' => 'form-control custom-input',
                                    'placeholder' => "122105155",
                                    'label' => false
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                                <label>Name on the Account</label>
                                <?= 
                                $this->Form->control('user_bank_detail.name_on_account', [
                                    'class' => 'form-control custom-input',
                                    'placeholder' => "Mike Edward",
                                    'label' => false
                                ]) 
                            ?>
                            </div>
                        <div class="form-group text-right mt-4">
                            <button type="submit" class="bg-red text-white py-2 px-4"
                                name="save">Update</button>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->Html->script([
        'Artist/UserBankDetails/edit'
    ], [
        'block' => 'scriptBottom'
    ]);
?>