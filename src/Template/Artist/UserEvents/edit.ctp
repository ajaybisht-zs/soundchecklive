<div class="col-lg-9">
    <div class="content h-100">
        <div class="bg-white h-100 p-4">
            <div class="row">
                <div class="col-12">
                    <h2 class="tab-title mb-4">Socail Media Handle</h2>
                    <?= $this->Flash->render();?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?= $this->Form->create($userProfile, ['type' => 'post', 'id'=> 'editMediaHandleForm']) ?>
                        <div class="form-group">
                            <label>Facebook</label>
                            <?= 
                                $this->Form->control('user_media_handle.facebook', [
                                    'class' => 'form-control',
                                    'placeholder' => "https://www.facebook.com/mike_edward",
                                    'label' => false
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Twitter</label>
                            <?= 
                                $this->Form->control('user_media_handle.twitter', [
                                    'class' => 'form-control',
                                    'placeholder' => "https://www.twitter.com/mike_edward",
                                    'label' => false
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Instagram</label>
                            <?= 
                                $this->Form->control('user_media_handle.instagram', [
                                    'class' => 'form-control',
                                    'placeholder' => "https://www.instagram.com/mike_edward",
                                    'label' => false
                                ]) 
                            ?>
                        </div>
                        <div class="form-group text-right mt-4">
                            <button type="submit" class="bg-red text-white py-2 px-4"
                                name="save">Save</button>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script([
        'common/jquery.validate',
        'Artist/UserMediaHandles/edit'
    ], [
        'block' => 'scriptBottom'
    ]);
?>