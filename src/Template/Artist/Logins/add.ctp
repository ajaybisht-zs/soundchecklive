<div class="page-title bg-dark py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center text-white">
                    <?= $this->Html->image('play.png');?>
                    Login Now
                </h2>
            </div>
        </div>
    </div>
</div>

<div class="register-form">
    <div class="container">

        <div class="row mb-5">
            <div class="col-12">                
                <?= $this->Form->create(null, ['class' => 'form-width mt-5']) ?>
                    <?= $this->Flash->render();?>
                    <div class="form-group">
                        <label>Email Address</label>
                        <?= 
                            $this->Form->control('email', [
                                'class' => 'form-control custom-input',
                                'label' => false,
                                'required'
                            ]) 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <?= 
                            $this->Form->control('password', [
                                'type' => 'password',
                                'class' => 'form-control custom-input',
                                'label' => false,
                                'required'
                            ]) 
                        ?>
                    </div>

                    <div class="form-group text-right">
                        <a class="tw-text-indigo-700" href="javascript:void(0)" data-toggle="modal" data-target="#forgotPassword">Forgot Password?</a>
                    </div>

                    <button class="bg-dark custom-input d-block w-100 text-uppercase text-white border-0"
                        type="submit">Login
                    </button>
                <?= $this->Form->end() ?>
                <p class="text-center my-4">Don't have an account? 
                <?= $this->Html->link(
                        'Register
                        here',
                        [
                            'controller' => 'Users', 
                            'action' => 'register',
                            'prefix' => 'artist',
                        ],
                        ['class' => 'tw-text-indigo-700']
                    );
                ?>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Forgot Password</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <?= 
            $this->Form->create(null, [
                'type' => 'post',
                'url' => [
                    'controller' => 'ForgotPasswords',
                    'action' => 'add',
                    'prefix' => 'artist'
                ]
            ]) 
        ?>
      <div class="modal-body">
        <div class="form-group">
            <?= 
                $this->Form->control('email', [
                    'type' => 'email',
                    'class' => 'form-control form-control-lg',
                    'label' => false,
                    'Placeholder' => 'Enter your email',
                    'required',
                ]) 
            ?>
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>