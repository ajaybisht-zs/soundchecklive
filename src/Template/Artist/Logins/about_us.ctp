<div class="container bg-white mt-4 mb-4 about-us">
    <h2 class="mb-4">About us</h2>

    <p><span class="text-red">Sound Check Live</span> is an entertainment marketplace for artist and content "rights holders" to pitch fans partnership opportunities in exchange for project financing.</p>
    <p>Artists and content "rights holders" can vend their rights to audiences at proposed price points. <span class="text-red">Sound Check Live</span> serves as a growing resource for artists while providing an open forum for content enthusiast.</p>

    <p>Artists are not lone individuals; they're operating businesses. Fans are not just consumers; they're individual label owners with a spending power that can move projects forward. Every artist needs a budget, and every fan can become a partner. That’s
        the <span class="text-red">Sound Check Live</span> moto.</p>



    <div class="row mt-5">
        <div class="col-sm-7">
            <h4 class="mb-3">How it works?</h4>
            <p>
                Every artist needs a budget; whether it is for recording, marketing or day-to-day living expenses. A single music composition requires studio time, engineering fees, producer licensing fees, mix, mastering, artwork, distribution, visuals and a host of
                promotional contracting.
            </p>

            <p>Case and point most musicians start their business in the red, with more debits than credits. Consequently, they seek lopsided deals because independence isn’t easy.
            </p>

            <p>We assist artist by strategizing campaigns to align them with their core fan base and acquire the financing they need. In the end, the artist maintains ownership of their master recordings and fans receive both content and a great incentive
                to help artists build.
            </p>
        </div>

        <div class="col-sm-5 mt-5">
            <?php
                echo $this->Html->image('slider1.jpg', [
                    'class' => 'w-100'
                ]);
            ?>

        </div>

    </div>

    <div class="row mt-5">
        <div class="col-sm-5 mt-5">
              <?php
                echo $this->Html->image('slider2.jpg', [
                    'class' => 'w-100'
                ]);
            ?>
        </div>

    <div class="col-sm-7">
           <h4 class="mb-3">
            FAN: DOUBLE YOUR MONEY!
        </h4>

        <p>As a fan, you’re going to stream the music anyway. Each stream counts as income for an artist. If you like it, you'll tell a friend or 2. Your “like” has just turned into an endorsement and more revenue for the artist as your friends are likely
            to stream the music as well.
        </p>
        <p>So rather than buy, stream or promote a song, PARTNER UP, making you and your favorite independent artist now business partners.</p>

        <p><span class="text-red">Sound Check Live</span>uses algorithmic technology to calculate how many streams per outlet it would take to double your investment and sets the marker there. Every time a check is cut to the artist, one will be cut to you as well — mailbox money.
        </p>
    </div>
     
    </div>

    <div class="row mt-5">
        <div class="col-sm-7">
               <h4 class="mb-3">
            AVAILABLE ON ALL PLATFORMS = BAD BUSINESS ​

        </h4>

        <p>Understanding your product is vital in every business, no matter the industry. The music business, however, is illusionary with many artists, managers, and executives falling victim to the pressures of being everywhere at all times. Not only does
            this devalue their product, but it also creates a business model that undervalues the entire industry.

        </p>
        <p>Focus your efforts on outlets that pay and market yourself with pride.
        </p>
        </div>
        <div class="col-sm-5">
               <?php
                echo $this->Html->image('slider8.jpg', [
                    'class' => 'w-100'
                ]);
            ?>
        </div>
     


    </div>

    <div class="text-center mt-5 ">
        <h3>How we SUPPORT the artist</h3>
        <p>
            <ul class="italic-style">
                <li> · Develop realistic number focused strategies</li>
                <li> · Create personalized budgets and campaigns</li>
                <li> · Finalize and distribute the product</li>
                <li>· Align outlined goals with specialized partners
                </li>
                <li>· Budget acquisition
                </li>
                <li>· Grow your core fan base</li>
                <li>· Introduction to new audiences</li>
                <li>· Achieve your royalty goal</li>
                <li>· Remain independent</li>
                <li>· Maintain ownership of your masters</li>
                <li>· Introduction to new revenue streams</li>

            </ul>


        </p>
    </div>

    <div class="text-center">
        <h3>How we INCENTIVIZE the fan</h3>
        <p>
            <ul class="italic-style">
                <div>Develop realistic number focused strategies</div>
                <li>· Find the music you like</li>
                <li>· Partner with your chosen artist</li>
                <div>We help every artist market their music to reach their goal
                </div>
                <li> · Artist masters are held in escrow</li>
                <li>· As milestones are reached fans will be notified
                </li>
                <li>· Once the goal has been achieved funds will be dispersed appropriately
                </li>


            </ul>


        </p>
    </div>
</div>