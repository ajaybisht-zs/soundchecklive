<div class="col-lg-9">
    <div class="content h-100">        
        <div class="bg-white h-100 p-4">
            <div class="row">
                <div class="col-12">
                 <div class="row"><?= $this->element('Banner/imagebanner');?></div>            
                    <h2 class="tab-title mb-4">Change Password</h2>
                    <?= $this->Flash->render();?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?= $this->Form->create(null, ['id' => 'ChangePasswordForm']) ?>
                        <div class="form-group">
                            <label>Current password</label>
                            <?= 
                                $this->Form->control('current_password', [
                                    'type' => 'password',
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter current password',
                                    'id' => 'current-password',
                                    'label' => false, 
                                    'required',
                                    'minlength' => 6
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <?= 
                                $this->Form->control('password', [
                                    'type' => 'password',
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter new password',
                                    'id' => 'password',
                                    'label' => false,
                                    'required',
                                    'minlength' => 8
                                ]) 
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Repeat New Password</label>
                            <?= 
                                $this->Form->control('confirm_password', [
                                    'type' => 'password',
                                    'class' => 'form-control',
                                    'placeholder' => 'Re-enter new password',
                                    'id' => 'confirm-password',
                                    'label' => false,
                                    'required',
                                    'minlength' => 6
                                ]) 
                            ?>
                        </div>
                        <div class="form-group text-right mt-4">
                            <button type="submit" class="bg-red text-white py-2 px-4"
                                name="save">Update</button>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->Html->script([
        'Artist/Logins/change-password'
    ], [
        'block' => 'scriptBottom'
    ]);
?>