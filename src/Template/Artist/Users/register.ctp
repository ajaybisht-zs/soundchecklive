<div class="page-title bg-dark py-5 register-header-mobile">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center text-white text-capitalize">
                    <?= $this->Html->image('play.png');?>
                    Register now
                </h2>
            </div>
        </div>
    </div>
</div>

<div class="register-form">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-3 py-md-5">
                <div class="col-sm-12 m-auto">
                    <h2>Sign up and create your budget.</h2>
                </div>

                <div class="form-sub-title bg-gray p-3 mt-3">
                    <p class="mb-0">Generate the budget you need, remain independent and own your masters.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php
                        echo $this->Form->create(null, [
                            'url' => [
                                'controller' => 'Users',
                                'action' => 'register',
                                'prefix' => 'artist'
                            ],
                            'id' => 'userSignup',
                            'class' => 'form-width'
                        ]);
                    ?>
                <div class="form-group">
                    <label>First Name *</label>
                    <?php
                                echo $this->Form->control('user_profile.first_name', [
                                    'type' => 'text',
                                    //'placeholder' => 'First Name *',
                                    'class' => 'form-control custom-input',
                                    'label' => false,
                                ]);
                            ?>
                </div>
                <div class="form-group">
                    <label>Last Name *</label>
                    <?php
                                echo $this->Form->control('user_profile.last_name', [
                                    'type' => 'text',
                                    //'placeholder' => 'Last Name *',
                                    'class' => 'form-control custom-input',
                                    'label' => false,
                                ]);
                            ?>
                </div>
                <div class="form-group">
                    <label>Artist Name *</label>
                    <?php
                                echo $this->Form->control('user_profile.artist_name', [
                                    'type' => 'text',
                                    //'placeholder' => 'Last Name *',
                                    'class' => 'form-control custom-input',
                                    'label' => false,
                                ]);
                            ?>
                </div>

                <div class="form-group">
                    <label>Email*</label>
                    <?php
                                echo $this->Form->control('email', [
                                    'type' => 'email',
                                    'class' => 'form-control custom-input',
                                    //'placeholder' => 'Email Address',
                                    'label' => false,
                                    'autocomplete' => 'off'
                                ]);
                            ?>
                </div>
                <div class="form-group">
                    <label>Password*</label>
                    <?= $this->Form->control('password',
                                [
                                    "class"=>"form-control custom-input",
                                    "label" => false,
                                   // 'placeholder' => __('Enter your password'),
                                    "autocomplete" => "off"
                                ]
                            )
                            ?>
                </div>
                <div class="form-group">
                    <label>Repeat Password*</label>
                    <?php
                                echo $this->Form->control('confirm_password', [
                                    'type' => 'password',
                                    'class' => 'form-control custom-input',
                                    //'placeholder' => 'confirm password',
                                    'label' => false,
                                    'autocomplete' => 'off'
                                ]);
                            ?>
                </div>

                <div class="form-group">
                    <label>Phone</label>
                    <?php
                                echo $this->Form->control('user_profile.phone_number', [
                                    //'type' => 'number',
                                    //'placeholder' => 'Phone *',
                                    'class' => 'form-control custom-input',
                                    'label' => false,
                                    'type' => 'tel',
                                    'id' => 'phone'
                                ]);
                            ?>
                </div>
                <div class="form-group">
                    <label>Zip code</label>
                    <?php
                                echo $this->Form->control('user_profile.zipcode', [
                                    'type' => 'text',
                                    //'placeholder' => 'Zip-code',
                                    'class' => 'form-control custom-input',
                                    'label' => false,
                                ]);
                            ?>
                </div>

                <div class="form-group">
                    <p><input type="checkbox" name="over18" value="0" id="agree" required/>  I am at least 18 years old and agree to the terms of use policy.</p>
                    <label id="over18-error" class="error" for="over18"></label>
                </div>

                <button class="bg-dark custom-input d-block w-100 text-uppercase text-white border-0" type="submit">Sign
                    up</button>
                <?= $this->Form->end(); ?>
                <p class="text-center my-4">Already have an account?
                    <?= $this->Html->link(__('Login Here'), ['controller' => 'Logins', 'action' => 'add', 'prefix' => 'artist']) ?>
                </p>
            </div>
        </div>
    </div>
</div>
<?php

    echo $this->Html->css(
        [
            'intlTelInput_new',
        ]
    );

    echo $this->Html->script([
                                'IntelJs/intlTelInput.min',
                                'frontend/user/validate_registration',
                            ], [
        'block' => 'scriptBottom'
    ]);
?>
