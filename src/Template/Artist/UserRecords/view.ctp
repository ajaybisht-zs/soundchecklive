<div class="col-lg-9">
    <div class="content h-100">
        <div class="bg-white h-100 p-4">
         <div class="row"><?= $this->element('Banner/imagebanner');?></div>
            <div class="row">
                <div class="col-12">
                    <h2 class="tab-title mb-4">My Records</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Record Name') ?> : </label>
                            <div class="col-md-8">
                                <?= h($userRecord->record_name) ?>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Artist Name') ?> : </label>
                            <div class="col-md-8">
                                <?= h($userRecord->artist_name) ?>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Writer') ?> : </label>
                            <div class="col-md-8">
                                <?= h($userRecord->writer) ?>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Producer') ?> : </label>
                            <div class="col-md-8">
                                <?= h($userRecord->producer) ?>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Features') ?> : </label>
                            <div class="col-md-8">
                                <?= h($userRecord->features) ?>
                            </div>
                        </div>

                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Keyword') ?> : </label>
                            <div class="col-md-8">
                                <?php
                                if(!empty($userRecord->user_record_keys)) {
                                    echo '<ul class="list-inline">';
                                    foreach ($userRecord->user_record_keys as $value) {
                                        if(!empty($value->keyword)) {
                                            echo '<li class="list-inline-item"><button class="badge badge-warning rounded-pill px-4 py-2 font-weight-normal">' . $value->keyword . '</button></li>';  
                                        }
                                    }
                                    echo '</ul>';
                                } else {
                                    echo('N/A');
                                }
                            ?>       
                            </div>
                        </div>

                        <div class="form-group form-row">
                            <label class="text-muted col-md-3"><?= __('Play Audio') ?> : </label>
                            <div class="col-md-4" style="height: 200px; width: 200px;">
                                   <?php
                                        if(!empty($userRecord->record_dir) && !empty($userRecord->record_file)) {
                                            $mp3Exists = $this->common->checkMp3($userRecord->record_dir,$userRecord->record_file);
                                            if($mp3Exists) {   
                                                echo $this->Html->media($mp3Exists, [
                                                    'fullBase' => true,
                                                    'text' => 'Fallback text',
                                                    'id' => 'audio',
                                                    'controls',
                                                    'style' => 'position: absolute; bottom: 0; left: 0; right: 0; padding: 0 12px; width: 100%;'
                                                ]);
                                            } else {
                                                echo('Mp3 Not Found');
                                            }
                                        }

                                    if(!empty($userRecord->cover_image_dir) && !empty($userRecord->cover_image)) {
                                        $coverImageExists = $this->common->checkCoverImage($userRecord->cover_image_dir,$userRecord->cover_image);
                                        if($coverImageExists) {
                                            echo $this->Html->image($coverImageExists, [
                                                'style' => 'height: 100%; width: 100%; object-fit: contain; background-color: #eee;'
                                            ]);
                                        } else {
                                            echo('cover Image Not Found');
                                        } 
                                    }
                                ?>
                            </div>
                        </div>

                        <div class="form-group mt-4">
                        <?php //$this->Html->link(__('Back'), $this->request->referer(),['class' => 'bg-red text-white py-2 px-4']) ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
