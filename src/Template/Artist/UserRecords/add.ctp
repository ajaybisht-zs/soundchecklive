<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserRecord $userRecord
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List User Records'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Record Keys'), ['controller' => 'UserRecordKeys', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Record Key'), ['controller' => 'UserRecordKeys', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userRecords form large-9 medium-8 columns content">
    <?= $this->Form->create($userRecord) ?>
    <fieldset>
        <legend><?= __('Add User Record') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('record_name');
            echo $this->Form->control('record_written_by');
            echo $this->Form->control('is_already_distributed');
            echo $this->Form->control('cover_image');
            echo $this->Form->control('cover_image_dir');
            echo $this->Form->control('artist_name');
            echo $this->Form->control('features');
            echo $this->Form->control('producer');
            echo $this->Form->control('writer');
            echo $this->Form->control('record_file');
            echo $this->Form->control('record_dir');
            echo $this->Form->control('video_pitch_file');
            echo $this->Form->control('video_pitch_path');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
