<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtistMusic $artistMusic
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artistMusic->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artistMusic->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Artist Musics'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="artistMusics form large-9 medium-8 columns content">
    <?= $this->Form->create($artistMusic) ?>
    <fieldset>
        <legend><?= __('Edit Artist Music') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('video_name');
            echo $this->Form->control('video_dir');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
