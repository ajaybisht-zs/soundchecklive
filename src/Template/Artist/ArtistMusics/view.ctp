<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtistMusic $artistMusic
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Artist Music'), ['action' => 'edit', $artistMusic->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Artist Music'), ['action' => 'delete', $artistMusic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artistMusic->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Artist Musics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artist Music'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="artistMusics view large-9 medium-8 columns content">
    <h3><?= h($artistMusic->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $artistMusic->has('user') ? $this->Html->link($artistMusic->user->id, ['controller' => 'Users', 'action' => 'view', $artistMusic->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Video Name') ?></th>
            <td><?= h($artistMusic->video_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Video Dir') ?></th>
            <td><?= h($artistMusic->video_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($artistMusic->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($artistMusic->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($artistMusic->modified) ?></td>
        </tr>
    </table>
</div>
