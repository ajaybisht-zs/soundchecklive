<div class="col-lg-9">
    <div class="content h-100">
        <div class="bg-white p-4">
            <div class="row">
                <div class="col-12">
                <div class="row"><?php //echo $this->element('Banner/imagebanner');?></div>
                    <h2 class="tab-title mb-3">Add Music</h2>
                    <?= $this->Flash->render();?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 class="tab-title border-bottom pb-2 mt-4 mb-4">Upload Music
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php
                        echo $this->Form->create(null, [
                            'url' => [
                                'controller' => 'ArtistMusics',
                                'action' => 'add',
                                'prefix' => 'artist'
                            ],
                            'id' => 'artist-music',
                            'class' => 'user',
                            'type' => 'file'
                        ]);
                    ?>
                        <div class="form-group mb-3">
                            <label>Upload Video</label>
                            <?php
                                echo $this->Form->control('video_name', [
                                    'type' => 'file',
                                    'label' => false,
                                    'id' => 'filer_input2'
                                ]);
                            ?>
                        </div>
                        <div class="form-group text-right mt-3">
                            <button class="btn bg-red text-white px-4 py-1" type="submit">Upload</button>
                        </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>

            <div class="col-12">
                <div class="alert alert-info py-3 d-flex align-items-center" role="alert">
                    <h4 class="alert-heading">Alert!</h4>
                    <p class="ml-3">You cannot add more than one music video</p>
                </div>
            </div>
            
            <div class="row">
                <?php if(!empty($artistVideo)) :
                    
                ?>
                <div class="col-12 mb-3">
                    <?php
                        $video = $this->Images->getHowWeWorkVideo($artistVideo);
                        if($video) {
                            echo $this->Html->media($video, [
                                'fullBase' => true,
                                'text' => 'Fallback text',
                                'controls',
                                'width' => '100%'
                            ]);
                        }

                    ?>
                </div>
                <?php
                   else : ?>
                    <div class="col-12">
                        <div class="alert alert-info py-3 d-flex align-items-center" role="alert">
                            <h4 class="alert-heading">Alert!</h4>
                            <p class="ml-3">No video uploaded yet</p>
                        </div>
                    </div>
                <?php
                    endif;
                ?>
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script([
            'frontend/user/validate_add_music',
            ], [
        'block' => 'scriptBottom'
    ]);
?>