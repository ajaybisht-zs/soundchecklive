<div class="page-title bg-dark py-5 mobile-black-header-setting">
    <div class="container">
        <div class="row">
            <div class="col-12 black-header-setting">
                <div class="play-img">
                    <?= $this->Html->image('play.png') ?>
                </div>
                <h2 class="text-center text-white">
                    Your almost done! Answer the questions to become a SCL member.
                </h2>

                <h5 class="text-center text-white mt-3">Film Director</h5>
            </div>
        </div>
    </div>
</div>
<div class="step-form-wraper">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-3 py-md-45">
                <div class="progress custom-progress ">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 5%" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

           <section class="col-12 step-from">
                <?= $this->Form->create($user,['type' => 'post','id' => 'birthDayForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 2 ]) ?>
                <div class="col-sm-12  text-center pt-3 question-section 
                        pb-3 border-bottom mb-5">
                    <div class="question-number">1/13</div>
                    <div class="question-text ">What's your date of birth?</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= 
                                    $this->Form->month('film_director_profile.birth', [
                                        'class' => 'form-control custom-input',
                                        'empty' => 'Month',
                                        'value' => (!empty($user->film_director_profile->birth_month))? $user->film_director_profile->birth_month:'1',
                                        'id' => 'birthMonth'
                                    ]); 
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= 
                                    $this->Form->day('film_director_profile.birth', [
                                        'class' => 'form-control custom-input',
                                        'empty' => 'Date',
                                        'value' => (!empty($user->film_director_profile->birth_day)) ? $user->film_director_profile->birth_day:'1',
                                        'id' => 'birthDay'
                                    ]); 
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                 <?php
                                    echo $this->Form->year('film_director_profile.birth', [
                                        'minYear' => 1950,
                                        'maxYear' => date('Y'),
                                        'class' => 'form-control custom-input',
                                        'empty' => 'Year',
                                        'value' => (!empty($user->film_director_profile->birth_year))? $user->film_director_profile->birth_year:2000,
                                        'id' => 'birthYear'
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button class=" black-btn custom-margin next">
                        Next
                    </button>
                </div>
            <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'post','id' => 'genreForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 3 ]) ?>
                <div class="col-sm-12  text-center pt-3 question-section 
                        pb-3 border-bottom mb-5">
                    <div class="question-number ">2/13</div>
                    <div class="question-text">Select your film genre</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                            $this->Form->control('film_director_profile.filmgenre_id', [
                                'options' => $genre,
                                'label' => false,
                                'class' => 'form-control custom-input',
                            ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type="submit">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
             <?= $this->Form->end() ?>
            </section>

           
            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'post','id' => 'genreForm1']) ?>
                <?= $this->Form->hidden('next', ['value' => 4 ]) ?>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">3/13</div>
                    <div class="question-text ">Are you legally affiliated with any studios, production companies or
                        producers? </div>
                </div>
                <div class="col-md-5 m-auto ">
                    <div class="text-center mt-5">
                        <label class="custom-radio-btn">Yes
                            <input type="radio" checked="checked" value="1" name="film_director_profile[are_you_affiliated_any]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn">No
                            <input type="radio" value="0" name="film_director_profile[are_you_affiliated_any]">
                            <span class="checkmark"></span>
                        </label>

                    </div>
                    <div class="form-group mt-4 ">
                        <input type="text" class="form-control custom-input required" placeholder="Name" name="film_director_profile[affiliated_with_name]" id="affiliated-name" value="<?= (!empty($user->film_director_profile->affiliated_with_name))? $user->film_director_profile->affiliated_with_name:''?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type="submit">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
            <?= $this->Form->create($user,['type' => 'post','id' => 'distributorForm1']) ?>
            <?= $this->Form->hidden('next', ['value' => 5 ]) ?>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">4/13</div>
                    <div class="question-text ">Have you had or do you currently have a distributor?
                    </div>
                </div>
                <div class="col-md-6 m-auto">
                    <div class="text-center mt-5">
                        <label class="custom-radio-btn">Yes
                            <input type="radio" checked="checked" value="1" name="film_director_profile[have_distributor]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn">No
                             <input type="radio"  value="0" name="film_director_profile[have_distributor]">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control custom-input" placeholder="Name" name="film_director_profile[distributor_name]" id="distributor-name" value="<?= (!empty($user->film_director_profile->distributor_name))? $user->film_director_profile->distributor_name:''?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type="submit">
                                Next
                            </button>
                        </div>
                    </div>
                </div>

            <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'post','id' => 'bioForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 6 ]) ?>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">5/13</div>
                    <div class="question-text">Tell your fans about yourself (Biography)</div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                            $this->Form->control('film_director_profile.biography', [
                                'type'  => 'textarea',
                                'label' => false,
                                'class' => 'form-control custom-input',
                                'placeholder' => "500 Characters Maximum",
                                'rows' => 4,
                                'maxlength'=> '500',
                                'required' => true
                            ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type="submit">
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">6/13</div>
                    <div class="question-text">Upload your profile picture.</div>
                </div>
                <?= $this->Form->create($user,['type' => 'file','id' => 'avatarForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 7 ]) ?>
                <div class="col-md-5 m-auto text-center upcoming-show-img">
                    <?php 
                        if(!empty($user->film_director_profile) && !empty($user->film_director_profile->avatar)) {
                            echo $this->Html->image(DS.str_replace('webroot/', '', $user->film_director_profile->avatar_dir.DS.$user->film_director_profile->avatar), [
                                    'class' => 'rounded-circle',
                                    'style' => 'width:80px; height:80px',
                                    'id' => 'avatarImage',
                                   // 'class' => 'rounded-pill'
                            ]);
                        }else {
                            echo $this->Html->image('no-profile-image.png', [
                                'id' => 'avatarImage',
                                'class' => 'rounded-pill'
                            ]);
                        }
                    ?>

                    <div class="upload-btn-wrapper">
                        <button class="btn">Upload</button>
                        <?= $this->Form->control('film_director_profile.avatar', [
                                    'type' => 'file', 
                                    'label' => false, 
                                    'accept' =>"image/*",
                                    'onchange' => 'updateImageDisplay(this,"avatarImage")'
                                ])
                            ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
             <?= $this->Form->end() ?>
            </section>

           <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">7/13</div>
                    <div class="question-text ">What are your social media handles?</div>
                </div>
                <?= $this->Form->create($user,['type' => 'post','id' => 'mediaHandlesForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 8 ]) ?>

                <div class="col-md-5 m-auto">
                    <div class="form-group relative-position">
                        <?= $this->Html->image('instagram.jpg') ?>

                        <label>Instagram</label>
                        <?= 
                            $this->Form->control('film_director_media_handle.instagram', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => 'Enter your URL',
                                'value' => (!empty($user->film_director_media_handle->instagram))? $user->film_director_media_handle->instagram:'',
                            ])
                        ?>                       
                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('twitter.jpg') ?>

                        <label>Twitter</label>
                        <?= 
                            $this->Form->control('film_director_media_handle.twitter', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => 'Enter your URL',
                                'value' => (!empty($user->film_director_media_handle->twitter))? $user->film_director_media_handle->twitter:'',
                            ])
                        ?>
                    </div>

                    <div class="form-group relative-position">
                         <?= $this->Html->image('facebook-f.png') ?>
                        <label>Facebook</label>
                        <?= 
                            $this->Form->control('film_director_media_handle.facebook', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => 'Enter your URL',
                                'value' => (!empty($user->film_director_media_handle->facebook))? $user->film_director_media_handle->facebook:'',
                            ])
                        ?>                   
                    </div>

                     <div class="form-group relative-position">
                        <?= $this->Html->image('youtube.png') ?>

                        <label>Youtube</label>
                        <?= 
                            $this->Form->control('film_director_media_handle.youtube', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => 'Enter your URL',
                                'value' => (!empty($user->film_director_media_handle->youtube))? $user->film_director_media_handle->youtube:'',
                            ])
                        ?>                       
                    </div>

                    <div class="form-group relative-position">
                        <?= $this->Html->image('sanapchat.png') ?>

                        <label>Add SnapChat</label>
                        <?= 
                            $this->Form->control('film_director_media_handle.snapchat', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => 'Enter your URL',
                                'value' => (!empty($user->film_director_media_handle->snapchat))? $user->film_director_media_handle->snapchat:'',
                            ])
                        ?>                       
                    </div>  

                    <div class="form-group relative-position">
                        <?= $this->Html->image('ticktok.jpg') ?>

                        <label>Add Tik Tok</label>
                        <?= 
                            $this->Form->control('film_director_media_handle.tiktok', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => 'Enter your Tik Tok URL',
                                'value' => (!empty($user->film_director_media_handle->tiktok))? $user->film_director_media_handle->tiktok:'',
                            ])
                        ?>
                    </div>

                        
                        <div class="form-group relative-position">
                            <?= $this->Html->image('twitch-icon.png') ?>

                            <label>Add Twitch</label>
                            <?= 
                                $this->Form->control('film_director_media_handle.twitch', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => 'Enter your Twitch URL',
                                    'value' => (!empty($user->film_director_media_handle->twitch))? $user->film_director_media_handle->twitch:'',
                                ])
                            ?>
                        
                        </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                 <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'post','id' => 'capitalGoalsForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 9 ]) ?>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number">8/13</div>
                    <div class="question-text ">How much funding are you seeking to launch your career? </div>
                </div>
                <div class="col-md-5 m-auto">

                    <div class="m-auto capital-goal">
                        <label class="custom-radio-btn d-block">$25,000 - $50,000
                            <input type="radio" <?= ( $user->film_director_profile->what_type_budget_seeking == 1)? 'checked':null ?>  value="1" name="film_director_profile[what_type_budget_seeking]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$50,001 - $100,000
                            <input type="radio" <?= ( $user->film_director_profile->what_type_budget_seeking == 2)? 'checked':null ?> value="2" name="film_director_profile[what_type_budget_seeking]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$100,001 - $250,000
                            <input type="radio" <?= ( $user->film_director_profile->what_type_budget_seeking == 3)? 'checked':null ?> value="3" name="film_director_profile[what_type_budget_seeking]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$250,001 - $500,000
                            <input type="radio" <?= ( $user->film_director_profile->what_type_budget_seeking == 4)? 'checked':null ?> value="4" name="film_director_profile[what_type_budget_seeking]">
                            <span class="checkmark"></span>
                        </label>
                        <label class="custom-radio-btn d-block">$500,001 - $1,000,000
                            <input type="radio" <?= ( $user->film_director_profile->what_type_budget_seeking == 5)? 'checked':null ?> value="5" name="film_director_profile[what_type_budget_seeking]">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'post','id' => 'budgetHelpsForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 10 ]) ?>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">9/13</div>
                    <div class="question-text">How will this budget be used to further your career and secure a ROI?
                    </div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                            $this->Form->control('film_director_profile.buget_used_for_roi', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => '500 Characters Maximum',
                                'rows' => 5,
                                'maxlength'=> '500'
                            ])
                        ?>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                 <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'file','id' => 'uploadProductForm']) ?>
                 <?= $this->Form->hidden('next', ['value' => 11 ]) ?> 
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">10/13</div>
                    <div class="question-text ">Upload your product media, movie trailer or director’s reel.
                    </div>
                </div>
                <div class="col-md-6 m-auto">
                    <label>Upload your movie cover art poster</label>
                    <div class="form-group relative-position">

                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <?= 
                                $this->Form->control('film_director_profile.poster_pic', [
                                    'type'  => 'file', 'label' => false,
                                    'accept' =>"image/*",
                                   (empty($user->film_director_profile->poster_pic))? 'required': null,
                                    'onchange' => 'updateImageName(this, "#cover-image-target5")'
                                ]) 
                            ?>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="" id="cover-image-target5">
                         <label id="film-director-profile-poster-pic-error" class="error" for="film-director-profile-poster-pic"></label>

                    </div>

                    <label>Upload your movie trailer or Director’s reel
                    </label>
                    <div class="form-group relative-position">

                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                             <?= 
                                $this->Form->control('film_director_profile.movie_reel', [
                                    'type' => 'file', 'label' => false,
                                    //'accept' => 'mp4',
                                    (empty($user->film_director_profile->movie_reel)) ? 'required': null,
                                    'onchange' => 'updateImageName(this, "#record-file-target12")'
                                ]) 
                            ?>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="" id="record-file-target12">

                        <label id="film-director-profile-movie-reel-error" class="error" for="film-director-profile-movie-reel"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>


            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'file','id' => 'keywordForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 12 ]) ?> 
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">11/13</div>
                    <div class="question-text ">Choose up to 5 “key words” you’d like to use to market your production.
                    </div>
                </div>
                <div class="col-md-5 m-auto">
                    <div class="form-group">
                        <?= 
                            $this->Form->control('film_director_profile.keyword_1', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => '#1',
                            ]);
                        ?>
                    </div>
                    <div class="form-group">
                      <?= 
                            $this->Form->control('film_director_profile.keyword_2', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => '#2',
                            ]);
                        ?>
                    </div>
                    <div class="form-group">
                        <?= 
                            $this->Form->control('film_director_profile.keyword_3', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => '#3',
                            ]);
                        ?>
                    </div>
                    <div class="form-group">
                       <?= 
                            $this->Form->control('film_director_profile.keyword_4', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => '#4',
                            ]);
                        ?>
                    </div>
                    <div class="form-group">
                        <?= 
                            $this->Form->control('film_director_profile.keyword_5', [
                                'class' => 'form-control custom-input', 
                                'label' => false,
                                'placeholder' => '#5',
                            ]);
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
            <?= $this->Form->end() ?>
            </section>


            <section class="col-12 step-from d-none">
                <?= $this->Form->create($user,['type' => 'file','id' => 'pitchVideoForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 13]) ?>
                <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
                    <div class="question-number ">12/13</div>
                    <div class="question-text ">Now that you are ready upload your video pitch.
                        <br>
                        <small>(60
                            sec max)</small>
                    </div>
                </div>
                <div class="col-md-6 m-auto">
                    <div class="form-group relative-position">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <?= 
                                $this->Form->control('film_director_profile.video_pitch_image', [
                                    'type'  => 'file', 'label' => false,
                                    'accept' =>"image/*",
                                    //(empty($userRecords) || empty($userRecords->cover_image) )? 'required': null,
                                    'onchange' => 'updateImageName(this, "#cover-image-target2")',
                                    'required' => true
                                ]) 
                            ?>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="Upload cover image" id='cover-image-target2'>
                       <label id="film-director-profile-video-pitch-image-error" class="error" for="film-director-profile-video-pitch-image"></label>
                    </div>  
                    <div class="form-group relative-position">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <?= 
                                $this->Form->control('film_director_profile.pitch_video', [
                                    'type' => 'file', 'label' => false,
                                    //'accept' => 'audio/mp4',
                                    (empty($user->film_director_profile->pitch_video)) ? 'required': null,
                                    'onchange' => 'updateImageName(this, "#video-pitch-file-target69")'
                                ]) 
                            ?>
                        </div>
                        <input type="text" class="form-control custom-input" placeholder="Upload your video"
                            id="video-pitch-file-target69">
                        <label id="film-director-profile-pitch-video-error" class="error" for="film-director-profile-pitch-video"></label>
                        <div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="pc-bold back-btn custom-margin previous" type="button">
                            Back
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <button class="pc-bold black-btn custom-margin next" type='submit'>
                                Next
                            </button>
                        </div>
                    </div>
                </div>
             <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3  border-bottom mb-5">
                    <div class="question-number">13/13</div>
                </div>
                <?=$this->Form->create($user,['type' => 'post','id' => 'PlanForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 14 ]) ?>
                <div class="col-md-6 m-auto">
                    <div class="row step-13">
                        <div class=" form-group col-12">
                            <label>Are you male or female?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[gender]" checked value="1" > Male</label>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[gender]" value="2"> Female</label>
                            </div>
                        </div>
                        <div class=" form-group col-12">
                            <label>What's your ethnicity?</label>
                            <?php 
                                $ethnicity = [
                                    1 => 'White or Caucasian',
                                    2 => 'Black or African American',
                                    3 => 'American Indian or Alaska Native',
                                    4 => 'Latino or Hispanic',
                                    5 => 'Asian',
                                    6 => 'Pacific Islander or Hawaiian',
                                    7 => 'Other',
                                ]

                            ?>
                            <?= 
                                $this->Form->control('film_director_profile.ethnicity', [
                                    'options' => $ethnicity,
                                    'label' => false,
                                    'class' => 'form-control',
                                    'empty' => 'Select',
                                    'required' => true
                                ])
                            ?>
                            
                        </div>
                        <div class=" form-group col-12">
                            <label>Where are you originally from? </label>
                           <div class="row">
                               <div class="col-sm-4">
                                   <label>Country</label>
                                    <?php
                                        $countries = $this->common->getAllCountry();
                                        $defaultCountry = 0;
                                        echo $this->Form->select('film_director_profile.country_id', $countries, [
                                            'class' => 'form-control',
                                            'label' => false,
                                            'data-select' => 'countries',
                                            'empty' => __('Please select a country'),
                                            'value' => $defaultCountry,
                                            'required' => true
                                        ]);
                                    ?>
                               </div>
                               <div class="col-sm-4">
                                <label>State</label>
                                <?php
                                    $states = [];
                                    /*if (isset($sessionProfile['business_profile']['country_id'])):
                                        $countryId = $sessionProfile['user_profile']['country_id'];
                                        $states = $this->state->findByCountryId($countryId);
                                    endif;*/
                                    echo $this->Form->select('film_director_profile.state_id', $states, [
                                        'class' => 'form-control',
                                        'data-select' => 'states',
                                         'required' => true
                                        //'value' => (!empty($sessionProfile))?$sessionProfile['user_profile']['state_id']:0,
                                    ]);
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <label>City</label>
                                <?php
                                    echo $this->Form->control('film_director_profile.city', [
                                        'type' => 'text',
                                        'placeholder' => 'City',
                                        'class' => 'form-control',
                                        'label' => false,
                                        'value' => '',
                                        'required' => true
                                    ]);
                                ?>
                            </div>
                           </div>
                        </div>

                        <div class=" form-group col-12">
                            <label>Do you have a booking agent or manager?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[is_booking_manager]" value=1 checked> Yes</label>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[is_booking_manager]" value=0> No</label>
                            </div>
                        </div>

                        <div class=" form-group col-12">
                            <label>Have you ever been marketed overseas?</label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[is_marketed_overseas]" checked value=1> Yes</label>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[is_marketed_overseas]" value= 0> No</label>
                            </div>
                        </div>
                        <div class=" form-group col-12">
                            <label>Do you have merchandise for sale? </label>
                            <div>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[is_merchandise_sale]" checked value=1> Yes</label>
                                <label class="radio-inline"><input type="radio" name="film_director_profile[is_merchandise_sale]" value=0> No</label>
                            </div>
                        </div>

            </div>
                </div>
                <div class="row">
                        <div class="col-6">
                            <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                            </div>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </section>

            <section class="col-12 step-from d-none">
                <div class="col-sm-12 text-center pt-3 question-section pb-3  border-bottom mb-5">
                 
                    <div class="question-number mb-3">14/14</div>
                    <div class="mb-2"><h5><b>Powered by 11</b></h5></div>
                    <div class="question-text">Jump start your career by entering the names and phone numbers of your 10
                        closet supporters and we’ll alert them!
                    </div>

                </div>
                 <?=$this->Form->create($user,['type' => 'post','id' => 'supporterForm']) ?>
                <?= $this->Form->hidden('next', ['value' => 15 ]) ?>
                    <div class="col-md-7 m-auto">
                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <h6><b>First Name</b></h6>
                            </div>
                            <div class="col-sm-6">
                                <h6><b>Phone Number</b></h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.0.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.0.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.1.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.1.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.2.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.2.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.3.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.3.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.4.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.4.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.5.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.5.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.6.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.6.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.7.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.7.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.8.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.8.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input text">
                                        <?= 
                                            $this->Form->control('user_supporters.9.name', [
                                                'class' => 'form-control custom-input', 
                                                'label' => false,
                                                'placeholder' => "name"
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= 
                                        $this->Form->control('user_supporters.9.phone_number', [
                                            'class' => 'form-control custom-input', 
                                            'label' => false,
                                            'placeholder' => "phone number"
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-6">
                            <button class="pc-bold back-btn custom-margin previous" type='button'><?= __('Back') ?></button>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <button class="pc-bold black-btn custom-margin" type='submit'>Next</button>
                            </div>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </section>

        </div>
    </div>
</div>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'FilmDirector/add',
        'FilmDirector/step-validate',
        'Artist/UserProfiles/country',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>