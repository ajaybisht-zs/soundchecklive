<div class="container mt-5 mb-5">
    <?php
        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'Users',
                'action' => 'index',
                'prefix' => 'influencer'
            ],
            'id' => 'influencer',
        ]);
    ?>
    <div class="inner-wraper-influencer col-sm-9 m-auto mt-4 p-4 pb-5">
        <div class="col-sm-12 text-center">
            <a href="<?= $this->Url->build(['controller' => 'homes', 'action' => 'index','prefix' => false]) ?>">
                <?= $this->Html->image('small-logo-png.png', ['alt' => 'logo','class' => 'mobile-logo']);?>  
            </a>
        </div>


        <div class="col-sm-12 mb-4">
            <h2>Become a SCL Influencer</h2>
        </div>
        <div class="row px-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('ig_user_name', [
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => 'Instagram username'

                        ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('ig_follower', [
                            'type' => 'number',
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => 'How many IG followers do you have?',
                            'min' => 0,

                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('t_user_name', [
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => 'Twitter username'

                        ]);
                    ?>

                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('t_follower', [
                            'type' => 'number',
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => 'How many Twitter followers do you have?',
                            'min' => 0,

                        ]);
                    ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12 mt-4">
            <label>Date of birth</label>
        </div>

        <div class="row px-3 ">
            <div class="col-sm-4">
                <div class="form-group">
                    <?= 
                        $this->Form->month('birth_month', [
                            'class' => 'form-control',
                            'empty' => 'Month',
                            'id' => 'birthMonth'
                        ]); 
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?= 
                        $this->Form->day('birth_day', [
                            'class' => 'form-control',
                            'empty' => 'Date',
                            'id' => 'birthDay'
                        ]); 
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?php
                        echo $this->Form->year('birth_year', [
                            'minYear' => 1950,
                            'maxYear' => date('Y'),
                            'class' => 'form-control',
                            'empty' => 'Year',
                            'id' => 'birthYear'
                        ]);
                    ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-check ">
                <input type="checkbox" class="form-check-input" id="ageabove18" value="0" name="over18">
                <label class="form-check-label" for="exampleCheck1">By checking here i agree that I am at least 18
                    years old and agree to the terms of use policy.</label>
            </div>
                <label id="over18-error" class="error" for="over18"></label>
        </div>

        <div class="col-sm-12 mt-5 text-right px-3">
            <button class="btn btn-submit">Submit</button>
        </div>
    </div>
 <?php echo $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script('frontend/influencer/validate', [
        'block' => 'scriptBottom'
    ]);
?>