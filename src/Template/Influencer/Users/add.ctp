<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Levels'), ['controller' => 'Levels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Level'), ['controller' => 'Levels', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Profiles'), ['controller' => 'UserProfiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Profile'), ['controller' => 'UserProfiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Media Handles'), ['controller' => 'UserMediaHandles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Media Handle'), ['controller' => 'UserMediaHandles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Bank Details'), ['controller' => 'UserBankDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Bank Detail'), ['controller' => 'UserBankDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Plans'), ['controller' => 'UserPlans', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Plan'), ['controller' => 'UserPlans', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fashion Designer Profiles'), ['controller' => 'FashionDesignerProfiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fashion Designer Profile'), ['controller' => 'FashionDesignerProfiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fashion Designer Media Handles'), ['controller' => 'FashionDesignerMediaHandles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fashion Designer Media Handle'), ['controller' => 'FashionDesignerMediaHandles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Film Director Profiles'), ['controller' => 'FilmDirectorProfiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Film Director Profile'), ['controller' => 'FilmDirectorProfiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Film Director Media Handles'), ['controller' => 'FilmDirectorMediaHandles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Film Director Media Handle'), ['controller' => 'FilmDirectorMediaHandles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Records'), ['controller' => 'UserRecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Record'), ['controller' => 'UserRecords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Events'), ['controller' => 'UserEvents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Event'), ['controller' => 'UserEvents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['controller' => 'UserMerchandises', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['controller' => 'UserMerchandises', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('role_id', ['options' => $roles]);
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('status');
            echo $this->Form->control('forgot_token');
            echo $this->Form->control('stripe_customer_id');
            echo $this->Form->control('step_completed');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('created_by');
            echo $this->Form->control('level_id', ['options' => $levels, 'empty' => true]);
            echo $this->Form->control('last_login');
            echo $this->Form->control('chat_session_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
