<div class="container mt-5 mb-5">
    <?php
        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'Users',
                'action' => 'notEligible',
                'prefix' => 'influencer'
            ],
            'id' => 'claim-unsucess',
        ]);
    ?>
    <div class="inner-wraper-influencer col-sm-6 m-auto mt-4 p-4 pb-5">
        <div class="col-sm-12 mb-4 mt-4">
            <h2>Sheesh. Your close but not quite there.</h2>
        </div>
        <div class="col-sm-12 mb-3">
            <p>
                Your follower count is close but Team Equity is exclusive to those with higher social media numbers.
                However no worries as you can still find your favorite Creator and Mobilize a campaign to help them
                grow and earn more will doing so.
            </p>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('partner_profile.first_name', [
                            'type' => 'text',
                            'placeholder' => 'First Name *',
                            'class' => 'form-control',
                            'label' => false,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('partner_profile.last_name', [
                            'type' => 'text',
                            'placeholder' => 'Last Name *',
                            'class' => 'form-control',
                            'label' => false,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('email', [
                            'type' => 'email',
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'label' => false,
                            'autocomplete' => 'off'
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?= $this->Form->control('password',
                        [
                            "class"=>"form-control",
                            "label" => false,
                            'placeholder' => __('Enter your password'),
                            "autocomplete" => "off"
                        ]
                        )
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('confirm_password', [
                            'type' => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'Re-enter Password',
                            'label' => false,
                            'autocomplete' => 'off'
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('partner_profile.phone_number', [
                            'class' => 'form-control',
                            'label' => false,
                            'type' => 'tel',
                            'id' => 'phone',
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-8">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('partner_profile.zipcode', [
                            'type' => 'text',
                            'placeholder' => 'Zip-code',
                            'class' => 'form-control',
                            'label' => false,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12 mt-5 text-right px-3">
            <button class="btn btn-submit">Mobilize</button>
        </div>
    </div>
<?= $this->Form->end(); ?>
</div>
<?php

    echo $this->Html->css(
        [
            'intlTelInput_new',
        ]
    );
    
    echo $this->Html->script([
            'IntelJs/intlTelInput.min',
            'frontend/influencer/validate_partner',
        ], [
        'block' => 'scriptBottom'
    ]);
?>