<?php 
//pr($this->request->getQuery('follwer'));
use Cake\I18n\Number;
?>
<div class="container mt-5 mb-5">
    <div class="inner-wraper-influencer col-sm-6 m-auto mt-4 p-2 pb-5 pt-5">
        <div class="col-sm-12 mb-4">
            <h2>Congratulations! Now let's get verified and start drafting.</h2>
        </div>
        <div class="col-sm-12 mb-3">
            <p>
                Per your social media profiles you have a combined follower count of <?= $this->Number->format($this->request->getQuery('follwer'))?> followers. Before you can draft Creators to your roster we need to make sure you are who you say you are. Verify your account by posting the image below to the social media accounts you signed up with and tag us @schecklive. If you prefer you can delete the image once you've been verified. After that your good to go.
            </p>
        </div>

        <div class="col-12 text-center mt-4">

        
        <?php
            echo $this->Html->image("infulencer-image.jpg", [
                "alt" => "infulencer",
                'class' => 'image-width',
                'url' => ['controller' => 'users', 'action' => 'download', 'prefix' => 'influencer']
            ]);
        ?>
    </div>

        <!-- <div class="row col-sm-10 m-auto pt-4 box-list-wraper">
            <div class="col-sm-6 text-center">
                <div class="instagram-text">Instagram</div>
                <a href="#">
                    <div class="red-list-box">
                        <div> I Am A</div>
                        <div>Sound check live</div>
                        <div>Influencer</div>
                    </div>
                </a>
              
            </div>
            <div class="col-sm-6 text-center twitter-box">
                <div class="instagram-text">Twitter</div>
                <a href="#">
                <div class="red-list-box">
                    <div> I Am A</div>
                    <div>Sound check live</div>
                    <div>Influencer</div>
                </div>
                </a>
            </div>
        </div> -->
        <div class="col-sm-12 mt-5 mb-4 text-center">
           <?php 
            echo $this->Html->link(
                    'Draft',
                    ['controller' => 'Catalogues', 'action' => 'index', 'prefix' => false],
                    ['class' => 'btn btn-submit']
                );
           ?>
        </div>
            
    </div>
</div>