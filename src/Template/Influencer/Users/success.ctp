<?php 
//pr($level);
?>
<div class="container mt-5 mb-5">
    <div class="inner-wraper-influencer col-sm-9 m-auto mt-4 p-1 p-sm-5 pb-5 ">
        <div class="col-sm-12 mb-4 mt-4 mt-sm-0  ">
            <h2>Great News!</h2>
        </div>
        <div class="col-sm-12">
            <p>Based on your follower count you qualify for Team Equity level “ <?= isset($level->level_name)?$level->level_name:''?>”. <br>At this level you’ll receive.

            </p>
            <ul>
                <li><?= isset($level->stack)?$level->stack:''?> ownership stake in all direct partner commitments</li>
                <li><?= isset($level->roster_slot)?$level->roster_slot:''?>  Roster slots
                </li>
                <li>New draft access after 24 hours
                </li>
                <?php 
                    if(!empty($level->feature)):
                ?>
                <li>
                    Access to <?= isset($level->feature)?$level->feature:''?> Model
                </li>
                <?php
                    endif;
                ?>

            </ul>
            <p>Claim your owners box and get working!</p>
        </div>
        <div class="col-sm-12 mt-5 text-right px-3">
            <?php 
                echo $this->Html->link(
                    'Claim',
                    ['controller' => 'Users', 'action' => 'claim', 'prefix' => 'influencer'],
                    ['class' => 'btn btn-submit']
                );
            ?>
        </div>
    </div>
</div>