<div class="container mt-5 mb-5">
    <?php
        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'Users',
                'action' => 'claim',
                'prefix' => 'influencer'
            ],
            'id' => 'claim-signup',
        ]);
    ?>
    <div class="inner-wraper-influencer col-sm-9 m-auto mt-4 p-4 pb-5">
        <div class="col-sm-12 text-center">
            <a href="<?= $this->Url->build(['controller' => 'homes', 'action' => 'index','prefix' => false]) ?>">
                <?= $this->Html->image('small-logo-png.png', ['alt' => 'logo','class' => 'mobile-logo']);?>
            </a>
        </div>
        <div class="col-sm-12 mb-4">
            <h2>Claim Your Owners Box!</h2>
        </div>
        <div class="row px-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('influencer_profile.first_name', [
                            'type' => 'text',
                            'placeholder' => 'First Name *',
                            'class' => 'form-control',
                            'label' => false,
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('influencer_profile.last_name', [
                            'type' => 'text',
                            'placeholder' => 'Last Name *',
                            'class' => 'form-control',
                            'label' => false,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-12">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('email', [
                            'type' => 'email',
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'label' => false,
                            'autocomplete' => 'off'
                        ]);
                    ?>
                </div>
            </div>
           
        </div>

        <div class="row px-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <?= $this->Form->control('password',
                        [
                            "class"=>"form-control",
                            "label" => false,
                            'placeholder' => __('Enter your password'),
                            "autocomplete" => "off"
                        ]
                        )
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('confirm_password', [
                            'type' => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'Re-enter Password',
                            'label' => false,
                            'autocomplete' => 'off'
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row px-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('influencer_profile.phone_number', [
                            'class' => 'form-control',
                            'label' => false,
                            'type' => 'tel',
                            'id' => 'phone',
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('influencer_profile.zipcode', [
                            'type' => 'text',
                            'placeholder' => 'Zip-code',
                            'class' => 'form-control',
                            'label' => false,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12 mt-5 text-right px-3">
            <button class="btn btn-submit">Create</button>
        </div>
    <?= $this->Form->end(); ?>
    </div>
</div>
<?php

    echo $this->Html->css(
        [
            'intlTelInput_new',
        ]
    );
    
    echo $this->Html->script([
            'IntelJs/intlTelInput.min',
            'frontend/influencer/validate_influencer',
        ], [
        'block' => 'scriptBottom'
    ]);
?>