<!--Start footer table -->
<table align="center" width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr>
            <td align="center">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"
                    bgcolor="#333333">
                    <tbody>
                        <tr>
                            <td align="center">
                                <table width="580" cellpadding="0" cellspacing="0" border="0"
                                    align="center">
                                    <tbody>
                                        <tr valign="top"
                                            style="text-align:center;font-size:13px;color:#fff;font-family: Montserrat,Helvetica,Arial,sans-serif;">

                                            <td style="text-align:center;padding: 10px;"><em>Copyright
                                                    &copy; <?= date('Y');?>
                                                </em><span
                                                    style="background-color:transparent; color:#fff;  font-size:13px">Soundchecklive.co
                                                </span><em>, All rights reserved.</em></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- End footer table -->