<aside class="bg-dark col px-0 position-absolute  d-none d-sm-none" id = "show-partner-modal" style="top:0;left:0;right:0;z-index:1031;">
    <div class="bg-danger form-row align-items-center justify-content-between py-3 text-white px-3">
        <div class="col">
            <h5>Collections</h5>
        </div>
        <div class="col-auto close" aria-label="Close">
            <span aria-hidden="true" class="h3">&times;</span>
        </div>
    </div>
    <div class="form-row align-items-center px-3 bg-blue-sidebar">
        <ul class="list-group list-group-flush w-100">
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Home',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>
                    <span class="align-top">
                        <?= __('Home') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Explore',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Explore') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Mobilize',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Mobilize') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Request',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Request') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Playlists',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Playlists') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Purchases',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Purchases') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Earnings',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Earnings') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Withdraw',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Withdraw') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Shop Merch',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Shop Merch') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent border-bottom border-white">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Partner Login',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                        
                    <span class="align-top">
                        <?= __('Partner Login') ?>
                    </span>
                </a>
            </li>
            <li class="list-group-item px-3 py-4 bg-transparent">
                <a href="" class="text-decoration-none text-white">
                    <?= $this->Html->image('partners-icon/home.svg', [
                        'alt' => 'Fan Signup',
                        'width' => "20px",
                        'class' => 'mr-3 align-top'
                    ]); ?>                         
                    <span class="align-top"> 
                        <?= __('Fan Signup') ?> 
                    </span>
                </a>
            </li>
        </ul>
    </div>
</aside>