<ul class="list-unstyled">
    <li class="active">
        <a href="#">
            <?= $this->Html->image('images/home-icon-gray.png') ?> Dashboard
        </a>
    </li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/music-and-multimedia.png') ?> SC Live
        </a>
    </li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/equity-draft.png') ?> Equity Draft
        </a>
    </li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/hand-graving-smartphone.png') ?> Mobilize
        </a>
    </li>
    <li class="divider"></li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/music.png') ?> Music
        </a>
    </li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/film-roll.png') ?> Film
        </a>
    </li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/merchandise.png') ?> Fashion
        </a>
    </li>
    <li class="divider"></li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/playlist.png') ?> Playlist
        </a>
    </li>
    <li>
        <a href="#">
            <?= $this->Html->image('images/random.png') ?> Shuffle
        </a>
    </li>

    <li>
        <a href="#">
            <?= $this->Html->image('images/shopping-cart.png') ?> Purchases
        </a>
    </li>
    <li class="divider"></li>
    <li>
                    <a href="#">
                    <?= $this->Html->image('images/merchandise.png') ?> Shop Merch
                    </a>
                </li>

                <li>
                    <a href="#">
                    <?= $this->Html->image('images/conversation.png') ?> Request
                    </a>
                </li>
                <li>
                    <a href="#">
                    <?= $this->Html->image('images/share.png') ?> Share
                    </a>
                </li>



                <li class="ml-4">
                    <a href="#">
                       Log In or Sign Up
                    </a>
                </li>
   
   
</ul>