<?php 
    if(!$highSclMusic->isEmpty()):
?>
<div class="col-sm-12 p-0 ">
    <h4>Highest SCL Score</h4>
    <div id="owl-hightscl" class="owl-carousel owl-theme">

    <?php foreach ($highSclMusic as $key => $record): ?>
        
        <div class="item">
            <div class="position-relative audio" data-url="<?= $this->Images->getSongObject($record) ?>">
                <div class="play-overlay">                        
                    <svg version="1.1" id="Capa_1" x="0px" y="0px"
                         viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" width="60px" height="60px" xml:space="preserve">
                    <circle style="fill:#e40734;" cx="29" cy="29" r="29"/>
                        <polygon style="fill:#FFFFFF;" points="44,29 22,44 22,29.273 22,14  "/>
                        <path style="fill:#FFFFFF;" d="M22,45c-0.16,0-0.321-0.038-0.467-0.116C21.205,44.711,21,44.371,21,44V14
                            c0-0.371,0.205-0.711,0.533-0.884c0.328-0.174,0.724-0.15,1.031,0.058l22,15C44.836,28.36,45,28.669,45,29s-0.164,0.64-0.437,0.826
                            l-22,15C22.394,44.941,22.197,45,22,45z M23,15.893v26.215L42.225,29L23,15.893z"/>
                    </svg>
                </div>
                <?php 
                    $cover = $this->Images->getRecordCover($record, 'thumbnail-');

                    if(!$cover) {
                        $cover = 'default-audio.png';
                    } 

                    echo $this->html->image($cover, [
                        'class' => 'object-fit-cover'
                    ]);
                ?>
            </div>

            <div class="artist-detail">
                <div class="row mt-3">
                    <div class="col-7">
                        <div class="artist-name  data-toggle=" modal" data-target="#exampleModalCenter">
                            <?php
                                $artist_name = $this->Text->truncate(
                                    h($record->artist_name),
                                    14,
                                    [
                                        'ellipsis' => '..',
                                        'exact' => false
                                    ]
                                );
                                echo $this->Html->link(
                                    ucwords($artist_name),
                                    [
                                        'controller' => 'Catalogues', 
                                        'action' => 'artistDetails',
                                        $record->id, 
                                        'prefix' => false
                                    ]
                                );
                            ?>
                        </div>
                        <div class="record-name" data-toggle="modal" data-target="#exampleModalCenter">
                            <?php
                                $record_name = $this->Text->truncate(
                                    h($record->record_name),
                                    14,
                                    [
                                        'ellipsis' => '..',
                                        'exact' => false
                                    ]
                                );
                                echo ucwords($record_name);
                            ?>
                        </div>
                    </div>
                    <div class="col-5 text-right">
                        <button class="btn btn-border get-detail" data-toggle="modal" data-target="#payment" data-toggle="modal" data-target="#become-partner" data-recordId="<?= $record->id?>">partner</button>
                    </div>
                </div>
                <div class="no-of-partner">Partners:  <?= $record->partner ?></div>
            </div>
        </div>
       

    <?php   endforeach; ?>


    </div>
</div>
<?php endif; ?>