<div class="col-sm-12 p-0 ">
    <div class="row">
        <div class="col-sm-6">
            <h4>Music Videos</h4>
        </div>
        <div class="col-sm-6 text-right view-all">
            <a href="#">View All</a>
        </div>
</div>
 
    
    <div id="music-video-FashionDesigner" class="owl-carousel owl-theme owl-nav-h">
        
        <?php 
            foreach($musicVideo as $key => $value):
                if(!empty($value->_matchingData['UserRecords'])) :
                    $record = $value->user_record;
                    $video = $this->Images->getUserVideoPitch($value->_matchingData['UserRecords']);
                    if($video) :
        ?>
                    <div class="item">

                        <?= $this->Html->media(
                            [ $video,
                                [
                                        'src' => 'video.ogg', 
                                        'type' => "video/ogg; codecs='theora, vorbis'",
                                ]
                            ],
                            [
                                'width' => "100%",
                                'controls'
                            ]
                        ) ?>
                        <div class="artist-detail">
                            <div class="row mt-3">
                                <div class="col-7">
                                    <div class="artist-name  data-toggle=" modal" data-target="#exampleModalCenter">
                                        <?php 
                                            $artist_name = $this->Text->truncate(
                                                h($record->artist_name),
                                                14,
                                                [
                                                    'ellipsis' => '..',
                                                    'exact' => false
                                                ]
                                            );
                                            echo $this->Html->link(
                                                ucwords($record->artist_name),
                                                [
                                                    'controller' => 'Catalogues', 
                                                    'action' => 'artistDetails',
                                                    $record->id, 
                                                    'prefix' => false
                                                ]
                                            );
                                        ?>
                                    </div>
                                    <div class="record-name" data-toggle="modal" data-target="#exampleModalCenter">
                                        <?php
                                            $record_name = $this->Text->truncate(
                                                h($record->record_name),
                                                14,
                                                [
                                                    'ellipsis' => '..',
                                                    'exact' => false
                                                ]
                                            );
                                            echo ucwords($record_name);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-5 text-right">
                                    <button class="btn btn-border get-payment-detail" data-toggle="modal" data-target="#payment" data-toggle="modal" data-target="#become-partner" data-recordId="<?= $record->id?>">partner</button>
                                </div>
                            </div>
                            <div class="no-of-partner">Partners:  <?= $record->partner ?></div>
                        </div>
                    </div>

        <?php 
                endif;
            endif;
            endforeach;
        ?>
    </div>
</div>
