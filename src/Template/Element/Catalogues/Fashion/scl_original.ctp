<?php 
   if(!empty($fashionDesignerSclOriginals)) :
?>
<div class="col-sm-12 p-0 ">
    <div class="row">
        <div class="col-sm-6">
            <h4>Sound Check Live Originals</h4>
        </div>
        <div class="col-sm-6 text-right view-all">
           <!--  <a href="#">View All</a> -->
        </div>
</div>
  
    <div id="owl-fasionDesignerSclOriginal" class="owl-carousel owl-theme owl-nav-h">
        
        <?php 
                //pr($fashionDesignerSclOriginals);die;
            foreach($fashionDesignerSclOriginals as $key => $value):
                if(!empty($value->fashion_designer_profile)) :
                    $cover = $this->Images->getFashionDesignerLastestGarment($value->fashion_designer_profile);
                    if($cover) :
        ?>
                    <div class="item">
                    <?php 
                       echo $this->Html->image($cover, [
                        'class' => 'object-fit-cover',
                        'style' => 'height:200px'
                    ]);
                    ?>
                        <div class="artist-detail">
                            <div class="row mt-3">
                                <div class="col-7">
                                    <div class="artist-name  data-toggle=" modal" data-target="#exampleModalCenter">
                                        <?php 
                                            $artist_name = $this->Text->truncate(
                                                h($value->fashion_designer_profile->first_name),
                                                14,
                                                [
                                                    'ellipsis' => '..',
                                                    'exact' => false
                                                ]
                                            );
                                            echo $this->Html->link(
                                                ucwords($value->fashion_designer_profile->first_name. ' '.$value->fashion_designer_profile->last_name),
                                                [
                                                    'controller' => 'Catalogues', 
                                                    'action' => 'fashionDesignerProfile',
                                                    $value->id, 
                                                    'prefix' => false
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                                <div class="col-5 text-right">
                                    <button class="btn btn-border get-payment-detail" data-toggle="modal" data-target="#payment" data-toggle="modal" data-target="#become-partner" data-recordId="<?php //$record->id?>">partner</button>
                                </div>
                            </div>
                        <div class="no-of-partner">Partners:  <?php // $record->partner ?></div>
                    </div>
                        
                    </div>

                    

                <?php 
                    endif;
                    endif;
                    endforeach;
                ?>
         </div>
</div>
<?php 
  endif;
?>
