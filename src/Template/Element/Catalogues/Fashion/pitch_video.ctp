<?php 
  if(!empty($fashionDesignerPitchVideo)) :
?>
<div class="col-sm-12 p-0 ">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="pb-2">Pitch Videos</h4>
        </div>
        <div class="col-sm-6 text-right view-all">
           <!--  <a href="#">View All</a> -->
        </div>
    </div>
 
    
    <div id="owl-fashionDesignerPitchVideo" class="owl-carousel owl-theme owl-nav-h">
        
        <?php
            foreach($fashionDesignerPitchVideo as $key => $value):
                if(!empty($value->fashion_designer_profile)) :
                    $video = $this->Images->filmDirectorPitchVideo($value->fashion_designer_profile);
                    if($video) :
        ?>
                    <div class="item">
                        <?= $this->Html->media(
                            [ $video,
                            ],
                            [
                                'width' => "100%",
                                'height' => '300px',
                                'controls',
                            ]
                        ) ?>

                        <div class="artist-detail">
                            <div class="row mt-3">
                                <div class="col-7">
                                    <div class="artist-name  data-toggle=" modal" data-target="#exampleModalCenter">
                                        <?php 
                                            $artist_name = $this->Text->truncate(
                                                h($value->fashion_designer_profile->first_name),
                                                14,
                                                [
                                                    'ellipsis' => '..',
                                                    'exact' => false
                                                ]
                                            );
                                            echo $this->Html->link(
                                                ucwords($value->fashion_designer_profile->first_name. ' '.$value->fashion_designer_profile->last_name),
                                                [
                                                    'controller' => 'Catalogues', 
                                                    'action' => 'fashionDesignerProfile',
                                                    $value->id, 
                                                    'prefix' => false
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                                <div class="col-5 text-right">
                                    <button class="btn btn-border get-payment-detail" data-toggle="modal" data-target="#payment" data-toggle="modal" data-target="#become-partner" data-recordId="<?php //$record->id?>">partner</button>
                                </div>
                            </div>
                        <div class="no-of-partner">Partners:  <?php // $record->partner ?></div>
                    </div>
                        
                    </div>

                    

                <?php 
                    endif;
                    endif;
                    endforeach;
                ?>
         </div>
</div>
<?php 
 endif;
?>
