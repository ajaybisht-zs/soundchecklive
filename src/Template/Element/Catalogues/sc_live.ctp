<div class="col-sm-12">
    <h4 class="pb-2">SC Live 1-on-1</h4>
    <div id="sclive" class="owl-carousel owl-theme owl-loaded owl-drag">

        <div class="owl-stage-outer">
            <div class="owl-stage"
                style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2989px;">

                <?php 
                   // pr($getOnlineArtist);
                    if(!empty($getOnlineArtist)) :
                        foreach ($getOnlineArtist as $key => $val) :
                           // pr($val);
                         
                ?>
                <div class="owl-item active" style="width: 193.454px; margin-right: 20px;">
                    <div class="item">
                        <div class="position-relative audio-">
                            <?php 
                                $video = $this->Images->artistAvatar($val->user_profile);
                                if($video) :
                                    echo $this->Html->image($video, ['alt' => 'user','class' => 'object-fit-cover mobile-width-img']);
                            ?>
                            <?php 
                                else:
                                echo $this->Html->image('no-profile-image.png', ['alt' => 'user','class' => 'object-fit-cover mobile-width-img']);
                                endif;
                            ?>

                            <div class="video-icon">
                                <a href="#" class="scl-payment" data-artist-id="<?= $val->id?>" data-price = "<?= $val->level->price*3?>">
                                    <?php echo $this->Html->image('video.png', ['alt' => 'video icon' ,'class' => 'scl-payment-']); ?>
                                </a>
                            </div>
                      
                        </div>
                        <div class="text-center mt-2">
                            <div class="name-artist"><?= isset($val->user_profile->artist_name)?ucwords($val->user_profile->artist_name):'N/A'?></div>
                            <div class="price">$<?= isset($val->level->price)?$val->level->price:'0'?></div>
                        </div>
                       


                    </div>
                </div>
                <?php 
                    endforeach;
                    endif;
                ?>
            </div>
        </div>
        <div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span
                    aria-label="Previous">‹</span></button><button type="button" role="presentation"
                class="owl-next"><span aria-label="Next">›</span></button></div>
        <div class="owl-dots disabled"></div>
    </div>
</div>


<!-- Modal -->
<?php
    echo $this->Html->css([
            'frontend/catalog/stripe',
        ]);
?>
<div class="modal fade" id="sclPaymentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"><strong>1-on-1 With <span style="color: #0062cc"><?= isset($val->user_profile->artist_name)?ucwords($val->user_profile->artist_name):'N/A'?></span></strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id = "payementsucceschat"></div>
            <div id="carderrorchat"></div> 
            <form action="/charge" method="post" id="scl-chat-form">
                <div class="modal-body">
                    <input type="hidden" name="user_id" id="uId" value="">
                    <input type="hidden" name="amount" id="price" value="">
                    <!-- <div class="row"> -->
                        <div class="form-group">
                            <input type="text" name="card_holder_name" class="form-control" placeholder="Name on Card" required maxlength="100">
                        </div>
                        <!-- <div class="col-sm-12"> -->
                            <div class="form-group">
                                <div id="card-livechat" class="card bg-light py-3">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>
                            </div>
                        <!-- Used to display Element errors. -->
                       <!--  </div> -->
                    <!-- </div> -->
                    <div class="col-sm-12 text-right mt-4 mb-4 mb-sm-0 text-right">
                        <?= $this->Html->image('verified-card-img.jpg',['class' => 'verified-img']) ?>
                    </div>

                <div id="card-errors-chat" role="alert"></div>
                </div>

                <div class="modal-footer">
                    <div class="col-6">
                        <i><strong>*3 minute minimum</strong></i>
                    </div>
                    <div class="col-6 text-right">
                       <button type="submit" class="btn btn-primary" id= "submit-btn-chatpayment">Pay Now</button>
                    </div>
                    <!-- <div class="text-left"><i><strong>*3 minute minimum</strong></i></div>
                    <button type="submit" class="btn btn-primary" id= "submit-btn-chatpayment">Pay Now</button> -->
                </div>
            </form>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script([
        //'Catalogues/stripe',
        ], [
        'block' => 'scriptBottom'
    ]);
?>