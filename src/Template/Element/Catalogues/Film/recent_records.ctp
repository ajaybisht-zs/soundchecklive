<div class="col-sm-12 p-0 ">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="pb-3">Recently Added</h4>
        </div>
        <div class="col-sm-6 text-right view-all">
           <!--  <a href="#">View All</a> -->
        </div>
</div>
 
    
    <div id="owl-filmDirectorRecentlyAddVideo" class="owl-carousel owl-theme owl-nav-h">
        
        <?php
            if(!empty($filmDirectorRecentRecords)) :
            foreach($filmDirectorRecentRecords as $key => $value):
                if(!empty($value->film_director_profile)) :
                    $video = $this->Images->getFilmDirectorRecentlyVideo($value->film_director_profile);
                    $recentlyAddedRecords = $this->Images->getFilmDirectorRecentlyVideoPoster($value->film_director_profile);
                    if($video) :
        ?>
                    <div class="item">
                        <?= $this->Html->media(
                            [ $video,
                            ],
                            [
                                'width' => "100%",
                                'height' => '300px',
                                'controls',
                                'poster' => env('BASE_URL').$recentlyAddedRecords
                            ]
                        ) ?>

                        <div class="artist-detail">
                            <div class="row mt-2">
                                <div class="col-7">
                                    <div class="artist-name  data-toggle=" modal" data-target="#exampleModalCenter">
                                        <?php 
                                            $artist_name = $this->Text->truncate(
                                                h($value->film_director_profile->first_name),
                                                14,
                                                [
                                                    'ellipsis' => '..',
                                                    'exact' => false
                                                ]
                                            );
                                            echo $this->Html->link(
                                                ucwords($value->film_director_profile->first_name. ' '.$value->film_director_profile->last_name),
                                                [
                                                    'controller' => 'Catalogues', 
                                                    'action' => 'filmDirectorProfile',
                                                    $value->id, 
                                                    'prefix' => false
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                                <div class="col-5 text-right">
                                    <button class="btn btn-border get-payment-detail" data-toggle="modal" data-target="#payment" data-toggle="modal" data-target="#become-partner" data-recordId="<?php //$record->id?>">partner</button>
                                </div>
                            </div>
                        <div class="no-of-partner">Partners:  <?php // $record->partner ?></div>
                    </div>
                        
                    </div>

                    

                <?php 
                    endif;
                    endif;
                    endforeach;
                endif;
                ?>
         </div>
</div>
