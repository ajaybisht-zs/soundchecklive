<?php use Cake\I18n\Number;?>
<?php 
//pr($limitedEditionApparel);
    if(!empty($limitedEditionApparel)):
?>
<div class="col-sm-12 p-0">
    <h4 class="pb-3">Limited Edition Apparel</h4>
    <div id="owl-film-merchandise" class="owl-carousel owl-theme">
        <?php foreach ($limitedEditionApparel as $key => $artistMerchandise):?>
        <div class="item">
            <?php
                $cover = $this->Images->getArtistMerchandiseImage($artistMerchandise->user_merchandise_images);
                if(!$cover) {
                    $cover = 'default-audio.png';
                } 

                echo $this->Html->image($cover, [
                    'class' => 'object-fit-cover'
                ]);
            ?>
            <div class="row mt-2">
                <div class="col-sm-12 song-name">
                    <?php
                    //pr($artistMerchandise->_matchingData['UserProfiles']->first_name);
                    echo isset($artistMerchandise->_matchingData['UserProfiles']->first_name)?ucfirst($artistMerchandise->_matchingData['UserProfiles']->first_name):'';
                    ?>     
                </div>
                <div class="col-sm-12 song-name">
                    <?php echo $this->Html->link(
                                    ucfirst($artistMerchandise->name),
                                [
                                    'controller' => 'Catalogues', 
                                    'action' => 'productDescriptions', 
                                    'prefix' => false,
                                    $artistMerchandise->id
                                ]
                            ); 
                    ?>     
                </div>
                <div class="col-sm-4 col-4 price">
                    <?= isset($artistMerchandise->price)?Number::currency($artistMerchandise->price,'USD'):'N/A'?>
                </div>
                <div class="col-sm-8 col-8 text-right">
                    <?php 
                        /*echo $this->Html->link(
                            'Shop Now',
                            ['controller' => 'Carts', 'action' => 'add', 'prefix' => false,base64_encode($artistMerchandise->id)],
                            ['class' => 'btn btn-border']
                        );*/
                    ?>
                    <?= $this->Form->postLink(
                        'Add To Cart',
                        [
                            'controller' => 'Carts',
                            'action' => 'add',
                            'prefix' => false, 
                            base64_encode($artistMerchandise->id)
                        ],
                        ['class' => 'btn btn-border']);
                    ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif;?>