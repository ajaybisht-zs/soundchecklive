<?php use Cake\Routing\Router;?>
<div class="col-sm-12 d-none d-sm-block">
    <div class="row">
        <div class="col-md-6">
            <?php
                $howWeworkVideo = $this->Images->getHowWeWorkVideo($howweworks);
                $howWeworkVideoPoster = $this->Images->getHowWeWorkVideoPoster($howweworks);
                if($howWeworkVideo) { ?>
                <h4><?= __($howweworks->title);?></h4>     
                <?php    
                    echo $this->Html->media(
                            [ $howWeworkVideo,
                            ],
                            [
                                'width' => "100%",
                                'controls',
                                'poster' => env('BASE_URL').$howWeworkVideoPoster
                            ]
                        );
                    }
                ?>
        </div>
        <div class="col-md-6">
            <?php
                $artistPitchVideo = $this->Images->getHowWeWorkVideo($pitchVideos);
                $artistPitchVideoPoster = $this->Images->getUserVideoPitchPoster($pitchVideos);
                if($artistPitchVideo) { ?>
                <h4><?= __($pitchVideos->title);?></h4>     
                <?php    
                    echo $this->Html->media(
                            [ $artistPitchVideo,
                            ],
                            [
                                'width' => "100%",
                                'controls',
                                'poster' => env('BASE_URL').$artistPitchVideoPoster
                            ]
                        );
                    }
            ?>
        </div>
    </div>
</div>


<div class="col-sm-12 d-block d-md-none">
    <div id="owl-mobile-slide-show" class="owl-carousel owl-theme">
        <div class="item">
            <?php
                $howWeworkVideo = $this->Images->getHowWeWorkVideo($howweworks);
                $howWeworkVideoPoster = $this->Images->getHowWeWorkVideoPoster($howweworks);
                if($howWeworkVideo) { ?>
                <h4><?= __($howweworks->title);?></h4>     
                <?php    
                    echo $this->Html->media(
                            [ $howWeworkVideo,
                            ],
                            [
                                'width' => "100%",
                                'controls',
                                'poster' => env('BASE_URL').$howWeworkVideoPoster
                            ]
                        );
                    }
                ?>
        </div>
        <div class="item">
            <?php
                $artistPitchVideo = $this->Images->getHowWeWorkVideo($pitchVideos);
                $artistPitchVideoPoster = $this->Images->getUserVideoPitchPoster($pitchVideos);
                if($artistPitchVideo) { ?>
                <h4><?= __($pitchVideos->title);?></h4>     
                <?php    
                    echo $this->Html->media(
                            [ $artistPitchVideo,
                            ],
                            [
                                'width' => "100%",
                                'controls',
                                'poster' => env('BASE_URL').$artistPitchVideoPoster
                            ]
                        );
                    }
            ?>
        </div>
    </div>
</div>