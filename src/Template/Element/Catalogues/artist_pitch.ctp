<div class="col-sm-12 p-0 ">
    <div class="row">
        <div class="col-sm-6">
            <h4>Artist Pitch Videos</h4>
        </div>
        <div class="col-sm-6 text-right view-all">
            <a href="#">View All</a>
        </div>
</div>
 
    
    <div id="owl-9" class="owl-carousel owl-theme owl-nav-h">
        
        <?php 
            foreach($artistPitchVideo as $key => $value):
                if(!empty($value->_matchingData['UserRecords'])) :
                    $record = $value->user_record;
                    $video = $this->Images->getUserVideoPitch($value->_matchingData['UserRecords']);
                    if($video) :
                    $recentlyAddedRecords = $this->Images->getArtistPitchVideo($value->_matchingData['UserRecords']);
                    //pr($recentlyAddedRecords);die;
        ?>
                    <div class="item">

                        <?= $this->Html->media(
                            [ $video,
                            ],
                            [
                                 'width' => "100%",
                                'height' => '300px',
                                'controls',
                                'poster' => env('BASE_URL').$recentlyAddedRecords
                            ]
                        ) ?>

                        <div class="artist-detail">
                            <div class="row mt-3">
                                <div class="col-7">
                                    <div class="artist-name  data-toggle=" modal" data-target="#exampleModalCenter">
                                        <?php 
                                            $artist_name = $this->Text->truncate(
                                                h($record->artist_name),
                                                14,
                                                [
                                                    'ellipsis' => '..',
                                                    'exact' => false
                                                ]
                                            );
                                            echo $this->Html->link(
                                                ucwords($record->artist_name),
                                                [
                                                    'controller' => 'Catalogues', 
                                                    'action' => 'artistDetails',
                                                    $record->id, 
                                                    'prefix' => false
                                                ]
                                            );
                                        ?>
                                    </div>
                                </div>
                                <div class="col-5 text-right">
                                    <button class="btn btn-border get-payment-detail" data-toggle="modal" data-target="#payment" data-toggle="modal" data-target="#become-partner" data-recordId="<?= $record->id?>">partner</button>
                                </div>
                            </div>
                            <div class="no-of-partner">Partners:  <?= $record->partner ?></div>
                        </div>
                    </div>

        <?php 
                endif;
            endif;
            endforeach;
        ?>
    </div>
</div>
