<div class="modal" tabindex="-1" role="dialog" id="openSmartLinkModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <?php
                    echo $this->Form->create(null, [
                        'url' => [
                            'controller' => 'Users',
                            'action' => 'addSmartLink',
                            'prefix' => 'admin',
                        ],
                        'id' => 'AddSmartLinkForm',
                        'class' => 'form-horizontal'
                    ]);
                ?> 
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Smart Link:</label>
                        <?php
                            echo $this->Form->control('smart_link', [
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ]);

                                echo $this->Form->hidden('id',['id' => 'user_id']);
                            ?>
                        </div>
          </div>
          <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Close</button>
          </div>
        <?= $this->Form->end(); ?>
        </div>
    </div>
</div>