<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <?php 
            echo $this->Form->create('', [
                    'url'   => ['controller' => 'Users', 'action' => 'changePassword','prefix' => 'admin'],
                    'type'  => 'post',
                    'id'    => 'change-password'
                    ]
                );
            ?>  
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= __('Change Password');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body"> 
                  <div class="form-group">
                    <label for="current-password" class="col-form-label">Current Password:</label>
                        <?php
                            echo $this->Form->control('old_password', array(
                                'class' => 'form-control',
                                'type' => 'password',
                                'label' => false,
                                'maxlength'=>100,
                                'id' => 'current-password'
                                )
                            );
                        ?>
                  </div>
                  <div class="form-group">
                    <label for="password" class="col-form-label">New Password:</label>
                        <?php
                            echo $this->Form->control('password', array(
                                'class' => 'form-control',
                                'type' => 'password',
                                'label' => false,
                                'maxlength'=>100,
                                'id' => 'password'
                                )
                            );
                        ?>
                  </div>
                   <div class="form-group">
                    <label for="old-password" class="col-form-label">Confirm Password:</label>
                        <?php
                            echo $this->Form->control('confirm_password', array(
                                'class' => 'form-control',
                                'type' => 'password',
                                'label' => false,
                                'maxlength'=>100,
                                'id' => 'old-password'
                                )
                            );
                        ?>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
         <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?= 
    $this->Html->script(
        [
          'backend/Admin/change_password'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>