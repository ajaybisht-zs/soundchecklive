<!-- Modal -->
<div class="modal fade artist-detail custom-model" id="exampleModalCenter" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-gray" id="exampleModalCenterTitle">Record Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <div class="col-5">Record Name</div>
                    <div>Lion (Original Motion Picture Soundtrack)</div>
                </div>
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <div class="col-5">Artist Name</div>
                    <div>Sia</div>
                </div>
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <div class="col-5">Producer name</div>
                    <div>Greg Kurstin</div>
                </div>
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <div class="col-5">Writer Name</div>
                    <div>Greg Kurstin</div>
                </div>
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <div class="col-5">No of Partner</div>
                    <div>10</div>
                </div>
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <div class="col-5">Tags</div>
                    <div class="tags">
                        <a href="#">Song</a>
                        <a href="#">Sia</a>
                        <a href="#">Rock</a>
                        <a href="#">Music</a>
                    </div>
                </div>
                <div class="d-flex text-gray py-2 custom-border justify-content-center text-center">
                    <button class="btn mt-3 partner-now">Partner Now</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Become a partner -->
<!-- <div class="modal fade become-partner custom-model" id="become-partner" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-gray" id="exampleModalCenterTitle">Become Partner Now</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex text-gray py-2 custom-border align-items-center">
                    <span class="doller">$</span>
                    <div class="payment">10</div>
                </div>
                <div>
                    <form action="#">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="text-gray">Name of card</label>
                                    <input type="text" name="card-name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="text-gray">Card Number</label>
                                    <input type="text" name="card-number" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="text-gray">CCV</label>
                                    <input type="text" name="ccv" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="text-gray">Exp. Month</label>
                                    <input type="text" name="ccv" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="text-gray">Exp. Day</label>
                                    <input type="text" name="ccv" class="form-control">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="d-flex text-gray py-2 custom-border justify-content-center text-center">
                    <button class="btn mt-3 pay-now">I pay</button>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- Become a partner -->
<div class="modal fade become-partner custom-model" id="play-song" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-gray" id="exampleModalCenterTitle">SoundCheckLive</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="single-song-player">
                  <img data-amplitude-song-info="cover_art_url"/>
                  <div class="bottom-container">
                    <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>

                    <div class="time-container">
                      <span class="current-time">
                        <span class="amplitude-current-minutes"></span>:<span class="amplitude-current-seconds"></span>
                      </span>
                      <span class="duration">
                        <span class="amplitude-duration-minutes"></span>:<span class="amplitude-duration-seconds"></span>
                      </span>
                    </div>

                    <div class="control-container">
                      <div class="amplitude-play-pause" id="play-pause"></div>
                      <div class="meta-container">
                        <span data-amplitude-song-info="name" class="song-name"></span>
                        <span data-amplitude-song-info="artist"></span>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    /*
  1. Base
*/
/*
  2. Components
*/
div.control-container {
  margin-top: 10px;
  padding-bottom: 10px; }
  div.control-container div.amplitude-play-pause {
    width: 74px;
    height: 74px;
    cursor: pointer;
    float: left;
    margin-left: 10px; }
  div.control-container div.amplitude-play-pause.amplitude-paused {
    background: url("https://521dimensions.com/img/open-source/amplitudejs/examples/single-song/play.svg");
    background-size: cover; }
  div.control-container div.amplitude-play-pause.amplitude-playing {
    background: url("https://521dimensions.com/img/open-source/amplitudejs/examples/single-song/pause.svg");
    background-size: cover; }
  div.control-container div.meta-container {
    float: left;
    width: calc(100% - 84px);
    text-align: center;
    color: white;
    margin-top: 10px; }
    div.control-container div.meta-container span[data-amplitude-song-info="name"] {
      font-family: "Open Sans", sans-serif;
      font-size: 18px;
      color: #fff;
      display: block; }
    div.control-container div.meta-container span[data-amplitude-song-info="artist"] {
      font-family: "Open Sans", sans-serif;
      font-weight: 100;
      font-size: 14px;
      color: #fff;
      display: block; }
  div.control-container:after {
    content: "";
    display: table;
    clear: both; }

/*
  Small only
*/
@media screen and (max-width: 39.9375em) {
  div.control-container div.amplitude-play-pause {
    background-size: cover;
    width: 64px;
    height: 64px; }
  div.control-container div.meta-container {
    width: calc(100% - 74px); } }
/*
  Medium only
*/
/*
  Large Only
*/
div.time-container {
  opacity: 0.5;
  font-family: 'Open Sans';
  font-weight: 100;
  font-size: 12px;
  color: #fff;
  height: 15px; }
  div.time-container span.current-time {
    float: left;
    margin-left: 5px; }
  div.time-container span.duration {
    float: right;
    margin-right: 5px; }

/*
  Small only
*/
/*
  Medium only
*/
/*
  Large Only
*/
progress.amplitude-song-played-progress {
  background-color: #313252;
  -webkit-appearance: none;
  appearance: none;
  width: 100%;
  height: 5px;
  display: block;
  cursor: pointer;
  border: none; }
  progress.amplitude-song-played-progress:not([value]) {
    background-color: #313252; }

progress[value]::-webkit-progress-bar {
  background-color: #313252; }

progress[value]::-moz-progress-bar {
  background-color: #00a0ff; }

progress[value]::-webkit-progress-value {
  background-color: #00a0ff; }

/*
  Small only
*/
/*
  Medium only
*/
/*
  Large Only
*/
/*
  3. Layout
*/
div.bottom-container {
  background-color: #202136;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px; }

/*
  Small only
*/
/*
  Medium only
*/
/*
  Large Only
*/
div#single-song-player {
  border-radius: 10px;
  margin: auto;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
  margin-top: 50px;
  width: 100%;
  max-width: 460px;
  -webkit-font-smoothing: antialiased; }
  div#single-song-player img[data-amplitude-song-info="cover_art_url"] {
    width: 100%;
    border-top-right-radius: 10px;
    border-top-left-radius: 10px; }

a.learn-more{
  display: block;
  width: 300px;
  margin: auto;
  margin-top: 30px;
  text-align: center;
  color: white;
  text-decoration: none;
  background-color: #202136;
  font-family: "Lato", sans-serif;
  padding: 20px;
  font-weight: 100;
}
</style>