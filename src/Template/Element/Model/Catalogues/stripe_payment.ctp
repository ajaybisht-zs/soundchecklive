<?php
    echo $this->Html->css([
            'frontend/catalog/stripe',
        ]);
?>

<!-- Become a partner -->
<div class="modal fade custom-model" id="payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title 
                text-gray" id="exampleModalLabel">Become Partner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id = "payementsucces"></div>
            <div id="carderror"></div> 
            <form action="/charge" method="post" id="payment-form">
                <div class="modal-body">
                     <div>
                         <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" value="10" name="amount">
                         </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Email address" name="email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name on your card" name="name">
                            <input type="hidden" name="user_record_id" value="" id="recordId">
                            <input type="hidden" name="type" value="0" id="typeId">
                        </div>
                    </div>
                    <div id="card-element" class="card bg-light py-3">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                    <!-- Used to display Element errors. -->
                   <div id="card-errors" role="alert"></div>
                </div>
                <div class="modal-footer border-0">
                    <button class="btn btn-primary pay-now" id= "submit-btn-txt">Submit Payment</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- <div class="modal fade" id="payment" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-gray" id="exampleModalCenterTitle">Become Partner Now</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="cell example example1" id="example-1">
            
            </div>
        </div>
    </div>
</div> -->
<?php
    echo $this->Html->script([
        //'Catalogues/stripe',
        ], [
        'block' => 'scriptBottom'
    ]);
?>
