<?php 
  $session = $this->request->getSession();
?>
<!-- Modal -->
<div class="modal fade" id="incoming-call" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-md-4">
            <?= $this->Html->image('mobile-splash.jpg',['height' => '50px'])?>
        </div>

        <div class="col-md-8">
            <h5 class="modal-title" id="exampleModalLabel">Incomming Call Alert</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <audio>
              <source id="source" src="" type="audio/ogg">
            </audio>
        </div>
      </div>
      <div class="modal-body">
        You've got money waiting
      </div>
      <div class="modal-footer">
        <?php
           echo $this->Html->link(
              'Accept',
              ['controller' => 'Catalogues', 'action' => 'videoChat',$session->read('Auth.User.id'),'prefix' => false],
              [
                  'class' => 'btn btn-success',
              ]
          );
        ?>
       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Accept</button> -->
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="disconnect-call">Reject</button>
      </div>
    </div>
  </div>
</div>