
            <?= 
            //pr($userProfile);
                $this->Form->create($userProfile, [
                    'url' => [
                        'controller' => 'UserProfiles',
                        'action' => 'edit',
                    ],
                    'type' => 'file', 
                    'id' => 'editBiographyForm',

                ]);
            ?>
            <div class="bg-white h-100">
                <div class="cover-img">
                    <?php 
                        $banner = $this->Images->artistBanner($userProfile->user_profile);
                        if($banner) {
                            echo $this->Html->image($banner, ['alt' => 'cover images']);

                        }else {
                            echo $this->Html->image('artist/cover.jpg', ['alt' => 'cover images']);
                        }
                    ?>
                    <div class="choose_file">
                        <span class="d-block">
                            <svg version="1.1" width="24px" height="24px" fill="#fff" x="0px" y="0px"
                                viewBox="0 0 490.667 490.667" style="enable-background:new 0 0 490.667 490.667;"
                                xml:space="preserve">
                                <g>
                                    <g>
                                        <g>
                                            <path
                                                d="M448,128h-67.627l-39.04-42.667H192v64h-64v64H64v213.333c0,23.467,19.2,42.667,42.667,42.667H448
                                           c23.467,0,42.667-19.2,42.667-42.667v-256C490.667,147.2,471.467,128,448,128z M277.333,405.333
                                           c-58.88,0-106.667-47.787-106.667-106.667S218.453,192,277.333,192S384,239.787,384,298.667S336.213,405.333,277.333,405.333z" />
                                            <polygon points="64,192 106.667,192 106.667,128 170.667,128 170.667,85.333 106.667,85.333 106.667,21.333 64,21.333 64,85.333 
                                           0,85.333 0,128 64,128            " />
                                            <path
                                                d="M277.333,230.4c-37.76,0-68.267,30.507-68.267,68.267h0c0,37.76,30.507,68.267,68.267,68.267
                                           c37.76,0,68.267-30.507,68.267-68.267S315.093,230.4,277.333,230.4z" />
                                        </g>
                                    </g>
                            </svg>
                        </span>
                        <?= 
                            $this->Form->file('user_profile.banner', [
                                'type' => 'file', 
                                'required' => false,
                                'accept' =>"image/*",
                                'label' => false
                            ]) 
                        ?>
                    </div>
                    <div class="profile-pic">
                        <?php 
                            $avatar = $this->Images->artistAvatar($userProfile->user_profile); 
                            if($avatar):
                                echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                        ?>
                        <?php else: ?>
                            <svg version="1.1" x="0px" y="0px" viewBox="0 0 53 53"
                                style="enable-background:new 0 0 53 53;" xml:space="preserve">
                                <path style="fill:#333333;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
                               c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
                               c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
                               c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
                               c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
                               C20.296,39.899,19.65,40.986,18.613,41.552z" />
                                <g>
                                    <path style="fill:#ffffff;"
                                        d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
                                   c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
                                   c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
                                   s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
                                   c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
                                   c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                                </g>
                            </svg>
                        <?php endif; ?>
                        <div class="choose_file upload-profile">
                            <span class="d-block">
                                <svg version="1.1" width="14px" height="14px" x="0px" y="0px"
                                    viewBox="0 0 469.331 469.331"
                                    style="enable-background:new 0 0 469.331 469.331;" xml:space="preserve">
                                    <g>
                                        <path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4
c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6
l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3
S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1
l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4
s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" />
                                    </g>
                                </svg>
                            </span>
                            <?= 
                                $this->Form->control('user_profile.avatar', [
                                    'type' => 'file',
                                    'accept' =>"image/*",
                                    'label' => false
                                ])
                            ?>
                        </div>

                    </div>
                    <div class="profile-name"><?= ucfirst($userProfile->full_name) ?></div>
                </div>
                <div class="p-4">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="tab-title mb-2">Edit Biography</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                                <?= $this->Flash->render();?>
                                <div class="form-group biography mt-3">
                                    <label>Biography</label>
                                    <?= 
                                        $this->Form->control('user_profile.biography', [
                                            'type' => 'textarea',
                                            'class' => 'form-control',
                                            'placeholder' => "Mauris felis risus, luctus lobortis hendrerit eu, tincidunt eget ipsum. Ut suscipit et erat vestibulum aliquam. Cras vehicula viverra aliquet. Nulla facilisi. In lorem arcu, gravida eget elementum in, efficitur at enim. Vestibulum sed mattis dui. Suspendisse vel porta magna.",
                                            'label' => false,
                                            'maxlength' => 300,
                                        ]) 
                                    ?>
                                    <small class="text-muted">
                                        300 character limit, plain text only.
                                    </small>
                                </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Facebook</label>
                        <?= 
                            $this->Form->control('user_media_handle.facebook', [
                                'class' => 'form-control  custom-input',
                                'placeholder' => "https://www.facebook.com/mike_edward",
                                'label' => false
                            ]) 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <?= 
                            $this->Form->control('user_media_handle.twitter', [
                                'class' => 'form-control  custom-input',
                                'placeholder' => "https://www.twitter.com/mike_edward",
                                'label' => false
                            ]) 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Instagram</label>
                        <?= 
                            $this->Form->control('user_media_handle.instagram', [
                                'class' => 'form-control  custom-input',
                                'placeholder' => "https://www.instagram.com/mike_edward",
                                'label' => false
                            ]) 
                        ?>
                    </div>                    

                    <div class="form-group">
                        <label>ROI </label>
                        <?= 
                            $this->Form->control('user_profile.budget_usage', [
                                'class' => 'form-control',
                                'label' => false,
                            ]) 
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Viedo pitch </label>
                        <?= 
                            $this->Form->hidden('user_records.0.id') 
                        ?>
                        <?= 
                            $this->Form->control('user_records.0.video_pitch_file', [
                                'type' => 'file',
                                'class' => 'form-control',
                                'label' => false,
                                'accept' => 'video/*'
                            ]) 
                        ?>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <?php
                                $video = $this->Images->getVideoPitch($record);
                                if($video) {
                                    echo $this->Html->media($video, [
                                        'fullBase' => true,
                                        'text' => 'Fallback text',
                                        'controls',
                                        'width' => '100%'
                                    ]);
                                }

                            ?>
                        </div>
                    </div>
                        
                    <div class="form-group text-right mt-4">
                        <button type="submit" class="bg-red text-white py-2 px-4"
                            name="submit">Update</button>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
      
<?php
    echo $this->Html->script([
        'Artist/UserProfiles/edit'
    ], [
        'block' => 'scriptBottom'
    ]);
?>