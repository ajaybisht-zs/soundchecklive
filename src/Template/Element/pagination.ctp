<?php if($this->Paginator->counter('{{pages}}') > env('RECORDS_PER_PAGE',1)): ?>
    <nav aria-label="Page navigation example" class="mt-4">
        <ul class="pagination justify-content-end">
            <?= $this->Paginator->prev('< ' . __('Previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' >') ?>
        </ul>
    </nav>
<?php endif; ?>