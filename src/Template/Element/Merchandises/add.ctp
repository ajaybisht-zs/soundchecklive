<section class="col-12 step-from d-none">
    <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
        <div class="question-number pc-bold">17/20</div>
        <div class="question-text pc-bold">Do you have Merchandise for sale?
        </div>
    </div>
    <div class="col-md-5 m-auto">
        <div class="text-center mb-5">
            <label class="custom-radio-btn">Yes
                <input type="radio" checked="checked" value="0" name="radio12">
                <span class="checkmark"></span>
            </label>
            <label class="custom-radio-btn">No
                <input type="radio" value="1" name="radio12">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="merchandise-sale-wraper" id="merchandiseContainer">
            <div class="merchandise-sale mb-5">
                <div class="form-group relative-position">
                    <div class="upload-btn-wrapper">
                        <button class="btn">Upload</button>
                        <input type="file" name="myfile" />
                    </div>
                    <input type="text" class="form-control custom-input"
                        placeholder="Upload your video">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control custom-input" placeholder="Name of Item">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control custom-input" placeholder="Price of Item">
                </div>
                <div class="form-group">
                    <select class="form-control custom-input">
                        <option>Colors Available</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control custom-input">
                        <option>Sizes Available</option>
                    </select>
                </div>
            </div>

            <div class="form-group text-right add-more">
                <button class="btn black-btn">Add More</button>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-6">
            <button class="pc-bold back-btn custom-margin previous">
                back
            </button>
        </div>
        <div class="col-6">
            <div class="text-right">
                <button class="pc-bold black-btn custom-margin next">
                    next
                </button>
            </div>
        </div>
    </div>
</section>