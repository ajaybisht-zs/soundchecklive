<section class="col-12 step-from d-none">
    <div class="col-sm-12 text-center pt-3 question-section pb-3 border-bottom mb-5">
        <div class="question-number pc-bold">18/20</div>
        <div class="question-text pc-bold">Do you have any upcoming shows?
        </div>
    </div>
    <div class="col-md-5 m-auto">
        <div class="text-center mb-5">
            <label class="custom-radio-btn">Yes
                <input type="radio" checked="checked" value="0" name="radio11">
                <span class="checkmark"></span>
            </label>
            <label class="custom-radio-btn">No
                <input type="radio" value="1" name="radio11">
                <span class="checkmark"></span>
            </label>
        </div>

        <div class="upcoming-shows-wraper" id="upcomingShowsContainer">
            <div class="upcoming-shows">
                <div class="mb-3">Upload Flyers</div>
                <div class="upcoming-show-img">
                    <img src="images/upcoming-shows.png">
                </div>

                <div class="upload-btn-wrapper mb-4">
                    <button class="btn">Upload</button>
                    <input type="file" name="myfile" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control custom-input" placeholder="City of Show">
                </div>
                <div class="form-group relative">
                    <img src="images/date-icon.png">
                    <input type="text" class="form-control custom-input" placeholder="Date">
                </div>
                <div class="form-group relative">
                    <img src="images/time-icon.png">
                    <input type="text" class="form-control custom-input" placeholder="Time">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control custom-input" placeholder="link for tickets">
                </div>
            </div>
            <div class="form-group text-right add-shows">
                <button class="btn black-btn">Add More</button>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-6">
            <button class="pc-bold back-btn custom-margin previous">
                back
            </button>
        </div>
        <div class="col-6">
            <div class="text-right">
                <button class="pc-bold black-btn custom-margin next">
                    next
                </button>
            </div>
        </div>
    </div>
</section>