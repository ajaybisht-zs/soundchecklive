<!-- Footer -->
<footer class="catalogue-footer mt-auto">
    <div class="container">
        <div class="col-12">
            <div class="border-custom">
                <div class="row">
                    <div class="col-sm-12 col-lg-3">
                        <?= $this->Html->image('white-logo.png')?>
                    </div>

                    <div class="col-12 d-md-none">
                        <h3 class="mt-5 mb-3 text-white">INDEPENDENCE IS LIFE</h3>
                    </div>
                    
                    <div class="col-lg-2 col-sm-12">
                        <h4>Company</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">What we do</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Upcoming events</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-12">
                        <h4>Quick Link</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">For artist</a></li>
                            <li><a href="#">FAQ'S</a></li>
                            <li><a href="#">Merchandise</a></li>

                        </ul>
                    </div>
                    <div class="col-lg-2 col-sm-12">
                        <h4>Contact Us</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">For artist</a></li>
                            <li><a href="#">FAQ'S</a></li>
                            <li><a href="#">Merchandise</a></li>

                        </ul>
                    </div>
                    <div class="col-lg-2 col-sm-12">
                        <h4>Follow Us</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">
                                <?= 
                                    $this->Html->image('artist/facebook-icon.png', [
                                        'class' => 'mr-2',
                                        'alt' => 'facebook'
                                    ]) 
                                ?> Facebook
                                </a>
                            </li>
                            <li><a href="#"><?= 
                                    $this->Html->image('artist/tiwtter.png', [
                                        'class' => 'mr-2',
                                        'alt' => 'twitter'
                                    ]) 
                                ?> Tiwtter</a></li>
                            <li><a href="#"><?= 
                                    $this->Html->image('artist/instagrams.png', [
                                        'class' => 'mr-2',
                                        'alt' => 'instagram'
                                    ]) 
                                ?> Instagram</a></li>

                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="copy-right text-center">
        <p>©2019 All copyrights reserved SoundCheckLive</p>
    </div>
</footer>