<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger alert-dismissible fade show" onclick="this.classList.add('hidden');" role="alert"><?= $message ?></div>
