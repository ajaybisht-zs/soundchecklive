<?php 
$controllerName = $this->request->getParam('controller');
$actionName = $this->request->getParam('action');
?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark py-0 top-fixed-nav">
    <a class="navbar-brand" href="<?= $this->Url->build(['controller' => 'homes', 'action' => 'index','prefix' => false]) ?>">
        <?= $this->Html->image('logo.png', ['alt' => 'logo','class' => 'd-none d-md-block']);?>
            <?= $this->Html->image('mob-logo.png', ['alt' => 'logo','class' => 'd-block d-md-none']);?>
    </a>
    <div class="ml-auto top-login">
        <?= 
            $this->Html->link(
                'Login',
                [
                    'controller' => 'Logins', 
                    'action' => 'add',
                    'prefix' => 'artist',
                ],
                ['class' => 'login mr-5']
            );
        ?>

        <?php 
            //if(($controllerName != 'UserProfiles' && $actionName != 'add')) :
            if((!in_array($actionName, ['register','add','index','success','claim','congrats']))) :
        ?>

            <?= $this->Html->link(
                    'Register',
                    [
                        'controller' => 'Users', 
                        'action' => 'register',
                        'prefix' => 'artist',
                    ],
                    ['class' => 'register bg-red px-4 mr-5']
                );
            ?>
        <?php endif;?>
    </div>
    <div class="outer-menu">
        <input class="checkbox-toggle" type="checkbox" />
        <div class="hamburger">
            <div></div>
        </div>
        <div class="menu">
            <div>
                <div>
                    <ul>
                        <li>
                            <?= $this->Html->link('Home',
                                                    [
                                                        'controller'=>'homes',
                                                        'action' => 'index',
                                                        'prefix' => false
                                                    ])
                                ?>
                        </li>
                        <li>
                                    <?=
                                        $this->Html->link(
                                            'CATALOG',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>

                                <li>
                                <?=
                                    $this->Html->link(
                                        'LE APPAREL',
                                        [
                                            'controller' => 'Catalogues',
                                            'action' => 'index',
                                            'prefix' => false
                                        ]
                                    );
                                ?>
                                </li>
                                <li>
                                    <?=
                                        $this->Html->link(
                                            'ARTIST LOGIN',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?=
                                        $this->Html->link(
                                            'PARTNER LOGIN',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>



                                <li>
                                    <?= $this->Html->link(
                                                'About us',
                                                '/about-us',
                                                ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>
                                <li>
                                    <?= $this->Html->link(
                                            'WHAT WE DO',
                                            '/what-we-do',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?= $this->Html->link(
                                            'HOW SCL WORKS?',
                                            '/how-scl-work',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?php  $this->Html->link(
                                            'FAQ’S',
                                            '/faq',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?= $this->Html->link(
                                            'Contact',
                                            '/contact-us',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>



                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>