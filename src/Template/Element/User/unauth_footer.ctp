<!-- footer -->
<footer class="footer mt-auto  bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-4 pb-2">
                <h2>INDEPENDENCE IS LIFE</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="copy-right mb-0 text-white">©<?= date('Y');?> All copyrights reserved SoundCheckLive</p>
            </div>
        </div>
    </div>
</footer>