<div class="footer-link d-block d-md-none">
    <ul class="d-flex flex-row justify-content-center align-items-center h-100 ">
        <li>
            <a href="/catalog">                        
                <?= $this->Html->image('live.jpeg', ['alt' => '1-on-1']);?>
                <span class="d-block text-dark">1-on-1</span>
            </a>
        </li>
        <li>
            <a href="/catalog">
                <?= $this->Html->image('shuffle.jpeg',['alt' =>'Shuffle'])?>
                <span class="d-block text-dark">Shuffle</span>
            </a>
        </li>
        <li>
            <a href="/catalog">
                <?= $this->Html->image('shop-merch.jpeg',['alt' =>'Shop Merch'])?>
                <span class="d-block text-dark">Shop Merch</span>
            </a>
        </li>
        <li>
            <a href="/catalog" class="show-partner-layout">
                <?= $this->Html->image('partner-icon.jpg',['alt' =>'Partners'])?>
                <span class="d-block text-dark">Partners</span>
            </a>
        </li>
    </ul>
</div>