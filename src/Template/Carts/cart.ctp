<?php
   use Cake\I18n\Number; 
?>
<div class="breadcrumb-nav">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Merchandise </li>
                        <li class="breadcrumb-item" aria-current="page">Product description</li>
                        <li class="breadcrumb-item active" aria-current="page">Cart</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="cart mt-5">
    <div class="container">
        <div class="row justify-content-center align-items-center mb-3 top-heading">
            <div class="col-lg col-md-2">Product Image</div>
            <div class="col-lg col-md-3 font-weight-500">Product Name</div>
            <div class="col-lg col-md-2 font-weight-500">Price</div>
            <div class="col-lg col-md-1 font-weight-500">Qty</div>
            <div class="col-lg col-md-2 font-weight-500">Total</div>
            <div class="col-lg col-md-2">Action</div>
        </div>
        <?php
            $total_price = 0; 
            if(!empty($cart['cart'])) {
                //pr($cart['cart']);
                foreach($cart['cart'] as $key => $val) {
                    $item_price = $val["quantity"]*$val["price"];
        ?>
        <div class="row justify-content-md-center align-items-center py-2 bg-light-gray ">
                <div class="col-lg col-md-2 col-3">
                    <?php 
                        $cover = $this->Images->getArtistMerchandiseImage($val['image']);
                        if(!$cover) {
                            $cover = 'dummy-img.jpg';
                        } 

                        echo $this->Html->image($cover, [
                            'class' => 'object-fit-cover',
                            'width' => '100%'
                        ]);
                    
                    ?>
                    <?php // $this->Html->image('dummy-img.jpg', ['alt' => 'logo','class' => 'product-dummy']);?>
                </div>
                <div class="col-lg col-md-3 col-9"><?= $val['name']?></div>
                <div class="col-lg col-md-2 col-4"><?= $val['price']?></div>
                <div class="col-lg col-md-1 col-2" data-recordId = "<= $val['record_id'] ?>">
                    <?= $this->Form->control('quantiy',[
                            'value' => isset($val['quantity'])?$val['quantity']:1,
                            'type' => 'number',
                            'label' => false,
                            'class' => 'numeric',
                            'data-recordId' => $val['record_id'],
                            'id' => 'quantity-<?php echo $key; ?>'
                            ]);
                        ?>
                </div>
                <div class="col-lg col-md-2 col-2" id="subtotal-<?php echo $key; ?>"><?= Number::currency($val['subtotal'], 'USD'); ?></div>
                <div class="col-lg text-right col-md-2 col-4">
                    <!-- <button class="bg-dark text-white remove-btn">Remove</button> -->
                    <?= $this->Form->postLink(
                        'Remove',
                        ['action' => 'removeItem', $key],
                        ['class' => 'bg-dark text-white remove-btn'])
                    ?>
                </div>
        </div>
         <?php
               $total_price += ($val["price"]*$val["quantity"]);
                    } 
                } else {
                    echo('Cart is empty');
                }
            ?>
        <div class="row">
            <div class="col-12 text-right mt-4">
                <p class="sub-total">Total <span class="ml-4" id="total-Price"><?= Number::currency($total_price, 'USD'); ?></span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right my-4">
            <?= $this->Html->link('checkout',
                ['controller' => 'carts', 'action' => 'checkout', 'prefix' => false],
                [
                'class' => 'btn bg-dark text-white py-2 px-4 font-weight-500'
                ]
                )?>
              <!--   <button class="btn bg-dark text-white py-2 px-4 font-weight-500">checkout</button> -->
            </div>
        </div>
    </div>
</div>
 <?= 
        $this->Html->script([
            //'frontend/jquery-3.3.1.min',
            'frontend/cart/update_cart'
        ], ['block' => true]) 
    ?>