<?php
    use Cake\I18n\Number; 
    $totalItemInCart = 0;
    $session = $this->request->getSession();
    $totalItemInCart+= count($session->read('cart'));
    $cartitem = $session->read('cart');
?>
<div class="container my-5">
    <div class="row">
        <div class="col-md-4 order-md-2 mb-4 cart-add">
            <h3 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Your cart</span>
                <span class="badge badge-secondary badge-pill"><?= $totalItemInCart?></span>
            </h3>
            <ul class="list-group mb-3">
            <?php
                $total_price = 0; 
                if(!empty($cartitem)) {
                    //pr();
                    foreach($cartitem as $key => $val) {
                        //$item_price = $val["quantity"]*$val["price"];
            ?>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0"><?= $val['name']?></h6>
                        <small class="text-muted">Brief description</small>
                    </div>
                    <span class="text-muted"><?= Number::currency($val["subtotal"],'USD') ?></span>
                </li>
            <?php
               $total_price += ($val["price"]*$val["quantity"]);
                    } 
                } else {
                    echo('Cart is empty');
                }
            ?>
                <li class="list-group-item d-flex justify-content-between">
                    <span>Total (USD)</span>
                    <strong><?= Number::currency($total_price,'USD')?></strong>
                </li>
            </ul>
        </div>
        <div class="col-md-8 order-md-1 bg-white billing-address py-4">
            <h3 class="mb-3">Billing address</h3>
            <?php
                echo $this->Form->create(null, [
                    'url' => [
                        'controller' => 'carts',
                        'action' => 'checkout',
                        'prefix' => false
                    ],
                    'id' => 'order-placed-form',
                    'class' => 'needs-validation',
                    'type' => 'file'
                ]);
            ?>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">First name</label>
                        <?php
                            echo $this->Form->control('first_name', [
                                'class' => 'form-control',
                                'label' => false,
                                'id' => 'firstName',
                                'required' => true
                            ]);
                        ?>
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Last name</label>
                        <?php
                            echo $this->Form->control('last_name', [
                                'class' => 'form-control',
                                'label' => false,
                                'id' => 'lastName',
                                'required' => true
                            ]);
                        ?>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="email">Email</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="email" class="form-control" id="email" placeholder="email" required="">
                        <div class="invalid-feedback" style="width: 100%;">
                            Please enter a valid email address for shipping updates.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="address">Address</label>
                    <?php
                        echo $this->Form->control('address1', [
                            'class' => 'form-control',
                            'label' => false,
                            'id' => 'address1',
                            'required' => true,
                            'placeholder' => "1234 Main St"
                        ]);
                    ?>
                    <div class="invalid-feedback">
                        Please enter your shipping address.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                    <?php
                        echo $this->Form->control('address2', [
                            'class' => 'form-control',
                            'label' => false,
                            'id' => 'address2',
                            'required' => true,
                            'placeholder' => "Apartment or suite"
                        ]);
                    ?>
                </div>

                <div class="row">
                    <div class="col-md-5 mb-3">
                        <label for="country">Country</label>
                        <select class="custom-select d-block w-100" id="country" required="">
                            <option value="">Choose...</option>
                            <option>United States</option>
                        </select>
                        <div class="invalid-feedback">
                            Please select a valid country.
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="state">State</label>
                        <select class="custom-select d-block w-100" id="state" required="">
                            <option value="">Choose...</option>
                            <option>California</option>
                        </select>
                        <div class="invalid-feedback">
                            Please provide a valid state.
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="zip">Zip</label>
                        <?php
                            echo $this->Form->control('zipcode', [
                                'class' => 'form-control',
                                'label' => false,
                                'id' => 'zip',
                                'required' => true,
                                'placeholder' => "zipcode"
                            ]);
                        ?>
                        <div class="invalid-feedback">
                            Zip code required.
                        </div>
                    </div>
                </div>
                <hr class="mb-4">
                <h3 class="mb-3">Payment</h3>
                <!-- <div class="d-block my-3">
                    <div class="custom-control custom-radio">
                        <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked=""
                            required="">
                        <label class="custom-control-label" for="credit">Credit card</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required="">
                        <label class="custom-control-label" for="debit">Debit card</label>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="cc-name">Name on card</label>
                        <input type="text" class="form-control" id="cc-name" placeholder="" required="">
                        <small class="text-muted">Full name as displayed on card</small>
                        <div class="invalid-feedback">
                            Name on card is required
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="cc-number">Credit card number</label>
                        <input type="text" class="form-control" id="cc-number" placeholder="" required="">
                        <div class="invalid-feedback">
                            Credit card number is required
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="cc-expiration">Expiration</label>
                        <input type="text" class="form-control" id="cc-expiration" placeholder="" required="">
                        <div class="invalid-feedback">
                            Expiration date required
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="cc-cvv">CVV</label>
                        <input type="text" class="form-control" id="cc-cvv" placeholder="" required="">
                        <div class="invalid-feedback">
                            Security code required
                        </div>
                    </div>
                </div>
                <hr class="mb-4">
                <button class="btn bg-dark text-white btn-lg btn-block checkout-btn" type="submit">
                    Continue to checkout
                </button>
             <?= $this->Form->end(); ?>
        </div>
    </div>
</div>