<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
        ]);
    ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>
</head>
    <body>
        <!--Start include fan manu -->
        <?= $this->element('Fan/fan_menu');?>
        <!--end fan menu -->
        <header>
        <?= $this->Flash->render();?>
        <?= $this->element('User/unauth_header');?>
        </header>
        <?= $this->Flash->render();?>
        <?= $this->fetch('content');?>
        <?= $this->element('User/unauth_footer');?>
        <!-- Start Sticky footer for mobile -->
        <?php 
            $controller= $this->request->getParam('controller');
            $action = $this->request->getParam('action');
            if($controller !='Catalogues' && $action != 'artistDetails') {
                echo $this->element('User/sticky_footer');
            }
        ?>
    <!-- End Sticky footer for mobile -->
    </body>
    <?= $this->Html->script('frontend/jquery-3.3.1.min');?>
    <?= $this->Html->script('frontend/bootstrap.bundle.min');?>
    <?= $this->Html->script('common/jquery.validate');?>
    <?= $this->Html->script('common/additional-methods');?>
    <?php
        $this->Html->scriptStart(['block' => true]);
        echo 'window.csrf = "'.$this->request->getParam("_csrfToken").'";';
        echo 'window.url = "'.$this->Url->build('/',true).'";';
        echo 'window.stipePublickey = "'.env('STRIPE_CLIENT_PublishableKey').'";';
        $this->Html->scriptEnd();
        echo $this->fetch('script');
        echo $this->fetch('scriptBottom');
    ?>

    <script>
         $(document).ready(function () {
            $(' .type-box').on('click', function () {
                $('[name="pricefield"]').attr('checked', false);
                let radioInput = $(this).find('input[type="radio"]');
                radioInput.attr('checked', true);
                $(".type-box").removeClass('active');
                $(this).addClass('active');
            });
        });
    </script>
</html>