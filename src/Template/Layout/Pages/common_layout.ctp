<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
      <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
        ]);
    ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>

</head>

<body class="d-flex flex-column h-100">
    <!--Start include fan manu -->
        <?= $this->element('Fan/fan_menu');?>
    <!--end fan menu -->

    <header>
        <?= $this->Flash->render();?>
        <?= $this->element('User/unauth_header');?>
    </header>
     <!-- <?= h($this->fetch('title')) ?> -->
    <?= $this->fetch('content');?>
    <!-- footer -->
    <?= $this->element('User/unauth_footer');?>

    <!-- Start Sticky footer for mobile -->
        <?= $this->element('User/sticky_footer');?>
    <!-- End Sticky footer for mobile -->

    
    <?=
        $this->Html->script([
            'frontend/jquery-3.3.1.min',
            'frontend/bootstrap.bundle.min',
            'frontend/owl.carousel',
            'Catalogues/index',
        ], ['block' => true])
    ?>

    <?php
        $this->Html->scriptStart(['block' => true]);
        echo 'window.csrf = "'.$this->request->getParam("_csrfToken").'";';
        echo 'window.url = "'.$this->Url->build('/',true).'";';
        $this->Html->scriptEnd();
        echo $this->fetch('script');
        echo $this->fetch('scriptBottom');
    ?>
    <script>
        $(document).on('click', '.show-partner-layout', function () {
                $('#show-partner-modal').removeClass('d-none');
        });

        $(document).on('click', '.close', function () {
            $('#show-partner-modal').addClass('d-none');
        });
    </script>
</body>

</html>