<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sound Check Live: Dashboard</title>

  <!-- Custom fonts for this template-->
  <?= $this->Html->css('vendor/all.min'); ?>
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">


  <!-- Custom styles for this template-->
  <?= $this->Html->css('sb-admin-2'); ?>
  <?= $this->Html->css('backend/common/bootstrap4-toggle.min'); ?>
  <?= $this->Html->css('backend/common/jquery.filer'); ?>
  <?= $this->Html->css('backend/common/jquery.filer-dragdropbox-theme'); ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

	<!-- Sidebar -->
	<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">
	  <!-- Sidebar - Brand -->
	  <?php 
		  	echo $this->Html->link(
			    $this->Html->image("logo/logo.png", ["alt" => "soundchecklive-logo"]),
			    [
			    	'controller' => 'users',
			    	'action' => 'index',
			    	'prefix' => 'admin'
			    ],
			    [
			    	'escape' => false,
			    	'class' => 'sidebar-brand d-none d-md-flex align-items-center justify-content-center mt-3 mb-4'
			    ]
			);
	  ?>

	  <?php 
		  	echo $this->Html->link(
			    $this->Html->image("mob-logo.png", ["alt" => "soundchecklive-logo"]),
			    [
			    	'controller' => 'users',
			    	'action' => 'index',
			    	'prefix' => 'admin'
			    ],
			    [
			    	'escape' => false,
			    	'class' => 'sidebar-brand d-flex d-md-none align-items-center justify-content-center mt-3 mb-4'
			    ]
			);
	  ?>

	  <!-- Divider -->
	  <hr class="sidebar-divider my-0">

	  <!-- Nav Item - Dashboard -->
	  <li class="nav-item">
		 <?php 
			echo $this->Html->link(
			    '<i class="fas fa-fw fa-tachometer-alt"></i>
		  <span>Dashboard</span></a>',
			    [
			    	'controller' => 'Users',
					'action' => 'index',
					'prefix' => 'admin'
				],
			    [
			    	'class' => 'nav-link',
			    	'escape' => false,
			    ]
			);
		?>
	  </li>

	  <!-- Divider -->
	  <hr class="sidebar-divider">

	  <!-- Nav Item - Pages Collapse Menu -->
	  <li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
		  <i class="fa fa-user"></i>
		  <span>Manage Artist</span>
		</a>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
		  <div class="bg-white py-2 collapse-inner rounded">
			<!-- <a class="collapse-item" href="buttons.html">Approved</a> -->
			<?php 

				echo $this->Html->link(
					'Pending',
					['controller' => 'Users', 'action' => 'pendingForApproval', 'prefix' => 'admin'],
					['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Approved',
				    ['controller' => 'Users', 'action' => 'approvedUser', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Trash',
				    ['controller' => 'Users', 'action' => 'trashedUsers', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Add Artist',
				    ['controller' => 'Users', 'action' => 'addUser', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

			?>
		  </div>
		</div>
	  </li>
	  <!-- Divider -->
	  <hr class="sidebar-divider mt-2">
	  <li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo2">
		  <i class="fa fa-user"></i>
		  <span>Manage Film Director</span>
		</a>
		<div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2" data-parent="#accordionSidebar">
		  <div class="bg-white py-2 collapse-inner rounded">
			<!-- <a class="collapse-item" href="buttons.html">Approved</a> -->
			<?php 

				echo $this->Html->link(
					'Pending',
					['controller' => 'Users', 'action' => 'pendingFilmDirector', 'prefix' => 'admin'],
					['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Approved',
				    ['controller' => 'Users', 'action' => 'approvedFilmDirector', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'SCL Originals Film Director',
				    ['controller' => 'Users', 'action' => 'addFilmDirector', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				/*echo $this->Html->link(
				    'Add Artist',
				    ['controller' => 'Users', 'action' => 'addUser', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);*/

			?>
		  </div>
		</div>
	  </li>

	  <hr class="sidebar-divider mt-2">
	  <li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo22" aria-expanded="true" aria-controls="collapseTwo22">
		  <i class="fa fa-user"></i>
		  <span>Fashion Designer</span>
		</a>
		<div id="collapseTwo22" class="collapse" aria-labelledby="headingTwo22" data-parent="#accordionSidebar">
		  <div class="bg-white py-2 collapse-inner rounded">
			<!-- <a class="collapse-item" href="buttons.html">Approved</a> -->
			<?php 

				echo $this->Html->link(
					'Pending',
					['controller' => 'Users', 'action' => 'pendingFashionDesigner', 'prefix' => 'admin'],
					['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Approved',
				    ['controller' => 'Users', 'action' => 'approvedFashionDesigner', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'SCL Fashion Designer',
				    ['controller' => 'Users', 'action' => 'addFashionDesigner', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);
			?>
		  </div>
		</div>
	  </li>
	  <hr class="sidebar-divider mt-2">
	  <li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseInfluencer" aria-expanded="true" aria-controls="collapseInfluencer">
		  <i class="fa fa-user"></i>
		  <span>Manage Influencer</span>
		</a>
		<div id="collapseInfluencer" class="collapse" aria-labelledby="headingInfluencer" data-parent="#accordionSidebar">
		  <div class="bg-white py-2 collapse-inner rounded">
			<!-- <a class="collapse-item" href="buttons.html">Approved</a> -->
			<?php 

				echo $this->Html->link(
					'Pending',
					['controller' => 'Influencers', 'action' => 'pendingInfluencer', 'prefix' => 'admin'],
					['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Approved',
				    ['controller' => 'Influencers', 'action' => 'approvedInfluencers', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);
			?>
		  </div>
		</div>
	  </li>
	  <hr class="sidebar-divider mt-2">

	  <li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePartners" aria-expanded="true" aria-controls="collapsePartners">
		  <i class="fa fa-user"></i>
		  <span>Manage Partners</span>
		</a>
		<div id="collapsePartners" class="collapse" aria-labelledby="headingPartners" data-parent="#accordionSidebar">
		  <div class="bg-white py-2 collapse-inner rounded">
			<!-- <a class="collapse-item" href="buttons.html">Approved</a> -->
			<?php 

				echo $this->Html->link(
					'Pending',
					['controller' => 'PartnerProfile', 'action' => 'pendingPartners', 'prefix' => 'admin'],
					['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Approved',
				    ['controller' => 'PartnerProfile', 'action' => 'approvedPartners', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);
			?>
		  </div>
		</div>
	  </li>
	  <hr class="sidebar-divider mt-2">

	  <!-- Nav Item - Pages Collapse Menu -->
	  <li class="nav-item">
		 <?php 
			echo $this->Html->link(
			    '<i class="fa fa-microphone"></i>
		  <span>Sound Check Live Original</span></a>',
			    [
			    	'controller' => 'UserProfiles',
					'action' => 'index',
					'prefix' => 'admin'
				],
			    [
			    	'class' => 'nav-link',
			    	'escape' => false,
			    ]
			);
		?>
	  </li>
	  <!-- Divider -->
	  <hr class="sidebar-divider">
	 <li class="nav-item">
		 <?php 
			echo $this->Html->link(
			    '<i class="fa fa-cart-plus"></i>
		  <span>Merchandise</span></a>',
			    [
			    	'controller' => 'UserMerchandises',
					'action' => 'index',
					'prefix' => 'admin'
				],
			    [
			    	'class' => 'nav-link',
			    	'escape' => false,
			    ]
			);
		?>
	  </li>
	  <hr class="sidebar-divider">
		  <li class="nav-item">
			 <?php 
				echo $this->Html->link(
				    '<i class="fas fa-dollar-sign"></i>
			  <span>Proof of Concepts</span></a>',
				    [
				    	'controller' => 'ProofOfConcepts',
						'action' => 'index',
						'prefix' => 'admin'
					],
				    [
				    	'class' => 'nav-link',
				    	'escape' => false,
				    ]
				);
			?>
		  </li>
	  <hr class="sidebar-divider">
	  <li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseeight" aria-expanded="true" aria-controls="collapseeight">
		  <i class="fas fa-fw fa-cog"></i>
		  <span>Settings</span>
		</a>
		<div id="collapseeight" class="collapse" aria-labelledby="headingeight" data-parent="#accordionSidebar">
		  <div class="bg-white py-2 collapse-inner rounded">
			<!-- <a class="collapse-item" href="buttons.html">Approved</a> -->
			<?php 
				echo $this->Html->link(
				    'Manage Color',
				    ['controller' => 'Colors', 'action' => 'index', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'Manage Size',
				    ['controller' => 'Sizes', 'action' => 'index', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    'How We Work',
				    ['controller' => 'Users', 'action' => 'howWeWorks', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    ' Artist Pitch of the Week',
				    ['controller' => 'ArtistPitchVideos', 'action' => 'pitchVideo', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    ' Disable Layout',
				    ['controller' => 'ArtistPitchVideos', 'action' => 'disable_layout', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);
				echo $this->Html->link(
				    ' Manage Levels',
				    ['controller' => 'Levels', 'action' => 'index', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);

				echo $this->Html->link(
				    ' Manage Specific People',
				    ['controller' => 'Peoples', 'action' => 'index', 'prefix' => 'admin'],
				    ['class' => 'collapse-item']
				);
			?>
		  </div>
		</div>
	  </li>
	  <hr class="sidebar-divider">
	   <li class="nav-item">
		 <?php 
			echo $this->Html->link(
			    '<i class="fa fa-power-off"></i>
		  <span>Logout</span></a>',
			    [
			    	'controller' => 'Users',
					'action' => 'logout',
					'prefix' => 'admin'
				],
			    [
			    	'class' => 'nav-link',
			    	'escape' => false,
			    ]
			);
		?>
	  </li>

	</ul>
	<!-- End of Sidebar -->

	<!-- Content Wrapper -->
	<div id="content-wrapper" class="d-flex flex-column">

	  <!-- Main Content -->
	  <div id="content">

		<!-- Topbar -->
		<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

		  <!-- Sidebar Toggle (Topbar) -->
		  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
			<i class="fa fa-bars"></i>
		  </button>

		  <!-- Topbar Search -->
		 <!--  <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
			<div class="input-group">
			  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
			  <div class="input-group-append">
				<button class="btn btn-primary" type="button">
				  <i class="fas fa-search fa-sm"></i>
				</button>
			  </div>
			</div>
		  </form> -->

		  <!-- Topbar Navbar -->
		  <ul class="navbar-nav ml-auto">

			<!-- Nav Item - Search Dropdown (Visible Only XS) -->
			<li class="nav-item dropdown no-arrow d-sm-none">
			  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-search fa-fw"></i>
			  </a>
			  <!-- Dropdown - Messages -->
			  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
				<form class="form-inline mr-auto w-100 navbar-search">
				  <div class="input-group">
					<input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
					<div class="input-group-append">
					  <button class="btn btn-primary" type="button">
						<i class="fas fa-search fa-sm"></i>
					  </button>
					</div>
				  </div>
				</form>
			  </div>
			</li>

			<div class="topbar-divider d-none d-sm-block"></div>

			<!-- Nav Item - User Information -->
			<li class="nav-item dropdown no-arrow">
			  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="mr-2 d-none d-lg-inline text-gray-600 small"><strong>Welcome Admin</strong></span>
				<!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
			  </a>
			  <!-- Dropdown - User Information -->
			  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
				<?php 
					echo $this->Html->link(
					    '<i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>'.__('Change Password'),
					    [
					    	'#'
						],
					    [
					    	'class' => 'dropdown-item ',
					    	'escape' => false,
					    	'data-toggle'=>'modal',
					    	'data-target'=> '#exampleModal'
					    ]
					);
				?>
				<div class="dropdown-divider"></div>
				<?php 
					echo $this->Html->link(
					    '<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>'.__('Logout'),
					    [
					    	'controller' => 'Users',
							'action' => 'logout',
							'prefix' => 'admin'
						],
					    [
					    	'class' => 'dropdown-item ',
					    	'escape' => false,
					    ]
					);
				?>

			  </div>
			</li>

		  </ul>

		</nav>
		<!-- End of Topbar -->

		<!-- Begin Page Content -->
		<div class="container-fluid">

		  <!-- Page Heading -->
		  <?php 
				echo $this->Flash->render();
             	echo $this->fetch('content');
             	echo $this->element('Model/change_password');
           ?>

		</div>
		<!-- /.container-fluid -->

	  </div>
	  <!-- End of Main Content -->

	  <!-- Footer -->
	  <footer class="sticky-footer bg-white">
		<div class="container my-auto">
		  <div class="copyright text-center my-auto">
			<span>Copyright &copy; Your Website 2019</span>
		  </div>
		</div>
	  </footer>
	  <!-- End of Footer -->

	</div>
	<!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
		  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		  </button>
		</div>
		<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
		<div class="modal-footer">
		  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
		  <a class="btn btn-primary" href="login.html">Logout</a>
		</div>
	  </div>
	</div>
  </div>

  <!-- Bootstrap core JavaScript-->
 <?php
    echo $this->Html->script([
      'vendor/jquery.min',
      'vendor/bootstrap.bundle.min',
      'common/bootstrap4-toggle.min',
      'common/jquery.validate',
      'common/additional-methods',
      'common/jquery.filer',
    ]);
    echo $this->fetch('script');
?>

  <!-- Core plugin JavaScript-->
<?php 
    echo $this->Html->script('vendor/jquery.easing.min');
?>
  <!-- Custom scripts for all pages-->
  <!-- Custom scripts for all pages-->
<?php 
    echo $this->Html->script('sb-admin-2.min');
?>
<?php
        $this->Html->scriptStart(['block' => true]);
        echo 'window.csrf = "'.$this->request->getParam("_csrfToken").'";';
        echo 'window.url = "'.$this->Url->build('/',true).'";';
        $this->Html->scriptEnd();
        echo $this->fetch('script');
        echo $this->fetch('scriptBottom');
	?>
	
	<script type="text/javascript">
	$(document).ready(function() {
	    // For each table within the content area...
	    $('table').each(function(t) {
	        // Add a unique id if one doesn't exist.
	        if (!this.id) {
	            this.id = 'table_' + t;
	        }
	        // Prepare empty variables.
	        var headertext = [],
	            theads = document.querySelectorAll('#' + this.id + ' thead'),
	            headers = document.querySelectorAll('#' + this.id + ' th'),
	            tablerows = document.querySelectorAll('#' + this.id + ' th'),
	            tablebody = document.querySelector('#' + this.id + ' tbody');
	        // For tables with theads...
	        for (var i = 0; i < theads.length; i++) {
	            // If they have more than 2 columns...
	            if (headers.length > 2) {
	                // Add a responsive class.
	                this.classList.add('responsive');
	                // Get the content of the appropriate th.
	                for (var i = 0; i < headers.length; i++) {
	                    var current = headers[i];
	                    headertext.push(current.textContent.replace(/\r?\n|\r/, ''));
	                }
	                // Apply that as a data-th attribute on the corresponding cells.
	                for (var i = 0, row; row = tablebody.rows[i]; i++) {
	                    for (var j = 0, col; col = row.cells[j]; j++) {
	                        col.setAttribute('data-th', headertext[j]);
	                    }
	                }
	            }
	        }
	    });
	});
</script>


</body>

</html>