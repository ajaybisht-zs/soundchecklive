<?php 
    $session = $this->getRequest()->getSession();
    $userInfo  = $session->read('Auth.User');
    $userId = $session->read('Auth.User.id');
?>
<!DOCTYPE html>
<html class="h-100">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= (!empty($title)) ?$title :'SoundCheckLive' ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap 4 -->
        <?= 
            $this->Html->css([
                'frontend/bootstrap.min',
                'frontend/main',
                'frontend/base',
                'frontend/nav',
                'frontend/style',
                'frontend/admin',
                'frontend/jside-menu',
                'backend/common/jquery.filer',
                'backend/common/jquery.filer-dragdropbox-theme'
            ], [
                'block' => true
            ]); 
        ?>
        <?= $this->fetch('css') ?>
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
        <!-- fonts icon     -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175926688-1"></script>
        <?= $this->Html->script('common/analatics') ?>
    </head>
    <body class="d-flex flex-column h-100">

        <?= $this->element('Navs/Artist/header') ?>
        <?= $this->element('Navs/Artist/sidebar') ?>
        
        <div class="container container-content">
            <div class="row">
                <?= $this->element('Navs/Artist/sidebar_desktop') ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
       
        <?= $this->element('Navs/Artist/footer')?> 

        <?= $this->fetch('modal') ?>
        <?= $this->element('Model/call_accept')?>
        <!-- Loding script files -->
        <?php 
            $this->Html->scriptStart(['block' => true]);
                echo 'window.csrf = "'.$this->request->getCookie('csrfToken').'";';
                echo 'window.url = "'.$this->Url->build('/',true).'";';
                echo 'window.userId = "'.$userId.'";';
            $this->Html->scriptEnd();
        ?>

        <?= 
         $this->Html->script([
            'frontend/jquery-3.3.1.min',
            'frontend/bootstrap.bundle.min',
            'frontend/jquery.jside.menu',
            'common/jquery.validate',
            'common/additional-methods',
            'common/jquery.filer',
            'common',
            'https://js.pusher.com/7.0/pusher.min.js',
            'common/notification',
            'https://js.pusher.com/beams/1.0/push-notifications-cdn.js',
            'common/beam'
         ], ['block' => true]) 
        ?>

        <?= $this->fetch('script') ?>
        <?= $this->fetch('scriptBottom') ?>

        <script type="text/javascript">            
            $(function () {
                $(".menu-container").jSideMenu({
                    jSidePosition: "position-left", //possible options position-left or position-right
                    jSideSticky: true, // menubar will be fixed on top, false to set static
                    jSideSkin: "default-skin", // to apply custom skin, just put its name in this string
                });
            }); 
        </script>
   </body>
</html>