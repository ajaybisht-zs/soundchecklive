<?php 
    $session = $this->getRequest()->getSession();
    $userInfo  = $session->read('Auth.User');
    $userId = $session->read('Auth.User.id');
?>
<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <!-- bootstrap css -->
    <?= $this->Html->css('frontend/bootstrap.min'); ?>
    <?= $this->Html->css('frontend/main'); ?>
    <!-- base css -->
    <?= $this->Html->css('frontend/base'); ?>
    <!-- navigation  -->
    <?= $this->Html->css('frontend/nav'); ?> 
    <!-- custom css -->
    <?= $this->Html->css('frontend/style'); ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175926688-1"></script>
    <?= $this->Html->script('common/analatics') ?>
</head>

<body class="d-flex flex-column h-100" id="page-top">
    <!--Start include fan manu -->
        <?= $this->element('Fan/fan_menu');?>
    <!--end fan menu -->

    <header>
        <?= $this->Flash->render();?>
        <?= $this->element('User/unauth_header');?>
    </header>
    <?= $this->fetch('content');?>
    <!-- footer -->
    <?= $this->element('User/unauth_footer');?>
    <?= $this->element('Model/call_accept')?>

    <!-- Start Sticky footer for mobile -->
        <?php 
            $controller= $this->request->getParam('controller');
            $action = $this->request->getParam('action');
            if($controller !='Catalogues' && $action != 'artistDetails') {
                echo $this->element('User/sticky_footer');
            }
        ?>
    <!-- End Sticky footer for mobile -->

    <?= $this->Html->script('frontend/jquery-3.3.1.min');?>
    <?= $this->Html->script('frontend/bootstrap.bundle.min');?>
    <?= $this->Html->script('common/jquery.validate');?>
    <?= $this->Html->script('common/additional-methods');?>
    <?= $this->Html->script('frontend/jquery.easing.min');?>
     <?= $this->Html->script('frontend/scrolling-nav');?>
     <?= $this->Html->script('common');?>
     <?= $this->Html->script('https://js.pusher.com/7.0/pusher.min.js');?>
     <?= $this->Html->script('common/notification');?>

    <?php
        $this->Html->scriptStart(['block' => true]);
        echo 'window.csrf = "'.$this->request->getParam("_csrfToken").'";';
        echo 'window.url = "'.$this->Url->build('/',true).'";';
        echo 'window.userId = "'.$userId.'";';

        $this->Html->scriptEnd();
        echo $this->fetch('script');
        echo $this->fetch('scriptBottom');
    ?>
    <script>
        $(document).on('click', '.show-partner-layout', function () {
                $('#show-partner-modal').removeClass('d-none');
        });

        $(document).on('click', '.close', function () {
            $('#show-partner-modal').addClass('d-none');
        });
        $(function() {

$(".progress").each(function() {

  var value = $(this).attr('data-value');
  var left = $(this).find('.progress-left .progress-bar');
  var right = $(this).find('.progress-right .progress-bar');

  if (value > 0) {
    if (value <= 50) {
      right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
    } else {
      right.css('transform', 'rotate(180deg)')
      left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
    }
  }

})

function percentageToDegrees(percentage) {

  return percentage / 100 * 360

}

});


     
    </script>
</body>

</html>