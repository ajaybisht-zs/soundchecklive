<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Profiles'), ['controller' => 'UserProfiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Profile'), ['controller' => 'UserProfiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Media Handles'), ['controller' => 'UserMediaHandles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Media Handle'), ['controller' => 'UserMediaHandles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Bank Details'), ['controller' => 'UserBankDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Bank Detail'), ['controller' => 'UserBankDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Plans'), ['controller' => 'UserPlans', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Plan'), ['controller' => 'UserPlans', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Records'), ['controller' => 'UserRecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Record'), ['controller' => 'UserRecords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Events'), ['controller' => 'UserEvents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Event'), ['controller' => 'UserEvents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['controller' => 'UserMerchandises', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['controller' => 'UserMerchandises', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('role_id', ['options' => $roles]);
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('status');
            echo $this->Form->control('forgot_token');
            echo $this->Form->control('stripe_customer_id');
            echo $this->Form->control('step_completed');
            echo $this->Form->control('is_deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
