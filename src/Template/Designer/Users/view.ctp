<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Profiles'), ['controller' => 'UserProfiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Profile'), ['controller' => 'UserProfiles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Media Handles'), ['controller' => 'UserMediaHandles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Media Handle'), ['controller' => 'UserMediaHandles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Bank Details'), ['controller' => 'UserBankDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Bank Detail'), ['controller' => 'UserBankDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Plans'), ['controller' => 'UserPlans', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Plan'), ['controller' => 'UserPlans', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Records'), ['controller' => 'UserRecords', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Record'), ['controller' => 'UserRecords', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Events'), ['controller' => 'UserEvents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Event'), ['controller' => 'UserEvents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['controller' => 'UserMerchandises', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['controller' => 'UserMerchandises', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->id, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Forgot Token') ?></th>
            <td><?= h($user->forgot_token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stripe Customer Id') ?></th>
            <td><?= h($user->stripe_customer_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Profile') ?></th>
            <td><?= $user->has('user_profile') ? $this->Html->link($user->user_profile->id, ['controller' => 'UserProfiles', 'action' => 'view', $user->user_profile->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Media Handle') ?></th>
            <td><?= $user->has('user_media_handle') ? $this->Html->link($user->user_media_handle->id, ['controller' => 'UserMediaHandles', 'action' => 'view', $user->user_media_handle->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Bank Detail') ?></th>
            <td><?= $user->has('user_bank_detail') ? $this->Html->link($user->user_bank_detail->id, ['controller' => 'UserBankDetails', 'action' => 'view', $user->user_bank_detail->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Plan') ?></th>
            <td><?= $user->has('user_plan') ? $this->Html->link($user->user_plan->id, ['controller' => 'UserPlans', 'action' => 'view', $user->user_plan->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step Completed') ?></th>
            <td><?= $this->Number->format($user->step_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $user->status ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $user->is_deleted ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related User Records') ?></h4>
        <?php if (!empty($user->user_records)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Record Name') ?></th>
                <th scope="col"><?= __('Record Written By') ?></th>
                <th scope="col"><?= __('Is Already Distributed') ?></th>
                <th scope="col"><?= __('Cover Image') ?></th>
                <th scope="col"><?= __('Cover Image Dir') ?></th>
                <th scope="col"><?= __('Artist Name') ?></th>
                <th scope="col"><?= __('Features') ?></th>
                <th scope="col"><?= __('Producer') ?></th>
                <th scope="col"><?= __('Writer') ?></th>
                <th scope="col"><?= __('Record File') ?></th>
                <th scope="col"><?= __('Record Dir') ?></th>
                <th scope="col"><?= __('Video Pitch File') ?></th>
                <th scope="col"><?= __('Video Pitch Path') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('No Of Partners') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->user_records as $userRecords): ?>
            <tr>
                <td><?= h($userRecords->id) ?></td>
                <td><?= h($userRecords->user_id) ?></td>
                <td><?= h($userRecords->record_name) ?></td>
                <td><?= h($userRecords->record_written_by) ?></td>
                <td><?= h($userRecords->is_already_distributed) ?></td>
                <td><?= h($userRecords->cover_image) ?></td>
                <td><?= h($userRecords->cover_image_dir) ?></td>
                <td><?= h($userRecords->artist_name) ?></td>
                <td><?= h($userRecords->features) ?></td>
                <td><?= h($userRecords->producer) ?></td>
                <td><?= h($userRecords->writer) ?></td>
                <td><?= h($userRecords->record_file) ?></td>
                <td><?= h($userRecords->record_dir) ?></td>
                <td><?= h($userRecords->video_pitch_file) ?></td>
                <td><?= h($userRecords->video_pitch_path) ?></td>
                <td><?= h($userRecords->created) ?></td>
                <td><?= h($userRecords->modified) ?></td>
                <td><?= h($userRecords->no_of_partners) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserRecords', 'action' => 'view', $userRecords->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserRecords', 'action' => 'edit', $userRecords->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserRecords', 'action' => 'delete', $userRecords->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userRecords->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Events') ?></h4>
        <?php if (!empty($user->user_events)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('City') ?></th>
                <th scope="col"><?= __('Event Date') ?></th>
                <th scope="col"><?= __('Event Time') ?></th>
                <th scope="col"><?= __('Link For Ticket') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Image Dir') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->user_events as $userEvents): ?>
            <tr>
                <td><?= h($userEvents->id) ?></td>
                <td><?= h($userEvents->user_id) ?></td>
                <td><?= h($userEvents->city) ?></td>
                <td><?= h($userEvents->event_date) ?></td>
                <td><?= h($userEvents->event_time) ?></td>
                <td><?= h($userEvents->link_for_ticket) ?></td>
                <td><?= h($userEvents->image) ?></td>
                <td><?= h($userEvents->image_dir) ?></td>
                <td><?= h($userEvents->created) ?></td>
                <td><?= h($userEvents->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserEvents', 'action' => 'view', $userEvents->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserEvents', 'action' => 'edit', $userEvents->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserEvents', 'action' => 'delete', $userEvents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userEvents->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Merchandises') ?></h4>
        <?php if (!empty($user->user_merchandises)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->user_merchandises as $userMerchandises): ?>
            <tr>
                <td><?= h($userMerchandises->id) ?></td>
                <td><?= h($userMerchandises->user_id) ?></td>
                <td><?= h($userMerchandises->name) ?></td>
                <td><?= h($userMerchandises->price) ?></td>
                <td><?= h($userMerchandises->created) ?></td>
                <td><?= h($userMerchandises->modified) ?></td>
                <td><?= h($userMerchandises->description) ?></td>
                <td><?= h($userMerchandises->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserMerchandises', 'action' => 'view', $userMerchandises->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserMerchandises', 'action' => 'edit', $userMerchandises->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserMerchandises', 'action' => 'delete', $userMerchandises->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandises->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
