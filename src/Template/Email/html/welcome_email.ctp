<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	  <!--[if !mso]><!-->
	  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	  <!--<![endif]-->
	  <!--[if (gte mso 9)|(IE)]>
	  <xml>
		<o:OfficeDocumentSettings>
		  <o:AllowPNG/>
		  <o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	  </xml>
	  <![endif]-->
	  <!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
	body {width: 600px;margin: 0 auto;}
	table {border-collapse: collapse;}
	table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
	img {-ms-interpolation-mode: bicubic;}
  </style>
<![endif]-->
	  <style type="text/css">
	body, p, div {
	  font-family: arial,helvetica,sans-serif;
	  font-size: 14px;
	}
	body {
	  color: #000000;
	}
	body a {
	  color: #1188E6;
	  text-decoration: none;
	}
	p { margin: 0; padding: 0; }
	table.wrapper {
	  width:100% !important;
	  table-layout: fixed;
	  -webkit-font-smoothing: antialiased;
	  -webkit-text-size-adjust: 100%;
	  -moz-text-size-adjust: 100%;
	  -ms-text-size-adjust: 100%;
	}
	img.max-width {
	  max-width: 100% !important;
	}
	.column.of-2 {
	  width: 50%;
	}
	.column.of-3 {
	  width: 33.333%;
	}
	.column.of-4 {
	  width: 25%;
	}
	@media screen and (max-width:480px) {
	  .preheader .rightColumnContent,
	  .footer .rightColumnContent {
		text-align: left !important;
	  }
	  .preheader .rightColumnContent div,
	  .preheader .rightColumnContent span,
	  .footer .rightColumnContent div,
	  .footer .rightColumnContent span {
		text-align: left !important;
	  }
	  .preheader .rightColumnContent,
	  .preheader .leftColumnContent {
		font-size: 80% !important;
		padding: 5px 0;
	  }
	  table.wrapper-mobile {
		width: 100% !important;
		table-layout: fixed;
	  }
	  img.max-width {
		height: auto !important;
		max-width: 100% !important;
	  }
	  a.bulletproof-button {
		display: block !important;
		width: auto !important;
		font-size: 80%;
		padding-left: 0 !important;
		padding-right: 0 !important;
	  }
	  .columns {
		width: 100% !important;
	  }
	  .column {
		display: block !important;
		width: 100% !important;
		padding-left: 0 !important;
		padding-right: 0 !important;
		margin-left: 0 !important;
		margin-right: 0 !important;
	  }
	  .social-icon-column {
		display: inline-block !important;
	  }
	}
  </style>
	  <!--user entered Head Start--><!--End Head user entered-->
	</head>
	<body>
	  <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size:14px; font-family:arial,helvetica,sans-serif; color:#000000; background-color:#FFFFFF;">
		<div class="webkit">
		  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#FFFFFF">
			<tr>
			  <td valign="top" bgcolor="#FFFFFF" width="100%">
				<table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
				  <tr>
					<td width="100%">
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
						  <td>
							<!--[if mso]>
	<center>
	<table><tr><td width="600">
  <![endif]-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width:600px;" align="center">
									  <tr>
										<td role="modules-container" style="padding:0px 0px 0px 0px; color:#000000; text-align:left;" bgcolor="#FFFFFF" width="100%" align="left"><table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
	<tr>
	  <td role="module-content">
		<p>Important information.</p>
	  </td>
	</tr>
  </table><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="3d0bfd0d-5cc1-4cc5-a8fd-6d5656b1f98a">
	<tbody>
	  <tr>
		<td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="right">
		  
			<a href="#">
				<img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:15% !important; width:15%; height:auto !important;" width="90" alt="" data-proportionally-constrained="true" data-responsive="true" src="<?= env('BASE_URL').'/img/logo-email.png'?>">
			</a>
		</td>
	  </tr>
	</tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="3bd31eed-8e6a-4335-b8f2-ef2f982e871d" data-mc-module-version="2019-10-22">
	<tbody>
	  <tr>
		<td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; font-family: &quot;courier new&quot;, courier, monospace"><strong>Welcome to Sound Check Live! Where Independence is Life.</strong></span></div>
<div style="font-family: inherit; text-align: inherit"><span style="font-family: &quot;courier new&quot;, courier, monospace"><br>
</span></div>
<div style="font-family: inherit; text-align: inherit"><span style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; font-family: &quot;courier new&quot;, courier, monospace">You're one step closer to truly mastering what so many people claim to be independence. Designed by industry professionals just like you, we understand the plight of the daily grind, especially when having little to no resources available. With that said we've designed 3 main components to help you grow your brand independently. It is our hope that you take full advantage of all opportunities set before you.</span><br>
</div><div></div></div></td>
	  </tr>
	</tbody>
  </table><table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="3a51c9f4-9050-4237-98d2-d52a524093b2">
	<tbody>
	  <tr>
		<td style="padding:0px 0px 10px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
		  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="10px" style="line-height:10px; font-size:10px;">
			<tbody>
			  <tr>
				<td style="padding:0px 0px 10px 0px;" bgcolor="#000000"></td>
			  </tr>
			</tbody>
		  </table>
		</td>
	  </tr>
	</tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:0px 0px 10px 0px;" bgcolor="#FFFFFF">
	<tbody>
	  <tr role="module-content">
		<td height="100%" valign="top"><table width="290" style="width:290px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-0">
	  <tbody>
		<tr>
		  <td style="padding:0px;margin:0px;border-spacing:0;"><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2d47ff6d-017a-4ba7-8140-3c81398bd544">
	<tbody>
	  <tr>
		<td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
		  
		<a href="https://soundchecklive.co/catalogues/index"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:100% !important; width:100%; height:auto !important;" width="290" alt="" data-proportionally-constrained="true" data-responsive="true" src="<?= env('BASE_URL').'/img/budget-genrator.jpg'?>"></a></td>
	  </tr>
	</tbody>
  </table></td>
		</tr>
	  </tbody>
	</table><table width="290" style="width:290px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-1">
	  <tbody>
		<tr>
		  <td style="padding:0px;margin:0px;border-spacing:0;"><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="ed50d165-2768-4718-a355-36243ec2e98b" data-mc-module-version="2019-10-22">
	<tbody>
	  <tr>
		<td style="padding:10px 0px 18px 0px; line-height:10px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: decimal; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px"><strong>Budget Generator</strong></span></div>
<div style="font-family: inherit; text-align: inherit"><span style="font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px"><br>
</span></div>
<ul>
  <li color="rgb(14, 16, 26)" style="text-align: inherit; font-family: &quot;courier new&quot;, courier, monospace; color: rgb(14, 16, 26); font-size: 14px; font-size: 14px"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px">Financial growth through crowdfunding directly with the general public as investment partners.</span></li>
</ul><div></div></div></td>
	  </tr>
	</tbody>
  </table></td>
		</tr>
	  </tbody>
	</table></td>
	  </tr>
	</tbody>
  </table><table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="edf64ae5-bb20-465a-af76-ecc4b53a708b">
	<tbody>
	  <tr>
		<td style="padding:0px 0px 10px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
		  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="10px" style="line-height:10px; font-size:10px;">
			<tbody>
			  <tr>
				<td style="padding:0px 0px 10px 0px;" bgcolor="#000000"></td>
			  </tr>
			</tbody>
		  </table>
		</td>
	  </tr>
	</tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:0px 0px 10px 0px;" bgcolor="#FFFFFF">
	<tbody>
	  <tr role="module-content">
		<td height="100%" valign="top"><table width="290" style="width:290px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-0">
	  <tbody>
		<tr>
		  <td style="padding:0px;margin:0px;border-spacing:0;"><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="a043653e-5b57-44f7-a4c8-f3b3f0c3e053">
	<tbody>
	  <tr>
		<td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
		  
		<a href="https://soundchecklive.co/catalogues/index"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:100% !important; width:100%; height:auto !important;" width="290" alt="" data-proportionally-constrained="true" data-responsive="true" src="<?= env('BASE_URL').'/img/income-genrator.jpeg'?>"></a></td>
	  </tr>
	</tbody>
  </table></td>
		</tr>
	  </tbody>
	</table><table width="290" style="width:290px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-1">
	  <tbody>
		<tr>
		  <td style="padding:0px;margin:0px;border-spacing:0;"><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="6Y2MHCJvpt9qEtUaZjyhQB" data-mc-module-version="2019-10-22">
	<tbody>
	  <tr>
		<td style="padding:10px 0px 18px 0px; line-height:10px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: decimal; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px"><strong>Income Generator</strong></span></div>
<div style="font-family: inherit; text-align: inherit"><span style="font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px"><br>
</span></div>
<ul>
  <li color="rgb(14, 16, 26)" style="text-align: inherit; font-family: &quot;courier new&quot;, courier, monospace; color: rgb(14, 16, 26); font-size: 14px; font-size: 14px"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px">Real-time live pay-per-view interactions with fans.</span></li>
  <li color="rgb(14, 16, 26)" style="text-align: inherit; font-family: &quot;courier new&quot;, courier, monospace; color: rgb(14, 16, 26); font-size: 14px; font-size: 14px"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px">Exclusive retailer for limited edition Creator merchandise.</span></li>
  <li color="rgb(14, 16, 26)" style="text-align: inherit; font-family: &quot;courier new&quot;, courier, monospace; color: rgb(14, 16, 26); font-size: 14px; font-size: 14px"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px">Access to paid fan requests model</span></li>
  <li style="text-align: inherit; font-family: &quot;courier new&quot;, courier, monospace"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: &quot;courier new&quot;, courier, monospace; font-size: 14px">Corporate sponsorship wall</span><span style="font-family: &quot;courier new&quot;, courier, monospace"><br>
</span></li>
</ul><div></div></div></td>
	  </tr>
	</tbody>
  </table></td>
		</tr>
	  </tbody>
	</table></td>
	  </tr>
	</tbody>
  </table><table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="71a7431a-1bb1-43c7-bd4c-c348f51536b0">
	<tbody>
	  <tr>
		<td style="padding:0px 0px 10px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
		  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="10px" style="line-height:10px; font-size:10px;">
			<tbody>
			  <tr>
				<td style="padding:0px 0px 10px 0px;" bgcolor="#000000"></td>
			  </tr>
			</tbody>
		  </table>
		</td>
	  </tr>
	</tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:0px 0px 10px 0px;" bgcolor="#FFFFFF">
	<tbody>
	  <tr role="module-content">
		<td height="100%" valign="top"><table width="290" style="width:290px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-0">
	  <tbody>
		<tr>
		  <td style="padding:0px;margin:0px;border-spacing:0;"><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="aCVyboDYeQ25UfFQth5MVk">
	<tbody>
	  <tr>
		<td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
		  
		<a href="https://soundchecklive.co/catalogues/index"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:100% !important; width:100%; height:auto !important;" width="290" alt="" data-proportionally-constrained="true" data-responsive="true" src="<?= env('BASE_URL').'/img/influence-genrator.jpg'?>"></a></td>
	  </tr>
	</tbody>
  </table></td>
		</tr>
	  </tbody>
	</table><table width="290" style="width:290px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-1">
	  <tbody>
		<tr>
		  <td style="padding:0px;margin:0px;border-spacing:0;"><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="jFapYCS9UbxfRuFHuYwwK3" data-mc-module-version="2019-10-22">
	<tbody>
	  <tr>
		<td style="padding:10px 0px 18px 0px; line-height:10px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: decimal; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"><strong>Influence Generator</strong></span></div>
<div style="font-family: inherit"><span style="font-family: courier new,courier,monospace"><br>
</span></div>
<ul>
  <li style="font-size: 11pt; color: rgb(14, 16, 26); font-family: courier new,courier,monospace"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace">League style </span><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"><strong>Equity Draft</strong></span><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"> for notable influencers</span></li>
  <li style="font-size: 11pt; color: rgb(14, 16, 26); font-family: courier new,courier,monospace"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace">Personalized growth campaigns </span><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"><strong>Mobilized</strong></span><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"> by fans</span></li>
  <li style="font-size: 11pt; color: rgb(14, 16, 26); font-family: courier new,courier,monospace"><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace">Direct map of </span><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"><strong>Connections</strong></span><span style="margin-top: 0pt; margin-bottom: 0pt; list-style-type: disc; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; line-height: 1.38; font-family: courier new,courier,monospace"> to your ideal Creator(s)</span></li>
</ul><div></div></div></td>
	  </tr>
	</tbody>
  </table></td>
		</tr>
	  </tbody>
	</table></td>
	  </tr>
	</tbody>
  </table><table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="f50ac48e-5173-4323-a783-16271e13971b">
	<tbody>
	  <tr>
		<td style="padding:0px 0px 10px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
		  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="10px" style="line-height:10px; font-size:10px;">
			<tbody>
			  <tr>
				<td style="padding:0px 0px 10px 0px;" bgcolor="#000000"></td>
			  </tr>
			</tbody>
		  </table>
		</td>
	  </tr>
	</tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2aade466-9fd9-471b-9aa0-ef1f5c384f6c" data-mc-module-version="2019-10-22">
	<tbody>
	  <tr>
		<td style="padding:18px 0px 18px 0px; line-height:10px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: center"><span style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: underline; text-decoration-style: initial; text-decoration-color: initial; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap; font-family: &quot;courier new&quot;, courier, monospace"><strong>The Deal</strong></span></div>
<div style="font-family: inherit; text-align: center"><span style="font-family: &quot;courier new&quot;, courier, monospace"><br>
</span></div>
<div style="font-family: inherit; text-align: center"><span style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; font-family: &quot;courier new&quot;, courier, monospace">After approval Sound Check Live will become your exclusive distributor and broker a deal between you and the general public for funding.</span></div>
<div style="font-family: inherit; text-align: center"><span style="font-family: &quot;courier new&quot;, courier, monospace"><br>
</span></div>
<div style="font-family: inherit; text-align: center"><span style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt; color: #0e101a; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; font-family: &quot;courier new&quot;, courier, monospace">A Sound Check Live representative is currently reviewing your submission. He/She will contact you momentarily for onboarding. Please take the time to read our Terms and Conditions thoroughly. Prior to your approval should you have any questions please call or email.</span></div>
<div style="font-family: inherit; text-align: center"><span style="font-family: &quot;courier new&quot;, courier, monospace"><br>
</span></div>
<div style="font-family: inherit; text-align: center"><span style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-variant-east-asian: normal; text-decoration-line: none; text-decoration-style: initial; text-decoration-color: initial; vertical-align: baseline; white-space: pre-wrap; font-family: &quot;courier new&quot;, courier, monospace">Again welcome to Sound Check Live and let's hustle hard!</span></div><div></div></div></td>
	  </tr>
	</tbody>
  </table><div data-role="module-unsubscribe" class="module" role="module" data-type="unsubscribe" style="color:#444444; font-size:12px; line-height:20px; padding:16px 16px 16px 16px; text-align:Center;" data-muid="4e838cf3-9892-4a6d-94d6-170e474d21e5"><div class="Unsubscribe--addressLine"><p class="Unsubscribe--senderName" style="font-size:12px; line-height:20px;">{{Sender_Name}}</p><p style="font-size:12px; line-height:20px;"><span class="Unsubscribe--senderAddress">{{Sender_Address}}</span>, <span class="Unsubscribe--senderCity">{{Sender_City}}</span>, <span class="Unsubscribe--senderState">{{Sender_State}}</span> <span class="Unsubscribe--senderZip">{{Sender_Zip}}</span></p></div><p style="font-size:12px; line-height:20px;"><a class="Unsubscribe--unsubscribeLink" href="{{{unsubscribe}}}" target="_blank" style="">Unsubscribe</a> - <a href="{{{unsubscribe_preferences}}}" target="_blank" class="Unsubscribe--unsubscribePreferences" style="">Unsubscribe Preferences</a></p></div></td>
									  </tr>
									</table>
									<!--[if mso]>
								  </td>
								</tr>
							  </table>
							</center>
							<![endif]-->
						  </td>
						</tr>
					  </table>
					</td>
				  </tr>
				</table>
			  </td>
			</tr>
		  </table>
		</div>
	  </center>
	</body>
</html>