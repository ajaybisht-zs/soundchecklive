<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');
</style>
<table cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" border="0" width="100%">
    <tbody>
        <!-- Start Email Header -->
        <?= $this->element('Email/email_header');?>
        <!-- End Email Header -->

        <tr>
            <td align="center">
                <table style="border:1px solid #f2f2f2" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" border="0" width="600">

                    <tbody>
                        <tr>
                            <td align="center" style="padding:0 px">
                                <table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">

                                    <tbody>
                                        <tr>
                                            <td align="center" height="25" style="font-size:1px;line-height:1px">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding:0 30px">
                                                <div style="color:#000000;font-family:Montserrat,Helvetica,Arial,sans-serif;font-size:25px;font-weight:400;line-height:36px">
                                                    Hi <?= $proofOfConcept->first_name?></strong>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="25" style="font-size:1px;line-height:1px">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding:0 30px">
                                                <div style="color:#5a5b5f;font-family:Montserrat,Helvetica,Arial,sans-serif;font-size:16px;line-height:27px;font-weight:400;text-align:left">
                                                    <p>
                                                   Thanks for investing in soundchecklive.co. we have received $ <?= $proofOfConcept->amount?>
                                                    </p>

                                                    <p style="margin-top:40px"><strong
                                                            style="color: rgb(34, 34, 34);">Best
                                                            Regards</strong><br />
                                                        <span
                                                            id="docs-internal-guid-357a4344-41df-fd23-cfe4-95145b60c6f5"><span
                                                                style="font-size: 14px;
                                                                color: rgb(34, 34, 34);
                                                                background-color: transparent;
                                                                vertical-align: baseline;
                                                                white-space: pre-wrap;
                                                                ">Soundcheck Support Team
                                                            </span></span>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <!--Start footer table -->
                <?= $this->element('Email/email_footer');?>
                <!-- End footer table -->

            </td>
        </tr>
    </tbody>
</table>