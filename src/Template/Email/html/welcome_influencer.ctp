<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" style="margin:0 auto !important;padding:0 !important;height:100% !important;width:100% !important;">
  <head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <!-- CSS Reset : BEGIN -->
    <!-- CSS Reset : END -->
    <!-- Progressive Enhancements : BEGIN -->
    <style>
      .heading-section .subheading::after{position:absolute;left:0;right:0;bottom:-10px;content:'';width:100%;height:2px;background:#0d0cb5;margin:0 auto}
    </style>
    <style type="text/css">
      /* What it does: Stops email clients resizing small text. */
               /* What it does: Centers email on Android 4.4 */
               /* What it does: Stops Outlook from adding extra spacing to tables. */
               /* What it does: A work-around for email clients meddling in triggered links. */
               *[x-apple-data-detectors],  /* iOS */
               .unstyle-auto-detected-links *,
               .aBn {
               border-bottom: 0 !important;
               cursor: default !important;
               color: inherit !important;
               text-decoration: none !important;
               font-size: inherit !important;
               font-family: inherit !important;
               font-weight: inherit !important;
               line-height: inherit !important;
               }
               /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
               /* If the above doesn't work, add a .g-img class to any image in question. */
               /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
               /* Create one of these media queries for each additional viewport size you'd like to fix */
               /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
               @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
               u ~ div .email-container {
               min-width: 320px !important;
               }
               }
               /* iPhone 6, 6S, 7, 8, and X */
               @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
               u ~ div .email-container {
               min-width: 375px !important;
               }
               }
               /* iPhone 6+, 7+, and 8+ */
               @media only screen and (min-device-width: 414px) {
               u ~ div .email-container {
               min-width: 414px !important;
               }
               }
               /*LOGO*/
               /*HEADING SECTION*/
               /*BLOG*/
               /*TESTIMONY*/
               @media screen and (max-width: 500px) {
               .icon{
               text-align: left;
               }
               .text-services{
               padding-left: 0;
               padding-right: 20px;
               text-align: left;
               }
               }
    </style>
  </head>
  <body width="100%" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-family:'Poppins', sans-serif;font-weight:400;font-size:15px;line-height:1.8;color:rgba(0,0,0,.4);margin:0 auto !important;padding:0 !important;height:100% !important;width:100% !important;margin: 0; padding: 0 !important; mso-line-height-rule: exactly;">
    <center style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width: 100%;">
      <div style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
         </div>
      <div style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;max-width: 600px; margin: 0 auto; margin-top: 30px !important; margin-bottom: 30px;" class="email-container">
        <!-- BEGIN BODY -->
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important;margin: auto; background: #f7f7f7;">
          <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
            <td valign="top" class="bg_white" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding:20px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;padding: 6px 1.5em; background: #00c3e8;">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#00c3e8" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important;">
                <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                  <td width="60%" class="logo" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;text-align: left; font-weight: normal;">
                    <h1 style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-family:'Poppins', sans-serif;color:#000000;margin-top:0;margin:0;font-weight: 500;">Sound Check Live</h1>
                  </td>
                  <td width="40%" class="logo" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;text-align: right;">
                    
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- end tr -->
            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
              <td class="bg_white" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding:20px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important;background: #fff;">
                  <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <td class="bg_white email-section" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding:20px;padding:2.5em;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;padding-bottom: 0;padding-right: 15px;padding-left: 15px;">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important;">
                        <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;">
                          <td align="left" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;padding:0;Margin:0;">
                            <h3 style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-family:'Poppins', sans-serif;color:#000000;margin-top:0;Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;font-style:normal;font-weight:normal;color:#666666;">
                              Congratulations <?php echo $user->influencer_profile->first_name.' '.$user->influencer_profile->last_name ?>. Your Sound Check Live Influencer account is approved by admin. Please <?php echo $this->Html->link('Login',['controller' => 'users', 'action' => 'login','prefix' =>'artist','_full' => true]);?>  to your account.
                                       <br style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                            </h3>
                          </td>
                        </tr>
                        <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <td class="bg_white " style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding:20px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;padding-top: 10px; padding-right: 15px;padding-left: 15px;">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important;">
                        <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;">
                          <td align="left" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;padding:0;Margin:0;">
                            <h3 style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-family:'Poppins', sans-serif;color:#000000;margin-top:0;Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;font-style:normal;font-weight:normal;color:#666666;">
                              <div style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">Sincerely,</div>
                              Team Sound check Live
                                       <br style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                            </h3>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                      </table>
                    </td>
                  </tr>
                  
                  
                  <!-- end: tr -->
                </table>
              </td>
            </tr>
            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
              <td style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;">
                <table style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important;">
                  <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <td style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;">
                      <ul class="list-unstyled" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;list-style:none;text-align:center;margin:0;">
                        <li class="list-inline-item" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;display:inline-block;">
                          <a href="#" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-decoration:none;color:#0d0cb5;">
                            <img src="http://stage.zealsofttechnologies.com/linkstorller/img/images/fb.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-interpolation-mode:bicubic;">                        </a>
                        </li>
                        <li class="list-inline-item" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;display:inline-block;">
                          <a href="#" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-decoration:none;color:#0d0cb5;">
                            <img src="http://stage.zealsofttechnologies.com/linkstorller/img/images/twitter.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-interpolation-mode:bicubic;"></a>
                        </li>
                        <li class="list-inline-item" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;display:inline-block;">
                          <a href="#" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-decoration:none;color:#0d0cb5;">
                            <img src="http://stage.zealsofttechnologies.com/linkstorller/img/images/instagram.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-interpolation-mode:bicubic;"></a>
                        </li>
                        <li class="list-inline-item" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;display:inline-block;">
                          <a href="#" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-decoration:none;color:#0d0cb5;">
                            <img src="http://stage.zealsofttechnologies.com/linkstorller/img/images/linkdin.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-interpolation-mode:bicubic;"></a>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  
                </table>
              </td>
            </tr>
          </table>
        </div>
      </center>
    </body>
  </html>