<?php 
    $session = $this->getRequest()->getSession();
    $userInfo  = $session->read('Auth.User');
    $userId = $session->read('Auth.User.id');
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <!-- bootstrap css -->
    <?= $this->Html->css('frontend/bootstrap.min'); ?>
    <?= $this->Html->css('frontend/main'); ?>
    <!-- base css -->
    <?= $this->Html->css('frontend/base'); ?>
    <!-- navigation  -->
    <?= $this->Html->css('frontend/nav'); ?>
    <!-- custom css -->
    <?= $this->Html->css('frontend/style'); ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <title>index</title>
    <style type="text/css">
        body {
            background-color: #fff;
        }

        @media (min-width: 768px) {
            body {
                padding-top: 0;
            }
        }
    </style>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175926688-1"></script>
    <?= $this->Html->script('common/analatics') ?>
</head>

<body>
    <!-- start fan menu for mobile -->
    <?= $this->element('Fan/fan_menu');?>
    <!-- end fan menu for mobile -->
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark py-0">
            <a class="navbar-brand"
                href="<?= $this->Url->build(['controller' => 'homes', 'action' => 'index','prefix' => false]) ?>">
                <?= $this->Html->image('logo.png', ['alt' => 'logo','class' => 'd-none d-md-block']);?>
                <?= $this->Html->image('mob-logo.png', ['alt' => 'logo','class' => 'd-block d-md-none']);?>
            </a>
            <div class="ml-auto top-login mr-5">
                <?php 
                    $session = $this->request->getSession(); 
                    $session = $session->read('Auth');
                    if(!$session):

                ?>
                <?= $this->Html->link(
                    '
                    Login',
                    [
                        'controller' => 'Logins',
                        'action' => 'add',
                        'prefix' => 'artist',
                    ],
                    ['class' => 'register bg-red px-4']
                );?>


                <?php 
                    else: 
                    echo  $this->Html->link(
                            'Dashboard',
                            [
                                'controller' => 'UserProfiles',
                                'action' => 'videoPitch',
                                'prefix' => 'artist',
                            ],
                            ['class' => 'login mr-2 mr-sm-4']
                        ); 
                endif;
                ?>
            </div>
            <div class="outer-menu ml-2">
                <input class="checkbox-toggle" type="checkbox" />
                <div class="hamburger">
                    <div></div>
                </div>
                <div class="menu">
                    <div>
                        <div>
                            <ul>
                                <li>
                                    <?=
                                    $this->Html->link(
                                        'Home',
                                        [
                                            'controller' => 'homes',
                                            'action' => 'index',
                                            'prefix' => false
                                        ]
                                    );
                                ?>
                                </li>
                                <li>
                                    <?=
                                        $this->Html->link(
                                            'CATALOG',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>

                                <li>
                                    <?=
                                    $this->Html->link(
                                        'LE APPAREL',
                                        [
                                            'controller' => 'Catalogues',
                                            'action' => 'index',
                                            'prefix' => false
                                        ]
                                    );
                                ?>
                                </li>
                                <li>
                                    <?=
                                        $this->Html->link(
                                            'ARTIST LOGIN',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?=
                                        $this->Html->link(
                                            'PARTNER LOGIN',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>



                                <li>
                                    <?= $this->Html->link(
                                                'About us',
                                                '/about-us',
                                                ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>
                                <li>
                                    <?= $this->Html->link(
                                            'WHAT WE DO',
                                            '/what-we-do',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?= $this->Html->link(
                                            'HOW SCL WORKS?',
                                            '/how-scl-work',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?php  $this->Html->link(
                                            'FAQ’S',
                                            '/faq',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?= $this->Html->link(
                                            'Contact',
                                            '/contact-us',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

    </header>
    <div class="home-slider d-none d-md-block">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators" style="left:-55px;">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item active" style="background-image: url('img/slider6.jpg')"></div>
                <div class="carousel-item" style="background-image: url('img/slider9.jpg')"></div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('img/slider4.jpg')"></div>
                <div class="carousel-item" style="background-image: url('img/slider2.jpg')"></div>
                <div class="carousel-item" style="background-image: url('img/slider13.jpg')"></div>
                <div class="carousel-item" style="background-image: url('img/slider12.jpg')"></div>
                <div class="carousel-item" style="background-image: url('img/slider3.jpg')"></div>
            </div>
        </div>
    </div>
    <div class="crowd-box desktop-crowd d-none d-sm-block">
        <a href="/catalog">
            <?= $this->Html->image('doller-new.png', ['alt' => 'icon','class' => 'crowd-settings']);?>
            Crowd
        </a>
    </div>
    <div class="main-text" style="postion:relative;">
        <div id="cw" class="crowd-box d-block d-sm-none ">
            <a href="/catalog">
                <?= $this->Html->image('doller-new.png', ['alt' => 'icon','class' => 'crowd-settings']);?>
                Crowd
            </a></div>
        <div class="slider-caption">
            <h1 class="mb-4 mt-4 mt-md-0">are you a </h1>
            <div class="button-block  d-block d-sm-none">
                <div class="row m-0">

                    <div class="col-6 " id="c1">
                        <a class="circle-box" href="/artist-signUp">
                            <div class="bg-blue">
                                <div class="text-block">
                                    <span class="text-small">Music</span> <br>
                                    <div class="big-text">Artist</div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-6 " id="c2">
                        <a class="circle-box" href="/filmDirector-signUp">
                            <div class="bg-blue">
                                <div class="text-block">
                                    <span class="text-small">Film </span> <br>
                                    <div class="big-text">Director</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="row m-0">
                    <div class="col-12 " id="c5">
                        <a class="circle-box"
                            href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'index','prefix' => 'influencer']) ?>">
                            <div class=" yellow-box">
                                <div class="text-block">
                                    <span class="text-small">Influencer</span> <br>
                                    <!-- <div class="big-text">Artist</div> -->
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="row mt-0 m-0">
                    <div class="col-6 " id="c3">
                        <a class="circle-box" href="/fashionDesigner-signUp">
                            <div class="bg-blue">
                                <div class="text-block">
                                    <span class="text-small">Fashion </span> <br>
                                    <div class="big-text">Designer</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-6" id="c4">
                        <a class="circle-box" href="/catalogues">
                            <div class="bg-red">
                                <div class="text-block">

                                    <div>fan</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="d-none d-sm-block">


                <?= $this->Html->link('<div class="bg-blue mb-md-5 mb-3 p-2s">
                    <span class="text-small">Music</span> <br>
                    <div>Artist</div>
                </div>',
                        [
                            'controller' => 'Users',
                            'action' => 'register',
                            'prefix' => 'artist',
                        ],
                        [
                             'escape' => false
                        ])
            ?>
                <?= $this->Html->link('<div class="bg-blue mb-md-5 mb-3 p-2s">
                    
                    <span class="text-small">Film </span><br>
                    <div>Director</div>
                </div>',
                        'filmDirector-signUp',
                        [
                             'escape' => false
                        ])
            ?>
                <?= $this->Html->link('<div class="bg-blue mb-md-5 mb-3 p-2s">
                    
                    <span class=" text-small">Fashion </span> <br>
                    <div>Designer</div>
                </div>',
                         'fashionDesigner-signUp',
                        [
                             'escape' => false
                        ])
            ?>

                <!-- <?php
                    $txtArtist = 'Music Artist';
                    $txtArtist =wordwrap($txtArtist,7,"<br>");

                    echo $this->Html->link($txtArtist,
                        [
                            'controller' => 'Users',
                            'action' => 'register',
                            'prefix' => 'artist',
                        ],
                        [
                            'class' => 'bg-blue mb-md-5 mb-3',
                            'escape' => false
                    ]);
            ?> -->

                <?= $this->Html->link('Influencer',
                        [
                            'controller' => 'Users',
                            'action' => 'index',
                            'prefix' => 'influencer',
                        ],
                        [
                            'class' => 'bg-white text-dark  fan p-2s '
                        ])
            ?>
                <?= $this->Html->link('fan',
                        [
                            'controller' => 'catalogues',
                            'action' => 'index',
                        ],
                        [
                            'class' => 'bg-white text-dark  fan p-2s '
                        ])
                ?>
            </div>
            <div class="my-5 my-md-0 text-white px-4 px-md-0 home-page-content pt-5">
                <div class="home-text">
                    Every creator needs a budget. Every
                    <br />fan becomes a partner.
                </div>
                <div class="card-deck">

                    <div">
                        <?php  
                        $howWeworkVideo = $this->Images->getHowWeWorkVideo($howWeWorks);
                        if(!empty($howWeworkVideo)) {
                            $video = env('BASE_URL').$howWeworkVideo;  
                        } else {
                             $video = '';
                        }
                    ?>
                        <a class="watch-video-link d-block d-sm-none" data-fancybox href="<?= $video?>">
                            Watch video to learn more
                        </a>
                </div>
            </div>
        </div>
    </div>
    </div>



    <!-- footer -->
    <footer class="footer bg-dark">
        <div class="container">
            <div class="row d-none">
                <div class="col-12 pt-4 pb-2">
                    <h2>BECause you know music doesn't mean you know the music business.©2019 SoundCheckLive</h2>
                </div>
            </div>
            <div class="row d-none">
                <div class="col-12">
                    <p class="copy-right mb-0 text-white">©2019 All copyrights reserved SoundCheckLive</p>
                </div>
            </div>
        </div>
        <?= $this->element('User/sticky_footer');?>
    </footer>




    <?php 
      if($isMobile):

    ?>
    <div class="modal fade proof-of-concept " id="partner-step-2" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <form id="regForm" action="">
                        <h5 class="proof-concept">Proof of concept</h5>
                        <?= $this->Html->image('mobile-splash.jpg', ['alt' => 'logo','style' => 'width:60px']);?>
                        <div class="powered-by mt-2">Powered by 11 </div>

                        <!-- One "tab" for each step in the form: -->
                        <div class="tab">
                            <div class="row border-bottom top-strip mb-3 mt-3">
                                <div class="col-4 p-0 active-border">1) Account</div>
                                <div class="col-4 p-0">2) Details</div>
                                <div class="col-4 p-0">3) Payment</div>
                            </div>
                            <div class="select-text">
                                Select your partnership amount:
                            </div>
                            <div class="all-text-required">
                                *All fields required
                            </div>
                            <div class="row mb-2">
                                <div class="col-6">
                                    <div class="type-box ">
                                        $30
                                        <input type="radio" value="ee" name="amount">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="type-box ">
                                        $40
                                        <input type="radio" value="" name="amount">
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6">
                                    <div class="type-box ">
                                        $40
                                        <input type="radio" value="" name="amount">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-10 m-auto pt-3 pb-2">
                                <div class="form-group">
                                    <label>Who sent you? (select one)</label>
                                    <?= 
                                        $this->Form->control('people', [
                                            'options' => $people,
                                            'label' => false,
                                            'class' => 'form-control',
                                            'empty' => 'Select'
                                        ])
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="tab">
                            <div class="row border-bottom top-strip mb-3 mt-3">

                                <div class="col-4 p-0">
                                    <?= $this->Html->image('check.png', ['alt' => 'logo','style' => 'width:10px']);?>
                                    Amount ($50)</div>
                                <div class="col-4 p-0 active-border">
                                    <?= $this->Html->image('check.png', ['alt' => 'logo','style' => 'width:10px']);?>
                                    Details</div>
                                <div class="col-4 p-0">3) Payment</div>
                            </div>
                            <div class="select-text">
                                Complete your $50 Sound Check Live partnership:
                            </div>
                            <div class="all-text-required">
                                *All fields required
                            </div>
                            <div class="row ">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Email Address">
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Number, Street, Apt.">
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-3">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="zip">
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="City">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>State</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Cell Phone">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab">
                            <div class="row border-bottom top-strip mb-3 mt-3">
                                <div class="col-4 p-0">
                                    <?= $this->Html->image('check.png', ['alt' => 'logo','style' => 'width:10px']);?>
                                    Amount ($50)</div>
                                <div class="col-4  p-0 ">
                                    <?= $this->Html->image('check.png', ['alt' => 'logo','style' => 'width:10px']);?>
                                    Details</div>
                                <div class="col-4 p-0 active-border ">3) Payment</div>
                            </div>
                            <div class="select-text mb-3">
                                Complete your $50 Sound Check Live partnership:
                            </div>
                            <div class="text-left">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="exampleRadios"
                                        id="exampleRadios1" value="option1" checked>
                                    <label class="form-check-label" for="exampleRadios1">

                                        <?= $this->Html->image('verified-card-img.jpg', ['alt' => 'logo','style' => 'width:150px']);?>
                                    </label>
                                </div>
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="exampleRadios"
                                        id="exampleRadios1" value="option1" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        <?= $this->Html->image('paypal.png', ['alt' => 'logo','style' => 'width:110px']);?>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Credit Card Number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="overflow:auto;">
                            <div style="">
                                <!-- <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button> -->
                                <button class="btn btn-continue mb-2" type="button" id="nextBtn"
                                    onclick="nextPrev(1)">Continue</button>
                            </div>
                        </div>

                        <!-- Circles which indicates the steps of the form: -->
                        <!-- <div style="text-align:center;margin-top:40px;">
                      <span class="step"></span>
                      <span class="step"></span>
                      <span class="step"></span>
                      <span class="step"></span>
                    </div> -->

                    </form>

                </div>

            </div>
        </div>
    </div>
    <?php 
    endif;
?>

    <!-- Modal -->
    <?php 
       // if($isMobile):

    ?>
    <div class="modal fade proof-of-concept " id="proof-first-screen" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <h5 class="proof-concept">Proof of concept</h5>
                    <?= $this->Html->image('mobile-splash.jpg', ['alt' => 'logo','style' => 'width:60px']);?>
                    <div class="powered-by mt-2">Powered by 11 </div>
                    <p class="mb-3">We're offering you a unique opportunity to INVEST in Sound Check Live. Yes, you read
                        correctly! Here's your chance to become our platform PARTNER!</p>



                    <div class="text-center col-10 m-auto pb-3">
                        <button class="btn btn-partner" id="partner-click">Partner</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php 
        //endif;
    ?>

    <?= $this->element('Model/call_accept')?>
    <?=
        $this->Html->script([
            'frontend/jquery-3.3.1.min',
            'frontend/bootstrap.bundle.min',
            'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js',
            'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js',
            'common',
            'https://js.pusher.com/7.0/pusher.min.js',
            'common/notification',
            //'https://js.pusher.com/beams/1.0/push-notifications-cdn.js',
            //'common/beam'
        ])
    ?>

    <script type="text/javascript">

        window.csrf = "<?=$this->request->getCookie('csrfToken')?>";
        window.url = "<?=$this->Url->build('/',true);?>";
        window.userId = "<?= $userId?>";
    </script>

    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Continue";
            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            //   if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
                // ... the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }
    </script>

    <script>

        $(document).ready(function () {
            $(' .type-box').on('click', function () {
                $('[name="pricefield"]').attr('checked', false);
                let radioInput = $(this).find('input[type="radio"]');
                radioInput.attr('checked', true);
                $(".type-box").removeClass('active');
                $(this).addClass('active');
            });
        });

        $('#proof-first-screen').modal('show');

        $(document).on('click', '#partner-click', function () {
            $('#proof-first-screen').modal('hide');
             window.location.href = window.url +'poweredby11';
            //$('#partner-step-2').modal('show');
        });

        $(document).on('click', '.show-partner-layout', function () {

            $('#show-partner-modal').removeClass('d-none');
        });

        $(document).on('click', '.close', function () {
            $('#show-partner-modal').addClass('d-none');
        });

        var width = $('#c1').width();

        function moveit() {
            var newTop = Math.floor(Math.random() * 350);
            var newLeft = Math.floor(Math.random() * 1024);
            var newDuration = Math.floor(Math.random() * 5000);

            $('#cw').animate({
                //top: newTop,
                left: newLeft,
            }, newDuration, function () {
                moveit();
            });

        }

        $(document).ready(function () {
            var width = $('#c1').width() + 80;
            //alert('hii');
            //$("#cw").animate({left: '250px'});
            //moveit();

            function animateMydiv() {
                $('#cw').animate({
                    'left': width + 'px'
                }, 9000,

                    function () {
                        $('#test').css({
                            opacity: 1.0,
                            visibility: "visible"
                        }, 9000).animate({
                            opacity: 0
                        }, 9000);
                    }).animate({
                        'left': '60px'
                    }, 9000, animateMydiv);

                /*$('#c2,#c4').animate({
                    'right': 30 + 'px'
                }, 9000,

                    function () {
                        $('#test2').css({
                            opacity: 1.0,
                            visibility: "visible"
                        }, 9000).animate({
                            opacity: 0
                        }, 9000);
                    }).animate({
                        'right': '10px'
                    }, 9000, animateMydiv);*/
            }

            animateMydiv();
        });
    </script>

</body>

</html>

<style>
    .top-strip {
        font-size: 14px;
    }

    .all-text-required {
        text-align: right;
        margin-bottom: 5px;
        font-size: 14px;
    }

    .select-text {
        font-weight: 600;
        text-align: left;
        margin-bottom: 10px;
        color: #000;
        font-size: 13px;
    }

    .active-border {
        border-bottom: 2px solid #51ade4;
        font-weight: 600;
        color: #282b49;
    }

    .btn-continue {
        background: #569489;
        color: #fff;
        border-radius: 25px;
        padding: 8px 22px;
    }

    .btn-continue:hover {
        background: #569489;
        color: #fff;
        border-radius: 25px;
        padding: 8px 22px;
    }

    #regForm {
        background-color: #ffffff;
        /* margin: 100px auto; */
        /* font-family: Raleway; */
        /* padding: 40px;
    width: 70%;
    min-width: 300px; */
    }


    input {
        padding: 10px;
        /* width: 100%; */
        font-size: 17px;
        border: 1px solid #aaaaaa;
    }

    /* Mark input boxes that gets an error on validation: */
    input.invalid {
        background-color: #ffdddd;
    }

    /* Hide all steps by default: */
    .tab {
        display: none;
    }

    button {
        background-color: #4CAF50;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        font-size: 17px;
        cursor: pointer;
    }

    button:hover {
        opacity: 0.8;
    }

    #prevBtn {
        background-color: #bbbbbb;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbbbbb;
        border: none;
        border-radius: 50%;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
        background-color: #4CAF50;
    }
</style>