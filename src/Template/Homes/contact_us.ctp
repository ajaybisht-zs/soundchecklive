<div class="container">
    <div class="bg-white mt-4 mb-4 about-us">
        <h2 class="mb-4">Contact Us</h2>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label> First Name</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Last Name</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Email Address</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Phone Number</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Message</label>
                    <textarea class="form-control" rows="4"></textarea>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <button class="btn bg-dark text-white">Submit</button>
            </div>
        </div>
    </div>
</div>