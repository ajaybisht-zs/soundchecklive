<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <!-- bootstrap css -->
    <?= $this->Html->css('frontend/bootstrap.min'); ?>
    <?= $this->Html->css('frontend/main'); ?>
    <!-- base css -->
    <?= $this->Html->css('frontend/base'); ?>
    <!-- navigation  -->
    <?= $this->Html->css('frontend/nav'); ?>
    <!-- custom css -->
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title>index</title>
    <style type="text/css">
        body {
            background-color: #fff;
        }

        @media (min-width: 768px) {
            body {
                padding-top: 0;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="mobile-slide d-flex">
            <?= $this->Html->image('mobile-splash.jpg', ['alt' => 'logo','style' => 'width:100px']);?>
            <div class="bottom-text-mobile-slide">Sound Check Live</div>
        </div>
    </div>

    <?php 
        $this->Html->scriptStart(['block' => true]);
            echo 'window.csrf = "'.$this->request->getCookie('csrfToken').'";';
            echo 'window.url = "'.$this->Url->build('/',true).'";';
        $this->Html->scriptEnd();
    ?>
    <?= $this->Html->script('frontend/jquery-3.3.1.min');?>
    <?= $this->Html->script('frontend/bootstrap.bundle.min');?>
    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js');?>
    <?= $this->Html->script('frontend/redirect'); ?>
    <?= $this->fetch('script') ?>
    <?= $this->fetch('scriptBottom') ?>
</body>

</html>

<style>
     .mobile-slide img {
        position: absolute;
        top: 41%;
        right: 0;
        left: 0;
        bottom: 0;
        margin: 0 auto;
    }

    .bottom-text-mobile-slide{
        color: red;
        position: absolute;
        bottom: 58px;
        left: 0;
        right: 0;
        text-align: center;
        font-weight: 600;
        font-size: 24px;
    }
</style>
