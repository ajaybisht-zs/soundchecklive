<div class="container concept-page">
    <div class="col-sm-8 m-auto concept-wraper p-3 p-sm-5">
        <div class="text-center">
            <h5 class="proof-concept">Proof of concept</h5>
            <img src="/img/mobile-splash.jpg" alt="logo" style="width:77px">
            <div class="powered-by mt-2">Powered by 11 </div>
        </div>
        <div class="row border-bottom top-strip mb-3 mt-3 mx-3">
            <div class="col-4 top-heading-step text-center active-border p-0 p-sm-2">1) Amount</div>
            <div class="col-4 top-heading-step text-center p-0 p-sm-2 ">2) Details</div>
            <div class="col-4 top-heading-step text-center p-0 p-sm-2 ">3) Payment</div>
        </div>
        <div class="select-text col-12">
            Select your partnership amount:
        </div>
        <div class="all-text-required">
            *All fields required
        </div>
        <?php
            echo $this->Form->create(null, [
                'url' => [
                    'controller' => 'PoweredByElevens',
                    'action' => 'index',
                    'prefix' => false
                ],
                'id' => 'step1',
            ]);
        ?>
        <div class="row mb-2 text-center">
            <div class="col-6">
                <div class="type-box ">
                    $10
                    <input type="radio" value="10" name="amount">
                </div>
            </div>
            <div class="col-6">
                <div class="type-box ">
                    $50
                    <input type="radio" value="50" name="amount">
                </div>
            </div>
        </div>
        <div class="row mb-2 text-center">
            <div class="col-6">
                <div class="type-box ">
                    $100
                    <input type="radio" value="100" name="amount">
                </div>
            </div>
            <div class="col-6">
                <div class="input-group ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">$</span>
                    </div>
                    <input type="number" class="form-control" name="custom_price">
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-10 m-auto pt-3 pb-2">
            <div class="form-group">
                <label>Who sent you? (select one)</label>
                <div class="input select">
                    <?= 
                        $this->Form->control('people_id', [
                            'options' => $people,
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => 'Select',
                            //'required' => true
                        ])
                    ?>
                </div>
            </div>
        </div>

        <div class="text-center">
            <button class="btn btn-continue mb-2" type="submit">Continue</button>
        </div>
    <?= $this->Form->end(); ?>
    </div>
</div>
<?php
    echo $this->Html->script('proofConcept/validate', [
        'block' => 'scriptBottom'
    ]);
?>