<?php
    use Cake\I18n\Number;
    $data = $this->request->getSession();
    $amount = $data->read('step1')['amount'];
?>
<div class="container concept-page">
    <div class="col-sm-8 m-auto concept-wraper p-3 p-sm-5">
        <div class="text-center">
            <h5 class="proof-concept">Proof of concept</h5>
            <img src="/img/mobile-splash.jpg" alt="logo" style="width:77px">
            <div class="powered-by mt-2">Powered by 11 </div>
        </div>
        <div class="row border-bottom top-strip mb-3 mt-3 mx-3">
            <div class="col-4 top-heading-step text-center  p-0 p-sm-2">  <?= $this->Html->image('check.png', ['alt' => 'logo']);?>Amount(<?= Number::currency($amount,'USD') ?>)</div>
            <div class="col-4 top-heading-step text-center active-border p-0 p-sm-2 ">2) Details</div>
            <div class="col-4 top-heading-step text-center p-0 p-sm-2 ">3) Payment</div>
        </div>
        <div class="select-text col-12">
           
            Complete your <?= Number::currency($amount,'USD') ?> Sound Check Live partnership:
                            
        </div>
        <div class="all-text-required">
            *All fields required
        </div>
         <?php
            echo $this->Form->create(null, [
                'url' => [
                    'controller' => 'PoweredByElevens',
                    'action' => 'details',
                    'prefix' => false
                ],
                'id' => 'step2',
                'class' => ''
            ]);
        ?>
        <div class="row ">
            <div class="col-12">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('email', [
                            'type' => 'email',
                            'class' => 'form-control',
                            'placeholder' => 'Email Address',
                            'label' => false,
                            'autocomplete' => 'off',
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-6">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('first_name', [
                            'type' => 'text',
                            'placeholder' => 'First Name *',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                   <?php
                        echo $this->Form->control('last_name', [
                            'type' => 'text',
                            'placeholder' => 'Last Name *',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-12">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('address', [
                            'type' => 'text',
                            'placeholder' => 'Number, Street, Apt.',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-3">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('zip_code', [
                            'type' => 'text',
                            'placeholder' => 'Zip',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-5">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('city', [
                            'type' => 'text',
                            'placeholder' => 'City',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?= 
                        $this->Form->control('state_id', [
                            'options' => $country,
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => 'Select',
                            'required' => true,
                            'placeholder' => 'State',
                            'required' => true,
                        ])
                    ?>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-12">
                <div class="form-group">
                    <?php
                        echo $this->Form->control('phone_number', [
                            'type' => 'text',
                            'placeholder' => 'Cell Phone',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ]);
                    ?>
                </div>
            </div>
        </div>
        

        <div class="text-center">
            <button class="btn btn-continue mb-2" type="submit">Continue</button>
        </div>
         <?= $this->Form->end(); ?>
    </div>
</div>
<?php
    echo $this->Html->script('proofConcept/validate', [
        'block' => 'scriptBottom'
    ]);
?>