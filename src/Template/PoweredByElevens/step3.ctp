<?php
    echo $this->Html->css([
            'frontend/catalog/stripe',
        ]);
?>
<div class="container concept-page">
    <div class="col-sm-8 m-auto concept-wraper p-3 p-sm-5">
        <div class="text-center">
            <h5 class="proof-concept">Proof of concept</h5>
            <img src="/img/mobile-splash.jpg" alt="logo" style="width:77px">
            <div class="powered-by mt-2">Powered by 11 </div>
        </div>
        <div class="row border-bottom top-strip mb-3 mt-3 mx-3">
            <div class="col-4 top-heading-step text-center  p-0 p-sm-2">  <?= $this->Html->image('check.png', ['alt' => 'logo']);?>Amount($50)</div>
            <div class="col-4 top-heading-step text-center  p-0 p-sm-2 "> <?= $this->Html->image('check.png', ['alt' => 'logo']);?> Details</div>
            <div class="col-4 top-heading-step text-center active-border p-0 p-sm-2 ">3) Payment</div>
        </div>
        <div class="select-text col-12">  
            Complete your $50 Sound Check Live partnership:                          
        </div>
        <div id = "payementsucces"></div>
        <div id="carderror"></div> 
        <form action="/charge" method="post" id="payment-form">
        <div class="text-left">
            <div class="form-check mb-4">
                <input class="form-check-input" type="radio" name="exampleRadios"
                    id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">

                    <?= $this->Html->image('verified-card-img.jpg', ['alt' => 'logo','style' => 'width:150px']);?>
                </label>
            </div>
            <div class="form-check mb-4">
                <input class="form-check-input" type="radio" name="exampleRadios"
                    id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">
                    <?= $this->Html->image('paypal.png', ['alt' => 'logo','style' => 'width:110px']);?>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- <div class="form-group">
                    <input class="form-control" placeholder="Creadit card number">
                </div> -->
                <div id="card-element" class="card bg-light py-3">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                    <!-- Used to display Element errors. -->
                <div id="card-errors" role="alert"></div>
            </div>
        </div>
        <div class="text-center mt-4">
            <button class="btn btn-continue mb-2 complete-partner pay-now" type="button" id= "submit-btn-txt">Complete Partnership</button>
        </div>
    </form>
    </div>
</div>
<?php
    echo $this->Html->script([
        'https://js.stripe.com/v3/',
            'frontend/stripe_donation',
        ], [
        'block' => 'scriptBottom'
    ]);
?>