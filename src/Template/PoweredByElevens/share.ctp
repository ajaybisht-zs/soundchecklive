<div class="container concept-page">
    <div class="col-sm-4 m-auto concept-wraper p-3">
        <div class="top-text-share">
           <?= $this->request->getQuery('name')?>, thank you for partnering with Sound Check live - the platform bulit for independents.
        </div>

        <div class="inner-share-wrpaer">
            <div class="keep">
                keep the momentum going !
            </div>

            <div class="border-custom-line"></div>

            <div class="invite-friend-text mb-2">
                Invite your friends to<br> donate:
            </div>

            <button class="btn btn-facebook" onClick="shareOnFB()">
                <?= $this->Html->image('facebook-white-icon.png', ['alt' => 'logo','class' => 'fb-white']);?> <div
                    class="fb-btn-text">Share on<br> Facebook</div>
            </button>

            <button class="btn btn-twitter" onClick="shareOntwitter()">
                <?= $this->Html->image('twitter-white-icon.png', ['alt' => 'logo','class' => 'fb-white']);?> <div
                    class="fb-btn-text">Share on<br> Twitter</div>
            </button>

            <button class="btn btn-email">
                <a  href="mailto:?subject=We're offering you a unique opportunity to INVEST in Sound Check Live. Yes, you read correctly! Here's your chance to become our platform PARTNER!e&amp;body=Check out this link to become partner http://soundcheck.test/poweredby11." title="Share by Email">
                    <?= $this->Html->image('email-white-icon.png', ['alt' => 'logo','class' => 'fb-white']);?> <div
                    class="fb-btn-text">Email your <br> friends</div>
                </a>
            </button>

        </div>
    </div>
</div>

 <script type="text/javascript">
 function shareOnFB(){
       var url = "https://www.facebook.com/sharer/sharer.php?u=https://soundchecklive.co/poweredby11&t=We're offering you a unique opportunity to INVEST in Sound Check Live. Yes, you read correctly! Here's your chance to become our platform PARTNER!";
       window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
       return false;
  }


function shareOntwitter(){
    var url = 'https://twitter.com/intent/tweet?url=https://soundchecklive.co/poweredby11&via=soundchecklive.co&text=We are offering you a unique opportunity to INVEST in Sound Check Live. Yes, you read correctly! Here your chance to become our platform PARTNER!';
    TwitterWindow = window.open(url, 'TwitterWindow',width=600,height=300);
    return false;
 }

</script>