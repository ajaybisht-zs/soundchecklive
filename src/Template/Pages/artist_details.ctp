<div class="catalouge-wraper clearfix">
    <div class="left-panel">
        <ul class="list-unstyled">
            <li class="active">
                <a href="#">
                    <?php echo $this->Html->image('images/home-icon-gray.png') ?> Home
                </a>
            </li>
            <li>
                <a href="#">
                    <svg version="1.1" x="0px" y="0px" width="24px" height="24px" fill="#7d8285"
                        viewBox="0 0 230.359 230.359" style="enable-background:new 0 0 230.359 230.359;"
                        xml:space="preserve">
                        <path
                            d="M210.322,5.604H20.023C8.982,5.604,0,14.587,0,25.628v142.519c0,11.048,8.982,20.037,20.023,20.037h62.14v21.572H58.241
                        c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h31.422h51.01h31.432c4.143,0,7.5-3.358,7.5-7.5c0-4.142-3.357-7.5-7.5-7.5
                        h-23.932v-21.572h62.149c11.049,0,20.037-8.988,20.037-20.037V25.628C230.359,14.587,221.371,5.604,210.322,5.604z M15,51.128
                        h200.359v91.521H15V51.128z M20.023,20.604h190.299c2.777,0,5.037,2.254,5.037,5.024v10.501H15V25.628
                        C15,22.858,17.253,20.604,20.023,20.604z M133.173,209.755h-36.01v-21.572h36.01V209.755z M210.322,173.183h-69.649h-51.01h-69.64
                        c-2.77,0-5.023-2.259-5.023-5.037V157.65h200.359v10.497C215.359,170.924,213.099,173.183,210.322,173.183z" />
                    </svg>
                    catalogue
                </a>
            </li>
        </ul>
    </div>
    <div class="right-panel">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="cover-img" style="background-image: url('../img/upcoming.jpg')">
                        <div class="row align-content-center h-100">
                            <div class="col-12 text-center">
                                <div class="user-profile">
                                    <svg version="1.1" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 53 53"
                                        style="enable-background:new 0 0 53 53;" xml:space="preserve">
                                        <path style="fill:#E7ECED;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
	c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
	c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
	c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
	c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
	C20.296,39.899,19.65,40.986,18.613,41.552z" />
                                        <g>
                                            <path style="fill:#e40434;" d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
		c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
		c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
		s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
		c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
		c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                                        </g>
                                    </svg>
                                </div>
                                <div class="artist">DJ Snake</div>
                                <div class="follow">
                                    <span class="total-partner">Partner : 10</span>
                                    <button class="partner">Partner now</button>
                                </div>
                                <div class="socail-media">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <svg version="1.1" x="0px" y="0px" width="30px" height="30pxs"
                                                    viewBox="0 0 291.319 291.319"
                                                    style="enable-background:new 0 0 291.319 291.319;"
                                                    xml:space="preserve">
                                                    <g>
                                                        <path style="fill:#3B5998;" d="M145.659,0c80.45,0,145.66,65.219,145.66,145.66c0,80.45-65.21,145.659-145.66,145.659
                                                                    S0,226.109,0,145.66C0,65.219,65.21,0,145.659,0z" />
                                                        <path style="fill:#FFFFFF;"
                                                            d="M163.394,100.277h18.772v-27.73h-22.067v0.1c-26.738,0.947-32.218,15.977-32.701,31.763h-0.055
                                                                    v13.847h-18.207v27.156h18.207v72.793h27.439v-72.793h22.477l4.342-27.156h-26.81v-8.366
                                                                    C154.791,104.556,158.341,100.277,163.394,100.277z" />
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                    <svg version="1.1" x="0px" y="0px" widht="30px" height="30px"
                                                    viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve">
                                               <g>
                                                   <path style="fill:#26A6D1;" d="M145.659,0c80.45,0,145.66,65.219,145.66,145.66c0,80.45-65.21,145.659-145.66,145.659
                                                       S0,226.109,0,145.66C0,65.219,65.21,0,145.659,0z"/>
                                                   <path style="fill:#FFFFFF;" d="M236.724,98.129c-6.363,2.749-13.21,4.597-20.392,5.435c7.338-4.27,12.964-11.016,15.613-19.072
                                                       c-6.864,3.96-14.457,6.828-22.55,8.366c-6.473-6.691-15.695-10.87-25.909-10.87c-19.591,0-35.486,15.413-35.486,34.439
                                                       c0,2.704,0.31,5.335,0.919,7.857c-29.496-1.438-55.66-15.158-73.157-35.996c-3.059,5.089-4.807,10.997-4.807,17.315
                                                       c0,11.944,6.263,22.504,15.786,28.668c-5.826-0.182-11.289-1.721-16.086-4.315v0.437c0,16.696,12.235,30.616,28.476,33.784
                                                       c-2.977,0.783-6.109,1.211-9.35,1.211c-2.285,0-4.506-0.209-6.673-0.619c4.515,13.692,17.625,23.651,33.165,23.925
                                                       c-12.153,9.249-27.457,14.748-44.089,14.748c-2.868,0-5.69-0.164-8.476-0.482c15.722,9.777,34.367,15.485,54.422,15.485
                                                       c65.292,0,100.997-52.51,100.997-98.029l-0.1-4.461C225.945,111.111,231.963,105.048,236.724,98.129z"/>
                                               </g>
                                               </svg>                                               
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                    <svg version="1.1"  x="0px" y="0px" width="30px" height="30px"
                                                    viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve">
                                               <g>
                                                   <path style="fill:#3F729B;" d="M145.659,0c80.44,0,145.66,65.219,145.66,145.66S226.1,291.319,145.66,291.319S0,226.1,0,145.66
                                                       S65.21,0,145.659,0z"/>
                                                   <path style="fill:#FFFFFF;" d="M195.93,63.708H95.38c-17.47,0-31.672,14.211-31.672,31.672v100.56
                                                       c0,17.47,14.211,31.672,31.672,31.672h100.56c17.47,0,31.672-14.211,31.672-31.672V95.38
                                                       C227.611,77.919,213.4,63.708,195.93,63.708z M205.908,82.034l3.587-0.009v27.202l-27.402,0.091l-0.091-27.202
                                                       C182.002,82.116,205.908,82.034,205.908,82.034z M145.66,118.239c22.732,0,27.42,21.339,27.42,27.429
                                                       c0,15.103-12.308,27.411-27.42,27.411c-15.121,0-27.42-12.308-27.42-27.411C118.23,139.578,122.928,118.239,145.66,118.239z
                                                        M209.65,193.955c0,8.658-7.037,15.704-15.713,15.704H97.073c-8.667,0-15.713-7.037-15.713-15.704v-66.539h22.759
                                                       c-2.112,5.198-3.305,12.299-3.305,18.253c0,24.708,20.101,44.818,44.818,44.818s44.808-20.11,44.808-44.818
                                                       c0-5.954-1.193-13.055-3.296-18.253h22.486v66.539L209.65,193.955z"/>
                                               </g>
                                               </svg>                                               
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Biography -->
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="bio heading">
                        <h2 class="text-white mb-3">Biography</h2>
                        <p class="text-light-gray">Maecenas et felis malesuada, pulvinar diam in, mollis diam.
                            Pellentesque nec laoreet mauris. Curabitur et ligula ut odio euismod commodo ac nec nisl.
                            Vestibulum molestie elit eget eleifend finibus. Donec blandit aliquet dolor sit amet
                            hendrerit. Duis ac purus felis. Etiam vel lectus id risus eleifend fermentum vel non diam.
                            Ut ut volutpat felis. Nullam imperdiet at magna a tempus. Aliquam et nunc nisl. Nulla vel
                            nunc sit amet tellus lobortis dictum non eget libero. Quisque non hendrerit dui, sit amet
                            egestas nisi. Maecenas venenatis porta nisi a dignissim. Nunc pellentesque laoreet justo et
                            dignissim. Donec eros tellus, tempor id risus a, tempus bibendum mi. Sed pellentesque nisi
                            ante.</p>
                    </div>
                    <div class="line mt-4"></div>
                </div>
            </div>
            <!-- record -->
            <div class="row">
                <div class="col-12">
                    <div class=" heading">
                        <h2 class="text-white mb-3">Record Details</h2>
                    </div>
                </div>
                <div class="col-4 mb-3">
                    <p class="text-light-gray records">
                        <span>Record Name :</span> Lion (Original Motion Picture Soundtrack)
                    </p>
                </div>
                <div class="col-4 mb-3">
                    <p class="text-light-gray records">
                        <span>Producer name :</span> Greg Kurstin
                    </p>
                </div>
                <div class="col-4 mb-3">
                    <p class="text-light-gray records tags">
                        <span>Tags :</span>
                        <a href="#">song</a>
                        <a href="#">Sia</a>
                        <a href="#">Music</a>
                        <a href="#">Rock</a>
                        <a href="#">Dance</a>
                    </p>
                </div>
                <div class="col-4">
                    <p class="text-light-gray records">
                        <span>Artist Name :</span> Sia
                    </p>
                </div>
                <div class="col-4">
                    <p class="text-light-gray records">
                        <span>Writer Name : :</span> Greg Kurstin
                    </p>
                </div>
                <div class="col-12 mb-4">
                    <div class="line mt-4"></div>
                </div>
            </div>
            <!-- video pitch -->
            <div class="row">
                <div class="col-12 heading">
                    <h2 class="text-white mb-3">Video Pitch</h2>
                </div>
                <div class="col-12 video-pitch mb-4">
                    <iframe src="https://www.youtube.com/embed/rJNFK-AaH_g" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            </div>
            <!-- upcoming show -->
            <div class="row">
                <div class="col-12 heading">
                    <h2 class="text-white mb-3">Upcomign Show</h2>
                </div>
                <div class="col-3">
                    <div class="upcoming-events">
                        <?= $this->Html->image('upcoming.jpg',  ['alt' => 'upcoming']);?>
                        <div class="show-details">
                            <ul>
                                <li>Place: California</li>
                                <li>Date: 07/08/2019</li>
                                <li>Time: 6 PM</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="upcoming-events">
                        <?= $this->Html->image('upcoming.jpg',  ['alt' => 'upcoming']);?>
                        <div class="show-details">
                            <ul>
                                <li>Place: California</li>
                                <li>Date: 07/08/2019</li>
                                <li>Time: 6 PM</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="upcoming-events">
                        <?= $this->Html->image('upcoming.jpg',  ['alt' => 'upcoming']);?>
                        <div class="show-details">
                            <ul>
                                <li>Place: California</li>
                                <li>Date: 07/08/2019</li>
                                <li>Time: 6 PM</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="upcoming-events">
                        <?= $this->Html->image('upcoming.jpg',  ['alt' => 'upcoming']);?>
                        <div class="show-details">
                            <ul>
                                <li>Place: California</li>
                                <li>Date: 07/08/2019</li>
                                <li>Time: 6 PM</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <footer class="catalogue-footer">
            <div class="col-12">
                <div class="border-custom">
                    <div class="row">
                        <div class="col-lg-3 col-sm-12">
                            <?= $this->html->image('white-logo.png') ?>
                        </div>

                        <div class="col-sm-6 col-lg-2">
                            <h4>Company</h4>
                            <ul class="list-unstyled">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">What we do</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Upcoming events</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-lg-2">
                            <h4>Quick Link</h4>
                            <ul class="list-unstyled">
                                <li><a href="#">For artist</a></li>
                                <li><a href="#">FAQ'S</a></li>
                                <li><a href="#">Merchandise</a></li>

                            </ul>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <h4>Contact Us</h4>
                            <ul class="list-unstyled">
                                <li><a href="#">For artist</a></li>
                                <li><a href="#">FAQ'S</a></li>
                                <li><a href="#">Merchandise</a></li>

                            </ul>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <h4>Follow Us</h4>
                            <ul class="list-unstyled">
                                <li><a href="#">
                                        <?= $this->html->image('images/facebook-icon.png') ?> Facebook</a></li>
                                <li><a href="#">
                                        <?= $this->html->image('images/tiwtter.png') ?> Tiwtter</a></li>
                                <li><a href="#">
                                        <?= $this->html->image('images/instagrams.png') ?> Instagram</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copy-right text-center">
            <p>©2019 All copyrights reserved SoundCheckLive</p>
        </div>
    </div>
</div>