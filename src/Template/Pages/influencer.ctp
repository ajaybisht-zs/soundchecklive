<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
        ]);
    ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>
</head>

<body>

    <div class="container mt-5 mb-5">
        <div class="inner-wraper-influencer col-sm-9 m-auto mt-4 p-4 pb-5">
            <div class="col-sm-12 text-center">
                <?= $this->Html->image('small-logo-png.png', ['alt' => 'logo','class' => 'mobile-logo']);?>
            </div>


            <div class="col-sm-12 mb-4">
                <h2>Become a SCL Influencer</h2>
            </div>

            <div class="row px-3">
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Instagram username">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="How many IG followers do you have?">
                    </div>
                </div>
            </div>
            <div class="row px-3">
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Twitter username">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="How many Twitter followers do you have?">
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-4">
                <label>Date of birth</label>
            </div>

            <div class="row px-3 ">
                <div class="col-sm-4">
                    <div class="form-group">
                        <select class="form-control">
                            <option>Month</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <select class="form-control">
                            <option>Day</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <select class="form-control">
                            <option>Year</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-check ">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">By checking here i agree that I am at least 18
                        years old and agree to the terms of use policy.</label>
                </div>
            </div>

            <div class="col-sm-12 mt-5 text-right px-3 mb-4 mb-sm-0">
                <button class="btn btn-submit">Submit</button>
            </div>


        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
</body>

</html>