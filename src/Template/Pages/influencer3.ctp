<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
        ]);
    ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>
</head>
<body>

    <div class="container mt-5 mb-5">
        <div class="inner-wraper-influencer col-sm-6 m-auto mt-4 p-4 pb-5">
            <div class="col-sm-12 mb-4 mt-4">
                <h2>Sheesh. Your close but not quite there.</h2>
            </div>
            <div class="col-sm-12 mb-3">
                <p>
                    Your follower count is close but Team Equity is exclusive to those with higher social media numbers.
                    However no worries as you can still find your favorite Creator and Mobilize a campaign to help them
                    grow and earn more will doing so.
                </p>
            </div>
            <div class="row px-3">
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="row px-3">
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Password">
                    </div>
                </div>
            </div>
            <div class="row px-3">
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Re-enter Password">
                    </div>
                </div>
            </div>
            <div class="row px-3">
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Zip Code">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 mt-5 text-right px-3">
                <button class="btn btn-submit">Mobilize</button>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
</body>

</html>