<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
        ]);
    ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>
</head>
<body>

    <div class="container mt-5 mb-5">
        <div class="inner-wraper-influencer col-sm-6 m-auto mt-4 p-2 pb-5 pt-5">
            <div class="col-sm-12 mb-4">
                <h2>Congrats. Now lets get verified and draft!</h2>
            </div>
            <div class="col-sm-12 mb-3">
                <p>
                    Per your social media profiles you have a combined follower count of 26,203 followers. Before you can draft Creators to your roster we need to make sure you are who you say you are. Verify your account by posting the image below to the social media accounts you signed up with and tag us @schecklive. If you prefer you can delete the image once you've been verified. After that your good to go.
                </p>
            </div>

            <div class="row col-sm-10 m-auto pt-4 box-list-wraper">
                <div class="col-sm-6 text-center">
                    <div class="instagram-text">Instagram</div>
                    <a href="#">
                        <div class="red-list-box">
                            <div> I Am A</div>
                            <div>Sound check live</div>
                            <div>Influencer</div>
                        </div>
                    </a>
                  
                </div>
                <div class="col-sm-6 text-center twitter-box">
                    <div class="instagram-text">Twitter</div>
                    <a href="#">
                    <div class="red-list-box">
                        <div> I Am A</div>
                        <div>Sound check live</div>
                        <div>Influencer</div>
                    </div>
                    </a>
                </div>
            </div>
            
          
           
           
           
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
</body>

</html>