<?php
    use Cake\Routing\Router;

?>
<?= $this->Html->css('backend/common/bootstrap4-toggle.min'); ?>
<?= $this->Html->css('backend/common/jquery.filer'); ?>

<div class="text-center mt-6">
	<label>Switch to Down site</label>
	<div>
    	<input type="checkbox"  class="dw" data-toggle="toggle" data-onstyle="success" data-url="/site-under-construction"<?php echo ($siteDown->status == 1) ? "checked" : ""?>>	
	</div>
</div>

<?php
    echo $this->Html->script([
      'vendor/jquery.min',
      'vendor/bootstrap.bundle.min',
      'common/bootstrap4-toggle.min',
      'backend/Admin/s_d',
    ]);
   // echo $this->fetch('script');
?>
