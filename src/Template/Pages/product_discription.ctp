<div class="breadcrumb-nav">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Merchandise </li>
                        <li class="breadcrumb-item active" aria-current="page">Product description</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mb-5">
        <div class="col-5 text-center">
            <div class="list-main-product my-5">
                <?= $this->html->image('product.jpg',['class' => 'display-img'])?>
                <div class="arrow-section d-block d-sm-none">
                    <div class="left-arrow"> <a href="javascript:void(0)"><img src="img/left-arrow.svg"> </a></div>
                    <div class="right-arrow"><a href="javascript:void(0)"><img src="img/right-arrow.svg"></a></div>
                </div>
            </div>
            <div class="icon-images text-left">
                <ul>
                    <li class="active">
                        <?= $this->html->image('product1.jpg')?>
                    </li>
                    <li>
                        <?= $this->html->image('product2.jpg')?>
                    </li>
                    <li>
                        <?= $this->html->image('product3.jpg')?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-7 mt-5">
            <div>
                <div class="product-title mb-3">
                    <h2>Ice Cube "Bye, Felicia." <br /> Charity Tee</h2>
                </div>
                <div class="product-discription mb-3">
                    <p>
                        Fusce mattis sagittis dolor sed lobortis. Vivamus vitae quam sollicitudin ipsum accumsan sodales
                        ongue at velit. Nullam id lobortis magna. Nunc ullamcorper condimentum consequat. Maecenas ut
                        convallis metus, bibendum sollicitudin diam.
                    </p>
                </div>
                <div class="product-price mb-1">$400</div>
                <div class="procuct-discount text-red mb-3">50% OFF WITH SOUND CHECK LIVE</div>
                <div class="product-color">
                    <div class="row">
                        <div class="col-12">
                            <h2>Color</h2>
                        </div>
                        <div class="col-auto pr-0">
                            <input type="checkbox" class="form-check-input">
                            <label class="form-check-label"
                                style="width: 30px; height: 30px; background: #000;"></label>
                        </div>
                        <div class="col-auto pr-0">
                            <input type="checkbox" class="form-check-input">
                            <label class="form-check-label"
                                style="width: 30px; height: 30px; background: #e40734;"></label>
                        </div>
                        <div class="col-auto pr-0">
                            <input type="checkbox" class="form-check-input">
                            <label class="form-check-label"
                                style="width: 30px; height: 30px; background: #2164bf;"></label>
                        </div>
                        <div class="col-auto pr-0">
                            <input type="checkbox" class="form-check-input">
                            <label class="form-check-label"
                                style="width: 30px; height: 30px; background: #7c00cb;"></label>
                        </div>
                    </div>
                </div>
                <div class="procuct-size mt-2 mb-2">
                    <div class="row">
                        <div class="col-12">
                            <h2>Size</h2>
                        </div>
                    </div>
                    <div class="product-size">
                        <div class="row">
                            <div class="col-auto pr-0">
                                <input type="checkbox" class="check-size">
                                <label class="label-check-size">38</label>
                            </div>
                            <div class="col-auto pr-0">
                                <input type="checkbox" class="check-size">
                                <label class="label-check-size">40</label>
                            </div>
                            <div class="col-auto pr-0">
                                <input type="checkbox" class="check-size">
                                <label class="label-check-size">42</label>
                            </div>
                            <div class="col-auto pr-0">
                                <input type="checkbox" class="check-size">
                                <label class="label-check-size">44</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-add mb-3 mt-3">
                    <span class="plus button mr-2">
                        <svg version="1.1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 52 52"
                            style="enable-background:new 0 0 52 52;" xml:space="preserve">
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28H28v11c0,1.104-0.896,2-2,2
                           s-2-0.896-2-2V28H13.5c-1.104,0-2-0.896-2-2s0.896-2,2-2H24V14c0-1.104,0.896-2,2-2s2,0.896,2,2v10h10.5c1.104,0,2,0.896,2,2
                           S39.604,28,38.5,28z" />
                        </svg>
                    </span>
                    <input type="text" name="qty" id="qty" maxlength="12" />
                    <span class="min button ml-2">
                        <svg version="1.1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 52 52"
                            style="enable-background:new 0 0 52 52;" xml:space="preserve">
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28h-25c-1.104,0-2-0.896-2-2
                                        s0.896-2,2-2h25c1.104,0,2,0.896,2,2S39.604,28,38.5,28z" />
                        </svg>
                    </span>
                </div>
                <div class="add-cart">
                    <button class="bg-dark text-white py-2 px-4 text-uppercase mb-4">
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 489 489" width="18px" height="18px" fill="#fff"
                            style="enable-background:new 0 0 489 489;" xml:space="preserve">
                            <g>
                                <path
                                    d="M440.1,422.7l-28-315.3c-0.6-7-6.5-12.3-13.4-12.3h-57.6C340.3,42.5,297.3,0,244.5,0s-95.8,42.5-96.6,95.1H90.3
                                            c-7,0-12.8,5.3-13.4,12.3l-28,315.3c0,0.4-0.1,0.8-0.1,1.2c0,35.9,32.9,65.1,73.4,65.1h244.6c40.5,0,73.4-29.2,73.4-65.1
                                            C440.2,423.5,440.2,423.1,440.1,422.7z M244.5,27c37.9,0,68.8,30.4,69.6,68.1H174.9C175.7,57.4,206.6,27,244.5,27z M366.8,462
                                            H122.2c-25.4,0-46-16.8-46.4-37.5l26.8-302.3h45.2v41c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5v-41h139.3v41
                                            c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5v-41h45.2l26.9,302.3C412.8,445.2,392.1,462,366.8,462z" />
                            </g>
                        </svg>
                        Add to bag
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Change image on thumbnail click.
    var thumbs = $(".icon-images").find("img");
    thumbs.click(function () {
        var src = $(this).attr("src");
        $(".icon-images").find('li').removeClass('active');
        $(this).closest('li').addClass('active')
        var dp = $(".display-img");
        dp.attr("src", src);
    });

    $('.left-arrow').on('click', function () {
        var current_li = $(".icon-images").find(".active");
        var li = $(current_li).prev("li");
        var src = $(li).find('img').prop('src');
        if (src) {
            $(current_li).removeClass('active')
            $(li).addClass('active');
            var dp = $(".display-img");
            dp.attr("src", src);
        }
    })

    $('.right-arrow').on('click', function () {
        var current_li = $(".icon-images").find(".active");
        var li = $(current_li).next("li");
        var src = $(li).find('img').prop('src');
        if (src) {
            $(current_li).removeClass('active')
            $(li).addClass('active');
            var dp = $(".display-img");
            dp.attr("src", src);
        }
    })
</script>