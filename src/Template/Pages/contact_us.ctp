<div class="container">
    <div class="bg-white mt-4 mb-4 about-us">
        <h2 class="mb-4">Contact Us</h2>
        <?php
                echo $this->Form->create(null, [
                    'url' => [
                        'controller' => 'Homes',
                        'action' => 'contactUs',
                        'prefix' => false
                    ],
                    'id' => 'contact-us',
                    'type' => 'file'
                ]);
            ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label> First Name</label>
                    <?php echo $this->Form->control('first_name', [
                            'type' => 'text',
                            'placeholder' => 'Full Name',
                            'class' => 'form-control',
                            'label' => false,
                            'id' => 'firstName',
                            'required' => true
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Last Name</label>
                    <?php echo $this->Form->control('last_name', [
                            'type' => 'text',
                            'placeholder' => 'Last Name',
                            'class' => 'form-control',
                            'label' => false,
                            'id' => 'lastName',
                            'required' => true
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Email Address</label>
                    <?php
                        echo $this->Form->control('email', [
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'Enter Email Address',
                            'label' => false,
                            'autocomplete' => 'off',
                            'required' => false,
                            'required' => true
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Phone Number</label>
                    <?php
                        echo $this->Form->control('phone', [
                            'type' => 'number',
                            'class' => 'form-control',
                            'placeholder' => 'Mobile Number',
                            'label' => false,
                            'autocomplete' => 'off',
                            'required' => false,
                            'required' => true
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label> Message</label>
                    <textarea class="form-control" rows="4" placeholder="Enter your Message here.." name="message"></textarea>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <button class="btn bg-dark text-white" type="submit">Submit</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
    </div>
</div>