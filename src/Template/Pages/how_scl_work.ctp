<div class="container p-0 bg-white mt-0 mt-sm-4">
    <video width="100%" controls="controls">
        <source src="https://soundchecklive.co/files/HowWeWorks/video_name/SCL Commercial J.mp4"
            type="video/mp4"></video>
</div>
<div class="container bg-white  mb-4 about-us how-scl-works">
    <h2 class="mb-4">HOW SCL WORKS?</h2>


    <div class="row justify-content-center align-items-center mb-custom">
        <div class="col-sm-6">
            <p>Every Creator needs a budget; whether it is for recording, marketing or day-to-day living expenses. Let’s take music for instance. A single music composition requires studio time, engineering fees, producer licensing fees, mix, mastering,
                artwork, distribution, visuals and a host of promotional contracting. Case and point most creators start their business in the red, with more debits than credits. Consequently, they seek lopsided deals because independence isn’t easy.
            </p>
        </div>

        <div class="col-sm-6">
            <?php
                echo $this->Html->image('slider1.jpg', [
                       'class' => 'w-100'
                ]);
            ?>
        </div>

    </div>
    <div class="row  justify-content-center align-items-center mb-custom">
        <div class="col-sm-6 d-none d-sm-block">
            <?php
                echo $this->Html->image('slider2.jpg', [
                       'class' => 'w-100'
                ]);
            ?>
        </div>
        <div class="col-sm-6">
            <p>We assist creators by strategizing campaigns to align them with their core fan base and acquire the financing they need. In the end, the creator maintains ownership of their intellectual properties and fans receive both content and a great
                incentive to help creators build.

            </p>
        </div>

        <div class="col-sm-6 d-sm-none">
            <?php
                echo $this->Html->image('slider2.jpg', [
                       'class' => 'w-100'
                ]);
            ?>
        </div>



    </div>

    <div class="row justify-content-center align-items-center mb-custom">
        <div class="col-sm-6">
            <h6>FAN: DOUBLE YOUR MONEY!</h6>
            <p>
                As a fan, you’re going to stream the music anyway. Each stream counts as income for an artist. If you like it, you'll tell a friend or 2. Your “like” has just turned into an endorsement and more revenue for the artist as your friends are likely to stream
                the music as well. So rather than buy, stream or promote a song, PARTNER UP, making you and your favorite independent artist now business partners.

            </p>
        </div>

        <div class="col-sm-6">
            <?php
                echo $this->Html->image('slider8.jpg', [
                       'class' => 'w-100'
                ]);
            ?>
        </div>

    </div>

    <div class="row justify-content-center align-items-center mb-custom">
        <div class="col-sm-12">

            <p>
                <span class="text-red">Sound Check Live</span> uses algorithmic technology to calculate what it would take to double your investment and sets the marker there. Every time a check is cut to the creator, one will be cut to you as well —
                mailbox money.

            </p>
        </div>



    </div>



    <div class="col-sm-12 text-center mt-5 mb-5">
        <?=
                                        $this->Html->link(
                                            'Mobilize Campaign',
                                            '#',
                                            [
                                                'class' => 'btn btn-blue',
                                                'escape' => false
                                            ]
                                        );
                                        ?>


    </div>
</div>