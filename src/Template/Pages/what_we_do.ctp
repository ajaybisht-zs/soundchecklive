<div class="container p-0 bg-white mt-0 mt-sm-4">
    <video width="100%" controls="controls">
        <source src="https://soundchecklive.co/files/HowWeWorks/video_name/SCL Commercial J.mp4"
            type="video/mp4">
    </video>
</div>
<div class="container bg-white  mb-4 about-us">
    <h2 class="mb-4">What we do</h2>
    <p><span class="text-red">Sound Check Live</span> supports ambitious creators by providing a reliable 24/7 platform
        for discovery and funding. We walk hand and hand with each creator to ensure success through development of
        realistic goals and fulfillment.
        Our business model is based around fan protection to further strengthen the relationship between Creators and
        Partners.
    </p>
    <div class="text-center mb-5 mt-5">
        <?php
            echo $this->Html->image('sneaker-image.jpg', [
            'class' => 'sneaker-img'
            ]);
        ?>
    </div>
    <div class="col-sm-12">
        <div class="row mb-5 mt-5">
            <div class="col-sm-6">
                <h5 class="mb-4 mt-4">Creator Support</h5>
                <ul class="list-disc pl-3">
                    <li>Develop realistic number focused strategies</li>
                    <li>Create personalized budgets and campaigns</li>
                    <li>Finalize and distribute the product</li>
                    <li>Align outlined goals with specialized partners</li>
                    <li>Assist in budget acquisition through fan engagement</li>
                    <li>Grow your core fan base</li>
                    <li>Introduce you to new audiences</li>
                    <li>Track your royalty goals</li>
                    <li>Introduce you to new revenue streams</li>
                </ul>
            </div>
            <div class="col-sm-6">
                <h5 class="mb-4 mt-4">Fan Support</h5>
                <ul class="list-disc pl-3">
                    <li>Find the content you like</li>
                    <li>Partner with your chosen creator</li>
                    <li>We help every creator market their product to reach specified goals</li>
                    <li>Creators intellectual properties are held in escrow</li>
                    <li>As milestones are reached partners will be notified</li>
                    <li>Once the goal has been achieved funds will be dispersed appropriately</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row browse-catalog">
        <div class="text-center col-12">
            <?=
                $this->Html->link(
                    'Browse catalog',
                    [
                        'controller' => 'Catalogues',
                        'action' => 'index',
                        'prefix' => false,
                        
                    ],
                    [
                        'class' => 'btn btn-blue',
                        'escape' => false
                    ]
                );
            ?>
        </div>
    </div>
</div>
