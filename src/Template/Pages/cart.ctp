<div class="breadcrumb-nav">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Merchandise </li>
                        <li class="breadcrumb-item" aria-current="page">Product description</li>
                        <li class="breadcrumb-item active" aria-current="page">Cart</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="cart mt-5">
    <div class="container">
        <div class="row justify-content-center align-items-center mb-3 top-heading">
            <div class="col-lg col-md-2">&nbsp</div>
            <div class="col-lg col-md-3 font-weight-500">Product Name</div>
            <div class="col-lg col-md-2 font-weight-500">Price</div>
            <div class="col-lg col-md-1 font-weight-500">Qty</div>
            <div class="col-lg col-md-2 font-weight-500">Total</div>
            <div class="col-lg col-md-2">&nbsp</div>
        </div>
        <div class="row justify-content-md-center align-items-center py-2 bg-light-gray ">
            <div class="col-lg col-md-2 col-3">
                <?= $this->Html->image('dummy-img.jpg', ['alt' => 'logo','class' => 'product-dummy']);?>
            </div>
            <div class="col-lg col-md-3 col-9">Ice Cube "Bye, Felicia." Charity Tee</div>
            <div class="col-lg col-md-2 col-4">$24.00</div>
            <div class="col-lg col-md-1 col-2">2</div>
            <div class="col-lg col-md-2 col-2">$48.00</div>
            <div class="col-lg text-right col-md-2 col-4">
                <button class="bg-dark text-white remove-btn">Remove</button>
            </div>
        </div>
        <div class="row justify-content-md-center align-items-center py-2">
            <div class="col-lg col-md-2 col-3">
                <?= $this->Html->image('dummy-img.jpg', ['alt' => 'logo','class' => 'product-dummy']);?>
            </div>
            <div class="col-lg col-md-3 col-9">Ice Cube "Bye, Felicia." Charity Tee</div>
            <div class="col-lg col-md-2 col-4">$24.00</div>
            <div class="col-lg col-md-1 col-2">2</div>
            <div class="col-lg col-md-2 col-2">$48.00</div>
            <div class="col-lg col-md-2 col-4 text-right">
                <button class="bg-dark text-white remove-btn">Remove</button>
            </div>
        </div>
        <div class="row justify-content-md-center align-items-center py-2 bg-light-gray">
            <div class="col-lg col-md-2 col-3">
                <?= $this->Html->image('dummy-img.jpg', ['alt' => 'logo','class' => 'product-dummy']);?>
            </div>
            <div class="col-lg col-md-3 col-9">Ice Cube "Bye, Felicia." Charity Tee</div>
            <div class="col-lg col-md-2 col-4">$24.00</div>
            <div class="col-lg col-md-1 col-2">2</div>
            <div class="col-lg col-md-2 col-2">$48.00</div>
            <div class="col-lg col-md-2 col-4 text-right">
                <button class="bg-dark text-white remove-btn">Remove</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right mt-4">
                <p class="sub-total">Subtotal <span class="ml-4">$144</span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right my-4">
                <button class="btn bg-dark text-white py-2 px-4 font-weight-500">checkout</button>
            </div>
        </div>
    </div>
</div>