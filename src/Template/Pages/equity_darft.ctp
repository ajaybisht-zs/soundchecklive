<?php
    // Called as Text
    use Cake\Utility\Text;

    $totalItemInCart = 0;
     $cart= '';
    $session = $this->request->getSession();
    // $totalItemInCart+= count($session->read('cart'));
    if($totalItemInCart != 0) {
        $cart = '<span class="badge badge-light position-absolute">'.$totalItemInCart.'</span>';
    }

    if($this->request->getParam('pass.0') == 'film') {
        $music = null;
        $film = 'active';
        $fashion = null;
    } elseif($this->request->getParam('pass.0') == 'fashion') {
        $music = null;
        $film = null;
        $fashion = 'active';
    } else {
        $music = 'active';
        $film = null;
        $fashion = null;
    }
    $session = $this->getRequest()->getSession();
    $userInfo  = $session->read('Auth.User');
    $userId = $session->read('Auth.User.id');
?>
<!doctype html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <!-- bootstrap css -->
    <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
            //'stickyaudioplayerjquery.min',
            'frontend/player'
        ], ['block' => true]);
    ?>
    <?= $this->fetch('css') ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title>Music Catalogue</title>
</head>

<body class="d-flex flex-column h-100 catalogue-page">
    <!--Start include fan manu -->
    <?= $this->element('Fan/fan_menu');?>
    <!--end fan menu -->
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark py-0">
            <a class="navbar-brand"
                href="<?= $this->Url->build(['controller' => 'homes', 'action' => 'index','prefix' => false]) ?>">
                <?= $this->Html->image('logo.png', ['alt' => 'logo','class' => 'd-none d-md-block']);?>
                <?= $this->Html->image('mob-logo.png', ['alt' => 'logo','class' => 'd-block d-md-none']);?>
            </a>

            <div class="ml-auto top-login d-flex align-items-center">
                <form class="form-inline d-inline-block search">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-secondary border border-secondary" id="basic-addon1">
                                <svg width="20" height="20" viewBox="0 0 25 22">
                                    <path class="fill_path" fill="#FFF" fill-rule="evenodd"
                                        d="M69.5 34a6.5 6.5 0 0 1 6.5 6.5c0 1.61-.59 3.09-1.56 4.23l.27.27h.79l5 5-1.5 1.5-5-5v-.79l-.27-.27A6.516 6.516 0 0 1 69.5 47a6.5 6.5 0 1 1 0-13zm0 2C67 36 65 38 65 40.5s2 4.5 4.5 4.5 4.5-2 4.5-4.5-2-4.5-4.5-4.5z"
                                        transform="translate(-59 -32)"></path>
                                </svg>
                            </span>
                        </div>
                        <input type="text"
                            class="form-control form-control-sm bg-secondary border border-secondary search"
                            placeholder="Search" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </form>

                <?= $this->Html->link(
                    '<svg version="1.1" class="mobile-cart" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 59 59" width="40px" height="40px" style="enable-background:new 0 0 59 59;" xml:space="preserve">
                        <g>
                            <g>
                                <circle style="fill:#FFFFFF;" cx="22" cy="48" r="5"/>
                                <path style="fill:#556080;" d="M22,54c-3.309,0-6-2.691-6-6s2.691-6,6-6s6,2.691,6,6S25.309,54,22,54z M22,44
                                    c-2.206,0-4,1.794-4,4s1.794,4,4,4s4-1.794,4-4S24.206,44,22,44z"/>
                            </g>
                            <g>
                                <circle style="fill:#FFFFFF;" cx="45" cy="48" r="5"/>
                                <path style="fill:#556080;" d="M45,54c-3.309,0-6-2.691-6-6s2.691-6,6-6s6,2.691,6,6S48.309,54,45,54z M45,44
                                    c-2.206,0-4,1.794-4,4s1.794,4,4,4s4-1.794,4-4S47.206,44,45,44z"/>
                            </g>
                            <path style="fill:#556080;" d="M55,48h-5.101c-0.553,0-1-0.447-1-1s0.447-1,1-1H55c0.553,0,1,0.447,1,1S55.553,48,55,48z"/>
                            <path style="fill:#556080;" d="M40.101,48H26.899c-0.553,0-1-0.447-1-1s0.447-1,1-1h13.201c0.553,0,1,0.447,1,1
                                S40.653,48,40.101,48z"/>
                            <g>
                                <path style="fill:#e40734;" d="M15,39L9.833,13H58v22.012C58,37.215,56.215,39,54.012,39H15"/>
                                <path style="fill:#556080;" d="M54.013,40H14.179L8.614,12H59v23.013C59,37.763,56.763,40,54.013,40z M15.821,38h38.191
                                    C55.66,38,57,36.66,57,35.013V14H11.052L15.821,38z"/>
                            </g>
                            <path style="fill:#556080;" d="M9.832,14c-0.48,0-0.904-0.347-0.985-0.836L8.152,9H6C5.447,9,5,8.553,5,8s0.447-1,1-1h3.848
                                l0.972,5.836c0.091,0.545-0.277,1.06-0.822,1.15C9.941,13.996,9.887,14,9.832,14z"/>
                            <circle style="fill:#e40734;" cx="3" cy="8" r="3"/>
                            <path style="fill:#556080;" d="M17.101,48H14c-1.406,0-2.758-0.603-3.707-1.652c-0.947-1.047-1.409-2.453-1.268-3.858
                                C9.28,39.972,11.548,38,14.188,38c0.553,0,1,0.447,1,1s-0.447,1-1,1c-1.627,0-3.021,1.182-3.173,2.69
                                c-0.087,0.855,0.184,1.678,0.761,2.316C12.348,45.638,13.158,46,14,46h3.101c0.553,0,1,0.447,1,1S17.653,48,17.101,48z"/>
                        </svg>'.$cart ,
                    [
                        'controller' => 'carts',
                        'action' => 'cart',
                        'prefix' => false,
                    ],
                    [
                        'class' => 'register px-4 mr-5 position-relative',
                        'escape' => false
                    ]
                );?>
            </div>
            <div class="outer-menu">
                <input class="checkbox-toggle" type="checkbox" />
                <div class="hamburger">
                    <div></div>
                </div>
                <div class="menu">
                    <div>
                        <div>
                            <ul>
                                <li>
                                    <?=
                                    $this->Html->link(
                                        'Home',
                                        [
                                            'controller' => 'homes',
                                            'action' => 'index',
                                            'prefix' => false
                                        ]
                                    );
                                ?>
                                </li>
                                <li>
                                    <?=
                                        $this->Html->link(
                                            'CATALOG',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>

                                <li>
                                    <?=
                                                $this->Html->link(
                                                    'LE APPAREL',
                                                    [
                                                        'controller' => 'Catalogues',
                                                        'action' => 'index',
                                                        'prefix' => false
                                                    ]
                                                );
                                            ?>
                                </li>

                                <li>
                                    <?=
                                        $this->Html->link(
                                            'ARTIST LOGIN',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false
                                            ]
                                        );
                                    ?>
                                </li>
                                <li>
                                    <?=
                                                $this->Html->link(
                                                    'PARTNER LOGIN',
                                                    [
                                                        'controller' => 'Catalogues',
                                                        'action' => 'index',
                                                        'prefix' => false
                                                    ]
                                                );
                                            ?>
                                </li>



                                <li>
                                    <?= $this->Html->link(
                                                'About us',
                                                '/about-us',
                                                ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>
                                <li>
                                    <?= $this->Html->link(
                                            'WHAT WE DO',
                                            '/what-we-do',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?= $this->Html->link(
                                            'HOW SCL WORKS?',
                                            '/how-scl-work',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?php  $this->Html->link(
                                            'FAQ’S',
                                            '/faq',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>

                                <li>
                                    <?= $this->Html->link(
                                            'Contact',
                                            '/contact-us',
                                            ['class' => 'login mr-2 mr-sm-4']
                                            );?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <div class="catalouge-wraper catalouge-wraper-equity  clearfix">
        <div class="left-panel">
            <!--Start catalouge sidebar -->
            <?= $this->element('Catalogues/catalogue_sidebar_new');?>
            <!--End catalouge sidebar -->
        </div>

        <div class="right-panel equity-right-panel ">
            <marquee class="marq" bgcolor="black" scrollamount="6" direction="left">
                <div class="bg-black text-white px-4 py-3">
                    <div class="px-3 lead d-inline-block">
                        <span class="mr-2">Top 20 Artists:</span>
                    </div>
                    <?php for ($i=0; $i < 100; $i++) { $rand = rand(1,100); ?>
                    <div class="px-3 lead d-inline-block">
                        <span class="mr-2">Sara Lee</span>
                        <span class="text-success"><?php echo "&#x025B4;"?></span>
                        <span class="ml-2">+<?php echo $rand ?></span>
                    </div>
                    <?php } ?>
                </div>
            </marquee>
            <div class="news-alert">
                <marquee class="marq marq-red-strip" bgcolor="#e40734" scrollamount="5" direction="left">
                    <div class=" text-white px-4 py-1">
                        <div class="px-2 lead-normal d-inline-block">

                        </div>
                        <?php for ($i=0; $i < 100; $i++) { $rand = rand(1,100); ?>

                        <div class="lead-normal d-inline-block mr-3">
                            ***News Alert:&nbsp; &nbsp; Current Royalty Rates - YouTube $.0009/Stream
                        </div>
                        <div class="lead-normal d-inline-block mr-3">
                            Apple Music $.0009/Stream
                        </div>
                        <div class="lead-normal d-inline-block mr-3">
                            Spotify $.0009/Stream
                        </div>
                        <div class="lead-normal d-inline-block mr-3">
                            Tidal $.0009/Stream
                        </div>
                        <?php } ?>
                    </div>
                </marquee>
            </div>

            <div class="row col-12 mt-3">
                <div class="col-sm-5 equity-nav">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">Available Drafts</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">My Roster</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">My Earnings</a>
                        </li>
                    </ul>

                </div>
                <div class="col-sm-7 d-flex align-items-center personal-short-code justify-content-end">

                    Personal Short Code: JNEL1 (text JNEL1 to 378489 or EQUITY) <span><button class="btn cashout">Cash
                            Out</button> </span>

                </div>
            </div>

            <div class="col-sm-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <table class="table table-equity">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Artist</th>
                                    <th scope="col">Album</th>
                                    <th scope="col">Date Added</th>
                                    <th>Time</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50px">
                                        <?php echo $this->Html->link($this->Html->image("slider13.jpg", array("alt" => "img")),"#", array('class' => 'equity-draft-img', 'escape' => false));?>

                                    </td>
                                    <td> Come Together <span class="label-equity">Explicit</span><span
                                            class="master-equity">Master</span></td>
                                    <td>Chris Brown, H.E.R.</td>
                                    <td>Indigo</td>
                                    <td>Last Week</td>
                                    <td>3:54</td>
                                    <td> <span>+</span>
                                        <span><?php echo $this->Html->link($this->Html->image("like.svg", array("alt" => "img")),"#", array('class' => 'equity-draft-svg', 'escape' => false));?></span>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                </div>
            </div>


            <footer class="catalogue-footer">
                <!--sticky audio footer -->
                <div id="single-song-player" class="d-none">
                    <div class="bottom-container">
                        <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>

                        <div class="control-container col">
                            <div class="row justify-content-between align-items-start">
                                <div class="col-auto px-0">
                                    <div class="form-row align-items-center">
                                        <div class="col-auto">
                                            <img data-amplitude-song-info="cover_art_url" class="border-0" />
                                        </div>
                                        <div class="col col-sm-auto">
                                            <div class="meta-container text-left leading-tight">
                                                <div>
                                                    <span data-amplitude-song-info="name" class="mr-1">Song:</span>
                                                    <span data-amplitude-song-info="name" class="song-name"></span>
                                                </div>
                                                <div class="text-xs">
                                                    <span class="mr-1">Artist:</span>
                                                    <span data-amplitude-song-info="artist"></span>
                                                </div>
                                                <div class="clearfix">
                                                    <span class="current-time text-xs pr-2">
                                                        <span class="amplitude-current-minutes"></span>:<span
                                                            class="amplitude-current-seconds"></span>
                                                    </span>
                                                    <span class="duration text-xs border-left pl-2">
                                                        <span class="amplitude-duration-minutes"></span>:<span
                                                            class="amplitude-duration-seconds"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row justify-content-end justify-content-md-center align-items-center">
                                        <div class="col-auto">
                                            <div class="row justify-content-center align-items-center">
                                                <div class="col px-0 px-md-3 amplitude-prev mr-md-3"></div>
                                                <div class="col px-0 px-md-3 amplitude-play-pause m-auto"
                                                    id="play-pause"></div>
                                                <div class="col px-0 px-md-3 amplitude-next ml-md-3"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end sticky audio footer -->
                <!-- End music tab -->

                <div class="col-12">
                    <div class="border-custom">
                        <div class="row">
                            <div class="col-lg-3 col-sm-12">

                                <?= $this->html->image('white-logo.png') ?>
                            </div>

                            <div class="col-12 d-md-none">
                                <h3 class="mt-5 mb-0 text-white">INDEPENDENCE IS LIFE</h3>
                            </div>

                            <div class="col-sm-6 col-lg-2">
                                <h4>Company</h4>
                                <ul class="list-unstyled">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">What we do</a></li>
                                    <li><a href="#">Upcoming events</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-lg-2">
                                <h4>Quick Link</h4>
                                <ul class="list-unstyled">
                                    <li><a href="#">For artist</a></li>
                                    <li><a href="#">FAQ'S</a></li>
                                    <li><a href="#">Merchandise</a></li>

                                </ul>
                            </div>
                            <div class="col-lg-2 col-sm-6">
                                <h4>Contact Us</h4>
                                <ul class="list-unstyled">
                                    <li><a href="#">For artist</a></li>
                                    <li><a href="#">FAQ'S</a></li>
                                    <li><a href="#">Merchandise</a></li>

                                </ul>
                            </div>
                            <div class="col-lg-2 col-sm-6">
                                <h4>Follow Us</h4>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="https://www.facebook.com/schecklive" target="_blank">
                                            <?= $this->Html->image('images/facebook-icon.png') ?> Facebook</a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/schecklive" target="_blank">
                                            <?= $this->Html->image('images/tiwtter.png') ?> Tiwtter</a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/schecklive/" target="_blank">
                                            <?= $this->Html->image('images/instagrams.png') ?> Instagram</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copy-right text-center">
                <p>©2019 All copyrights reserved SoundCheckLive</p>
            </div>
        </div>
        <?=
    $this->Html->script([
        'frontend/jquery-3.3.1.min',
        'frontend/bootstrap.bundle.min',
        'frontend/owl.carousel',
        'common/howler.core.min',
        'common/stickyaudioplayerjquery.min',
        'Catalogues/index',
        'Catalogues/open-payment-model',
        'https://js.stripe.com/v3/',
        'Catalogues/stripe',
        'Catalogues/single-song-player',
        'common',
        'Catalogues/scl-live',
        'https://js.pusher.com/7.0/pusher.min.js',
        'common/notification'
    ], ['block' => true])
?>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.0.2/dist/amplitude.js"></script>
        <?= $this->fetch('script') ?>
        <?= $this->fetch('scriptBottom') ?>
        <script>
            $(document).on('click', '.show-partner-layout', function () {
                $('#show-partner-modal').removeClass('d-none');
                $('#show-partner-modal').addClass('sidebar-transition');
            });

            $(document).on('click', '.close', function () {
                $('#show-partner-modal').addClass('d-none');
            });
            $(document).on('click', '.checkbox-toggle', function () {
                $('body').toggleClass('over-flow');
            });
        </script>
        <style type="text/css">
            .right-panel h4 {
                font-size: 16px;
                font-weight: bold;
            }

            .over-flow {
                overflow: hidden;
            }
        </style>
</body>

</html>


<script>
    // Set the date we're counting down to
    var countDownDate = new Date("OCT 2, 2020 00:00:00").getTime();
    // Update the count down every 1 second
    var x = setInterval(function () {
        // Get today's date and time
        var now = new Date().getTime();
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = days + "DYS " + hours + "HRS "
            + minutes + "MIN " + seconds + "SEC";
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
</script>
