<div class="container p-0 bg-white mt-0 mt-sm-4">
    <video width="100%" controls="controls">
        <source src="https://soundchecklive.co/files/HowWeWorks/video_name/SCL Commercial J.mp4"
            type="video/mp4"></video>
</div>
<div class="container bg-white  mb-4 about-us">
    <h2 class="mb-4">About us</h2>
    <p><span class="text-red">Sound Check Live</span> is a marketplace for creators of music, film and fashion to pitch fans partnership opportunities in exchange for project financing. Creators can vend their rights to audiences at strategized price points
        and utilize the budgets earned to grow their brand.
        <span class="text-red">Sound Check Live</span> serves as a growing resource for creators while providing an open forum for fans and enthusiasts.</p>

    <p>Creators are not lone individuals; they're operating businesses. Fans are not just consumers; they're individual owners with a spending power that can move projects forward. Every creator needs a budget, and every fan can become a partner. That’s
        the <span class="text-red">Sound Check Live</span> moto.
    </p>
    <div class="col-sm-12 text-center quote-img">
        <?php
                echo $this->Html->image('founder-quote.jpg', [
                ]);
            ?>
    </div>
    <div class="col-sm-12 text-center mt-5 mb-5">
            <?=
                                        $this->Html->link(
                                            'Browse catalog',
                                            [
                                                'controller' => 'Catalogues',
                                                'action' => 'index',
                                                'prefix' => false,
                                                
                                            ],
                                            [
                                                'class' => 'btn btn-blue',
                                                'escape' => false
                                            ]
                                        );
                                        ?>

      
    </div>
</div>