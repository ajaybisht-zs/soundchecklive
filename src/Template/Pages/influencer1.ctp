<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <?=
        $this->Html->css([
            'frontend/bootstrap.min',
            'frontend/main',
            'frontend/base',
            'frontend/nav',
            'frontend/style',
            'frontend/owl.carousel',
        ]);
    ?>
    <!-- google fonts css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,800,900&display=swap"
        rel="stylesheet">
    <title><?= isset($title)?$title:''?></title>
</head>

<body>

    <div class="container mt-5 mb-5">
        <div class="inner-wraper-influencer col-sm-9 m-auto mt-4 p-1 p-sm-5 pb-5 ">
            <div class="col-sm-12 mb-4 mt-4 mt-sm-0  ">
                <h2>Great News!</h2>
            </div>
            <div class="col-sm-12">
                <p>Based on your follower count you qualify for Team Equity level “I”. <br>At this level you’ll receive.

                </p>
                <ul>
                    <li>8% ownership stake in all direct partner commitments</li>
                    <li>10 Roster slots
                    </li>
                    <li>New draft access after 24 hours
                    </li>
                    <li>Access to SC Live 1-on-1 Model

                </ul>
                <p>Claim your owners box and get working!</p>
            </div>
            <div class="col-sm-12 mt-5 text-right px-3">
                <button class="btn btn-submit">Claim!</button>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
</body>

</html>