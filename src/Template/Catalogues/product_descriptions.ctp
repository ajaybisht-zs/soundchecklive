<?php use Cake\I18n\Number;?>
<div class="breadcrumb-nav">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Merchandise </li>
                        <li class="breadcrumb-item active" aria-current="page">Product description</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mb-5">
        <div class="col-5 text-center">
            <div class="list-main-product my-5">
            <?php if(!empty($userMerchandise->user_merchandise_images)) :
                $res = $this->common->showThumbNailImage($userMerchandise->user_merchandise_images);
            ?>
                <?php if($res[0]) { ?>
                    <?= $this->Html->image($res[0],['alt' => 'merchandise-image', 'class' => 'w-100 display-img'])?>
                <?php } else {?>
                    <?= $this->Html->image('Noimage.png',['alt' => 'merchandise-image', 'class' => 'w-100'])?>
                <?php } ?>
            <?php 
                endif; 
            ?>
                <div class="arrow-section d-block d-sm-none">
                    <div class="left-arrow"> <a href="javascript:void(0)"><?= $this->Html->image('left-arrow.svg')?></div>
                    <div class="right-arrow"><a href="javascript:void(0)"><?= $this->Html->image('right-arrow.svg')?></a></div>
                </div>
            </div>
            <div class="icon-images text-left">
                <ul>
                    <?php if(!empty($userMerchandise->user_merchandise_images)) :
                        $res = $this->common->showThumbNailImage($userMerchandise->user_merchandise_images);
                    ?>
                    <?php if($res[0]) : 
                        foreach($res as $key => $val) :
                    ?>
                    <li class="<?= ($key==0)?'active':null ?>">
                        <?= $this->Html->image($val,['alt' => 'merchandise-image', 'class' => ''])?>
                    </li>
                    <?php 
                        endforeach;
                        endif;
                        endif;
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-7 mt-5">
            <div>
                <div class="product-title mb-3">
                    <h2><?= isset($userMerchandise->name)?$userMerchandise->name :'N/A'?></h2>
                </div>
                <div class="product-discription mb-3">
                    <p>
                        <?= isset($userMerchandise->description)?$userMerchandise->description :'N/A'?>
                    </p>
                </div>
                <div class="product-price mb-1"> <?= isset($userMerchandise->price)?Number::currency($userMerchandise->price,'USD'):'N/A'?></div>
                <div class="procuct-discount text-red mb-3">50% OFF WITH SOUND CHECK LIVE</div>
                <div class="product-color">
                    <div class="row">
                        <div class="col-12">
                            <h2>Color</h2>
                        </div>
                        <?php foreach($userMerchandise->colors as $val) :?>
                        <div class="col-auto pr-0">
                            <input type="radio" class="form-check-input" name="colors">
                            <label class="form-check-label"
                                style="width: 30px; height: 30px; background: <?= $val->hex_code?>;"></label>
                        </div>
                    <?php endforeach;?>
                    </div>
                </div>
                <div class="procuct-size mt-2 mb-2">
                    <div class="row">
                        <div class="col-12">
                            <h2>Size</h2>
                        </div>
                    </div>
                    <div class="product-size">
                        <div class="row">
                            <?php foreach($userMerchandise->sizes as $val) :?>
                                <div class="col-auto pr-0">
                                    <input type="radio" class="check-size" name="size">
                                    <label class="label-check-size"><?= $val->size?></label>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="product-add mb-3 mt-3 button-container">
                    <button class="px-2 min button mr-2 cart-qty-minus d-inline-block outline-none">
                        <svg version="1.1" x="0px" y="0px" width="22px" height="22px" viewBox="0 0 52 52"
                            style="enable-background:new 0 0 52 52;" xml:space="preserve">
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28h-25c-1.104,0-2-0.896-2-2
                                        s0.896-2,2-2h25c1.104,0,2,0.896,2,2S39.604,28,38.5,28z" />
                        </svg>
                    </button>
                    <input type="number" name="qty" id="qty" class = "qty" maxlength="12" value = 1 />
                    <button class="px-2 plus button ml-2 cart-qty-plus d-inline-block outline-none">
                        <svg version="1.1" x="0px" y="0px"  width="22px" height="22px" viewBox="0 0 52 52"
                            style="enable-background:new 0 0 52 52;" xml:space="preserve">
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28H28v11c0,1.104-0.896,2-2,2
                           s-2-0.896-2-2V28H13.5c-1.104,0-2-0.896-2-2s0.896-2,2-2H24V14c0-1.104,0.896-2,2-2s2,0.896,2,2v10h10.5c1.104,0,2,0.896,2,2
                           S39.604,28,38.5,28z" />
                        </svg>
                    </button>
                </div>
                <div class="add-cart">
                    <button class="bg-dark text-white py-2 px-4 text-uppercase mb-4">
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 489 489" width="18px" height="18px" fill="#fff"
                            style="enable-background:new 0 0 489 489;" xml:space="preserve">
                            <g>
                                <path
                                    d="M440.1,422.7l-28-315.3c-0.6-7-6.5-12.3-13.4-12.3h-57.6C340.3,42.5,297.3,0,244.5,0s-95.8,42.5-96.6,95.1H90.3
                                            c-7,0-12.8,5.3-13.4,12.3l-28,315.3c0,0.4-0.1,0.8-0.1,1.2c0,35.9,32.9,65.1,73.4,65.1h244.6c40.5,0,73.4-29.2,73.4-65.1
                                            C440.2,423.5,440.2,423.1,440.1,422.7z M244.5,27c37.9,0,68.8,30.4,69.6,68.1H174.9C175.7,57.4,206.6,27,244.5,27z M366.8,462
                                            H122.2c-25.4,0-46-16.8-46.4-37.5l26.8-302.3h45.2v41c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5v-41h139.3v41
                                            c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5v-41h45.2l26.9,302.3C412.8,445.2,392.1,462,366.8,462z" />
                            </g>
                        </svg>
                        Add to bag
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
  $this->Html->scriptStart(['block' => true]);
?>
 var thumbs = $(".icon-images").find("img");
    thumbs.click(function() {
        var src = $(this).attr("src");
        $(".icon-images").find('li').removeClass('active');
        $(this).closest('li').addClass('active')
        var dp = $(".display-img");
        dp.attr("src", src);
    });
    
    $('.left-arrow').on('click', function() {
        var current_li = $(".icon-images").find(".active");
        var li = $(current_li).prev("li");
        var src = $(li).find('img').prop('src');
        if (src) {
            $(current_li).removeClass('active')
            $(li).addClass('active');
            var dp = $(".display-img");
            dp.attr("src", src);
        }
    })
    
    $('.right-arrow').on('click', function() {
        var current_li = $(".icon-images").find(".active");
        var li = $(current_li).next("li");
        var src = $(li).find('img').prop('src');
        if (src) {
            $(current_li).removeClass('active')
            $(li).addClass('active');
            var dp = $(".display-img");
            dp.attr("src", src);
        }
    })

    var incrementPlus;
    var incrementMinus;

    var buttonPlus  = $(".cart-qty-plus");
    var buttonMinus = $(".cart-qty-minus");
    var incrementPlus = buttonPlus.click(function() {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");
        $n.val(Number($n.val())+1 );
    });

    var incrementMinus = buttonMinus.click(function() {
            var $n = $(this)
            .parent(".button-container")
            .find(".qty");
        var amount = Number($n.val());
        if (amount > 0) {
            $n.val(amount-1);
        }
    });

<?php
  $this->Html->scriptEnd();
?>
