<?php 
    $session = $this->getRequest()->getSession();
    $userInfo  = $session->read('Auth.User');
    $currentuser = $session->read('Auth.User.id');
?>
<?php 
    $baseUrl = $this->Url->build('/',true);
?>

<html>
<head>
    <title> SCL Call </title>
    <!-- <link href="css/app.css" rel="stylesheet" type="text/css"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
    <!-- Bootstrap CSS -->
</head>
<body>
	<div class="video-page">
	    <div class="video-wraper" id='videoSubscriber'>
	    </div>
	    <div class="screen-wraper" id='screenSubscriber'>
	    </div>
        <div class="action-button">
            <ul class="list-inline">
                <li  class="circle-box-" title="Soundchecklive.co"> 
                	 <?php echo $this->Html->image('mobile-splash.jpg',['height' => '50px']);?>
                </li>
                <li id='stop' title='Leave session' class="circle-box" data-url="<?= $baseUrl.'Catalogues'.DS.'index'?>">  
                    <?= $this->Html->image('stop.png');?>
                </li>
                <li id='mute' class="circle-box" title="Mute mice">  
                    <?= $this->Html->image('microphone.png');?>
                </li>
                <li  class="circle-box" title="Timer">  
                 <div class="countdown"></div>
                </li>
            </ul>
        </div>
        <div class="video-box" id="videoPublisher">
            <!-- <img src="{{asset('images/video-img.png')}}" > -->
        </div>
        <div class="screen-box" id="screenPublisher">
            <!-- <img src="{{asset('images/video-img.png')}}" > -->
        </div>
	</div>

	<!-- video modal -->
<div class="overlay-wraper <?=($userId == $currentuser)?'d-none':''?>" id="rm-ringing">
    <div class="overlay-setting">
    <div class="overlay-text">
    <?= $this->Html->image('images/correct.png') ?>
        <div>
          Hang tight. Ringing.
        </div>
        <div class="txt-green pt-2"><?= isset($user->user_profile->artist_name)?$user->user_profile->artist_name:''?></div>
        <div>1-on-1</div>
        <!-- <div class="stay-inline mt-4">
        stay in line.
        </div> -->
    </div>
</div>
</div>
	<script type="text/javascript">
	    // replace these values with those generated in your TokBox Account
	    var apiKey = "<?= $apiKey?>";
	    var sessionId = "<?= $sessionId?>";
	    var token = "<?= $token?>";
	    window.csrf = "<?=$this->request->getCookie('csrfToken')?>";
    	window.url = "<?=$this->Url->build('/',true);?>";
	</script>
	<?=
	    $this->Html->script([
	        'frontend/jquery-3.3.1.min',
	        'frontend/bootstrap.bundle.min',
	        'Catalogues/call',
	        'common',
	    ], ['block' => false])
	?>
	<style>
		.circle-box{
		    background: #dadbe0;
		    position: relative;
		    height: 55px;
		    width: 55px;
		    border-radius: 50%;
		    text-align: center;
		}
		.microphone{
		    position:relative;
		}
		.slash img{
		    position: absolute;
		    width: 35px;
		}
		.microphone::before {
		    width: 31px;
		    content: " ";
		    background-image: url(/images/slash.png);
		    position: absolute;
		    left: 10px;
		    top: 9px;
		    height: 31px;
		}
		.share-screen {
		    position:relative;
		}
		.share-screen::before {
		    width: 31px;
		    content: " ";
		    background-image: url(/images/slash.png);
		    position: absolute;
		    left: 10px;
		    top: 10px;
		    height: 32px;
		}
	    body {
		    margin:0;
		    background:#333;
		}
	    .video-wraper {
	        position: relative;
	        background: #333;
	        height:100%;
	    }
		.screen-wraper {
	        position: relative;
	        background: #333;
	        height:100%;
	        display: none;
	    }
	    .action-button{
		    background: #fff;
		    position: absolute;
		    width: 50%;
		    left: 0;
		    right: 0;
		    margin: 0 auto;
		    bottom: 10px;
		    text-align:center;
		    border-radius:4px;
		    display: none;
		    z-index: 1;
		}
	    .action-button li {
		    list-style:none;
		    display:inline-block;
		    cursor:pointer;
		    margin-right: 10px;
		}
	    .action-button li:hover{
		    background:#ccc;
		}
		.action-button li img {
		    width:37px;
		    /* margin-right: 10px; */
		    margin-top: 9px;
		}
	    .action-button ul {
	        margin: 0;
	        padding: 7px 0;
	    }
	    .video-box {
	        position: absolute;
	        right: 20px;
	        bottom: 75px;
	    }
	    .screen-box {
	        position: absolute;
	        left: 20px;
	        bottom: 75px;
	        display: none;
	    }
	    @media screen and (max-width:767px) { 
		    .action-button{
		        width:100%;
		    }
		    .video-box {
		        position: absolute;
		        right: 20px;
		        bottom: 117px;
		        width: 150px;
		    }
		    .screen-box {
		        position: absolute;
		        right: 20px;
		        bottom: 117px;
		        width: 150px;
		        display: none;
		    }
		    .video-box div.OT_mirrored {
		        width:150px;
		    }
		}
	</style>

	<style>

    .txt-green{
        color:green !important;
    }

    .overlay-wraper .overlay-text {
        color:#fff !important;
        text-transform:initial!important;
        font-family: 'Poppins', sans-serif !important;
        font-weight:normal!important;
      
    }

.overlay-wraper {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #000;
        z-index: 2;
        cursor: auto;
        opacity: 0.9;
    }
    .overlay {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        z-index: 2;
        cursor: auto;
        opacity: 0.9;
    }
    .overlay-text {
        left: 0;
        right: 0;
        color: #231c1c;
        text-align: center;
        font-size: 20px;
        text-transform: uppercase;
        opacity: 1;
        font-weight: bold;
        font-family: 'lulocleanw01-oneboldregular';
    }
    .stay-inline{
        left: 0;
        right: 0;
        color: #231c1c;
        text-align: center;
        font-size: 20px;
        text-transform: uppercase;
        opacity: 1;
        font-weight: bold;
        font-family: 'lulocleanw01-oneboldregular';
    }
    #demo {
        color: red;
        margin-top: 25px;
        font-size: 23px;
    }
    .overlay-setting{
        position: absolute;
        left: 0;
        right: 0;
        top: 24%;
    }

    .overlay-text img {
        width:98px !important;
        margin-bottom:10px;
    }
    @media (max-width: 767px) {

        .overlay-wraper .overlay-text {
    font-size: 22px !important;
}
        .overlay-text ,
        .stay-inline{
            font-size: 15px;
        }
    }
</style>
</body>
</html>