<?php use Cake\I18n\Number;?>
<?=
    $this->Html->css([
        'frontend/owl.carousel',
        'frontend/player'
    ]);
?>
    <div class="catalouge-wraper clearfix">
        <div class="left-panel">
            <!--Start catalouge sidebar -->
            <?= $this->element('Catalogues/catalogue_sidebar');?>
                <!--End catalouge sidebar -->
        </div>
        <div class="right-panel">
            <div class="col-12 ll">
                <div class="row">
                    <div class="col-12">
                        <div class="cover-img" style="background-image: url('../img/upcoming.jpg')">
                            <div class="row align-content-center h-100">
                                <div class="col-12 text-center">
                                    <div class="user-profile">
                                        <?php 
                                    $avatar = $this->Images->artistAvatar($record->user->user_profile); 
                                    if($avatar):
                                        echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                                ?>
                                        <?php else: ?>
                                        <svg version="1.1" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 53 53" style="enable-background:new 0 0 53 53;" xml:space="preserve">
                                        <path style="fill:#E7ECED;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
	c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
	c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
	c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
	c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
	C20.296,39.899,19.65,40.986,18.613,41.552z" />
                                        <g>
                                            <path style="fill:#e40434;" d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
		c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
		c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
		s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
		c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
		c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                                        </g>
                                    </svg>
                                        <?php endif;?>
                                    </div>
                                    <div class="artist">
                                        <?php 
                                        echo isset($record->user->user_profile->artist_name)?$record->user->user_profile->artist_name:''
                                    ?>
                                    </div>
                                    <div> <span class="total-partner">Partners :
                                        <?= isset($record->partner)?
                                                $record->partner:'0'
                                        ?>
                                    </span></div>
                                    <div class="follow mt-3">

                                        <button class="partner">Partner now</button>
                                        <button class="partner ml-3">Mobilize</button>
                                        <button class="partner ml-3">Request</button>
                                    </div>
                                    <div class="socail-media">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <svg version="1.1" x="0px" y="0px" width="30px" height="30pxs" viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve">
                                                    <g>
                                                        <path style="fill:#3B5998;" d="M145.659,0c80.45,0,145.66,65.219,145.66,145.66c0,80.45-65.21,145.659-145.66,145.659
                                                                    S0,226.109,0,145.66C0,65.219,65.21,0,145.659,0z" />
                                                        <path style="fill:#FFFFFF;"
                                                            d="M163.394,100.277h18.772v-27.73h-22.067v0.1c-26.738,0.947-32.218,15.977-32.701,31.763h-0.055
                                                                    v13.847h-18.207v27.156h18.207v72.793h27.439v-72.793h22.477l4.342-27.156h-26.81v-8.366
                                                                    C154.791,104.556,158.341,100.277,163.394,100.277z" />
                                                    </g>
                                                </svg>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <svg version="1.1" x="0px" y="0px" widht="30px" height="30px" viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve">
                                                    <g>
                                                        <path style="fill:#26A6D1;" d="M145.659,0c80.45,0,145.66,65.219,145.66,145.66c0,80.45-65.21,145.659-145.66,145.659
                                                       S0,226.109,0,145.66C0,65.219,65.21,0,145.659,0z" />
                                                        <path style="fill:#FFFFFF;"
                                                            d="M236.724,98.129c-6.363,2.749-13.21,4.597-20.392,5.435c7.338-4.27,12.964-11.016,15.613-19.072
                                                       c-6.864,3.96-14.457,6.828-22.55,8.366c-6.473-6.691-15.695-10.87-25.909-10.87c-19.591,0-35.486,15.413-35.486,34.439
                                                       c0,2.704,0.31,5.335,0.919,7.857c-29.496-1.438-55.66-15.158-73.157-35.996c-3.059,5.089-4.807,10.997-4.807,17.315
                                                       c0,11.944,6.263,22.504,15.786,28.668c-5.826-0.182-11.289-1.721-16.086-4.315v0.437c0,16.696,12.235,30.616,28.476,33.784
                                                       c-2.977,0.783-6.109,1.211-9.35,1.211c-2.285,0-4.506-0.209-6.673-0.619c4.515,13.692,17.625,23.651,33.165,23.925
                                                       c-12.153,9.249-27.457,14.748-44.089,14.748c-2.868,0-5.69-0.164-8.476-0.482c15.722,9.777,34.367,15.485,54.422,15.485
                                                       c65.292,0,100.997-52.51,100.997-98.029l-0.1-4.461C225.945,111.111,231.963,105.048,236.724,98.129z" />
                                                    </g>
                                                </svg>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <svg version="1.1" x="0px" y="0px" width="30px" height="30px" viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve">
                                                    <g>
                                                        <path style="fill:#3F729B;" d="M145.659,0c80.44,0,145.66,65.219,145.66,145.66S226.1,291.319,145.66,291.319S0,226.1,0,145.66
                                                       S65.21,0,145.659,0z" />
                                                        <path style="fill:#FFFFFF;"
                                                            d="M195.93,63.708H95.38c-17.47,0-31.672,14.211-31.672,31.672v100.56
                                                       c0,17.47,14.211,31.672,31.672,31.672h100.56c17.47,0,31.672-14.211,31.672-31.672V95.38
                                                       C227.611,77.919,213.4,63.708,195.93,63.708z M205.908,82.034l3.587-0.009v27.202l-27.402,0.091l-0.091-27.202
                                                       C182.002,82.116,205.908,82.034,205.908,82.034z M145.66,118.239c22.732,0,27.42,21.339,27.42,27.429
                                                       c0,15.103-12.308,27.411-27.42,27.411c-15.121,0-27.42-12.308-27.42-27.411C118.23,139.578,122.928,118.239,145.66,118.239z
                                                        M209.65,193.955c0,8.658-7.037,15.704-15.713,15.704H97.073c-8.667,0-15.713-7.037-15.713-15.704v-66.539h22.759
                                                       c-2.112,5.198-3.305,12.299-3.305,18.253c0,24.708,20.101,44.818,44.818,44.818s44.808-20.11,44.808-44.818
                                                       c0-5.954-1.193-13.055-3.296-18.253h22.486v66.539L209.65,193.955z" />
                                                    </g>
                                                </svg>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="available-outlest mt-4"><a href="#">Click to Stream on Available Outlets
                                </div>
                                <div class="total-partner mt-2"><?php 
                                        echo isset($record->user->user_profile->smart_link)?$record->user->user_profile->smart_link:'';
                                    ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-sm-12">
                    <ul class="list-inline middle-nav-custom">
                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#pitchVideo">Partner Pitch
                                Video</a></li>
                                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#biography">Biography</a>
                                        </li>
                                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#music">Music</a></li>
                                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#roi">ROI</a></li>
                                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#musicVideo">Music
                                Video</a></li>
                                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#merchandise">Merchandise</a></li>
                                        <li class="list-inline-item mr-4"><a class="js-scroll-trigger" href="#upcomingShows">Upcoming
                                Events</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <!-- Start Artist video pitch -->

                                <div class="row" id="#pitchVideo">
                                    <div class="col-12 heading">
                                        <h2 class="text-white mb-3">Partner Pitch Video</h2>
                                    </div>
                                    <div class="col-12 col-md-9  video-pitch mb-4">
                                        <?php  if(!empty($record->video_pitch_file) || !empty($record->video_pitch_path)) :?>
                                        <?php
                        $video = $this->Images->getVideoPitch($record);
                        if($video) {
                            echo $this->Html->media($video, [
                                'fullBase' => true,
                                'text' => 'Fallback text',
                                'controls',
                                'width' => '100%',
                                'class' => 'pitch-video-width'
                            ]);
                        }

                    ?>
                                            <?php endif;?>
                                    </div>

                                    <div class="col-md-3 partner-now-section">
                                        <div class="form-group partner-now-input">
                                            <?php
                            echo $this->Html->image('dollar-sign-symbol-bold-text.png', ['alt' => 'doller sign']);
                              echo $this->Html->image('dollar-sign-symbol-mobile.png', ['alt' => 'doller sign']);
                            ?>
                                                <input class="form-control" type="text" placeholder=" 10 min">
                                        </div>

                                        <button class="btn-red btn w-100">Partner Now</button>
                                        <button class="btn-share-now btn w-100 mt-3 d-none d-sm-block">Share Now</button>
                                    </div>
                                    <div class="col-12 mb-4">
                                        <div class="line mt-4"></div>
                                    </div>
                                </div>

                                <!---end Artist video pitch-->

                                <!-- Biography -->
                                <?php if(!empty($record->user->user_profile->biography)) :?>
                                <div class="row" id="biography">
                                    <div class="col-12 mb-4">
                                        <div class="bio heading">
                                            <h2 class="text-white mb-3">Biography</h2>
                                            <p class="text-light-gray">
                                                <?= h($record->user->user_profile->biography)?>
                                            </p>
                                        </div>
                                        <div class="line mt-4"></div>
                                    </div>
                                </div>
                                <?php endif;?>

                                <!-- mp3 record -->
                                <?php  if(!empty($record->record_file) || !empty($record->record_dir)) :?>
                                <div class="row" id="music">
                                    <div class="col-12 ">
                                        <div class=" heading">
                                            <h2 class="text-white mb-3">Music</h2>
                                        </div>
                                    </div>
                                    <a href="#" class="m-auto">
                                        <?php //pr($record);?>
                                        <div class="position-relative audio" data-url="<?= $this->Images->getSongObject($record) ?>">
                                            <div class="play-overlay">
                                                <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" width="80px" height="80px" xml:space="preserve">
                                <circle style="fill:#e40734;" cx="29" cy="29" r="29" />
                                <polygon style="fill:#FFFFFF;" points="44,29 22,44 22,29.273 22,14  " />
                                <path style="fill:#FFFFFF;"
                                    d="M22,45c-0.16,0-0.321-0.038-0.467-0.116C21.205,44.711,21,44.371,21,44V14
                                        c0-0.371,0.205-0.711,0.533-0.884c0.328-0.174,0.724-0.15,1.031,0.058l22,15C44.836,28.36,45,28.669,45,29s-0.164,0.64-0.437,0.826
                                        l-22,15C22.394,44.941,22.197,45,22,45z M23,15.893v26.215L42.225,29L23,15.893z" />
                            </svg>
                                            </div>
                                            <?php

                                $cover = $this->Images->getRecordCover($record, 'thumbnail-');
                                if(!$cover) {
                                    $cover = 'default-audio.png';
                                } 

                                echo $this->html->image($cover, [
                                    'class' => 'object-fit-cover'
                                ]);
                            ?>
                                        </div>
                                    </a>
                                    <div class="line mt-4"></div>
                                </div>
                                <?php endif;?>

                                <!-- Start ROI -->
                                <?php if(!empty($record->user->user_profile->budget_usage)) :?>
                                <div class="row" id="roi">
                                    <div class="col-12 mb-4">
                                        <div class="bio heading">
                                            <h2 class="text-white mb-3 mt-4">ROI</h2>
                                            <p class="text-light-gray">
                                                <?= h($record->user->user_profile->budget_usage)?>
                                            </p>
                                        </div>
                                        <div class="line mt-4"></div>
                                    </div>
                                </div>
                                <?php endif;?>
                                <!-- End ROI -->

                                <!-- upcoming show -->
                                <?php if(!empty($record->user->user_events)) :
                foreach($record->user->user_events as $show) :
                $res = $this->common->showMerchandiseImages($show->user_event_images);
            ?>
                                <div class="row" id="upcomingShows">
                                    <div class="col-12 heading">
                                        <h2 class="text-white mb-3">Upcoming Shows</h2>
                                    </div>
                                    <div class="col-3">
                                        <div class="upcoming-events">
                                            <?php if($res) { ?>
                                            <?= $this->Html->image($res[0],['alt' => 'merchandise-image', 'class' => 'w-100'])?>
                                                <?php } else {?>
                                                <?= $this->Html->image('Noimage.png',['alt' => 'merchandise-image', 'class' => 'w-100'])?>
                                                    <?php } ?>
                                                    <div class="show-details">
                                                        <ul>
                                                            <li>Place:
                                                                <?= isset($show->city)?ucfirst($show->city):'N/A'?>
                                                            </li>
                                                            <li>Date:
                                                                <?= isset($show->event_date)?$show->event_date->format('d-m-Y'):'N/A'?>
                                                            </li>
                                                            <li>Time:
                                                                <?= isset($show->event_time)?ucfirst($show->event_time):'N/A'?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-4">
                                        <div class="line mt-4"></div>
                                    </div>
                                </div>
                                <?php
                endforeach;
                endif; 
            ?>

                                    <!-- Start Music video -->
                                    <?php if(!empty($artistVideo)) : ?>
                                    <div class="row" id="musicVideo">
                                        <div class="col-12 heading">
                                            <h2 class="text-white mb-3">Music Video</h2>
                                        </div>
                                        <div class="col-12 col-md-6 offset-md-3 video-pitch mb-4">
                                            <?php
                                $video = $this->Images->getHowWeWorkVideo($artistVideo);
                                if($video) {
                                    echo $this->Html->media($video, [
                                        'fullBase' => true,
                                        'text' => 'Fallback text',
                                        'controls',
                                        'width' => '100%'
                                    ]);
                                }
                            ?>
                                        </div>
                                    </div>
                                    <?php
                endif; 
            ?>
                                        <!-- End Music video -->

                                        <!-- Start Artist Merchandise-->
                                        <?php if(!empty($record->user->user_merchandises)) { ?>
                                        <div class="row" id="merchandise">
                                            <div class="col-12 heading">
                                                <h2 class="text-white mb-3 mt-2">
                                                    <?= __('Artist Merchandise');?>
                                                </h2>
                                            </div>
                                            <?php 
                    foreach ($record->user->user_merchandises as $key => $artistMerchandise) {
                ?>
                                            <div class="col-auto">
                                                <?php
                                $cover = $this->Images->getArtistMerchandiseImage($artistMerchandise->user_merchandise_images);
                                if(!$cover) {
                                    $cover = 'default-audio.png';
                                } 

                                echo $this->Html->image($cover, [
                                    'class' => 'object-fit-cover- img-fluid'
                                ]);
                            ?>
                                                    <div class="song-name pt-3">
                                                        <?php 
                                    echo $this->Html->link(
                                        ucfirst($artistMerchandise->name),
                                        [
                                        'controller' => 'Catalogues', 
                                        'action' => 'productDescriptions', 
                                        'prefix' => false,
                                        $artistMerchandise->id
                                        ]
                                    );
                                ?>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-sm-4 col-4 price">
                                                            <?= isset($artistMerchandise->price)?Number::currency($artistMerchandise->price,'USD'):'N/A'?>
                                                        </div>
                                                        <div class="col-sm-8 col-8 text-right">
                                                            <?= $this->Form->postLink(
                                        'Add To Cart',
                                        [
                                            'controller' => 'Carts',
                                            'action' => 'add',
                                            'prefix' => false, 
                                            base64_encode($artistMerchandise->id)
                                        ],
                                        ['class' => 'btn btn-border']);
                                    ?>
                                                                </button>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <?php } ?>
                                        <!-- End Artist Merchandise-->

                                        <!-- Start Similer Artist -->
                                        <?php if(!empty($similerArtist)) ://pr($similerArtist);?>
                                        <div class="row">
                                            <div class="col-12 heading">
                                                <h2 class="text-white mb-3">Similer Artist</h2>
                                            </div>
                                            <div class="owl-carousel owl-theme" id="similer-artist">
                                                <?php foreach($similerArtist as $artist) :?>
                                                <?php 
                                $avatar = $this->Images->artistAvatar($artist->user_profile);
                                //pr($avatar);
                            ?>
                                                <?php if($avatar):?>
                                                <div class="item">
                                                    <?php 
                                        echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:200px; height:200px;']);
                                    ?>
                                                </div>
                                                <?php  
                            endif;
                            endforeach;
                        ?>
                                            </div>
                                        </div>
                                        <?php endif;?>
                                        <!-- End Similer Artist -->


                                        <footer class="catalogue-footer">
                                            <!--sticky audio footer -->
                                            <div id="single-song-player" class="d-none">
                                                <div class="bottom-container">
                                                    <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>
                                                    <!--
                        <div class="time-container clearfix">
                            <span class="current-time float-left">
                                <span class="amplitude-current-minutes"></span>:<span
                                    class="amplitude-current-seconds"></span>
                            </span>
                            <span class="duration">
                                <span class="amplitude-duration-minutes"></span>:<span
                                    class="amplitude-duration-seconds"></span>
                            </span>
                        </div> -->

                                                    <div class="control-container col">
                                                        <div class="row justify-content-between align-items-center">
                                                            <div class="col-auto px-1">
                                                                <div class="form-row align-items-center">
                                                                    <div class="col-auto">
                                                                        <img data-amplitude-song-info="cover_art_url" class="border-0" />
                                                                    </div>
                                                                    <div class="col col-sm-auto">
                                                                        <div class="meta-container text-left leading-tight">
                                                                            <div>
                                                                                <span class="mr-1">Song:</span>
                                                                                <span data-amplitude-song-info="name" class="song-name"></span>
                                                                            </div>
                                                                            <div class="text-xs">
                                                                                <span class="mr-1">Artist:</span>
                                                                                <span data-amplitude-song-info="artist"></span>
                                                                            </div>
                                                                            <div class="clearfix">
                                                                                <span class="current-time text-xs pr-2">
                                                        <span class="amplitude-current-minutes"></span>:<span class="amplitude-current-seconds"></span>
                                                                                </span>
                                                                                <span class="duration text-xs border-left pl-2">
                                                        <span class="amplitude-duration-minutes"></span>:<span class="amplitude-duration-seconds"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="row justify-content-end justify-content-md-center align-items-center">
                                                                    <div class="col-auto">
                                                                        <div class="row justify-content-center align-items-center">
                                                                            <div class="col px-0 px-md-3 amplitude-prev mr-md-3"></div>
                                                                            <div class="col px-0 px-md-3 amplitude-play-pause m-auto" id="play-pause"></div>
                                                                            <div class="col px-0 px-md-3 amplitude-next ml-md-3"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-auto">
                                    <svg  class ="close-player" style="fill: #fff" width="25px" height="25px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm1.41-1.41A8 8 0 1 0 15.66 4.34 8 8 0 0 0 4.34 15.66zm9.9-8.49L11.41 10l2.83 2.83-1.41 1.41L10 11.41l-2.83 2.83-1.41-1.41L8.59 10 5.76 7.17l1.41-1.41L10 8.59l2.83-2.83 1.41 1.41z"/>
                                    </svg>
                                </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end sticky audio footer -->
                                            <div class="col-12">
                                                <div class="border-custom">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-sm-12
                            ">

                                                            <?= $this->html->image('white-logo.png') ?>
                                                        </div>

                                                        <div class="col-12 d-md-none">
                                                            <h3 class="mt-5 mb-0 text-white">INDEPENDENCE IS LIFE</h3>
                                                        </div>

                                                        <div class="col-sm-6 col-lg-2">
                                                            <h4>Company</h4>
                                                            <ul class="list-unstyled">
                                                                <li><a href="#">Home</a></li>
                                                                <li><a href="#">About Us</a></li>
                                                                <li><a href="#">What we do</a></li>
                                                                <li><a href="#">Upcoming events</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-sm-6 col-lg-2">
                                                            <h4>Quick Link</h4>
                                                            <ul class="list-unstyled">
                                                                <li><a href="#">For artist</a></li>
                                                                <li><a href="#">FAQ'S</a></li>
                                                                <li><a href="#">Merchandise</a></li>

                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-2 col-sm-6">
                                                            <h4>Contact Us</h4>
                                                            <ul class="list-unstyled">
                                                                <li><a href="#">For artist</a></li>
                                                                <li><a href="#">FAQ'S</a></li>
                                                                <li><a href="#">Merchandise</a></li>

                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-2 col-sm-6">
                                                            <h4>Follow Us</h4>
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="https://www.facebook.com/schecklive" target="_blank">
                                                                        <?= $this->Html->image('images/facebook-icon.png') ?> Facebook</a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://twitter.com/schecklive" target="_blank">
                                                                        <?= $this->Html->image('images/tiwtter.png') ?> Tiwtter</a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://www.instagram.com/schecklive/" target="_blank">
                                                                        <?= $this->Html->image('images/instagrams.png') ?> Instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </footer>
                            </div>
                        </div>

                        <style type="text/css">
                            .right-panel h2,
                            .right-panel h4 {
                                font-size: 16px;
                                font-weight: bold;
                            }
                        </style>
                        <?=
    $this->Html->script([
        'common/stickyaudioplayerjquery.min',
        'Artist/VideoPitch/player',
        'Catalogues/single-song-player',
        'frontend/owl.carousel',
        'Catalogues/similer-artist'
    ], ['block' => true])
?>
                            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.0.2/dist/amplitude.js"></script>