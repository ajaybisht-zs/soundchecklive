<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Add Colors</h1>
    </div>
    <?php
        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'Colors',
                'action' => 'add',
                'prefix' => 'admin'
            ],
            'id' => 'sound-live-colors',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="hexCode">hex code</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('hex_code', [
                        'type' => 'text',
                        'placeholder' => 'Hex Code',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'hexCode'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="hexCodeName">hex code name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('name', [
                        'type' => 'text',
                        'placeholder' => 'Color Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'hexCodeName'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script([
            'backend/Admin/color_validate',
            ], [
        'block' => 'scriptBottom'
    ]);
?>
