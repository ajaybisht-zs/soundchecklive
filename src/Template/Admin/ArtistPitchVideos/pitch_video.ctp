<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4"><?= __('Artist Pitch of the Week');?></h1>
    </div>
        <?php
            echo $this->Form->create(null, [
                'url' => [
                    'controller' => 'ArtistPitchVideos',
                    'action' => 'pitchVideo',
                    'prefix' => 'admin'
                ],
                'id' => 'artist-pitch-video',
                'class' => 'user',
                'enctype' => 'multipart/form-data'
            ]);
        ?>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('title', [
                        'type' => 'text',
                        'placeholder' => 'Title',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'value' => isset($findCurrentVideo->title)?$findCurrentVideo->title:'',
                    ]);
                ?>
            </div>
        </div>
        <input type="hidden" name="id" value="<?= isset($findCurrentVideo->id)?$findCurrentVideo->id:''?>">

        <div class="form-group">
            <label for="exampleFormControlFile2"><?= __('Upload poster(jpg)'); ?></label>
            <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile2" name="poster_name" accept="image/*">
        </div>

        <div class="col-8">
            <?php
                $pitchPoster = $this->Images->getHowWeWorkVideoPoster($findCurrentVideo);
                if($pitchPoster) { ?>
                <h4><?= __('Poster Image');?></h4>     
                <?php    
                    echo $this->Html->image($pitchPoster,['class' => 'img-fluid']);
                    } else {
                        echo "No Poster Image Uploaded";
                    }
                ?>
        </div>

        <div class="form-group">
            <label for="exampleFormControlFile2"><?= __('Upload Video'); ?></label>
            <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile2" name="video_name">
        </div>

        <div class="col-8">
            <?php
                $howWeworkVideo = $this->Images->getHowWeWorkVideo($findCurrentVideo);
                if($howWeworkVideo) { ?>
                <h4><?= __($findCurrentVideo->title);?></h4>     
                <?php    
                    echo $this->Html->media(
                            [ $howWeworkVideo,
                            ],
                            [
                                'width' => "60%",
                                'controls'
                            ]
                        );
                    }
                ?>
        </div>

        <div class="form-group row">          
            <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script('backend/Admin/validate_artist_pitch_video', [
        'block' => 'scriptBottom'
    ]);
?>
