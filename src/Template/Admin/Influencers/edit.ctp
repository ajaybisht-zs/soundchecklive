<?php
    $this->Form->setTemplates([
        'radioContainer' => '{{content}}',
        'radioWrapper' => '<div class="form-check form-check-inline mr-3">{{label}}</div>'
    ]);
?>
<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Edit Influencer Information</h1>
    </div>
    <?php
    //pr($influencer);
        echo $this->Form->create($influencer, [
            'url' => [
                'controller' => 'Influencers',
                'action' => 'edit',
                'prefix' => 'admin'
            ],
            'id' => 'admin-influencer',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Instagram User Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                        echo $this->Form->control('influencer_profile.ig_user_name', [
                            'type' => 'text',
                            'class' => 'form-control form-control-user',
                            'label' => false,
                            'placeholder' => 'Instagram username'

                        ]);
                    ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="levels">Instagram Follower</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('influencer_profile.ig_follower', [
                        'type' => 'number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'placeholder' => 'Instagram Follower'

                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Twitter User Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                        echo $this->Form->control('influencer_profile.t_user_name', [
                            'type' => 'text',
                            'class' => 'form-control form-control-user',
                            'label' => false,
                            'placeholder' => 'Twitter username'

                        ]);
                    ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="levels">Twitter Follower</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('influencer_profile.t_follower', [
                        'type' => 'number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'placeholder' => 'Twitter Follower'

                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">First Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('influencer_profile.first_name', [
                        'type' => 'text',
                        'placeholder' => 'First Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'firstName'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Last Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('influencer_profile.last_name', [
                        'type' => 'text',
                        'placeholder' => 'Last Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Phone Number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('influencer_profile.phone_number', [
                        'type' => 'text',
                        'placeholder' => 'phone number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">zipcode</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('influencer_profile.zipcode', [
                        'type' => 'text',
                        'placeholder' => 'zipcode',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
       
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Date of Birth</label>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->month('influencer_profile.birth_month', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Month',
                        'value' => (!empty($influencer->influencer_profile->birth_month))? $influencer->influencer_profile->birth_month:'1',
                        'id' => 'birthMonth'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->day('influencer_profile.birth_day', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Date',
                        'value' => (!empty($influencer->influencer_profile->birth_day)) ? $influencer->influencer_profile->birth_day:'1',
                        'id' => 'birthDay'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                 <?php
                    echo $this->Form->year('influencer_profile.birth_year', [
                        'minYear' => 1950,
                        'maxYear' => date('Y'),
                        'class' => 'form-control custom-input',
                        'empty' => 'Year',
                        'value' => (!empty($influencer->influencer_profile->birth_year))? $influencer->influencer_profile->birth_year:2000,
                        'id' => 'birthYear'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script([
            'frontend/influencer/edit_admin_validate'
            ], [
        'block' => 'scriptBottom'
    ]);
?>