<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
      <div class="col-lg-7">
        <div class="p-5">
          <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Influencer Information</h1>
          </div>
          <div class="row">
          		<div class="col-4"><label><strong>First Name</strong></label></div>
          		<div class="col-8"><?php echo isset($influencer->influencer_profile->first_name)?ucfirst($influencer->influencer_profile->first_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
	          	<div class="col-4"><label><strong>Last Name</strong></label></div>
	          	<div class="col-8"><?php echo isset($influencer->influencer_profile->last_name)?ucfirst($influencer->influencer_profile->last_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
	          	<div class="col-4"><label><strong>Email Address</strong></label></div>
	          	<div class="col-8"><?php echo isset($influencer->email)?ucfirst($influencer->email):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
	          	<div class="col-4"><label><strong>Phone Number</strong></label></div>
	          	<div class="col-8"><?php echo isset($influencer->influencer_profile->phone_number)?ucfirst($influencer->influencer_profile->phone_number):'N/A'?></div>
          </div>
           <hr>
           <div class="row">
	          	<div class="col-4"><label><strong>Zipcode</strong></label></div>
	          	<div class="col-8"><?php echo isset($influencer->influencer_profile->zipcode)?ucfirst($influencer->influencer_profile->zipcode):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Instagram User Name</strong></label></div>
              <div class="col-8"><?php echo isset($influencer->influencer_profile->ig_user_name)?ucfirst($influencer->influencer_profile->ig_user_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Instagram Follower</strong></label></div>
              <div class="col-8"><?php echo isset($influencer->influencer_profile->ig_follower)?ucfirst($influencer->influencer_profile->ig_follower):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Twitter User Name</strong></label></div>
              <div class="col-8"><?php echo isset($influencer->influencer_profile->t_user_name)?ucfirst($influencer->influencer_profile->t_user_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Twitter Follower</strong></label></div>
              <div class="col-8"><?php echo isset($influencer->influencer_profile->t_follower)?ucfirst($influencer->influencer_profile->t_follower):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Profile Pic</strong></label></div>
              <div class="col-8">
              <?php 
                            $avatar = $this->Images->artistAvatar($influencer->influencer_profile); 
                            if($avatar):
                                echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                        ?>
                        <?php else: ?>
                            <svg version="1.1" x="0px" y="0px" width="120px" height="120px" viewBox="0 0 53 53"
                                style="enable-background:new 0 0 53 53;" xml:space="preserve">
                                <path style="fill:#333333;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
                               c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
                               c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
                               c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
                               c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
                               C20.296,39.899,19.65,40.986,18.613,41.552z" />
                                <g>
                                    <path style="fill:#ffffff;"
                                        d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
                                   c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
                                   c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
                                   s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
                                   c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
                                   c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                                </g>
                            </svg>
                        <?php endif; ?>
                
              </div>
          </div>
           <hr>
           <div class="row">
                <div class="col-4"><label><strong>D.O.B</strong></label></div>
                <div class="col-8">
                    <?php 
                     if(!empty($influencer->influencer_profile->birth_year)):
                        echo($influencer->influencer_profile->birth_day.'/'.$influencer->influencer_profile->birth_month.'/'.$influencer->influencer_profile->birth_year);
                    endif;
                    ?>        
                </div>
          </div>
          <hr>
        <div class="row">
                <div class="col-4"><label><strong>Status</strong></label></div>
                <div class="col-8">
                    <?php 
                        if($influencer->status == 1):
                    ?>
                    <span class="badge badge-success">Active</span>
                <?php else :?>
                    <span class="badge badge-danger">Inactive</span>
                <?php endif;?>
               </div>
        </div>
        <hr>
        <div class="row">
                <div class="col-4"><label><strong>Equity Level</strong></label></div>
                <div class="col-8">
                    <?php 
                     if(!empty($influencer->influencer_profile->equity_level)):
                        echo($influencer->influencer_profile->equity_level->level_name);
                    else:
                    echo('N/A');
                    endif;
                    ?>        
                </div>
          </div>
          <hr>
       <div class="row">
          	<div class="col-4"><label><strong>Created</strong></label></div>
          	<div class="col-8"><?php echo isset($influencer->created)?ucfirst($influencer->created):'N/A'?></div>
      </div>
          <hr>
           <div class="row">
	          	<div class="col-4"><label><strong>Modified</strong></label></div>
	          	<div class="col-8"><?php echo isset($influencer->modified)?ucfirst($influencer->modified):'N/A'?></div>
          </div>
          <div class="row mt-5">
          	<?= $this->Html->link(__('Back'), $this->request->referer(),['class' => 'btn btn-secondary btn-lg text-right']) ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
