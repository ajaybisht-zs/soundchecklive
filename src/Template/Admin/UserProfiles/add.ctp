<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Add New Sound Live Original</h1>
    </div>
        <?php
            echo $this->Form->create(null, [
                'url' => [
                    'controller' => 'UserProfiles',
                    'action' => 'add',
                    'prefix' => 'admin'
                ],
                'id' => 'sound-live-add',
                'class' => 'user',
                'enctype' => 'multipart/form-data'
            ]);
        ?>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('record_name', [
                        'type' => 'text',
                        'placeholder' => 'Record Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
            <div class="col-sm-6">
                <?php
                    echo $this->Form->control('artist_name', [
                        'type' => 'text',
                        'placeholder' => 'Artist Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('features', [
                        'type' => 'text',
                        'placeholder' => 'features',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
            <div class="col-sm-6">
                <?php
                    echo $this->Form->control('producer', [
                        'type' => 'text',
                        'placeholder' => 'Producers',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('writer', [
                        'type' => 'text',
                        'placeholder' => 'writers',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
            <div class="col-sm-6">
                <label for="exampleFormControlFile1">Upload Cover Image</label>
                <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile1" name="cover_image">
            </div>
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile2">Upload mp3</label>
            <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile2" name="record_file">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile3">Enter Keyword</label>
            <?php
                echo $this->Form->control('user_record_keys.0.keyword', [
                    'type' => 'text',
                    'placeholder' => '#1',
                    'class' => 'form-control form-control-user',
                    'label' => false,
                ]);
            ?>
            <br>
            <?php
                echo $this->Form->control('user_record_keys.1.keyword', [
                    'type' => 'text',
                    'placeholder' => '#2',
                    'class' => 'form-control form-control-user',
                    'label' => false,
                ]);
            ?>
            <br>
           <?php
                echo $this->Form->control('user_record_keys.2.keyword', [
                    'type' => 'text',
                    'placeholder' => '#3',
                    'class' => 'form-control form-control-user',
                    'label' => false,
                ]);
            ?>
            <br>
            <?php
                echo $this->Form->control('user_record_keys.3.keyword', [
                    'type' => 'text',
                    'placeholder' => '#4',
                    'class' => 'form-control form-control-user',
                    'label' => false,
                ]);
            ?>
            <br>
            <?php
                echo $this->Form->control('user_record_keys.4.keyword', [
                    'type' => 'text',
                    'placeholder' => '#5',
                    'class' => 'form-control form-control-user',
                    'label' => false,
                ]);
            ?>
        </div>
        <div class="form-group row">          
            <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script('backend/Admin/validate_sound_check_add', [
        'block' => 'scriptBottom'
    ]);
?>
