<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
        <div class="row">
              <div class="col-lg-7">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Sound Check Live Original Detail</h1>
                  </div>
                  <div class="row">
                        <div class="col-4"><label><strong>Record Name</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->record_name)?ucfirst($userRecords->record_name):'N/A'?></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-4"><label><strong>Artist Name</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->artist_name)?ucfirst($userRecords->artist_name):'N/A'?></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-4"><label><strong>Producer</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->producer)?ucfirst($userRecords->producer):'N/A'?></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-4"><label><strong>Writer</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->writer)?ucfirst($userRecords->writer):'N/A'?></div>
                  </div>
                   <hr>
                   <div class="row">
                        <div class="col-4"><label><strong>Features</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->features)?ucfirst($userRecords->features):'N/A'?></div>
                  </div>
                  <hr>
                    <div class="row">
                        <div class="col-4"><label><strong>Play Audio</strong></label></div>
                        <div class="col-5" style="height: 200px; width: 200px;">
                            <?php
                            if(!empty($userRecords->record_dir) && !empty($userRecords->record_file)) {
                                $mp3Exists = $this->common->checkMp3($userRecords->record_dir,$userRecords->record_file);
                                if($mp3Exists) {   
                                    echo $this->Html->media($mp3Exists, [
                                        'fullBase' => true,
                                        'text' => 'Fallback text',
                                        'id' => 'audio',
                                        'controls',
                                        'style' => 'position: absolute; bottom: 0; left: 0; right: 0; padding: 0 12px; width: 100%;'
                                    ]);
                                } else {
                                    echo('Mp3 Not Found');
                                }
                            }

                            if(!empty($userRecords->cover_image_dir) && !empty($userRecords->cover_image)) {
                                $coverImageExists = $this->common->checkCoverImage($userRecords->cover_image_dir,$userRecords->cover_image);
                                if($coverImageExists) {
                                    echo $this->Html->image($coverImageExists, [
                                        'style' => 'height: 100%; width: 100%; object-fit: contain; background-color: #eee;'
                                    ]);
                                } else {
                                    echo('cover Image Not Found');
                                } 
                            }
                            ?>
                        </div>
                     </div>
                    <hr>
                   <div class="row">
                        <div class="col-4"><label><strong>Keywords</strong></label></div>
                        <div class="col-8">
                            <?php
                                if(!empty($userRecords->user_record_keys)) {
                                    echo '<ul class="list-inline">';
                                    foreach ($userRecords->user_record_keys as $value) {
                                        if(!empty($value->keyword)) {
                                            echo '<li class="list-inline-item"><button class="btn btn-primary btn-sm">' . $value->keyword . '</button></li>';  
                                        }
                                    }
                                    echo '</ul>';
                                } else {
                                    echo('N/A');
                                }
                            ?>       
                        </div>
                  </div>
                  <hr>
                   <div class="row">
                        <div class="col-4"><label><strong>Created</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->created)?ucfirst($userRecords->created):'N/A'?></div>
                  </div>
                  <hr>
                   <div class="row">
                        <div class="col-4"><label><strong>Modified</strong></label></div>
                        <div class="col-8"><?php echo isset($userRecords->modified)?ucfirst($userRecords->modified):'N/A'?></div>
                  </div>
                  <div class="row">
                    <?= $this->Html->link(__('Back'), $this->request->referer(),['class' => 'btn btn-secondary btn-lg text-right']) ?>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>