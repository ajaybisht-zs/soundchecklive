<div class="card-header mb-3 row justify-content-between align-items-center">
    <div class="col px-0">
        <h2 class="m-0 font-weight-bold text-info"><?=__('Sound Check Live Original')?></h2>
    </div>
    <div class="col-auto">  
        <?php 
            echo $this->Html->link(
                'Add New Record',
                [
                    'controller' =>'UserProfiles',
                    'action' => 'add'
                ],
                ['class' => 'btn btn-secondary btn-lg']
            );
        ?>
    </div>
</div>
<div class="table-responsive  mobile-res-tb ">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
          <th>Id</th>
          <th>Record Name</th>
          <th>Artist Name</th>
          <th>Producer</th>
          <th>Writer</th>
          <th>Features</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Action</th>
        </tr>
    </thead>
    <!-- <tfoot>
        <tr>
          <th>Id</th>
          <th>Record Name</th>
          <th>Artist Name</th>
          <th>Producer</th>
          <th>Writer</th>
          <th>Features</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Action</th>
        </tr>
    </tfoot> -->
    <tbody>
        <?php
            if ($userRecords->isEmpty()) :
        ?> 
        <tr>
            <td colspan="12" class="text-center no-data-found">
                <h3><?= __('No records found!') ?></h3>
            </td>
        </tr> 
      
        <?php else : foreach($userRecords as $val) : ?>
        <tr>
          <td><?= $val->id?></td>
          <td><?php echo isset($val->record_name)?ucfirst($val->record_name):'N/A'?></td>
          <td><?php echo isset($val->artist_name)?ucfirst($val->artist_name):'N/A'?></td>
          <td><?php echo isset($val->producer)?ucfirst($val->producer):'N/A'?></td>
          <td><?php echo isset($val->writer)?ucfirst($val->writer):'N/A'?></td>
          <td><?php echo isset($val->features)?ucfirst($val->features):'N/A'?></td>
          <td><?= $val->created?></td>
          <td><?= $val->modified?></td>
          <td><?= 
                $this->Html->link(__('<span class="fa fa-eye icon-setting"></span>&nbsp; view'), 
                                                [
                                                    'controller' => 'UserProfiles',
                                                    'action' => 'view',
                                                    $val->id,
                                                    'prefix' => 'admin'
                                                ], [
                                                'escape' => false
                                    ])?></td>
        </tr>
    <?php 
        endforeach;
        endif;
    ?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>
