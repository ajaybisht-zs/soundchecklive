<?php 
    use Cake\I18n\Number;
    use Cake\Routing\Router;
?>
<div class="proofOfConcepts view large-9 medium-8 columns content">
    <h1><?= h($proofOfConcept->id) ?> (<?= isset($proofOfConcept->last_name)?ucfirst($proofOfConcept->first_name.' '.$proofOfConcept->last_name):'N/A' ?>) </h1>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($proofOfConcept->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Charge Id') ?></th>
            <td><?= h($proofOfConcept->charge_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= isset($proofOfConcept->first_name)?ucfirst($proofOfConcept->first_name):'N/A' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= isset($proofOfConcept->last_name)?ucfirst($proofOfConcept->last_name):'N/A' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($proofOfConcept->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Zip Code') ?></th>
            <td><?= h($proofOfConcept->zip_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($proofOfConcept->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= $proofOfConcept->country->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= isset($proofOfConcept->state->name)?$proofOfConcept->state->name:'N/A' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone Number') ?></th>
            <td><?= h($proofOfConcept->phone_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->currency($proofOfConcept->amount,'USD') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Who Sent') ?></th>
            <td><?= $proofOfConcept->people->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td>
                <?php 
                    if($proofOfConcept->status == 1) {
                        echo('Success');
                    } else {
                        echo('Failed');
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($proofOfConcept->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($proofOfConcept->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Failure Reason') ?></h4>
        <?= $this->Text->autoParagraph(h($proofOfConcept->failure_reason)); ?>
    </div>
</div>
