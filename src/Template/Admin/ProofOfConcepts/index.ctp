<?php 
    use Cake\I18n\Number;
    use Cake\Routing\Router;
?>
<div class="card-header mb-3 row justify-content-between align-items-center">
    <div class="col px-0">
        <h2 class="m-0 font-weight-bold text-info"><?=__('Proof of Concepts')?></h2>
    </div>
    
</div>
<div class="mobile-res-tb">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
            <th scope="col"><?= $this->Paginator->sort('people_id','Who Sent') ?></th>
            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
            <th scope="col"><?= $this->Paginator->sort('charge_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('Action') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if ($proofOfConcepts->isEmpty()) :
        ?> 
        <tr>
            <td colspan="12" class="text-center no-data-found">
                <h3><?= __('No records found!') ?></h3>
            </td>
        </tr> 
      
        <?php else : foreach($proofOfConcepts as $proofOfConcept) : ?>
        <tr>
                <td><?= $proofOfConcept->id ?></td>
                <td><?= isset($proofOfConcept->first_name)?ucfirst($proofOfConcept->first_name):'' ?></td>
                <td><?= isset($proofOfConcept->last_name)?ucfirst($proofOfConcept->last_name):'' ?></td>
                <td><?= $this->Number->currency($proofOfConcept->amount,'USD') ?></td>
                <td><?= isset($proofOfConcept->people->name)?ucfirst($proofOfConcept->people->name):'' ?></td>
                <td><?= h($proofOfConcept->email) ?></td>
                <td><?= h($proofOfConcept->charge_id) ?></td>
            <td>
            <?= 
                    $this->Html->link(__('<span class="fa fa-eye icon-setting"></span>&nbsp; View'), 
                                            [
                                                'controller' => 'ProofOfConcepts',
                                                'action' => 'view',
                                                $proofOfConcept->id,
                                                'prefix' => 'admin'
                                            ], [
                                            'escape' => false
                                ])?>
             <?= $this->Form->postLink(
                    __('<span class="fa fa-trash icon-setting"></span> &nbsp; Delete'), [
                            'action' => 'delete', 
                            $proofOfConcept->id,
                        ], [
                            'confirm' => __('Are you sure you want to delete # {0}?', $proofOfConcept->id),
                            'escape' => false,
                            'class' => 'ml-2'
                            
                        ]) 
            ?>
                                    
            </td>
        </tr>
    <?php 
        endforeach;
        endif;
    ?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>