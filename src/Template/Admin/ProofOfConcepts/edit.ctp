<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProofOfConcept $proofOfConcept
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $proofOfConcept->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $proofOfConcept->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Proof Of Concepts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="proofOfConcepts form large-9 medium-8 columns content">
    <?= $this->Form->create($proofOfConcept) ?>
    <fieldset>
        <legend><?= __('Edit Proof Of Concept') ?></legend>
        <?php
            echo $this->Form->control('amount');
            echo $this->Form->control('people_id');
            echo $this->Form->control('email');
            echo $this->Form->control('charge_id');
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('address');
            echo $this->Form->control('zip_code');
            echo $this->Form->control('city');
            echo $this->Form->control('country_id', ['options' => $countries]);
            echo $this->Form->control('phone_number');
            echo $this->Form->control('status');
            echo $this->Form->control('failure_reason');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
