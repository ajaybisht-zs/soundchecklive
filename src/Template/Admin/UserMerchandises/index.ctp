<?php 
    use Cake\I18n\Number;
    use Cake\Routing\Router;
?>
<div class="card-header mb-3 row justify-content-between align-items-center">
    <div class="col px-0">
        <h2 class="m-0 font-weight-bold text-info"><?=__('Merchandise')?></h2>
    </div>
    <div class="col-auto">  
        <?php 
            echo $this->Html->link(
                'Add New Merchandise',
                [
                    'controller' =>'UserMerchandises',
                    'action' => 'add'
                ],
                ['class' => 'btn btn-secondary btn-lg']
            );
        ?>
    </div>
</div>
<div class="table-responsive  mobile-res-tb">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
          <th>Id</th>
          <th>Image</th>
          <th>Name</th>
          <th>price</th>
          <th>Status</th>
          <th>Added By</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Action</th>
        </tr>
    </thead>
    <!-- <tfoot>
        <tr>
          <th>Id</th>
          <th>Image</th>
          <th>Name</th>
          <th>price</th>
          <th>Status</th>
          <th>Added By</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Action</th>
        </tr>
    </tfoot> -->
    <tbody>
        <?php
            if ($userMerchandises->isEmpty()) :
        ?> 
        <tr>
            <td colspan="12" class="text-center no-data-found">
                <h3><?= __('No records found!') ?></h3>
            </td>
        </tr> 
      
        <?php else : foreach($userMerchandises as $val) : ?>
        <tr>
            <td><?= $val->id?></td>
            <td>
                <?php 
                    if(!empty($val->user_merchandise_images)) {
                        $res = $this->common->showMerchandiseImages($val->user_merchandise_images);
                        if(!empty($res[0])) {
                          echo $this->Html->image($res[0],
                                [
                                    'width' => '100%', 
                                    'height' => '100%', 
                                    'style' => 'height: 50px; width: 50px;'
                            ]);  
                        }
                    } else {
                        echo $this->Html->image('Noimage.png',
                            [
                                'width' => '100%', 
                                'height' => '100%', 
                                'style' => 'height: 50px; width: 50px;'
                        ]);
                    }
                ?> 
            </td>
            <td><?php echo isset($val->name)?ucfirst($val->name):'N/A'?></td>
            <td><?php echo isset($val->price)?Number::currency($val->price, 'USD'):'N/A'?></td>
            <td><input type="checkbox"  class = "approve_merchandise" data-toggle="toggle" data-onstyle="success" data-url="<?php 
                                                            echo router::url(
                                                            [      
                                                                'controller' => 'UserMerchandises',                                                
                                                                "action" => "approveMarchandise",
                                                                $val->id,
                                                                'prefix' => 'admin',
                                                            ])?>"<?php echo ($val->status == 1) ? "checked" : ""?>></td>
            <td>
                <?php 
                    if($val->user->role_id == 1) {
                        echo('Admin');
                    } else {
                        echo isset($val->user->user_profile->first_name)?ucfirst($val->user->user_profile->first_name).' '.ucfirst($val->user->user_profile->last_name):'N/A';
                    }
                ?>
            </td>
            <td><?= $val->created?></td>
            <td><?= $val->modified?></td>
            <td><?= 
            $this->Html->link(__('<span class="fa fa-eye icon-setting"></span>&nbsp; view'), 
                                            [
                                                'controller' => 'UserMerchandises',
                                                'action' => 'view',
                                                $val->id,
                                                'prefix' => 'admin'
                                            ], [
                                            'escape' => false
                                ])?></td>
        </tr>
    <?php 
        endforeach;
        endif;
    ?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>
<?php
    echo $this->Html->script(
        [
            'backend/Admin/approve_marchandise',
            ], 
            [
        'block' => 'scriptBottom'
]);
