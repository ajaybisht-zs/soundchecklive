<?php use Cake\I18n\Number;?>
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
        <div class="row">
              <div class="col-lg-7">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Merchandise Detail</h1>
                  </div>
                  <div class="row">
                        <div class="col-4"><label><strong> Merchandise Name</strong></label></div>
                        <div class="col-8"><?php echo isset($userMerchandise->name)?ucfirst($userMerchandise->name):'N/A'?></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-4"><label><strong>Price</strong></label></div>
                        <div class="col-8"><?php echo isset($userMerchandise->price)?Number::currency($userMerchandise->price, 'USD'):'N/A'?></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-4"><label><strong>Description</strong></label></div>
                        <div class="col-8"><?php echo isset($userMerchandise->description)?ucfirst($userMerchandise->description):'N/A'?></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-4"><label><strong>Images</strong></label></div>
                        <div class="col-8">
                            <div class="form-row">
                              <?php
                                    if(!empty($userMerchandise->user_merchandise_images)) {
                                        $res = $this->common->showMerchandiseImages($userMerchandise->user_merchandise_images);
                                        if($res) {
                                           foreach($res as $val) { 
                                ?>
                                            <div class="col border mr-2" style="height: 150px; width: 150px; object-fit: contain;">
                                                <?php 
                                                    echo $this->Html->image($val,
                                                        [
                                                            'width' => '100%', 
                                                            'height' => '100%', 
                                                            'style' => 'object-fit: contain'
                                                    ]);
                                                 ?>
                                            </div>
                                <?php
                                           }
                                        } else {
                                            echo ('N/A');
                                        }
                                    }
                                    else {
                                        echo ('No Images Found');
                                    }
                              ?>
                            </div>
                        </div>
                  </div>
                  <hr>
                    <style type="text/css">
                        .color-code input {
                        visibility: hidden;
                        margin-left: 0px;
                        width: 30px;
                        height: 30px;
                        margin-top: 0;
                    }
                    /*.color-code input + label {
                    border: 2px solid transparent;
                    }*/
                    .color-code input:checked + label {
                        position: relative;
                    }

                    .color-code input:checked + label::after {
                        position: absolute;
                        left: -4px;
                        right: 0px;
                        top: -4px;
                        bottom: 0;
                        content: "";
                        display: block;
                        border: 2px solid #000;
                        width: 38px;
                        height: 38px;
                        border-radius: 50%;
                    }
                    </style>

                    <div class="form-check">
                        <div class="form-row">
                             <div class="col-4"><label><strong>Colors</strong></label></div>
                            <?php 
                                if(!empty($userMerchandise->colors)) :
                                    foreach($userMerchandise->colors as $key => $val) : 
                            ?>
                            <div class="col-auto color-code">
                                <input type="checkbox" class="form-check-input" id="exampleCheck<?php echo $key?>" style="background-color:#000000" name="colors[_ids][]"  value="<?php echo $key?>">
                                <label class="form-check-label rounded-pill" for="exampleCheck<?php echo $key?>" style="background-color:<?php echo $val->hex_code?>; width: 30px; height: 30px;"></label>
                            </div> 
                            <?php 
                                endforeach;
                                else : echo ('N/A'); 
                                endif;
                            ?>
                        </div>
                    </div> 
                    <hr>

                    <div class="row">
                        <div class="col-4"><label><strong>Size</strong></label></div>
                        <div class="col-8">
                            <?php
                                if(!empty($userMerchandise->sizes)) {
                                    echo '<ul class="list-inline">';
                                    foreach ($userMerchandise->sizes as $value){
                                        if(!empty($value->size)) {
                                            echo '<li class="list-inline-item"><button class="btn btn-primary btn-sm">' . $value->size . '</button></li>';  
                                        }
                                    }
                                    echo '</ul>';
                                } else {
                                    echo('N/A');
                                }
                            ?>      
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-4"><label><strong>Status</strong></label></div>
                        <div class="col-8">
                        <?php 
                            if($userMerchandise->status == 1):
                        ?>
                            <span class="badge badge-success">Active</span>
                        <?php else :?>
                            <span class="badge badge-danger">Inactive</span>
                        <?php endif;?>
                            
                        </div>
                  </div>
                  <hr>
                   <div class="row">
                        <div class="col-4"><label><strong>Created</strong></label></div>
                        <div class="col-8"><?php echo isset($userMerchandise->created)?ucfirst($userMerchandise->created):'N/A'?></div>
                  </div>
                  <hr>
                   <div class="row">
                        <div class="col-4"><label><strong>Modified</strong></label></div>
                        <div class="col-8">
                        <?php echo isset($userMerchandise->modified)?ucfirst($userMerchandise->modified):'N/A'?>
                            
                        </div>
                  </div>
                    <hr>
                  <div class="row">
                    <div class="offset-4 col-8">
                        <?= $this->Html->link(__('Back'), $this->request->referer(),['class' => 'btn btn-secondary btn-lg text-right']) ?>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>