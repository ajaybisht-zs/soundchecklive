<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Add New Merchandise</h1>
    </div>
        <?php
            echo $this->Form->create(null, [
                'url' => [
                    'controller' => 'UserMerchandises',
                    'action' => 'add',
                    'prefix' => 'admin'
                ],
                'id' => 'sound-live-merchandise',
                'class' => 'user',
                'type' => 'file'
            ]);
        ?>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('name', [
                        'type' => 'text',
                        'placeholder' => 'Merchandise Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
            <div class="col-sm-6">
                <?php
                    echo $this->Form->control('price', [
                        'type' => 'number',
                        'placeholder' => 'price',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-12 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('description', [
                        'placeholder' => 'description',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->select(
                        'sizes._ids',
                            $sizes,
                            [
                                'multiple' => true,
                                'class' => 'form-control form-control-user',
                                'label' => false,
                                'id' => 'exampleFormControlSelect1'
                            ]
                        );
                ?>
            </div>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <div class="form-row form-group">
                    <div class="col-12">
                        <label for="exampleFormControlFile1">Choose Colors:  </label>
                    </div>
                    <?php foreach($colors as $key => $val) :?>
                        <div class="col-auto color-code">
                            <input type="checkbox" class="form-check-input" id="exampleCheck<?php echo $key?>" style="background-color:#000000" name="colors[_ids][]"  value="<?php echo $key?>">
                            <label class="form-check-label rounded-pill" for="exampleCheck<?php echo $key?>" style="background-color:<?php echo $val?>; width: 30px; height: 30px;"></label>
                        </div> 
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <style type="text/css">
            .color-code input {
                visibility: hidden;
                margin-left: 0px;
                width: 30px;
                height: 30px;
                margin-top: 0;
           }
           /*.color-code input + label {
                border: 2px solid transparent;
           }*/
            .color-code input:checked + label {
              position: relative;
            }

            .color-code input:checked + label::after {
                position: absolute;
                left: -4px;
                right: 0px;
                top: -4px;
                bottom: 0;
                content: "";
                display: block;
                border: 2px solid #000;
                width: 38px;
                height: 38px;
                border-radius: 50%;
            }
        </style>

        <div class="form-group row">
            <div class="col-sm-12">
                <label for="exampleFormControlFile1">Upload Cover Image</label>
               <?php
                    echo $this->Form->control('user_merchandise_images', [
                        'type' => 'file',
                       
                        //'class' => 'form-control form-control-user',
                        //'multiple' => 'multiple',
                        'label' => false,
                        'id' => 'filer_input2'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">          
            <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script([
            'backend/Admin/merchandise',
            'backend/Admin/custom'
            ], [
        'block' => 'scriptBottom'
    ]);
?>
