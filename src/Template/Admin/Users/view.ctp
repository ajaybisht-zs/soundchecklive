<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
      <div class="col-lg-7">
        <div class="p-5">
          <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Artist Information</h1>
          </div>
          <div class="row">
          		<div class="col-4"><label><strong>First Name</strong></label></div>
          		<div class="col-8"><?php echo isset($user->user_profile->first_name)?ucfirst($user->user_profile->first_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
	          	<div class="col-4"><label><strong>Last Name</strong></label></div>
	          	<div class="col-8"><?php echo isset($user->user_profile->last_name)?ucfirst($user->user_profile->last_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
	          	<div class="col-4"><label><strong>Email Address</strong></label></div>
	          	<div class="col-8"><?php echo isset($user->email)?ucfirst($user->email):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
	          	<div class="col-4"><label><strong>Phone Number</strong></label></div>
	          	<div class="col-8"><?php echo isset($user->user_profile->phone_number)?ucfirst($user->user_profile->phone_number):'N/A'?></div>
          </div>
           <hr>
           <div class="row">
	          	<div class="col-4"><label><strong>Zipcode</strong></label></div>
	          	<div class="col-8"><?php echo isset($user->user_profile->zipcode)?ucfirst($user->user_profile->zipcode):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Biography</strong></label></div>
              <div class="col-8"><?php echo isset($user->user_profile->biography)?ucfirst($user->user_profile->biography):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Artist Name</strong></label></div>
              <div class="col-8"><?php echo isset($user->user_profile->artist_name)?ucfirst($user->user_profile->artist_name):'N/A'?></div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Profile Pic</strong></label></div>
              <div class="col-8">
              <?php 
                            $avatar = $this->Images->artistAvatar($user->user_profile); 
                            if($avatar):
                                echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                        ?>
                        <?php else: ?>
                            <svg version="1.1" x="0px" y="0px" width="120px" height="120px" viewBox="0 0 53 53"
                                style="enable-background:new 0 0 53 53;" xml:space="preserve">
                                <path style="fill:#333333;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
                               c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
                               c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
                               c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
                               c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
                               C20.296,39.899,19.65,40.986,18.613,41.552z" />
                                <g>
                                    <path style="fill:#ffffff;"
                                        d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
                                   c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
                                   c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
                                   s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
                                   c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
                                   c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                                </g>
                            </svg>
                        <?php endif; ?>
                
              </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-4"><label><strong>Tags</strong></label></div>
            <div class="col-8">
            <p class="text-light-gray">
            <?php 
                            if(!empty($user->user_records[0]->user_record_keys)) { ?>
                            <?php 
                                foreach ($user->user_records[0]->user_record_keys as $key => $value) {
                                    if(!empty($value->keyword)) :  
                            ?>
                             <a href="#" class="badge badge-pill badge-dark mr-1">
                                <?php
                                    echo $value->keyword;
                                ?>    
                            </a>
                            <?php
                                   endif;      
                                }
                            }
                        ?>
              </div>
          </div>
          <hr>
          <div class="row">
              <div class="col-4"><label><strong>Are you currently signed or independent?</strong></label></div>
              <div class="col-8">
                <?php 
                  if($user->user_profile->is_signed == 1) :
                    echo 'Yes';
                  else :
                    echo 'No';
                endif;
                ?>
                
              </div>
          </div>
           <hr>
           <div class="row">
              <div class="col-4"><label><strong>Which Performance rights organization are you affiliated with?</strong></label></div>
              <div class="col-8">
                <?php 
                  if($user->user_profile->is_performance_rights_affiliated == 1) :
                    echo 'I do not know';
                  else :
                    echo 'None';
                endif;
                ?>
                
              </div>
          </div>
           <hr>
           <div class="row">
                <div class="col-4"><label><strong>D.O.B</strong></label></div>
                <div class="col-8">
                    <?php 
                     if(!empty($user->user_profile->birth_year)):
                        echo($user->user_profile->birth_day.'/'.$user->user_profile->birth_month.'/'.$user->user_profile->birth_year);
                    endif;
                    ?>        
                </div>
          </div>
          <hr>
          <hr>
           <div class="row">
                <div class="col-3"><label><strong>User Media Handle</strong></label></div>
                <div class="col-3">
                    <?php 
                        echo isset($user->user_media_handle->facebook)?$user->user_media_handle->facebook:'N/A';
                    ?>        
                </div>
                <div class="col-3">
                    <?php 
                        echo isset($user->user_media_handle->twitter)?$user->user_media_handle->twitter:'N/A';
                    ?>        
                </div>
                <div class="col-3">
                    <?php 
                        echo isset($user->user_media_handle->instagram)?$user->user_media_handle->instagram:'N/A';
                    ?>        
                </div>
          </div>
          <hr>
          <div class="row">
                <div class="col-4"><label><strong>What type of budget do you realistically need?</strong></label></div>
                <div class="col-8">
                    <?php 
                        echo $buget = $this->common->getBuget($user->user_profile->capital_goals);
                    ?>
                </div>
          </div>
           <hr>
           <div class="row">
                <div class="col-4"><label><strong>How will this budget be used to further your career and secure a ROI?</strong></label></div>
                <div class="col-8">
                    <?php 
                        echo isset($user->user_profile->budget_usage)?$user->user_profile->budget_usage:'N/A';
                    ?>  
                </div>
          </div>
          <hr>
          <div class="row">
                <div class="col-4"><label><strong>IS your record ready for release?</strong></label></div>
                <div class="col-8">
                    <?php 
                      if($user->user_profile->is_ready_for_release == 1) :
                        echo 'Yes, the record has been mixed and mastered';
                      else :
                        echo 'No, the record still needs work';
                    endif;
                ?> 
                </div>
          </div>
        <hr>
        <div class="row">
	          	<div class="col-4"><label><strong>Did you write this record?</strong></label></div>
	          	<div class="col-8">
                <?php 
                    if(!empty($user->user_records[0])) :
                        if($user->user_records[0]->record_written_by == 0){
                           echo "Yes, I wrote the record myself";
                        }
                        elseif($user->user_records[0]->record_written_by == 1){
                            echo "Yes, I wrote the record along with other writers
                            ";
                        }
                        elseif($user->user_records[0]->record_written_by == 2) {
                            echo "No, somebody else wrote the record for me
                            ";
                        }
                    endif;  
                ?>
	           </div>
        </div>
        <hr>
        <div class="row">
                <div class="col-4"><label><strong>Has this record already been distributed</strong></label></div>
                <div class="col-8">
                <?php 
                    if(!empty($user->user_records[0])) :
                        if($user->user_records[0]->is_already_distributed == 1):
                           echo "Yes"; 
                        endif;
                        if($user->user_records[0]->is_already_distributed == 0):
                           echo "No"; 
                        endif;
                    endif;
                ?>
               </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-4"><label><strong>Record Name</strong></label></div>
            <div class="col-8">
                <?php 
                    echo isset($user->user_records[0]->record_name)?ucfirst($user->user_records[0]->record_name):'N/A';
                ?> 
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-4"><label><strong>Artist Name</strong></label></div>
            <div class="col-8">
                <?php 
                    echo isset($user->user_records[0]->artist_name)?ucfirst($user->user_records[0]->artist_name):'N/A';
                ?> 
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-4"><label><strong>Writer</strong></label></div>
            <div class="col-8">
                <?php 
                    echo isset($user->user_records[0]->writer)?ucfirst($user->user_records[0]->writer):'N/A';
                ?> 
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-4"><label><strong>Features</strong></label></div>
            <div class="col-8">
                <?php 
                    echo isset($user->user_records[0]->features)?ucfirst($user->user_records[0]->features):'N/A';
                ?> 
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-4"><label><strong>upload the record information</strong></label></div>
            <div class="col-8">
                <?php
                    if(!empty($user->user_records[0]->record_dir) && !empty($user->user_records[0]->record_file)) {
                        $mp3Exists = $this->common->checkMp3($user->user_records[0]->record_dir,$user->user_records[0]->record_file);
                        if($mp3Exists) {   
                            echo $this->Html->media($mp3Exists, [
                                'fullBase' => true,
                                'text' => 'Fallback text',
                                'id' => 'audio',
                                'controls',
                                'style' => 'position: absolute; bottom: 0; left: 0; right: 0; padding: 0 12px; width: 100%;'
                            ]);
                        } else {
                            echo('Mp3 Not Found');
                        }
                    }

                if(!empty($user->user_records[0]->cover_image_dir) && !empty($user->user_records[0]->cover_image)) {
                    $coverImageExists = $this->common->checkCoverImage($user->user_records[0]->cover_image_dir,$user->user_records[0]->cover_image);
                    if($coverImageExists) {
                        echo $this->Html->image($coverImageExists, [
                            'style' => 'height: 100%; width: 100%; object-fit: contain; background-color: #eee;'
                        ]);
                    } else {
                        echo('cover Image Not Found');
                    } 
                }
                ?>
            </div>
        </div>
        <hr>
        <div class="row">
                <div class="col-4"><label><strong>video pitch</strong></label></div>
                <div class="col-8">
                    <?php
                    if(!empty($user->user_records[0])) {
                        $video = $this->Images->getVideoPitch($user->user_records[0]);
                        if($video) {
                            echo $this->Html->media($video, [
                                'fullBase' => true,
                                'text' => 'Fallback text',
                                'controls',
                                'width' => '100%'
                            ]);
                        }
                    } else {
                        echo('No video uploaded by artist');
                    }                       
                    ?>
               </div>
        </div>
        <hr>
        <div class="row">
                <div class="col-4"><label><strong>Status</strong></label></div>
                <div class="col-8">
                    <?php 
                        if($user->status == 1):
                    ?>
                    <span class="badge badge-success">Active</span>
                <?php else :?>
                    <span class="badge badge-danger">Inactive</span>
                <?php endif;?>
               </div>
        </div>
              <hr>
                    <table class="table table-bordered">
                        <h3 class="text-center">Supporters</h3>
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Phone Number</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                            if(!empty($user->user_supporters)) :
                                foreach ($user->user_supporters as $key => $val) :
                        ?>
                        <tr>
                          <th scope="row"><?= $key+1 ?></th>
                          <td><?php echo isset($val->name)?$val->name:'N/A'?></td>
                          <td><?php echo isset($val->phone_number)?$val->phone_number:'N/A'?></td>
                        </tr>
                        <?php
                            endforeach;
                            else:
                        ?>
                        <tr>
                          <td colspan="5" class="text-center no-data-found">
                            <h5><?= __('No Supporters found!') ?></h5>
                        </td> 
                        </tr>
                        <?php 
                            endif;
                        ?>
                      </tbody>
                    </table>
                  <hr>
        <hr>
       <div class="row">
          	<div class="col-4"><label><strong>Created</strong></label></div>
          	<div class="col-8"><?php echo isset($user->created)?ucfirst($user->created):'N/A'?></div>
      </div>
          <hr>
           <div class="row">
	          	<div class="col-4"><label><strong>Modified</strong></label></div>
	          	<div class="col-8"><?php echo isset($user->modified)?ucfirst($user->modified):'N/A'?></div>
          </div>
          <div class="row mt-5">
          	<?= $this->Html->link(__('Back'), $this->request->referer(),['class' => 'btn btn-secondary btn-lg text-right']) ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
