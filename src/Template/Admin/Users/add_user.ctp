<?php
    $this->Form->setTemplates([
        'radioContainer' => '{{content}}',
        'radioWrapper' => '<div class="form-check form-check-inline mr-3">{{label}}</div>'
    ]);
?>
<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4"><?= __('Add New User');?></h1>
    </div>
    <?php
        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'Users',
                'action' => 'addUser',
                'prefix' => 'admin'
            ],
            'id' => 'backend-user',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">First Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.first_name', [
                        'type' => 'text',
                        'placeholder' => 'First Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'firstName'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Last Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.last_name', [
                        'type' => 'text',
                        'placeholder' => 'Last Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Artist Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.artist_name', [
                        'type' => 'text',
                        'placeholder' => 'Artist Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'artist-name'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Email </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('email', [
                        'type' => 'email',
                        'class' => 'form-control form-control-user',
                        'placeholder' => 'Email Address',
                        'label' => false,
                        'autocomplete' => 'off'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Password</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= $this->Form->control('password',
                    [
                        "class"=>"form-control form-control-user",
                        "label" => false,
                       'placeholder' => __('Enter your password'),
                        "autocomplete" => "off"
                    ]
                )
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">confirm password</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                     echo $this->Form->control('confirm_password', [
                        'type' => 'password',
                        'class' => 'form-control form-control-user',
                        'placeholder' => 'confirm password',
                        'label' => false,
                        'autocomplete' => 'off'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Phone Number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.phone_number', [
                        'type' => 'text',
                        'placeholder' => 'phone number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">zipcode</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.zipcode', [
                        'type' => 'text',
                        'placeholder' => 'zipcode',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Date of Birth</label>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->month('user_profile.birth_month', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Month',
                        'value' => (!empty($user->user_profile->birth_month))? $user->user_profile->birth_month:'1',
                        'id' => 'birthMonth'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->day('user_profile.birth_day', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Date',
                        'value' => (!empty($user->user_profile->birth_day)) ? $user->user_profile->birth_day:'1',
                        'id' => 'birthDay'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                 <?php
                    echo $this->Form->year('user_profile.birth_year', [
                        'minYear' => 1950,
                        'maxYear' => date('Y'),
                        'class' => 'form-control custom-input',
                        'empty' => 'Year',
                        'value' => (!empty($user->user_profile->birth_year))? $user->user_profile->birth_year:2000,
                        'id' => 'birthYear'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Genre</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('user_profile.genre_id', [
                        'options' => $genres,
                        'label' => false,
                        'class' => 'form-control custom-input',
                        'empty' => '------'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">Are you currently signed or independent?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.is_signed', [
                        'options' => [
                            ['value' => 1, 'text' => 'independent', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'signed', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
                 <?= 
                    $this->Form->control('user_profile.signed_by', [
                        'label' => false,
                        'class' => 'form-control custom-input d-none signed-input',
                        'placeholder' => 'Name of label?'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Biography</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('user_profile.biography', [
                        'type' => 'text',
                        'placeholder' => 'Biography',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="performance">Which Performance Rights Organization (PRO) are you affiliated with?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.is_performance_rights_affiliated', [
                        'options' => [
                            ['value' => 0, 'text' => 'None', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'i do not know', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => 'Name of Agency', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
                <?php 
                    $hide = 'd-none';
                ?>
                <?= 
                    $this->Form->control('user_profile.performance_rights_organization', [
                        'label' => false,
                        'class' => 'form-control custom-input '.$hide,
                        'placeholder'=>'Name of Agency',
                        'id' => 'agency-name',
                        'required' => true
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="profile-image">Upload Profile pic</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
            <?= $this->Form->control('user_profile.avatar', [
                        'type' => 'file', 
                        'label' => false, 
                        'accept' =>"image/*",
                        'class' => 'form-control form-control-user'
                        //'onchange' => 'updateImageDisplay(this,"avatarImage")'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="facebook-url">Facebook Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_media_handle.facebook', [
                        'type' => 'text',
                        'placeholder' => 'facebook url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="twitter-url">Twitter Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_media_handle.twitter', [
                        'type' => 'text',
                        'placeholder' => 'twitter url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="instagram-url">Instagram Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_media_handle.instagram', [
                        'type' => 'text',
                        'placeholder' => 'Instagram url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="budget">What type of budget do you realistically need?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.capital_goals', [
                        'options' => [
                            ['value' => 0, 'text' => '$25,000 - $50,000', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => '$50,001 - $100,000', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => '$100,001 - $250,000', 'class' => 'mr-2'],
                            ['value' => 3, 'text' => '$250,001 - $500,000', 'class' => 'mr-2'],
                            ['value' => 4, 'text' => '$500,001 - $1,000,000', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="buget-use">How will this budget be used to further your career and secure a ROI
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('user_profile.budget_usage', [
                        'type' => 'text',
                        'placeholder' => 'buget use',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="recordName">Record Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.record_name', [
                        'type' => 'text',
                        'placeholder' => 'Record Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artist_name">Artist Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.artist_name', [
                        'type' => 'text',
                        'placeholder' => 'Artist Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">Features</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('user_record.features', [
                        'type' => 'text',
                        'placeholder' => 'Features',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">Writer</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.writer', [
                        'type' => 'text',
                        'placeholder' => 'writer',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">Producer</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.producer', [
                        'type' => 'text',
                        'placeholder' => 'Producer',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="audio-file">Upload Cover Image</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('user_record.cover_image', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        //'accept' => '.mp3,audio/*'
                    ]) 
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="audio-file">Upload Record mp3</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('user_record.record_file', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' => '.mp3,audio/*'
                    ]) 
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-1">Keyword #1</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.0.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 1',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-2">Keyword #2</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.1.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 2',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-3">Keyword #3</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.2.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 3',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Keyword #4</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.3.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 4',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Now that you are ready upload your video pitch(60 sec max)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->hidden('user_record.id') 
                ?>
                <?= 
                    $this->Form->control('user_record.video_pitch_file', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' => 'video/*'
                    ]) 
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Select Plan</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('user_plan.plan_id', [
                        'options' => $plan,
                        'label' => false,
                        'class' => 'form-control custom-input',
                        'empty' => '------'
                    ])
                ?>
            </div>
        </div>
        
       
        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'Artist/UserProfiles/backend-user-validate',
        'Artist/UserProfiles/validate-backend-add-user',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>
