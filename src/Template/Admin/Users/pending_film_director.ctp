<?php
    use Cake\Routing\Router;
?>
<div class="card-header py-3">
    <h2 class="m-0 font-weight-bold text-info text-center">Pending for approval(Film Director)</h2>
</div>
<div class="table-responsive mobile-res-tb">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
          <th>Id</th>
          <th>Production Company</th>
          <th>First Name</th>
          <th>LastName</th>
          <th>Email</th>
          <th>Phone No</th>
          <th>Age</th>
          <th>Status</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Approved</th>
          <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php
        if ($users->isEmpty() ) :
        ?> 
            <tr>
                <td colspan="12" class="text-center no-data-found">
                    <h3><?= __('No records found!') ?></h3>
                </td>
            </tr> 
        <?php else: foreach($users as $val) : //pr($val);?>
            <tr>
              <td><?= $val->id?></td>
               <td><?= isset($val->film_director_profile->production_company)?$val->film_director_profile->production_company:'N/A'?></td>
              <td><?= isset($val->film_director_profile->first_name)?$val->film_director_profile->first_name:'N/A'?></td>
              <td><?= isset($val->film_director_profile->last_name)?$val->film_director_profile->last_name:'N/A'?></td>
              <td><?= isset($val->email)?$val->email:'N/A'?></td>
              <td><?= isset($val->film_director_profile->phone_number)?$val->film_director_profile->phone_number:'N/A'?></td>
              <td>
              <?php 
                    if(!empty($val->film_director_profile->birth_year)) {  
                        $then = DateTime::createFromFormat("Y/m/d", $val->film_director_profile->birth_year.'/'.$val->film_director_profile->birth_month.'/'.$val->film_director_profile->birth_day);
                        $diff = $then->diff(new DateTime());
                        echo $diff->format("%y year %m month %d day\n");
                    } else {
                        echo 0;
                    }
                ?>
            </td>
              <td>
                <?php 
                    if($val->status == 1):
                ?>
                    <span class="badge badge-success">Active</span>
                <?php else :?>
                    <span class="badge badge-danger">Inactive</span>
                <?php endif;?>
              </td>
              <td><?= $val->created?></td>
              <td><?= $val->modified?></td>
              <td>
                <input type="checkbox"  class = "approve_user" data-toggle="toggle" data-onstyle="success" data-url="<?php 
                                                            echo router::url(
                                                            [      
                                                                'controller' => 'Users',                                                
                                                                "action" => "approveFilmDirector",
                                                                $val->id,
                                                                'prefix' => 'admin',
                                                            ])?>"<?php echo ($val->status == 1) ? "checked" : ""?>>
              </td>
              <td>
                <?= 
                    $this->Html->link(__('<span class="fa fa-edit"></span>&nbsp; Edit'), 
                    [
                      'controller' => 'Users',
                      'action' => 'editFilmDirector',
                      $val->id,
                      'prefix' => 'admin'
                    ], [
                    'escape' => false
                  ])?>
              </td>
            </tr>
        <?php endforeach;endif;?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>
<?= $this->element('Model/SmartLink/add_smart_link');?>
<?php
    echo $this->Html->script(
        [
            'backend/Admin/user_approve',
            'backend/Admin/add_smart_link'
            ], 
            [
        'block' => 'scriptBottom'
  ]); 
?>