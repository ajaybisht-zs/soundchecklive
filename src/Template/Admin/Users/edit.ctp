<?php
    $this->Form->setTemplates([
        'radioContainer' => '{{content}}',
        'radioWrapper' => '<div class="form-check form-check-inline mr-3">{{label}}</div>'
    ]);
?>
<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Edit User Information</h1>
    </div>
    <?php
        echo $this->Form->create($user, [
            'url' => [
                'controller' => 'Users',
                'action' => 'edit',
                'prefix' => 'admin'
            ],
            'id' => 'sound-live-colors',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Artist Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.artist_name', [
                        'type' => 'text',
                        'placeholder' => 'Artist Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'artist-name'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="levels">Set Level Price</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('level_id', [
                        'options' => $levels,
                        'label' => false,
                        'class' => 'form-control custom-input'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Profile pic</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php 
                    $avatar = $this->Images->artistAvatar($user->user_profile); 
                    if($avatar):
                        echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                ?>
                <?php else: ?>
                    <svg version="1.1" x="0px" y="0px" width="120px" height="120px" viewBox="0 0 53 53"
                        style="enable-background:new 0 0 53 53;" xml:space="preserve">
                        <path style="fill:#333333;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
                        c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
                        c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
                        c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
                        c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
                        C20.296,39.899,19.65,40.986,18.613,41.552z" />
                        <g>
                            <path style="fill:#ffffff;"
                                d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
                            c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
                            c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
                            s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
                            c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
                            c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                        </g>
                    </svg>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="profile-image">No Partners</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= $this->Form->control('user_record.no_of_partners', [
                        'label' => false, 
                        'class' => 'form-control form-control-user',
                        'value' => ($user->user_record)?$user->user_record->partner:0
                    ])
                ?>
                <small class="text-danger">Real No. of Partners: <?= ($user->user_record)?$user->user_record->total_fans:0 ?><br/>
                     Note: on frontend partners no. will be actual partners + mannual partner
                </small>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="profile-image">Change Profile pic</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
            <?= $this->Form->control('user_profile.avatar', [
                        'type' => 'file', 
                        'label' => false, 
                        'accept' =>"image/*",
                        'class' => 'form-control form-control-user'
                        //'onchange' => 'updateImageDisplay(this,"avatarImage")'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">First Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.first_name', [
                        'type' => 'text',
                        'placeholder' => 'First Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'firstName'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Last Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.last_name', [
                        'type' => 'text',
                        'placeholder' => 'Last Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Phone Number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.phone_number', [
                        'type' => 'text',
                        'placeholder' => 'phone number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">zipcode</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_profile.zipcode', [
                        'type' => 'text',
                        'placeholder' => 'zipcode',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Biography</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('user_profile.biography', [
                        'type' => 'text',
                        'placeholder' => 'Biography',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Genre</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('user_profile.genre_id', [
                        'options' => $genres,
                        'label' => false,
                        'class' => 'form-control custom-input'
                    ])
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Date of Birth</label>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->month('user_profile.birth_month', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Month',
                        'value' => (!empty($user->user_profile->birth_month))? $user->user_profile->birth_month:'1',
                        'id' => 'birthMonth'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->day('user_profile.birth_day', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Date',
                        'value' => (!empty($user->user_profile->birth_day)) ? $user->user_profile->birth_day:'1',
                        'id' => 'birthDay'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                 <?php
                    echo $this->Form->year('user_profile.birth_year', [
                        'minYear' => 1950,
                        'maxYear' => date('Y'),
                        'class' => 'form-control custom-input',
                        'empty' => 'Year',
                        'value' => (!empty($user->user_profile->birth_year))? $user->user_profile->birth_year:2000,
                        'id' => 'birthYear'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="recordName">Record Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.record_name', [
                        'type' => 'text',
                        'placeholder' => 'Record Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artist_name">Artist Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.artist_name', [
                        'type' => 'text',
                        'placeholder' => 'Artist Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">Features</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('user_record.features', [
                        'type' => 'text',
                        'placeholder' => 'Features',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">Writer</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.writer', [
                        'type' => 'text',
                        'placeholder' => 'writer',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">Producer</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.producer', [
                        'type' => 'text',
                        'placeholder' => 'Producer',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="facebook-url">Facebook Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_media_handle.facebook', [
                        'type' => 'text',
                        'placeholder' => 'facebook url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="twitter-url">Twitter Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_media_handle.twitter', [
                        'type' => 'text',
                        'placeholder' => 'twitter url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="instagram-url">Instagram Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_media_handle.instagram', [
                        'type' => 'text',
                        'placeholder' => 'Instagram url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-1">Keyword #1</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.0.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 1',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-2">Keyword #2</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.1.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 2',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-3">Keyword #3</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.2.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 3',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Keyword #4</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_record.user_record_keys.3.keyword', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 4',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Change Video Pitch</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->hidden('user_record.id') 
                ?>
                <?= 
                    $this->Form->control('user_record.video_pitch_file', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' => 'video/*'
                    ]) 
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="video-pitch">Video pitch</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    $video = $this->Images->getVideoPitch($user->user_record);
                    if($video) {
                        echo $this->Html->media($video, [
                            'fullBase' => true,
                            'text' => 'Fallback text',
                            'controls',
                            'width' => '100%'
                        ]);
                    } else {
                        echo('video not uploaded');
                    }

                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="video-pitch">Audio</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    if(!empty($user->user_record->record_dir) && !empty($user->user_record->record_file)) {
                        $mp3Exists = $this->common->checkMp3($user->user_record->record_dir,$user->user_record->record_file);
                        if($mp3Exists) {   
                            echo $this->Html->media($mp3Exists, [
                                'fullBase' => true,
                                'text' => 'Fallback text',
                                'id' => 'audio',
                                'controls',
                                'style' => 'position: absolute; bottom: 0; left: 0; right: 0; padding: 0 12px; width: 100%;'
                            ]);
                        } else {
                            echo('mp3 Not uploaded');
                        }
                    } else {
                        echo('mp3 not Found');
                    }

                    if(!empty($user->user_record->cover_image_dir) && !empty($user->user_record->cover_image)) {
                        $coverImageExists = $this->common->checkCoverImage($user->user_record->cover_image_dir,$user->user_record->cover_image);
                        if($coverImageExists) {
                            echo $this->Html->image($coverImageExists, [
                                'style' => 'height: 100%; width: 100%; object-fit: contain; background-color: #eee;',
                            ]);
                        } else {
                            echo('cover Image Not Found');
                        } 
                    }
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="audio-file">Change Audio</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->hidden('user_record.id') ;
                ?>
                <?= 
                    $this->Form->control('user_record.record_file', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' => '.mp3,audio/*'
                    ]) 
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="bank_name">Bank Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_bank_detail.bank_name', [
                        'type' => 'text',
                        'placeholder' => 'Enter bank name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="account-holder-name">Account Holder Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_bank_detail.name_on_account', [
                        'type' => 'text',
                        'placeholder' => 'Enter account holder name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="account-number">Account Number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_bank_detail.account_number', [
                        'type' => 'text',
                        'placeholder' => 'Enter account number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">Routing Number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('user_bank_detail.routing_number', [
                        'type' => 'text',
                        'placeholder' => 'Enter routing number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">Are you currently signed or independent?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.is_signed', [
                        'options' => [
                            ['value' => 0, 'text' => 'independent', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'signed', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="performance">Which Performance rights organization are you affiliated with?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.is_performance_rights_affiliated', [
                        'options' => [
                            ['value' => 0, 'text' => 'None', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'i do not know', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="budget">What type of budget do you realistically need?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.capital_goals', [
                        'options' => [
                            ['value' => 0, 'text' => '$25,000 - $50,000', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => '$50,001 - $100,000', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => '$100,001 - $250,000', 'class' => 'mr-2'],
                            ['value' => 3, 'text' => '$250,001 - $500,000', 'class' => 'mr-2'],
                            ['value' => 4, 'text' => '$500,001 - $1,000,000', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="buget-use">How will this budget be used to further your career and secure a ROI
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('user_profile.budget_usage', [
                        'type' => 'text',
                        'placeholder' => 'buget use',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="record ready">IS your record ready for release?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_profile.is_ready_for_release', [
                        'options' => [
                            ['value' => 0, 'text' => 'No, the record still needs work', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'Yes, the record has been mixed and mastered', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="did-you-write">Did you write this record?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_record.record_written_by', [
                        'options' => [
                            ['value' => 0, 'text' => 'Yes, I wrote the record myself', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'Yes, I wrote the record along with oter writers', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => 'No, somebody else wrote the record for me', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="has-record-distributed">Has this record already been distributed</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('user_record.record_written_by', [
                        'options' => [
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>

         <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">Supporters</label>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.0.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.0.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.1.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.1.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.2.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.2.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.3.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.3.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.4.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.4.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.5.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.5.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.6.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.6.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.7.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.7.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.8.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.8.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                
        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script([
            'backend/Admin/color_validate',
            ], [
        'block' => 'scriptBottom'
    ]);
?>
