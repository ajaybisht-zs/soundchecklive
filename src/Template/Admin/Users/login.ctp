<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= __('Login');?></title>

  <!-- Custom fonts for this template-->
  <?= $this->Html->css('vendor/all.min'); ?>
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <?= $this->Html->css('sb-admin-2'); ?>
  <?= $this->Html->css('style'); ?>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                <?php echo $this->Flash->render(); ?>
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <?php 
                    echo $this->Form->create(
                        null,
                        [
                            'type' => 'post',
                            'class' => 'user',
                            'novalidate' => true,
                            'id' => 'login-form',
                            'name' => 'loginform'
                        ]
                    );
                ?>
                    <div class="form-group">
                        <?= $this->Form->control('email',
                                [
                                    "class"=>"form-control form-control-user",
                                    "id" => "exampleInputEmail",
                                    'describedby' => 'emailHelp',
                                    'Placeholder' => __('Enter Email Address...'),
                                    "autocomplete" => "off"
                                ]
                            )
                        ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('password',
                                [
                                    "class"=>"form-control form-control-user",
                                    "id" => "exampleInputPassword",
                                    'placeholder' => __('Enter your password'),
                                    "autocomplete" => "off"
                                ]
                            )
                        ?>
                    </div>
                    <!-- <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div> -->
                    <?=
                        $this->Form->button(
                            __('Login'),
                            [
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-user btn-block'
                            ]
                        );
                    ?>
                    <hr>
                <?= $this->Form->end(); ?>
                  <!-- <hr> -->
                  <!-- <div class="text-center">
                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
 <?php
    echo $this->Html->script([
      'vendor/jquery.min',
      'vendor/bootstrap.bundle.min',
    ]);
?>

  <!-- Core plugin JavaScript-->
<?php 
    echo $this->Html->script('vendor/jquery.easing.min');
?>
  <!-- Custom scripts for all pages-->
<?php 
    echo $this->Html->script('sb-admin-2.min');
?>
</body>

</html>
