<?php
    $this->Form->setTemplates([
        'radioContainer' => '{{content}}',
        'radioWrapper' => '<div class="form-check form-check-inline mr-3">{{label}}</div>'
    ]);
?>
<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4"><?= __('Add New Film Director');?></h1>
    </div>
    <?php
        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'Users',
                'action' => 'addFilmDirector',
                'prefix' => 'admin'
            ],
            'id' => 'add-director',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">First Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.first_name', [
                        'type' => 'text',
                        'placeholder' => 'First Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'firstName'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Last Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.last_name', [
                        'type' => 'text',
                        'placeholder' => 'Last Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Production Company or Studio  *</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.production_company', [
                        'type' => 'text',
                        'placeholder' => 'Production Company',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'artist-name'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Email </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('email', [
                        'type' => 'email',
                        'class' => 'form-control form-control-user',
                        'placeholder' => 'Email Address',
                        'label' => false,
                        'autocomplete' => 'off'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName1">Password</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= $this->Form->control('password',
                    [
                        "class"=>"form-control form-control-user",
                        "label" => false,
                       'placeholder' => __('Enter your password'),
                        "autocomplete" => "off"
                    ]
                )
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">confirm password</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                     echo $this->Form->control('confirm_password', [
                        'type' => 'password',
                        'class' => 'form-control form-control-user',
                        'placeholder' => 'confirm password',
                        'label' => false,
                        'autocomplete' => 'off'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Phone Number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.phone_number', [
                        'class' => 'form-control custom-input',
                        'label' => false,
                        'type' => 'tel',
                        'id' => 'phone'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">zipcode</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.zipcode', [
                        'type' => 'text',
                        'placeholder' => 'zipcode',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">What's your date of birth?</label>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->month('film_director_profile.birth_month', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Month',
                        'value' => (!empty($user->film_director_profile->birth_month))? $user->film_director_profile->birth_month:'1',
                        'id' => 'birthMonth'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->day('film_director_profile.birth_day', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Date',
                        'value' => (!empty($user->film_director_profile->birth_day)) ? $user->film_director_profile->birth_day:'1',
                        'id' => 'birthDay'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                 <?php
                    echo $this->Form->year('film_director_profile.birth_year', [
                        'minYear' => 1950,
                        'maxYear' => date('Y'),
                        'class' => 'form-control custom-input',
                        'empty' => 'Year',
                        'value' => (!empty($user->film_director_profile->birth_year))? $user->film_director_profile->birth_year:2000,
                        'id' => 'birthYear'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Select your film genre</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('film_director_profile.genre_id', [
                        'options' => $genres,
                        'label' => false,
                        'class' => 'form-control custom-input',
                        'empty' => '------',
                        'required' => true
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">Are you legally affiliated with any studios, production companies or producers? 
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.are_you_affiliated_any', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'value' => 1
                    ]);
                ?>
                 <?= 
                    $this->Form->control('film_director_profile.affiliated_with_name', [
                        'label' => false,
                        'class' => 'form-control custom-input signed-input',
                        'placeholder' => 'Name?',
                        'id' => 'affiliated-name'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number55">Have you had or do you currently have a distributor?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.have_distributor', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'value' => 1
                    ]);
                ?>
                 <?= 
                    $this->Form->control('film_director_profile.distributor_name', [
                        'label' => false,
                        'class' => 'form-control custom-input signed-input',
                        'placeholder' => 'Name?',
                        'id' => 'distributor-name',

                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Tell your fans about yourself (Biography)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('film_director_profile.biography', [
                        'type' => 'text',
                        'placeholder' => 'Biography',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'required' => true
                    ]);
                ?>
            </div>
        </div>

       

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="profile-image">Upload Profile pic</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
            <?= $this->Form->control('film_director_profile.avatar', [
                        'type' => 'file', 
                        'label' => false, 
                        'accept' =>"image/*",
                        'class' => 'form-control form-control-user'
                        //'onchange' => 'updateImageDisplay(this,"avatarImage")'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="facebook-url">Facebook Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.facebook', [
                        'type' => 'text',
                        'placeholder' => 'facebook url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="twitter-url">Twitter Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.twitter', [
                        'type' => 'text',
                        'placeholder' => 'twitter url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="instagram-url">Instagram Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.instagram', [
                        'type' => 'text',
                        'placeholder' => 'Instagram url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="instagram-url">Youtube</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.youtube', [
                        'type' => 'text',
                        'placeholder' => 'youtube url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="snapchat-url">Snapchat</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.snapchat', [
                        'type' => 'text',
                        'placeholder' => 'snapchat url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="tiktok-url">Tiktok</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.tiktok', [
                        'type' => 'text',
                        'placeholder' => 'tiktok url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="twitch-url">Twitch</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_media_handle.twitch', [
                        'type' => 'text',
                        'placeholder' => 'twitch url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="budget">What type of budget are you realistically seeking?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.what_type_budget_seeking', [
                        'options' => [
                            ['value' => 1, 'text' => '$25,000 - $50,000', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => '$50,001 - $100,000', 'class' => 'mr-2'],
                            ['value' => 3, 'text' => '$100,001 - $250,000', 'class' => 'mr-2'],
                            ['value' => 4, 'text' => '$250,001 - $500,000', 'class' => 'mr-2'],
                            ['value' => 5, 'text' => '$500,001 - $1,000,000', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="buget-use">How will this budget be used to further your career and secure a ROI?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('film_director_profile.buget_used_for_roi', [
                        'type' => 'text',
                        'placeholder' => 'buget use',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'required' => true
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="recordName">Upload your product media, movie trailer or director’s reel.(image )</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.poster_pic', [
                        'type' => 'file',
                        'placeholder' => 'Upload your movie cover art poster',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' =>"image/*",
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artist_name">Upload your movie trailer or Director’s reel(mp4)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.movie_reel', [
                        'type' => 'file',
                        'placeholder' => 'Upload movie reel',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' =>"video/mp4",
                    ]);
                ?>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-1">Keyword #1</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.keyword_1', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 1',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-2">Keyword #2</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.keyword_2', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 2',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-3">Keyword #3</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.keyword_3', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 3',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Keyword #4</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.keyword_4', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 4',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Keyword #5</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('film_director_profile.keyword_5', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 5',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

      
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Now that you are ready upload your video pitch.(60 sec max)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('film_director_profile.pitch_video', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' => 'video/mp4'
                    ]) 
                ?>
            </div>
        </div>
       
       <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">Are you male or female?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.gender', [
                        'options' => [
                            ['value' => 1, 'text' => 'Male', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => 'Female', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">What's your ethnicity?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">

                <?php 
                    $ethnicity = [
                        1 => 'White or Caucasian',
                        2 => 'Black or African American',
                        3 => 'American Indian or Alaska Native',
                        4 => 'Latino or Hispanic',
                        5 => 'Asian',
                        6 => 'Pacific Islander or Hawaiian',
                        7 => 'Other',
                    ]

                ?>

               <?= 
                    $this->Form->control('film_director_profile.ethnicity', [
                        'options' => $ethnicity,
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => 'Select',
                        'required' => true
                    ])
                ?>

            </div>
        </div>

        <div class=" form-group col-12">
            <label>Where are you originally from? </label>
                <div class="row">
                   <div class="col-sm-4">
                       <label>Country</label>
                        <?php
                            $countries = $this->common->getAllCountry();
                            $defaultCountry = 0;
                            echo $this->Form->select('film_director_profile.country_id', $countries, [
                                'class' => 'form-control',
                                'label' => false,
                                'data-select' => 'countries',
                                'empty' => __('Please select a country'),
                                'value' => $defaultCountry,
                                'required' => true
                            ]);
                        ?>
                   </div>
                   <div class="col-sm-4">
                    <label>State</label>
                    <?php
                        $states = [];
                        /*if (isset($sessionProfile['business_profile']['country_id'])):
                            $countryId = $sessionProfile['user_profile']['country_id'];
                            $states = $this->state->findByCountryId($countryId);
                        endif;*/
                        echo $this->Form->select('film_director_profile.state_id', $states, [
                            'class' => 'form-control',
                            'data-select' => 'states',
                             'required' => true
                            //'value' => (!empty($sessionProfile))?$sessionProfile['user_profile']['state_id']:0,
                        ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label>City</label>
                    <?php
                        echo $this->Form->control('film_director_profile.city', [
                            'type' => 'text',
                            'placeholder' => 'City',
                            'class' => 'form-control',
                            'label' => false,
                            'value' => '',
                            'required' => true
                        ]);
                    ?>
                </div>
               </div>
            </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-numbers">Do you have a booking agent or manager?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.is_booking_manager', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'value' => 1

                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-numbers1">Have you ever been marketed overseas?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.is_marketed_overseas', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'required' => true,
                        'value' => 1
                        
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-numbersr">Do you have merchandise for sale?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('film_director_profile.is_merchandise_sale', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'required' => true,
                        'value' => 1
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>

<?php

    echo $this->Html->css(
        [
            'intlTelInput_new',
        ]
    );
?>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'IntelJs/intlTelInput.min',
        'FilmDirector/validate_admin_add_director',
        'Artist/UserProfiles/country',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>
