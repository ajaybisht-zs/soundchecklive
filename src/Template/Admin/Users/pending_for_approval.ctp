<?php
    use Cake\Routing\Router;
?>
<div class="card-header py-3">
    <h2 class="m-0 font-weight-bold text-info text-center">Pending for approval</h2>
</div>
<div class="table-responsive mobile-res-tb">

<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
          <th>Id</th>
          <th>Artist Name</th>
          <th>First Name</th>
          <th>LastName</th>
          <th>Age</th>
          <th>Membership Level</th>
          <th>Partner</th>
          <th>Status</th>
          <th>Smart Link</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Approved</th>
          <th>Action</th>
        </tr>
    </thead>
    <!-- <tfoot>
        <tr>
          <th>Id</th>
          <th>Artist Name</th>
          <th>First Name</th>
          <th>LastName</th>
          <th>Age</th>
          <th>Membership Level</th>
          <th>Partner</th>
          <th>Status</th>
          <th>Smart Link</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Approved</th>
          <th>Action</th>
        </tr>
    </tfoot> -->
    <tbody>
    <?php
        if ($users->isEmpty() ) :
        ?> 
            <tr>
                <td colspan="12" class="text-center no-data-found">
                    <h3><?= __('No records found!') ?></h3>
                </td>
            </tr> 
        <?php else: foreach($users as $val) :?>
            <tr>
              <td><?= $val->id?></td>
               <td><?= isset($val->user_profile->artist_name)?$val->user_profile->artist_name:'N/A'?></td>
              <td><?= isset($val->user_profile->first_name)?$val->user_profile->first_name:'N/A'?></td>
              <td><?= isset($val->user_profile->last_name)?$val->user_profile->last_name:'N/A'?></td>
              <td>
              <?php 
                    if(!empty($val->user_profile->birth_year)) {  
                        $then = DateTime::createFromFormat("Y/m/d", $val->user_profile->birth_year.'/'.$val->user_profile->birth_month.'/'.$val->user_profile->birth_day);
                        $diff = $then->diff(new DateTime());
                        echo $diff->format("%y year %m month %d day\n");
                    } else {
                        echo 0;
                    }
                ?>
            </td>
            <td><?= isset($val->user_plan['plan']->title)?$val->user_plan['plan']->title:'N/A'?></td>
            <td>
              <?php 
                  if(!empty($val->user_records)) {
                     echo count($val->user_records[0]->user_record_payments);
                  } else {
                    echo 0;
                  }
              ?>
            </td>
              <td>
                <?php 
                    if($val->status == 1):
                ?>
                    <span class="badge badge-success">Active</span>
                <?php else :?>
                    <span class="badge badge-danger">Inactive</span>
                <?php endif;?>
              </td>
              <td><?= isset($val->user_profile->smart_link)?$val->user_profile->smart_link:'N/A'?></td>
              <td><?= $val->created?></td>
              <td><?= $val->modified?></td>
              <td>
                <input type="checkbox"  class = "approve_user" data-toggle="toggle" data-onstyle="success" data-url="<?php 
                                                            echo router::url(
                                                            [      
                                                                'controller' => 'Users',                                                
                                                                "action" => "approveUser",
                                                                $val->id,
                                                                'prefix' => 'admin',
                                                            ])?>"<?php echo ($val->status == 1) ? "checked" : ""?>>
              </td>
              <td><?= 
                    $this->Html->link(__('<span class="fa fa-eye icon-setting"></span>&nbsp; view'), 
                                            [
                                                'controller' => 'Users',
                                                'action' => 'view',
                                                $val->id,
                                                'prefix' => 'admin'
                                            ], [
                                            'escape' => false
                    ])?>
                    <?= 
						$this->Html->link(__('<span class="fa fa-edit"></span>&nbsp; Edit'), 
						[
							'controller' => 'Users',
							'action' => 'edit',
							$val->id,
							'prefix' => 'admin'
						], [
						'escape' => false
					])?>
                 <?= $this->Form->postLink(
                    __('<span class="fa fa-trash icon-setting"></span> &nbsp; Delete'), [
                            'action' => 'delete', 
                            $val->id,
                        ], [
                            'confirm' => __('Are you sure you want to delete # {0}?', $val->id),
                            'escape' => false,
                            'class' => 'ml-2'
                            
                        ]) 
                ?>

                <?php                                                                             
                    echo $this->Html->link(
                        '<span class="fa fa-link"></span>&nbsp;Smart Link', 
                        '#',
                        [
                            'escape' => false, 
                            'block' => 'scriptBottom',
                            'title' => 'Disapprove',
                            'data-toggle' => 'modal',
                            'class' => 'btn btn-danger btn-sm add-smart-link',
                            'data-id' => $val->id,
                        ] 
                    );
                ?>      
                </td>
            </tr>
        <?php endforeach;endif;?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>
<?= $this->element('Model/SmartLink/add_smart_link');?>
<?php
    echo $this->Html->script(
        [
            'backend/Admin/user_approve',
            'backend/Admin/add_smart_link'
            ], 
            [
        'block' => 'scriptBottom'
  ]); 
?>