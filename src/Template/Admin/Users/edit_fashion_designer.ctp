<?php
    $this->Form->setTemplates([
        'radioContainer' => '{{content}}',
        'radioWrapper' => '<div class="form-check form-check-inline mr-3">{{label}}</div>'
    ]);
?>
<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4"><?= __('Edit Fashion Designer');?></h1>
    </div>
    <?php
        echo $this->Form->create($user, [
            'url' => [
                'controller' => 'Users',
                'action' => 'editFashionDesigner',
                'prefix' => 'admin'
            ],
            'id' => 'add-designer',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">First Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.first_name', [
                        'type' => 'text',
                        'placeholder' => 'First Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'firstName'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Last Name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.last_name', [
                        'type' => 'text',
                        'placeholder' => 'Last Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="levels">Set Level Price</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('level_id', [
                        'options' => $levels,
                        'label' => false,
                        'class' => 'form-control custom-input'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Designer or Brand Name *</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.brand_name', [
                        'type' => 'text',
                        'placeholder' => 'Designer or Brand Name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'artist-name'
                    ]);
                ?>
            </div>
        </div>

    

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="zipcode">zipcode</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.zipcode', [
                        'type' => 'text',
                        'placeholder' => 'zipcode',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="phone_number">Phone number</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.phone_number', [
                        'type' => 'text',
                        'placeholder' => 'enter phone number',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">What's your date of birth?</label>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->month('fashion_designer_profile.birth_month', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Month',
                        'value' => (!empty($user->fashion_designer_profile->birth_month))? $user->fashion_designer_profile->birth_month:'1',
                        'id' => 'birthMonth'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                <?= 
                    $this->Form->day('fashion_designer_profile.birth_day', [
                        'class' => 'form-control custom-input',
                        'empty' => 'Date',
                        'value' => (!empty($user->fashion_designer_profile->birth_day)) ? $user->fashion_designer_profile->birth_day:'1',
                        'id' => 'birthDay'
                    ]); 
                ?>
            </div>
            <div class="col-sm-2 mb-3 mb-sm-0">
                 <?php
                    echo $this->Form->year('fashion_designer_profile.birth_year', [
                        'minYear' => 1950,
                        'maxYear' => date('Y'),
                        'class' => 'form-control custom-input',
                        'empty' => 'Year',
                        'value' => (!empty($user->fashion_designer_profile->birth_year))? $user->fashion_designer_profile->birth_year:2000,
                        'id' => 'birthYear'
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Select your designer style</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('fashion_designer_profile.style_id', [
                        'options' => $genres,
                        'label' => false,
                        'class' => 'form-control custom-input',
                         'class' => 'form-control custom-input',
                        'value' => $user->fashion_designer_profile->style_id,
                        'required' => true
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number"> Are you a licensed brand or up-and-coming independent line?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('fashion_designer_profile.is_licensed_brand_or_independent_line', [
                        'options' => [
                            ['value' => 0, 'text' => 'Licensed Brand', 'class' => 'mr-2'],
                            ['value' => 1, 'text' => 'Independent Line', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'value' => 0
                    ]);
                ?>
                 <?= 
                    $this->Form->control('fashion_designer_profile.parent_company', [
                        'label' => false,
                        'class' => 'form-control custom-input signed-input',
                        'placeholder' => 'Name?',
                        'id' => 'parent_company',
                        'value' => $user->fashion_designer_profile->parent_company
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="lastName">Tell your fans about yourself (Biography)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('fashion_designer_profile.biography', [
                        'type' => 'text',
                        'placeholder' => 'Biography',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'required' => true
                    ]);
                ?>
            </div>
        </div>

       <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artistName">Profile pic</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php 
                    $avatar = $this->Images->artistAvatar($user->fashion_designer_profile); 
                    if($avatar):
                        echo $this->Html->image($avatar, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                ?>
                <?php else: ?>
                    <svg version="1.1" x="0px" y="0px" width="120px" height="120px" viewBox="0 0 53 53"
                        style="enable-background:new 0 0 53 53;" xml:space="preserve">
                        <path style="fill:#333333;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
                        c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
                        c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
                        c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
                        c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
                        C20.296,39.899,19.65,40.986,18.613,41.552z" />
                        <g>
                            <path style="fill:#ffffff;"
                                d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
                            c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
                            c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
                            s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
                            c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
                            c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z" />
                        </g>
                    </svg>
                <?php endif; ?>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="profile-image">Upload your Artist profile picture.</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
            <?= $this->Form->control('fashion_designer_profile.avatar', [
                        'type' => 'file', 
                        'label' => false, 
                        'accept' =>"image/*",
                        'class' => 'form-control form-control-user'
                        //'onchange' => 'updateImageDisplay(this,"avatarImage")'
                    ])
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="facebook-url">Facebook Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.facebook', [
                        'type' => 'text',
                        'placeholder' => 'facebook url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="twitter-url">Twitter Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.twitter', [
                        'type' => 'text',
                        'placeholder' => 'twitter url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="instagram-url">Instagram Url</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.instagram', [
                        'type' => 'text',
                        'placeholder' => 'Instagram url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="instagram-url">Youtube</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.youtube', [
                        'type' => 'text',
                        'placeholder' => 'youtube url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="snapchat-url">Snapchat</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.snapchat', [
                        'type' => 'text',
                        'placeholder' => 'snapchat url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="tiktok-url">Tiktok</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.tiktok', [
                        'type' => 'text',
                        'placeholder' => 'tiktok url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="twitch-url">Twitch</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_media_handle.twitch', [
                        'type' => 'text',
                        'placeholder' => 'twitch url',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="budget">What type of budget are you realistically seeking?</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('fashion_designer_profile.what_type_budget_seeking', [
                        'options' => [
                            ['value' => 1, 'text' => '$25,000 - $50,000', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => '$50,001 - $100,000', 'class' => 'mr-2'],
                            ['value' => 3, 'text' => '$100,001 - $250,000', 'class' => 'mr-2'],
                            ['value' => 4, 'text' => '$250,001 - $500,000', 'class' => 'mr-2'],
                            ['value' => 5, 'text' => '$500,001 - $1,000,000', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="buget-use">How will this budget be used to further your career and secure a ROI?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->textarea('fashion_designer_profile.buget_used_for_roi', [
                        'type' => 'text',
                        'placeholder' => 'buget use',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'required' => true
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-1">Keyword #1</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.keyword_1', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 1',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-2">Keyword #2</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.keyword_2', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 2',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-3">Keyword #3</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.keyword_3', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 3',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Keyword #4</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.keyword_4', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 4',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Keyword #5</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.keyword_5', [
                        'type' => 'text',
                        'placeholder' => 'Keyword 5',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="video-pitch">Product featured Image</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    $video = $this->Images->getFashionDesignerLastestGarment($user->fashion_designer_profile);
                    if($video) {
                         echo $this->Html->image($video, ['class' => 'rounded-circle', 'style' => 'width:120px; height:120px;']);
                    } else {
                        echo('featured image not uploaded');
                    }

                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="recordName">Upload or Change a picture of your featured product</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.garment_pic', [
                        'type' => 'file',
                        'placeholder' => 'Upload your product featured image',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'accept' =>"image/*",
                    ]);
                ?>
            </div>
        </div>

         <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="video-pitch">Video of your creative process</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    $video = $this->Images->getProcessVideo($user->fashion_designer_profile);
                    if($video) {
                          echo $this->Html->media($video, [
                            'fullBase' => true,
                            'text' => 'Fallback text',
                            'controls',
                            'width' => '100%'
                        ]);
                    } else {
                        echo('video not uploaded');
                    }

                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="artist_name">Upload a video of your creative process(mp4)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('fashion_designer_profile.process_video', [
                        'type' => 'file',
                        'placeholder' => 'Upload process_video',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        //'accept' =>"video/mp4",
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="video-pitch">Pitch Video</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    $video = $this->Images->filmDirectorPitchVideo($user->fashion_designer_profile);
                    if($video) {
                          echo $this->Html->media($video, [
                            'fullBase' => true,
                            'text' => 'Fallback text',
                            'controls',
                            'width' => '100%'
                        ]);
                    } else {
                        echo('video not uploaded');
                    }

                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="keyword-4">Now that you are ready upload your video pitch.(60 sec max)</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    $this->Form->control('fashion_designer_profile.pitch_video', [
                        'type' => 'file',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        //'accept' => 'video/mp4'
                    ]) 
                ?>
            </div>
        </div>

       <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">Are you male or female?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('fashion_designer_profile.gender', [
                        'options' => [
                            ['value' => 1, 'text' => 'Male', 'class' => 'mr-2'],
                            ['value' => 2, 'text' => 'Female', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-number">What's your ethnicity?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">

                <?php 
                    $ethnicity = [
                        1 => 'White or Caucasian',
                        2 => 'Black or African American',
                        3 => 'American Indian or Alaska Native',
                        4 => 'Latino or Hispanic',
                        5 => 'Asian',
                        6 => 'Pacific Islander or Hawaiian',
                        7 => 'Other',
                    ]

                ?>

               <?= 
                    $this->Form->control('fashion_designer_profile.ethnicity', [
                        'options' => $ethnicity,
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => 'Select',
                        'required' => true
                    ])
                ?>

            </div>
        </div>

        <div class=" form-group col-12">
            <label>Where are you originally from? </label>
                <div class="row">
                   <div class="col-sm-4">
                       <label>Country</label>
                        <?php
                            $countries = $this->common->getAllCountry();
                            $defaultCountry = 0;
                            echo $this->Form->select('fashion_designer_profile.country_id', $countries, [
                                'class' => 'form-control',
                                'label' => false,
                                'data-select' => 'countries',
                                'empty' => __('Please select a country'),
                                'value' => isset($user->fashion_designer_profile)?$user->fashion_designer_profile->country_id:0,
                                'required' => true
                            ]);
                        ?>
                   </div>
                   <div class="col-sm-4">
                    <label>State</label>
                    <?php
                        $states = [];
                        $stateDefault = 0;

                        if (isset($user->fashion_designer_profile->country_id)):
                            $countryId = $user->fashion_designer_profile->country_id;
                            $states = $this->common->findByCountryId($countryId);
                            $stateDefault = $user->fashion_designer_profile->state_id;
                        endif;
                        echo $this->Form->select('fashion_designer_profile.state_id', $states, [
                            'class' => 'form-control',
                            'data-select' => 'states',
                             'required' => true,
                            'default' => $stateDefault,
                        ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label>City</label>
                    <?php
                        echo $this->Form->control('fashion_designer_profile.city', [
                            'type' => 'text',
                            'placeholder' => 'City',
                            'class' => 'form-control',
                            'label' => false,
                            'value' => isset($user->fashion_designer_profile)?$user->fashion_designer_profile->city:'',
                            'required' => true
                        ]);
                    ?>
                </div>
               </div>
            </div>


        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-numbers">Do you have a brand manager?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('fashion_designer_profile.is_brand_manager_agent', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'value' => 1

                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-numbers1">Have you ever been marketed overseas?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('fashion_designer_profile.is_performed_overseas', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'required' => true,
                        'value' => 1
                        
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="routing-numbersr">Do you have merchandise for sale?
            </label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?= 
                    // Create a radio set with our custom wrapping div.
                    $this->Form->control('fashion_designer_profile.is_merchandise_sale', [
                        'options' => [
                            ['value' => 1, 'text' => 'Yes', 'class' => 'mr-2'],
                            ['value' => 0, 'text' => 'No', 'class' => 'mr-2'],
                         ],
                        'type' => 'radio',
                        'label' => false,
                        'required' => true,
                        'value' => 1
                    ]);
                ?>
            </div>
        </div>

        
         <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="firstName">Supporters</label>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.0.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.0.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.1.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.1.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.2.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.2.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.3.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.3.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.4.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.4.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.5.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.5.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.6.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.6.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.7.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.7.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input text">
                                <?= 
                                    $this->Form->control('user_supporters.8.name', [
                                        'class' => 'form-control custom-input', 
                                        'label' => false,
                                        'placeholder' => "name"
                                    ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= 
                                $this->Form->control('user_supporters.8.phone_number', [
                                    'class' => 'form-control custom-input', 
                                    'label' => false,
                                    'placeholder' => "phone number"
                                ])
                            ?>
                        </div>
                    </div>
                </div>

        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>

<?php

    echo $this->Html->css(
        [
            'intlTelInput_new',
        ]
    );
?>
<?php 
    echo $this->Html->script([
        'common/jquery.validate', 
        'common/additional-methods', 
        'IntelJs/intlTelInput.min',
        'FashionDesigner/validate_admin_add_fashion_designer',
        'Artist/UserProfiles/country',
    ], [
        'block' => 'scriptBottom'
    ]); 
?>
