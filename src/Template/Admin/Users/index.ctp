<?php 
use Cake\I18n\Number;
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-primary shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pending for approval Artist</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($pendingForApproval)?$this->Html->link($pendingForApproval,['controller' => 'Users','action' => 'pendingForApproval']):0?>
				</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-success shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Approved Artist</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($approvedUser)?$this->Html->link($approvedUser,['controller' => 'Users','action' => 'approvedUser']):0?>
			</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-success shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Sound Check Live Original</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800"><?= isset($soundCheckLiveOriginal)?$soundCheckLiveOriginal:0?></div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-microphone fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-primary shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pending for approval Film Director</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($pendingFilmDirector)?$this->Html->link($pendingFilmDirector,['controller' => 'Users','action' => 'pendingFilmDirector']):0?>
					
				</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-success shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Approved Film Director</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($approvedFilmDirector)?$this->Html->link($approvedFilmDirector,['controller' => 'Users','action' => 'approvedFilm-Director']):0?>
			</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-primary shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pending for approval Fashion Director</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($pendingFashionDesigner)?$this->Html->link($pendingFashionDesigner,['controller' => 'Users','action' => 'pendingFashionDesigner']):0?>
					
				</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-success shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Approved Fashion Designer</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($approvedFashionDesigner)?$this->Html->link($approvedFashionDesigner,['controller' => 'Users','action' => 'approvedFashionDesigner']):0?>
			</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-primary shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pending Influencer</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($pendingInfluencer)?$this->Html->link($pendingInfluencer,['controller' => 'Influencers','action' => 'pendingInfluencer']):0?>
					
				</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-success shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Approved Influencer</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($approvedInfluencer)?$this->Html->link($approvedInfluencer,['controller' => 'Influencers','action' => 'approvedInfluencers']):0?>
			</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-success shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Proof of concept Earning</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">
				<?= isset($totalEarning->total_earning)?$this->Html->link($this->Number->currency($totalEarning->total_earning,'USD'),['controller' => 'proofOfConcepts','action' => 'index']):0?>
			</div>
		  </div>
		  <div class="col-auto">
			<i class="fa fa-user fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>

  

  <!-- Earnings (Monthly) Card Example -->
  <!-- <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-info shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
			<div class="row no-gutters align-items-center">
			  <div class="col-auto">
				<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
			  </div>
			  <div class="col">
				<div class="progress progress-sm mr-2">
				  <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-auto">
			<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div> -->

  <!-- Pending Requests Card Example -->
  <!-- <div class="col-xl-3 col-md-6 mb-4">
	<div class="card border-left-warning shadow h-100 py-2">
	  <div class="card-body">
		<div class="row no-gutters align-items-center">
		  <div class="col mr-2">
			<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests</div>
			<div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
		  </div>
		  <div class="col-auto">
			<i class="fas fa-comments fa-2x text-gray-300"></i>
		  </div>
		</div>
	  </div>
	</div>
  </div>-->
</div> 

<!-- Content Row -->