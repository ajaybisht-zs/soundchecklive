<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">How We Works</h1>
    </div>
        <?php
            echo $this->Form->create(null, [
                'url' => [
                    'controller' => 'Users',
                    'action' => 'howWeWorks',
                    'prefix' => 'admin'
                ],
                'id' => 'how-we-work',
                'class' => 'user',
                'enctype' => 'multipart/form-data'
            ]);
        ?>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('title', [
                        'type' => 'text',
                        'placeholder' => 'Title',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'value' => isset($findCurrentVideo->title)?$findCurrentVideo->title:'',
                    ]);
                ?>
            </div>
        </div>
        <input type="hidden" name="id" value="<?= isset($findCurrentVideo->id)?$findCurrentVideo->id:''?>">

        <!-- <div class="form-group">
            <label for="exampleFormControlFile1">Upload Cover Image</label>
            <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile1" name="cover_image">
        </div> -->

        <div class="form-group">
            <label for="exampleFormControlFile2"><?= __('Upload poster(jpg)'); ?></label>
            <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile2" name="poster_name" accept="image/*">
        </div>

        <div class="col-8">
            <?php
                $howWeworkVideoPoster = $this->Images->getHowWeWorkVideoPoster($findCurrentVideo);
                if($howWeworkVideoPoster) { ?>
                <h4><?= __('Poster Image');?></h4>     
                <?php    
                    echo $this->Html->image($howWeworkVideoPoster,['class' => 'img-fluid']);
                    } else {
                        echo "No Poster Image Uploaded";
                    }
                ?>
        </div>

        <div class="form-group">
            <label for="exampleFormControlFile2">Upload Video</label>
            <input type="file" class="form-control-file form-control-user" id="exampleFormControlFile2" name="video_name">
        </div>


        <div class="col-8">
            <?php
                $howWeworkVideo = $this->Images->getHowWeWorkVideo($findCurrentVideo);
                if($howWeworkVideo) { ?>
                <h4><?= __($findCurrentVideo->title);?></h4>     
                <?php    
                    echo $this->Html->media(
                            [ $howWeworkVideo,
                            ],
                            [
                                'width' => "60%",
                                'controls'
                            ]
                        );
                    }
                ?>
        </div>

        <div class="form-group row">          
            <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script('backend/Admin/validate_how_we_work', [
        'block' => 'scriptBottom'
    ]);
?>
