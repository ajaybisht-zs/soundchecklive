<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Edit Levels</h1>
    </div>
    <?php
        echo $this->Form->create($level, [
            'url' => [
                'controller' => 'Levels',
                'action' => 'edit',
                'prefix' => 'admin'
            ],
            'id' => 'sound-live-colors',
            'class' => 'user',
            'type' => 'file'
        ]);
    ?>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="hexCode">Level name</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('level_name', [
                        'type' => 'text',
                        'placeholder' => 'Level name',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'hexCode',
                        'required' => true
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-capitalize" for="hexCodeName">Price</label>
            <div class="col-sm-6 mb-3 mb-sm-0">
                <?php
                    echo $this->Form->control('price', [
                        'type' => 'text',
                        'placeholder' => 'Enter Price',
                        'class' => 'form-control form-control-user',
                        'label' => false,
                        'id' => 'hexCodeName'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 offset-2 mb-3 mb-sm-0">          
                <button class="btn btn-primary btn-user px-5 m-auto" type="submit">Save</button>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>
<?php
    echo $this->Html->script([
            'backend/Admin/color_validate',
            ], [
        'block' => 'scriptBottom'
    ]);
?>
