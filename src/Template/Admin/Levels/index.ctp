<?php 
    use Cake\I18n\Number;
    use Cake\Routing\Router;
?>
<div class="card-header mb-3 row justify-content-between align-items-center">
    <div class="col px-0">
        <h2 class="m-0 font-weight-bold text-info"><?=__('Levels')?></h2>
    </div>
    <div class="col-auto">  
        <?php 
            echo $this->Html->link(
                'Add 
                Levels',
                [
                    'controller' =>'Levels',
                    'action' => 'add'
                ],
                ['class' => 'btn btn-secondary btn-lg']
            );
        ?>
    </div>
</div>
<div class="mobile-res-tb">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('level_name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('price') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if ($levels->isEmpty()) :
        ?> 
        <tr>
            <td colspan="12" class="text-center no-data-found">
                <h3><?= __('No records found!') ?></h3>
            </td>
        </tr> 
      
        <?php else : foreach ($levels as $level): ?>
        <tr>
                <td><?= $this->Number->format($level->id) ?></td>
                <td><?= h($level->level_name) ?></td>
                <td><?= Number::currency($level->price,'USD') ?></td>
                <td><?= h($level->created) ?></td>
                <td><?= h($level->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $level->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $level->id], ['confirm' => __('Are you sure you want to delete # {0}?', $level->id)]) ?>
                </td>
            </tr>
    <?php 
        endforeach;
        endif;
    ?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>