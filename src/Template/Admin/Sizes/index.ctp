<?php 
    use Cake\I18n\Number;
    use Cake\Routing\Router;
?>
<div class="card-header mb-3 row justify-content-between align-items-center">
    <div class="col px-0">
        <h2 class="m-0 font-weight-bold text-info"><?=__('Sizes')?></h2>
    </div>
    <div class="col-auto">  
        <?php 
            echo $this->Html->link(
                'Add 
            Sizes',
                [
                    'controller' =>'Sizes',
                    'action' => 'add'
                ],
                ['class' => 'btn btn-secondary btn-lg']
            );
        ?>
    </div>
</div>
<div class="mobile-res-tb">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
          <th>Id</th>
          <th>Size</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Action</th>
        </tr>
    </thead>
    <!-- <tfoot>
        <tr>
         <th>Id</th>
          <th>Size</th>
          <th>Created</th>
          <th>Modified</th>
          <th>Action</th>
        </tr>
    </tfoot> -->
    <tbody>
        <?php
            if ($sizes->isEmpty()) :
        ?> 
        <tr>
            <td colspan="12" class="text-center no-data-found">
                <h3><?= __('No records found!') ?></h3>
            </td>
        </tr> 
      
        <?php else : foreach($sizes as $val) : ?>
        <tr>
            <td><?= $val->id?></td>
            <td><?php echo isset($val->size)?ucfirst($val->size):'N/A'?></td>
            <td><?= $val->created?></td>
            <td><?= $val->modified?></td>
            <td>
            <?= 
                    $this->Html->link(__('<span class="fa fa-edit icon-setting"></span>&nbsp; Edit'), 
                                            [
                                                'controller' => 'Sizes',
                                                'action' => 'edit',
                                                $val->id,
                                                'prefix' => 'admin'
                                            ], [
                                            'escape' => false
                                ])?>
             <?= $this->Form->postLink(
                    __('<span class="fa fa-trash icon-setting"></span> &nbsp; Delete'), [
                            'action' => 'delete', 
                            $val->id,
                        ], [
                            'confirm' => __('Are you sure you want to delete # {0}?', $val->id),
                            'escape' => false,
                            'class' => 'ml-2'
                            
                        ]) 
            ?>
                                    
            </td>
        </tr>
    <?php 
        endforeach;
        endif;
    ?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>