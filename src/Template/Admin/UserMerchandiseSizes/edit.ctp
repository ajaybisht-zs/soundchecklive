<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMerchandiseSize $userMerchandiseSize
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userMerchandiseSize->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandiseSize->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Merchandise Sizes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['controller' => 'UserMerchandises', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['controller' => 'UserMerchandises', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userMerchandiseSizes form large-9 medium-8 columns content">
    <?= $this->Form->create($userMerchandiseSize) ?>
    <fieldset>
        <legend><?= __('Edit User Merchandise Size') ?></legend>
        <?php
            echo $this->Form->control('user_merchandise_id');
            echo $this->Form->control('size_id');
            echo $this->Form->control('user_merchandises._ids', ['options' => $userMerchandises]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
