<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMerchandiseSize $userMerchandiseSize
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Merchandise Size'), ['action' => 'edit', $userMerchandiseSize->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Merchandise Size'), ['action' => 'delete', $userMerchandiseSize->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandiseSize->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Merchandise Sizes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Merchandise Size'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['controller' => 'UserMerchandises', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['controller' => 'UserMerchandises', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userMerchandiseSizes view large-9 medium-8 columns content">
    <h3><?= h($userMerchandiseSize->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Size Id') ?></th>
            <td><?= h($userMerchandiseSize->size_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userMerchandiseSize->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Merchandise Id') ?></th>
            <td><?= $this->Number->format($userMerchandiseSize->user_merchandise_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userMerchandiseSize->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userMerchandiseSize->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related User Merchandises') ?></h4>
        <?php if (!empty($userMerchandiseSize->user_merchandises)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Image Dir') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userMerchandiseSize->user_merchandises as $userMerchandises): ?>
            <tr>
                <td><?= h($userMerchandises->id) ?></td>
                <td><?= h($userMerchandises->user_id) ?></td>
                <td><?= h($userMerchandises->name) ?></td>
                <td><?= h($userMerchandises->price) ?></td>
                <td><?= h($userMerchandises->image) ?></td>
                <td><?= h($userMerchandises->image_dir) ?></td>
                <td><?= h($userMerchandises->created) ?></td>
                <td><?= h($userMerchandises->modified) ?></td>
                <td><?= h($userMerchandises->description) ?></td>
                <td><?= h($userMerchandises->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserMerchandises', 'action' => 'view', $userMerchandises->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserMerchandises', 'action' => 'edit', $userMerchandises->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserMerchandises', 'action' => 'delete', $userMerchandises->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandises->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
