<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMerchandiseSize[]|\Cake\Collection\CollectionInterface $userMerchandiseSizes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Merchandise Size'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Merchandises'), ['controller' => 'UserMerchandises', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Merchandise'), ['controller' => 'UserMerchandises', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userMerchandiseSizes index large-9 medium-8 columns content">
    <h3><?= __('User Merchandise Sizes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_merchandise_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('size_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userMerchandiseSizes as $userMerchandiseSize): ?>
            <tr>
                <td><?= $this->Number->format($userMerchandiseSize->id) ?></td>
                <td><?= $this->Number->format($userMerchandiseSize->user_merchandise_id) ?></td>
                <td><?= h($userMerchandiseSize->size_id) ?></td>
                <td><?= h($userMerchandiseSize->created) ?></td>
                <td><?= h($userMerchandiseSize->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userMerchandiseSize->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userMerchandiseSize->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userMerchandiseSize->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMerchandiseSize->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
