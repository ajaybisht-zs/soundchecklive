<?php
    use Cake\Routing\Router;
    use Cake\Mailer\MailerAwareTrait;
?>
<div class="card-header py-3">
    <h2 class="m-0 font-weight-bold text-info text-center">Pending for approval(Partners)</h2>
</div>
<div class="table-responsive mobile-res-tb">
<table class="table table-bordered header-text-left" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
          <th>Id</th>
          <th>First Name</th>
          <th>LastName</th>
          <th>Email</th>
          <th>Phone No</th>
          <th>Age</th>
          <th>Instagrm Name(Follower)</th>
          <th>Twitter Name(Follower)</th>
          <th>Status</th>
          <th>Created</th>
          <th>Approved</th>
          <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php
        if ($users->isEmpty() ) :
        ?> 
            <tr>
                <td colspan="12" class="text-center no-data-found">
                    <h3><?= __('No records found!') ?></h3>
                </td>
            </tr> 
        <?php else: foreach($users as $val) : //pr($val);?>
            <tr>
              <td><?= $val->id?></td>
              <td><?= isset($val->partner_profile->first_name)?$val->partner_profile->first_name:'N/A'?></td>
              <td><?= isset($val->partner_profile->last_name)?$val->partner_profile->last_name:'N/A'?></td>
              <td><?= isset($val->email)?$val->email:'N/A'?></td>
              <td><?= isset($val->partner_profile->phone_number)?$val->partner_profile->phone_number:'N/A'?></td>
              <td>
              <?php 
                    if(!empty($val->partner_profile->birth_year)) {  
                        $then = DateTime::createFromFormat("Y/m/d", $val->partner_profile->birth_year.'/'.$val->partner_profile->birth_month.'/'.$val->partner_profile->birth_day);
                        $diff = $then->diff(new DateTime());
                        echo $diff->format("%y year %m month %d day\n");
                    } else {
                        echo 0;
                    }
                ?>
            </td>
            <td><?= isset($val->partner_profile->ig_user_name)?$val->partner_profile->ig_user_name.'('.$val->partner_profile->ig_follower.')':'N/A'?></td>
            <td><?= isset($val->partner_profile->t_user_name)?$val->partner_profile->t_user_name.'('.$val->partner_profile->t_follower.')':'N/A'?></td>
              <td>
                <?php 
                    if($val->status == 1):
                ?>
                    <span class="badge badge-success">Active</span>
                <?php else :?>
                    <span class="badge badge-danger">Inactive</span>
                <?php endif;?>
              </td>
              <td><?= $val->created?></td>
              <td>
                <input type="checkbox"  class = "approve_user" data-toggle="toggle" data-onstyle="success" data-url="<?php 
                                                            echo router::url(
                                                            [      
                                                                'controller' => 'PartnerProfile',                                                
                                                                "action" => "approveUser",
                                                                $val->id,
                                                                'prefix' => 'admin',
                                                            ])?>"<?php echo ($val->status == 1) ? "checked" : ""?>>
              </td>
              <td>
            <?= 
              $this->Html->link(__('<span class="fa fa-eye icon-setting"></span>&nbsp; view'), 
              [
                'controller' => 'PartnerProfile',
                'action' => 'view',
                $val->id,
                'prefix' => 'admin'
              ], [
              'escape' => false
            ])?>
            <?= 
              $this->Html->link(__('<span class="fa fa-edit"></span>&nbsp; Edit'), 
              [
                'controller' => 'PartnerProfile',
                'action' => 'edit',
                $val->id,
                'prefix' => 'admin'
              ], [
              'escape' => false
            ])?>
            <?= $this->Form->postLink(
              __('<span class="fa fa-trash icon-setting"></span> &nbsp; Delete'), [
                'action' => 'delete', 
                $val->id,
              ], [
                'confirm' => __('Are you sure you want to delete # {0}?', $val->id),
                'escape' => false,
                'class' => 'ml-2'
                
              ]) 
            ?> 
        </td>
            </tr>
        <?php endforeach;endif;?>
    </tbody>
</table>
</div>
<?= $this->element('pagination');?>
<?php
    echo $this->Html->script(
        [
            'backend/Admin/user_influencer',
            ], 
            [
        'block' => 'scriptBottom'
  ]); 
?>