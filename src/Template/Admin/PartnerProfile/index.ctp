<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PartnerProfile[]|\Cake\Collection\CollectionInterface $partnerProfile
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Partner Profile'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Equity Levels'), ['controller' => 'EquityLevels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Equity Level'), ['controller' => 'EquityLevels', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="partnerProfile index large-9 medium-8 columns content">
    <h3><?= __('Partner Profile') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('equity_level_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('zipcode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birth_year') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birth_month') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birth_day') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ig_user_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ig_follower') ?></th>
                <th scope="col"><?= $this->Paginator->sort('t_user_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('t_follower') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($partnerProfile as $partnerProfile): ?>
            <tr>
                <td><?= $this->Number->format($partnerProfile->id) ?></td>
                <td><?= $partnerProfile->has('user') ? $this->Html->link($partnerProfile->user->id, ['controller' => 'Users', 'action' => 'view', $partnerProfile->user->id]) : '' ?></td>
                <td><?= $partnerProfile->has('equity_level') ? $this->Html->link($partnerProfile->equity_level->id, ['controller' => 'EquityLevels', 'action' => 'view', $partnerProfile->equity_level->id]) : '' ?></td>
                <td><?= h($partnerProfile->first_name) ?></td>
                <td><?= h($partnerProfile->last_name) ?></td>
                <td><?= h($partnerProfile->phone_number) ?></td>
                <td><?= h($partnerProfile->zipcode) ?></td>
                <td><?= $this->Number->format($partnerProfile->birth_year) ?></td>
                <td><?= $this->Number->format($partnerProfile->birth_month) ?></td>
                <td><?= $this->Number->format($partnerProfile->birth_day) ?></td>
                <td><?= h($partnerProfile->ig_user_name) ?></td>
                <td><?= $this->Number->format($partnerProfile->ig_follower) ?></td>
                <td><?= h($partnerProfile->t_user_name) ?></td>
                <td><?= $this->Number->format($partnerProfile->t_follower) ?></td>
                <td><?= h($partnerProfile->created) ?></td>
                <td><?= h($partnerProfile->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $partnerProfile->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $partnerProfile->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $partnerProfile->id], ['confirm' => __('Are you sure you want to delete # {0}?', $partnerProfile->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
