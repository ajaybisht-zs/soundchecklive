<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PartnerProfile $partnerProfile
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Partner Profile'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Equity Levels'), ['controller' => 'EquityLevels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Equity Level'), ['controller' => 'EquityLevels', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="partnerProfile form large-9 medium-8 columns content">
    <?= $this->Form->create($partnerProfile) ?>
    <fieldset>
        <legend><?= __('Add Partner Profile') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('equity_level_id', ['options' => $equityLevels, 'empty' => true]);
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('phone_number');
            echo $this->Form->control('zipcode');
            echo $this->Form->control('birth_year');
            echo $this->Form->control('birth_month');
            echo $this->Form->control('birth_day');
            echo $this->Form->control('ig_user_name');
            echo $this->Form->control('ig_follower');
            echo $this->Form->control('t_user_name');
            echo $this->Form->control('t_follower');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
