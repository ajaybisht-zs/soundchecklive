<?php
namespace App\Test\TestCase\Controller\Influencer;

use App\Controller\Influencer\UsersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Influencer\UsersController Test Case
 *
 * @uses \App\Controller\Influencer\UsersController
 */
class UsersControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Users',
        'app.Roles',
        'app.UserProfiles',
        'app.UserMediaHandles',
        'app.UserBankDetails',
        'app.UserPlans',
        'app.UserRecords',
        'app.UserEvents',
        'app.UserMerchandises',
        'app.FashionDesignerProfiles',
        'app.FashionDesignerMediaHandles',
        'app.FilmDirectorProfiles',
        'app.FilmDirectorMediaHandles',
        'app.Levels',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
