<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilmgenresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilmgenresTable Test Case
 */
class FilmgenresTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FilmgenresTable
     */
    public $Filmgenres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Filmgenres',
        'app.FilmDirectorProfiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Filmgenres') ? [] : ['className' => FilmgenresTable::class];
        $this->Filmgenres = TableRegistry::getTableLocator()->get('Filmgenres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Filmgenres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
