<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfluencerProfileTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfluencerProfileTable Test Case
 */
class InfluencerProfileTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InfluencerProfileTable
     */
    public $InfluencerProfile;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.InfluencerProfile',
        'app.Users',
        'app.Levels',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InfluencerProfile') ? [] : ['className' => InfluencerProfileTable::class];
        $this->InfluencerProfile = TableRegistry::getTableLocator()->get('InfluencerProfile', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InfluencerProfile);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
