<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HowWeWorksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HowWeWorksTable Test Case
 */
class HowWeWorksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HowWeWorksTable
     */
    public $HowWeWorks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HowWeWorks',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HowWeWorks') ? [] : ['className' => HowWeWorksTable::class];
        $this->HowWeWorks = TableRegistry::getTableLocator()->get('HowWeWorks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HowWeWorks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
