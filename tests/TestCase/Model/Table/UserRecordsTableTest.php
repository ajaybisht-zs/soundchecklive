<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserRecordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserRecordsTable Test Case
 */
class UserRecordsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserRecordsTable
     */
    public $UserRecords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserRecords',
        'app.Users',
        'app.UserRecordKeys'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserRecords') ? [] : ['className' => UserRecordsTable::class];
        $this->UserRecords = TableRegistry::getTableLocator()->get('UserRecords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserRecords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
