<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProofOfConceptsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProofOfConceptsTable Test Case
 */
class ProofOfConceptsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProofOfConceptsTable
     */
    public $ProofOfConcepts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProofOfConcepts',
        'app.People',
        'app.Charges',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProofOfConcepts') ? [] : ['className' => ProofOfConceptsTable::class];
        $this->ProofOfConcepts = TableRegistry::getTableLocator()->get('ProofOfConcepts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProofOfConcepts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
