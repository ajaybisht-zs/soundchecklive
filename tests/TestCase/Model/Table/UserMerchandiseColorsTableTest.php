<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserMerchandiseColorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserMerchandiseColorsTable Test Case
 */
class UserMerchandiseColorsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserMerchandiseColorsTable
     */
    public $UserMerchandiseColors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserMerchandiseColors',
        'app.UserMerchandises',
        'app.Colors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserMerchandiseColors') ? [] : ['className' => UserMerchandiseColorsTable::class];
        $this->UserMerchandiseColors = TableRegistry::getTableLocator()->get('UserMerchandiseColors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserMerchandiseColors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
