<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PartnerProfileTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PartnerProfileTable Test Case
 */
class PartnerProfileTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PartnerProfileTable
     */
    public $PartnerProfile;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PartnerProfile',
        'app.Users',
        'app.EquityLevels',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PartnerProfile') ? [] : ['className' => PartnerProfileTable::class];
        $this->PartnerProfile = TableRegistry::getTableLocator()->get('PartnerProfile', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PartnerProfile);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
