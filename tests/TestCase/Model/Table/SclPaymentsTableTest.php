<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SclPaymentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SclPaymentsTable Test Case
 */
class SclPaymentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SclPaymentsTable
     */
    public $SclPayments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SclPayments',
        'app.UserRecords',
        'app.Transitions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SclPayments') ? [] : ['className' => SclPaymentsTable::class];
        $this->SclPayments = TableRegistry::getTableLocator()->get('SclPayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SclPayments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
