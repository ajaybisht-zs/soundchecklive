<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserMerchandiseImagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserMerchandiseImagesTable Test Case
 */
class UserMerchandiseImagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserMerchandiseImagesTable
     */
    public $UserMerchandiseImages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserMerchandiseImages',
        'app.UserMerchandises'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserMerchandiseImages') ? [] : ['className' => UserMerchandiseImagesTable::class];
        $this->UserMerchandiseImages = TableRegistry::getTableLocator()->get('UserMerchandiseImages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserMerchandiseImages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
