<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserMerchandisesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserMerchandisesTable Test Case
 */
class UserMerchandisesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserMerchandisesTable
     */
    public $UserMerchandises;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserMerchandises',
        'app.Users',
        'app.UserMerchandiseSizes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserMerchandises') ? [] : ['className' => UserMerchandisesTable::class];
        $this->UserMerchandises = TableRegistry::getTableLocator()->get('UserMerchandises', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserMerchandises);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
