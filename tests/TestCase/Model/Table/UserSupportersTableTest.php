<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserSupportersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserSupportersTable Test Case
 */
class UserSupportersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserSupportersTable
     */
    public $UserSupporters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserSupporters',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserSupporters') ? [] : ['className' => UserSupportersTable::class];
        $this->UserSupporters = TableRegistry::getTableLocator()->get('UserSupporters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserSupporters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
