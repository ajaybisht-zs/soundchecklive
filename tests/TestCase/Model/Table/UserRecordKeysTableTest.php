<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserRecordKeysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserRecordKeysTable Test Case
 */
class UserRecordKeysTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserRecordKeysTable
     */
    public $UserRecordKeys;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserRecordKeys',
        'app.UserRecords'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserRecordKeys') ? [] : ['className' => UserRecordKeysTable::class];
        $this->UserRecordKeys = TableRegistry::getTableLocator()->get('UserRecordKeys', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserRecordKeys);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
