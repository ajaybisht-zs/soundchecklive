<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilmDirectorProfilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilmDirectorProfilesTable Test Case
 */
class FilmDirectorProfilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FilmDirectorProfilesTable
     */
    public $FilmDirectorProfiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FilmDirectorProfiles',
        'app.Users',
        'app.Filmgenres',
        'app.Countries',
        'app.States'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FilmDirectorProfiles') ? [] : ['className' => FilmDirectorProfilesTable::class];
        $this->FilmDirectorProfiles = TableRegistry::getTableLocator()->get('FilmDirectorProfiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FilmDirectorProfiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
