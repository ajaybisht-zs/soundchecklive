<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserMerchandiseSizesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserMerchandiseSizesTable Test Case
 */
class UserMerchandiseSizesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserMerchandiseSizesTable
     */
    public $UserMerchandiseSizes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserMerchandiseSizes',
        'app.UserMerchandises',
        'app.Sizes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserMerchandiseSizes') ? [] : ['className' => UserMerchandiseSizesTable::class];
        $this->UserMerchandiseSizes = TableRegistry::getTableLocator()->get('UserMerchandiseSizes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserMerchandiseSizes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
