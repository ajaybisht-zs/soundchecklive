<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PitchVideosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PitchVideosTable Test Case
 */
class PitchVideosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PitchVideosTable
     */
    public $PitchVideos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PitchVideos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PitchVideos') ? [] : ['className' => PitchVideosTable::class];
        $this->PitchVideos = TableRegistry::getTableLocator()->get('PitchVideos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PitchVideos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
