(function(){
	$('#editBankDetailForm').validate({
        rules: {
            "user_bank_detail[bank_name]": {
                required: true
            },
            "user_bank_detail[account_number]": {
                required: true
            },
            "user_bank_detail[routing_number]": {
                required: true
            },
            "user_bank_detail[name_on_account]": {
                required: true
            },
        },
    });
})();