(function(){
	
	$('.add-shows').on('click', function () {
        let lenght = $('.upcoming-shows').length;
        if (lenght >= 3) {
            return false;
        }
        // Test to see if the browser supports the HTML template element by checking
        // for the presence of the template element's content attribute.
        if ('content' in document.createElement('template')) {

            // Instantiate the table with the existing HTML tbody
            // and the row with the template
            var template = document.querySelector('#upcomingShows');

            // Clone the new row and insert it into the table
            var tbody = document.querySelector("#upcomingShowsContainer");
            var clone = document.importNode(template.content, true);
            // tbody.appendChild(clone);
            tbody.insertBefore(clone, this);
        } else {
            // Find another way to add the rows to the table because 
            // the HTML template element is not supported.
        }
    });

    $(document).on('click', '.remove-show-btn', function () {
        $(this).closest('.upcoming-shows').remove();
    });

    $('#addUserEventForm').validate();
    
    $( "#event-date" ).datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('#event-time').timepicker({
        timeFormat: 'h:mm p',
        interval: 10,
        minTime: '10',
        maxTime: '6:00pm',
        defaultTime: '11',
        startTime: '10:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
})();