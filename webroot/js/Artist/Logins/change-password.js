(function(){
	$('#ChangePasswordForm').validate({
		rules: {
            "current_password": {
                required:true,
                minlength:8
            },
            "password": {
                required:true,
                minlength:8
            },
            "confirm_password": {
                required:true,
                minlength:8,
                equalTo: '#password'
            }
        },
	})
})()