$(function () {
	/**
	 * Handle Country change
	 */
	$('[data-select="countries"]').on('change', function () {
		let countryId = $(this).val(),
			statesSelector = $(this).closest('.row').find('[data-select="states"]');
		getStates(countryId, statesSelector);
	});
})

/**
 * Get state list based on countries
 */
var getStates = function (countryId, selector, defaultValue= false) {
	let url = window.url+`api/Users/getStates/${countryId}.json`;
	$.ajax({
		url: url,
		method:'get',	
		/*beforeSend: function (xhr) {
			xhr.setRequestHeader('X-CSRF-Token', window.csrf);
		}*/
	}).done(function(data) {	 	
		let toAppend = '';

		selector.html('');

		$.each(data, function(key, value) {
			toAppend += '<option value='+ key +'>'+value+'</option>';
		});
		selector.append(toAppend);

		if (defaultValue) {
			selector.val(defaultValue);
		}
		return true;

	}).fail(function(jqXHR, textStatus, error){
	   //console.log(jqXHR.responseJSON);
	});
}