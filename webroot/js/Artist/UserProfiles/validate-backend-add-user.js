$(document).ready(function() {
    validateSignUp("#backend-user");
    $(document).on('keydown', '#password, #confirm-password, #email', function(e) {
        if (e.keyCode == 32) return false;
    });
});

jQuery.validator.addMethod("noSpace", function(value, element) {
    console.log(value);
    return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");


jQuery.validator.addMethod("emailValid", function(value, element) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return $.trim(value).match(pattern) ? true : false;
}, "Please enter a valid email address");

// validate signup form on keyup and submit
var validateSignUp = function(element) {
    $(element).validate({
        rules: {
           'user_profile[first_name]': {
                required: true,
                noSpace: true,
                minlength: 3,
            },
            'user_profile[last_name]': {
                required: true,
                noSpace: true,
                minlength: 3,
            },
            'user_profile[artist_name]': {
                required: true,
                //noSpace: true,
                minlength: 3,
            },   
            'email': {
                required: true,
                email: true,
                noSpace: true,
                emailValid: true,
                remote:{
                    url: window.url+'api/Users/checkEmailSignup.json',
                },
            },
            'password': {
                required: true,
                minlength:6,
            },
            'confirm_password': {
                required: true,
                minlength:6,
                equalTo:"#password",
            },
            'user_profile[zipcode]': {
                required: true,
                noSpace: true,
                maxlength:10
            },
            'user_profile[phone_number]': {
                number: true,
                noSpace: true,
                maxlength:12
            },
            'user_profile[genre_id]': {
                required: true,
            },
            "user_profile[is_signed]": {
                required: true,
            },
            "user_profile[signed_by]": {
                required: true,
            },
            "user_profile[biography]": {
                required: true,
            },
            "user_profile[is_performance_rights_affiliated]": {
                required: true,
            },
            "user_profile[avatar]": {
                required: true,
                extension: "jpg|jpeg|png"
            },
            "user_profile[budget_usage]": {
                required: true,
            },
            "user_record[record_name]": {
                required: true,
            },
            "user_record[artist_name]": {
                required: true,
            },
            "user_record[features]": {
                required: true,
            },
            "user_record[writer]": {
                required: true,
            },
            "user_record[producer]": {
                required: true,
            },
            "user_record[cover_image]": {
                required: true,
                extension: "jpg|jpeg|png"
            },
            "user_record[record_file]": {
                required: true,
                extension: "mp3"
            },
            "user_record[video_pitch_file]": {
                required: true,
                extension: "mp4"
            },
            "user_profile[genre_id]": {
                required: true,
            },
        },
        messages: {
            'email': {
                required: "Please enter a valid email address",
                remote: "The email address you have entered is already registered"
            },
            'user_profile[first_name]': {
                required: "Please enter your first name",
                minlength: "Your firstname must be at least 3 characters long"
            },
            'user_profile[last_name]': {
                required: "Please enter your last name",
                minlength: "Your lastname must be at least 3 characters long"
            },
            'password': {
                required: "Please enter a password",
                minlength: "Your password must be at least 6 characters long"
            },
            'confirm_password': {
                equalTo: "Please enter the same password."
            },
            'user_profile[zipcode]': {
                required: "Please enter the zipcode."
            },
            'user_profile[phone_number]': {
                required: "Please enter a phone number",
            },
            'user_profile[avatar]': {
                required: "Upload jpg png image only",
            },
            'user_record[video_pitch_file]': {
                required: "Upload mp4 video only",
            },
            'user_record[record_file]': {
                required: "Upload mp3 audio only",
            }
        }
    });
};

