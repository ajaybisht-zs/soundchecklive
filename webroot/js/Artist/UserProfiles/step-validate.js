(function () {
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= 5000000)
    }, 'File size must be less than 5 Mb');

    $.validator.addMethod('filessizemp3', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 10 Mb');

    $.validator.addMethod('filessizemp4', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 100 Mb');

    jQuery.validator.addMethod("noSpace", function(value, element) {
    console.log(value);
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    //step 1
    $('#birthDayForm').validate({
        rules: {
            "birth[day]": {
                required: true,
                validateDate: true
            },
            "birth[month]": {
                required: true,
            },
            "birth[year]": {
                required: true,
            }
        },
    });
    //step 2
    $('#genreForm').validate({
        rules: {
            "user_profile[genre_id]": {
                required: true,
            }
        },
    });
    //step 3
    $('#signedForm').validate({
        rules: {
            "user_profile[signed_by]": {
                required: true,
            }
        },
    });
    //step 4
    $('#biographyForm').validate({
        rules: {
            "user_profile[biography]": {
                required: true,
                minlength: 200,
                maxlength: 500,
            }
        },
        messages: {
            'user_profile[biography]': {
                minlength: "Please enter minimum 200 chararter",
                maxlength: "Please enter less than 500 chararter",
            }
        },
    });
    // step 5
    $('#affilatedForm').validate({
        rules: {
            "user_profile[performance_rights_organization]": {
                required: true,
            }
        },
    });

    // step 6
    $('#avatarForm').validate({
        rules: {
            "user_profile[avatar]": {
                required: true,
                accept: "image/*",
                filesize: 5000000,
            }
        },
        messages: {
            'user_profile[avatar]': {
                required: "Please upload profile picture",
                accept:"Please upload image only"            }
        }
    });
    // step 9
    $('#budgetHelpsForm').validate({
        rules: {
            "user_profile[budget_usage]": {
                required: true,
                minlength: 200,
                maxlength: 500,
            },
            messages: {
            'user_profile[budget_usage]': {
                minlength: "Please enter minimum 200 chararter",
                maxlength: "Please enter less than 500 chararter",
            }
        },
        },
    });
     // step 10
    $('#recordInfoForm').validate({
        rules: {
            'record_name': {
                required: true,
               // noSpace: true,
                minlength: 3,
            },
            'features': {
                required: true,
                //noSpace: true,
                minlength: 3,
            },
            'producer': {
                required: true,
                //noSpace: true,
                minlength: 3,
            },
            'writer': {
                required: true,
               // noSpace: true,
                minlength: 3,
            },
            "cover_image": {
                required: true,
                accept: "image/*",
                filesize: 5000000,
            },
            "record_file": {
                required: true,
                extension: "mp3|mp4|wav",
                //accept: "image/*",
                filessizemp3: 10000000,
            }
        },
        messages: {
            'cover_image': {
                required: "Please upload profile picture",
                accept:"Please upload image only"
            },
            'record_file': {
                required: "Please upload mp3,wav and mp4 only",
                extension:"Please upload mp3,wav and mp4 only"
                //accept:"Please upload image only"
            }
        }
    });

     // step 12
    $('#videoPitchForm').validate({
        rules: {
            "video_pitch_file": {
                required: true,
                extension: "mp4|mov",
                //accept: "image/*",
                filessizemp4: 100000000,
            }
        },
        messages: {
            'video_pitch_file': {
                required: "Please upload mp4,mov",
                extension:"Please upload mp4,mov only"
                //accept:"Please upload image only"
            }
        }
    });
    // step 13
    $('#recordInfoForm').validate({
        rules: {
            "record_name": {
                required: true,
            },
            "artist_name": {
                required: true,
            },
            "features": {
                required: true,
            },
            "producer": {
                required: true,
            },
            "writer": {
                required: true,
            }
        },
    });
    // step 16
    $('#bankDetailForm').validate({
        rules: {
            "user_bank_detail[bank_name]": {
                required: true,
            },
            "user_bank_detail[name_on_account]": {
                required: true,
            },
            "user_bank_detail[account_number]": {
                required: true,
            },
            "user_bank_detail[routing_number]": {
                required: true,
            },
        },
    });

})();

$(document).on('click','#artistFormSubmit',function() {
    var form = $( "#recordInfoForm" );
    if(form.valid()) {
        $('#overlay').removeClass('d-none');
        $('#artistFormSubmit').prop("disabled", true);
        $("#artistFormSubmit").text('Uploading. Please wait.....');
        $('#recordInfoForm').submit();
    } else {
        $('#artistFormSubmit').prop("disabled", false);
        $('#overlay').addClass('d-none');
    } 
});


$(document).on('click','#videoPitchFromSubmit',function() {
    var form = $( "#videoPitchForm" );
    if(form.valid()) {
        $('.video-pitch-overlay').removeClass('d-none');
        $('#videoPitchFromSubmit').prop("disabled", true);
        $("#videoPitchFromSubmit").text('Uploading. Please wait.....');
        $('#videoPitchForm').submit();
    } else {
        $('#videoPitchFromSubmit').prop("disabled", false);
        $('.video-pitch-overlay').addClass('d-none');
    } 
});
