(function(){
	$('#editBiographyForm').validate({
        rules: {
            "user_profile[biography]": {
                required:true,
                maxlength: 300
            },
            "user_media_handle[facebook]": {
                url: true
            },
            "user_media_handle[twitter]": {
                url: true
            },
            "user_media_handle[instagram]": {
                url: true
            },
        },
    });
})();