 $(function () {
     $(document).on('change', "input[name='user_profile[is_performance_rights_affiliated]']", function () {
        var inputCheckBoxValue = $(this).val();
        if (inputCheckBoxValue == 2) {
            $('#agency-name').removeClass('d-none');
        } else {
            $('#agency-name').addClass('d-none');
            $('#agency-name').val('');
        }
    });

    $(document).on('change', "input[name='user_profile[is_signed]']", function () {
        checkSigned()
    });
 });

 function checkSigned() {
    if ($("input[name='user_profile[is_signed]']:checked").val() == 0) {
        $(document).find('.signed-input').removeClass('d-none');
    } else {
        $(document).find('.signed-input').addClass('d-none');
    }

}