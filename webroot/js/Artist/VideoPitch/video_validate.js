(function(){
	$('#editVideoPitchForm').validate({
        rules: {
            "video_pitch_file": {
                required: true,
                 extension: "mp4"
            }
        },
    });
})();