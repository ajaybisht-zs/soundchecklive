(function(){
	$('#editMediaHandleForm').validate({
        rules: {
            "user_media_handle[facebook]": {
                url: true
            },
            "user_media_handle[twitter]": {
                url: true
            },
            "user_media_handle[instagram]": {
                url: true
            },
        },
    });
})();