(function(){

	$('.add-more').on('click', function () {
        let lenght = $('.merchandise-sale').length;
        if (lenght >= 3) {
            return false;
        }
        // Test to see if the browser supports the HTML template element by checking
        // for the presence of the template element's content attribute.
        if ('content' in document.createElement('template')) {

            // Instantiate the table with the existing HTML tbody
            // and the row with the template
            var template = document.querySelector('#merchandise');

            // Clone the new row and insert it into the table
            var tbody = document.querySelector("#merchandiseContainer");
            var clone = document.importNode(template.content, true);
            // tbody.appendChild(clone);
            tbody.insertBefore(clone, this);
        } else {
            // Find another way to add the rows to the table because 
            // the HTML template element is not supported.
        }
    });

    $(document).on('click', '.remove-btn', function () {
        $(this).closest('.merchandise-sale').remove();
    });
    
})();