(function () {
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 5 Mb');

    $.validator.addMethod('filessizemp3', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 10 Mb');

    $.validator.addMethod('filessizemp4', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 50 Mb');
    //step 1
    $('#birthDayForm').validate({
        rules: {
            "birth[day]": {
                required: true,
                validateDate: true
            },
            "birth[month]": {
                required: true,
            },
            "birth[year]": {
                required: true,
            }
        },
    });
    //step 2
    $('#genreForm1').validate({
        rules: {
            "fashion_designer_profile[style_id]": {
                required: true,
            },
            /*"film_director_profile[affiliated_with_name]": {
                required: true,
            }*/
        },
    });
    //step 3
    $('#signedForm').validate({
        rules: {
            "user_profile[signed_by]": {
                required: true,
            }
        },
    });

    $('#distributorForm1').validate({
        rules: {
            "film_director_profile[distributor_name]": {
                required: true,
            },
            /*"film_director_profile[affiliated_with_name]": {
                required: true,
            }*/
        },
    });
    //step 4
    $('#bioForm').validate({
        rules: {
            "film_director_profile[biography]": {
                required: true,
                minlength: 200,
                maxlength: 500,

            }
        },
         messages: {
            'film_director_profile[biography]': {
                minlength: "Please enter minimum 200 chararter",
                maxlength: "Please enter less than 500 chararter",
            }
        },
    });
    // step 5
    $('#affilatedForm').validate({
        rules: {
            "user_profile[performance_rights_organization]": {
                required: true,
            }
        },
    });

    // step 6
    $('#avatarForm').validate({
        rules: {
            "film_director_profile[avatar]": {
                required: true,
                accept: "image/*",
                filesize: 5000000,
            }
        },
        messages: {
            'film_director_profile[avatar]': {
                required: "Please upload profile picture",
                accept:"Please upload image only"
            }
        }
    });
    // step 9
    $('#budgetHelpsForm').validate({
        rules: {
            "film_director_profile[buget_used_for_roi]": {
                required: true,
                minlength: 200,
                maxlength: 500,
            }
        },
         messages: {
            'film_director_profile[buget_used_for_roi]': {
                minlength: "Please enter minimum 200 chararter",
                maxlength: "Please enter less than 500 chararter",
            }
        },
    });

     // step 10
    $('#uploadProductForm').validate({
        rules: {
            "film_director_profile[poster_pic]": {
                required: true,
                accept: "image/*",
                filesize: 5000000,
            },
            "film_director_profile[movie_reel]": {
                required: true,
                extension: "mp4|mov",
                //accept: "image/*",
                filessizemp4: 50000000,
            }
        },
        messages: {
            'film_director_profile[poster_pic]': {
            required: "Please upload your movie cover art poster",
            accept:"Please upload image only"
            },
            'film_director_profile[movie_reel]': {
                required: "Please upload mp4,mov",
                extension:"Please upload mp4,mov only"
                //accept:"Please upload image only"
            }
        }
    });

     // step 12
    $('#pitchVideoForm').validate({
        rules: {
            "film_director_profile[pitch_video]": {
                required: true,
                extension: "mp4|mov",
                //accept: "image/*",
                filessizemp4: 50000000,
            }
        },
        messages: {
            'film_director_profile[pitch_video]': {
                required: "Please upload mp4,mov",
                extension:"Please upload mp4,mov only"
                //accept:"Please upload image only"
            }
        }
    });
    // step 13
    $('#recordInfoForm').validate({
        rules: {
            "record_name": {
                required: true,
            },
            "artist_name": {
                required: true,
            },
            "features": {
                required: true,
            },
            "producer": {
                required: true,
            },
            "writer": {
                required: true,
            }
        },
    });
    // step 16
    $('#bankDetailForm').validate({
        rules: {
            "user_bank_detail[bank_name]": {
                required: true,
            },
            "user_bank_detail[name_on_account]": {
                required: true,
            },
            "user_bank_detail[account_number]": {
                required: true,
            },
            "user_bank_detail[routing_number]": {
                required: true,
            }
        },
    });

})();
