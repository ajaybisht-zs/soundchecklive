(function () {
    jQuery.validator.addMethod("emailValid", function(value, element) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return $.trim(value).match(pattern) ? true : false;
    }, "Please enter a valid email address");


    jQuery.validator.addMethod("noSpace", function(value, element) {
    console.log(value);
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    //step 1
    $('#step1').validate({
        rules: {
            "people_id": {
                required: true,
            },
        },
    });
    //step 2
    $('#step2').validate({
        rules: {
            "email": {
                required: true,
                email: true,
                noSpace: true,
                emailValid: true,
            },
            'first_name': {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 100,
            },
            'last_name': {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 100,
            },
            'address': {
                required: true,
                maxlength: 1000,
            },
            'zip_code': {
                required: true,
                maxlength: 15,
            },
            'city': {
                required: true,
                maxlength: 100,
            },
            'state_id': {
                required: true,
            },
            'phone_number': {
                required: true,
                maxlength: 18,
            },
        },
    });
})();
