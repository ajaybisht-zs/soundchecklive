(function(){
	$('#resetPasswordForm').validate({
		rules: {
            "password": {
                required:true,
                minlength:8
            },
            "confirm_password": {
                required:true,
                minlength:8,
                equalTo: '#password'
            }
        },
	})
})()