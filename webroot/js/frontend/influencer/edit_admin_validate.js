$(document).ready(function() {
	validateInfluencer("#admin-influencer");
	$(document).on('keydown', '#password, #confirm-password, #email', function(e) {
	    if (e.keyCode == 32) return false;
	});

	/*$('input[type="checkbox"]').click(function(){
		if($("#ageabove18").prop('checked') == true){
	   		$("#ageabove18").val(1);
	   		$("#ageabove18").removeClass('required');
		} else {
			$("#ageabove18").val(0);
			$("#ageabove18").addClass('required');
		}
	});*/
});

jQuery.validator.addMethod("noSpace", function(value, element) {
	console.log(value);
	return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");


jQuery.validator.addMethod("emailValid", function(value, element) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return $.trim(value).match(pattern) ? true : false;
}, "Please enter a valid email address");

// validate signup form on keyup and submit
var validateInfluencer = function(element) {
	$(element).validate({
		rules: {
		   'influencer_profile[ig_user_name]': {
				required: true,
				noSpace: true,
				minlength: 3,
			},
			'influencer_profile[ig_follower]': {
				required: true,
				number: true,
			},
			'influencer_profile[t_user_name]': {
				required: true,
				noSpace: true,
				minlength: 3,
			}, 
			'influencer_profile[t_follower]': {
				required: true,
				number: true,
			},
			'influencer_profile[first_name]': {
				required: true,
				noSpace: true,
				minlength: 3,
			},
			'influencer_profile[last_name]': {
				required: true,
				noSpace: true,
				minlength: 3,
			}, 
		},
		messages: {
			'influencer_profile[ig_user_name]': {
				required: "Please instagram user name",
				minlength: "Your instagram must be at least 3 characters long"
			},
			'influencer_profile[ig_follower]': {
				required: "Please enter your followers",
				number: "Enter only numeric"
			},
			'influencer_profile[t_user_name]': {
				required: "Please Twitter user name",
				minlength: "Your instagram must be at least 3 characters long"
			},
			'influencer_profile[t_follower]': {
				required: "Please enter your followers",
				number: "Enter only numeric"
			},
			
		}
	});
};

