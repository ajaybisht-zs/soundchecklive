$(document).ready(function() {
	validateUserMerchandise("#user-merchandise");
	$(document).on('keydown', '#password, #confirm-password, #email', function(e) {
	    if (e.keyCode == 32) return false;
	});
});

jQuery.validator.addMethod("noSpace", function(value, element) {
	console.log(value);
	return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");

// validate signup form on keyup and submit
var validateUserMerchandise = function(element) {
	$(element).validate({
		rules: {
		   'name': {
				required: true,
				noSpace: true,
				minlength: 3,
			},
			'price': {
				required: true,
				noSpace: true,
				number: true
			},   
			'description': {
				required: true,
				minlength: 3,
			},
		   	'user_merchandise_images[]': {
				required: true,
			},
			'sizes[_ids][]': {
				required: true,
			},
			'colors[_ids][]': {
				required: true,
		  	},
		},
		messages: {
			'name': {
				extension: "Please enter item name",
			},
			'price': {
				extension: "Please enter price",
			},
			'description': {
				required: "Please enter description",
			},
			'user_merchandise_images': {
				required: "Please upload image",
			},
			'sizes[_ids][]': {
				required: "Please select size",
			},
			'colors[_ids][]': {
				required: "Please select color",
			}
		}
	});
};

