$(document).ready(function() {
	validateAddVideo("#artist-music");
});


// validate signup form on keyup and submit
var validateAddVideo = function(element) {
	$(element).validate({
		rules: {
		   'video_name': {
				required: true,
				extension: "mp4"
			},
		},
		messages: {
			'video_name': {
				extension: "Please upload only mp4 video",
			},
		}
	});
};

