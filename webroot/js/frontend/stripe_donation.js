$(function () {
    var style = {
    base: {
     // Add your base input styles here. For example:
     fontSize: '16px',
     color: "#969696",
     
    }
    };
    var stripe = Stripe(window.stipePublickey);
    var elements = stripe.elements();
    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
           displayError.textContent = event.error.message;
           $("#submit-btn-txt").attr("disabled", false);
        } else {
           displayError.textContent = '';
           $("#submit-btn-txt").attr("disabled", false);
        }
    });

    var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
         event.preventDefault();
            $("#submit-btn-txt").attr("disabled", true);

        stripe.createToken(card).then(function(result) {
           if (result.error) {
             // Inform the customer that there was an error.
             var errorElement = document.getElementById('card-errors');
             errorElement.textContent = result.error.message;
            } else {
             // Send the token to your server.
             stripeTokenHandler(result.token);
            }
        });
    });

    function stripeTokenHandler(token) {
     // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        let formData = new FormData(form);
         // Submit the form
        chargePayment(formData);
        //form.submit();
    }

    function chargePayment(data) {
        var btnText = $('#submit-btn-txt').text();
         $.ajax({
                url: window.url +'PoweredByElevens/payment/',
                type:'post',
                processData: false,
                contentType: false,
                dataType: 'json',
                data :data,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', window.csrf);
                    $('#submit-btn-txt').text('Your Payment is inprogess Please Wait....');
                },   
                success: function(data) {
                $("#submit-btn-txt").attr("disabled", false);
                  if(data.status == true){
                    $('#submit-btn-txt').text(btnText);
                    $('#submit-btn-txt').hide();
                    $("#payment-form")[0].reset();
                    $('#payementsucces').html('<div class="alert alert-success"></div>');
                    $("#payementsucces>div").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>Your payment has been successfully processed.</strong>');
                    setTimeout(function(){
                            window.location.href = window.url +'PoweredByElevens/share/?name='+data.name;
  
                    },1000);
                } else {
                $("#submit-btn-txt").attr("disabled", false);
                $('#submit-btn-txt').text(btnText);
                $('#submit-btn-txt').hide();
                $("#payment-form")[0].reset();
                $('#carderror').html('<div class="alert alert-danger"></div>');
                $("#carderror>div").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>Failed!</strong>'+data.message);
                /*setTimeout(function(){
                            window.location.href = window.url +'catalogues/index';
  
                    },3000);*/
                 // window.location.href = window.url +'poweredby11';
                }
            }
        });
    }
});