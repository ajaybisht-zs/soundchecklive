$(function () {
    $('.numeric').on('keyup', function (event) {
        var quantity = Math.round($(this).val());
        ajaxcart($(this).attr("data-recordId"), quantity);
    });

    function ajaxcart(id, quantity) {
        $.ajax({
            type: "POST",
            url: window.url + 'carts/itemUpdate/',
            data: {
                id: id,
                quantity: quantity
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', window.csrf);
            },
            success: function (data) {
                var totalPrice = 0;
                $.each(data, function (key, value) {
                    if ($('#subtotal-' + key).html() != "$" + value.subtotal) {
                        $('#quantity-' + key).val(value.quantity);
                        $('#subtotal-' + key).html("$" + value.subtotal).animate({
                            backgroundColor: "#ff8"
                        }, 100).animate({
                            backgroundColor: 'transparent'
                        }, 500);
                    }

                    totalPrice += (value.price * value.quantity);

                    $('#total-Price').html("$" + totalPrice).animate({
                        backgroundColor: "#ff8"
                    }, 100).animate({
                        backgroundColor: 'transparent'
                    }, 500);
                });
            },
            error: function () {
                window.location.replace("/carts/clearCart");
            }
        });
    }
});
