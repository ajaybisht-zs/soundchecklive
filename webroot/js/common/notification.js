$(function () {
	var user_id = window.userId;
    Pusher.logToConsole = true;
    var pusher = new Pusher('9d0c4f7191c7c641a4ee', {
      cluster: 'mt1',
      encrypted: true
    });
    var channel = pusher.subscribe('soundechecklive');
    channel.bind('my-event', function(data) {
      if (data.user_id == user_id) {
        //alert(JSON.stringify(data));
        $('#incoming-call').modal('show');
        $('audio #source').attr('src', window.url+'ring.mp3');
        $('audio').get(0).load();
        $('audio').get(0).play();
        //updateNotification(user_id);
        //return false;
      }
    });

    $('#disconnect-call').on('click', function (event) {
      $('audio').get(0).pause();
    });

  /*$('#navbarDropdown-1').on('click', function (event) {
      updateNotificationOnDropDown();
  });*/
});

function updateNotification()
{
  var countElement = $('[id ^="count-"]');
  $.ajax({
    url: window.url + 'api/Notifications/getMessage.json',
    success: function (output) {
      var count = output.response.count;
      var messages = output.response.data;
      alert("You have" +count +"New Notifications" );
       countElement.html(count);
      $.each(messages,function(key, value){
        element = '<a  title="'+value.notification_message+'" class="dropdown-item">'+value.notification_message+'</a>';
      });
      $('#notification-After-Approve').after(element);
      if (count) {
        $('#bellshake').effect("shake");
      }
    }
  });
}

function updateNotificationOnDropDown() {
  $.ajax({
    url: window.url + 'api/Notifications/updateMessage.json',
    success: function (output) {
      var countElement = $('[id ^="count-"]');
      if (output.response) {
        count = 0;
        countElement.html(count);
        //$('#notificationHeader').html('');
      }
    }
  })
}