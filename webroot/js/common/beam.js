if(window.userId) {
    const currentUserId = 'user-'+window.userId;
    const beamsClient = new PusherPushNotifications.Client({
        instanceId: 'e7c7ffca-f2cc-4718-8da2-eba3e64d98ee',
    })
    const tokenProvider = new PusherPushNotifications.TokenProvider({
      url: window.url+'homes/getToken', 
      queryParams: { user_id: currentUserId }, // URL query params your auth endpoint needs
      headers: { 'Content-type': 'application/json' }, // Headers your auth endpoint needs
    });

    console.log('testing tokne',tokenProvider);
    beamsClient
    .start()
    .then(beamsClient => beamsClient.getDeviceId())
    .then(deviceId => console.log('Successfully registered with Beams. Device ID:', deviceId))
    .then(() => beamsClient.setUserId(currentUserId, tokenProvider))
    .catch(console.error);

}