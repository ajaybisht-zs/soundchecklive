$(document).ready(function() {
	$(document).on('click','.add-smart-link', function(e){
		e.preventDefault();
		$("#openSmartLinkModal").modal();
		var userID = $(this).attr("data-id");
		$('#user_id').val(userID);
	});
	validateAddSmartLink("#AddSmartLinkForm");

});
	var validateAddSmartLink = function(element) {
		$(element).validate({
			rules: {
			   'smart_link': {
					required: true,
					url: true,
				}
			},
		});
	};

$(document).on('click','.close-modal', function(e){
	e.preventDefault();
	/*$('#AddSmartLinkForm')[0].reset();
    $( "#AddSmartLinkForm" ).validate().resetForm();*/
});