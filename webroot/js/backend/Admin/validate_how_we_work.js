$(document).ready(function() {
	validateHowWeWork("#how-we-work");
});

jQuery.validator.addMethod("noSpace", function(value, element) {
	console.log(value);
	return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");

// validate signup form on keyup and submit
var validateHowWeWork = function(element) {
	$(element).validate({
		rules: {
		   'title': {
				required: true,
				//noSpace: true,
				minlength: 3,
			},
		  	'video_name': {
				//required: true,
				extension: "mp4"
		  	},
		},
		messages: {
			'title': {
				extension: "Please enter title",
			},
			'video_name': {
				extension: "Please upload mp4 only",
				required : "Please upload mp4 video"
			},
		}
	});
};

