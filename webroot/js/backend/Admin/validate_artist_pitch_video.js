$(document).ready(function() {
	validateArtistPitchVideo("#artist-pitch-video");
});


var validateArtistPitchVideo = function(element) {
	$(element).validate({
		rules: {
		   'title': {
				required: true,
				minlength: 3,
			},
		  	'video_name': {
				extension: "mp4"
		  	},
		},
		messages: {
			'title': {
				required: "Please enter title",
			},
			'video_name': {
				extension: "Please upload mp4 only",
				required : "Please upload mp4 video"
			},
		}
	});
};

