$(function () { 
    $(".approve_merchandise").on("change",function(){ 
        if ($(this).is(':checked')) {
            var url = $(this).attr('data-url')+"/1";
        }
        else {
            var url = $(this).attr('data-url')+"/0";
        }
    $.ajax({
        type:"GET",
        url : url,
        dataType:'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', window.csrf);
            $('#loader').show();
        },
        success: function(data){
            console.log(data);                              
        }, 
            fail: function(xhr, textStatus, errorThrown){
            console.log(xhr);     
        }     
        }); 
    });
});