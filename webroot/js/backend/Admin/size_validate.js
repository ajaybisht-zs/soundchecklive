$(document).ready(function() {
	validateSizes("#sound-live-sizes");
	$(document).on('keydown', '#password, #confirm-password, #email', function(e) {
	    if (e.keyCode == 32) return false;
	});
});

jQuery.validator.addMethod("noSpace", function(value, element) {
	console.log(value);
	return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");


// validate signup form on keyup and submit
var validateSizes = function(element) {
	$(element).validate({
		rules: {
		   'size': {
				required: true,
				noSpace: true,
			},
		},
	});
};

