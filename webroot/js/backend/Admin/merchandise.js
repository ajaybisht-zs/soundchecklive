$(document).ready(function() {
	validateSoundCheckOriginalAdd("#sound-live-merchandise");
	$(document).on('keydown', '#password, #confirm-password, #email', function(e) {
	    if (e.keyCode == 32) return false;
	});
});

jQuery.validator.addMethod("noSpace", function(value, element) {
	console.log(value);
	return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");


jQuery.validator.addMethod("emailValid", function(value, element) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return $.trim(value).match(pattern) ? true : false;
}, "Please enter a valid email address");

// validate signup form on keyup and submit
var validateSoundCheckOriginalAdd = function(element) {
	$(element).validate({
		rules: {
		   'name': {
				required: true,
				noSpace: true,
				minlength: 3,
			},
			'price': {
				required: true,
				noSpace: true,
			},   
			'description': {
				required: true,
				minlength: 3,
			},
		   	'user_merchandise_images[]': {
				required: true,
			},
			'sizes[_ids][]': {
				required: true,
			},
			'colors[_ids][]': {
				required: true,
		  	},
		},
		messages: {
			'cover_image': {
				extension: "Please upload only jpg,png,gif",
			},
			'record_file': {
				extension: "Please upload mp3 only",
			},
			'record_name': {
				required: "Please enter record name",
			},
			'artist_name': {
				required: "Please enter artist name",
			},
			'features': {
				required: "Please enter features",
			},
			'producer': {
				required: "Please enter producers name."
			},
			'writer': {
				required: "Please enter writers name",
			}
		}
	});
};

