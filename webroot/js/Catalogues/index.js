(function() {
    var player;
    var setting = {
        0: {
            items: 2,
            nav: true
        },
        600: {
            items: 3,
            nav: true,
            margin: 20
        },
        1000: {
            items: 5,
            nav: true,
            loop: false,
            margin: 20
        },
        1400: {
            items: 7,
            nav: true,
            loop: false,
            margin: 20
        }
    };

    var artistPitch = {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 2,
            nav: true,
            margin: 20
        },
        768: {
            items: 2,
            nav: true,
            margin: 20
        },
        1000: {
            items: 3,
            nav: true,
            loop: false,
            margin: 20
        },
        1400: {
            items: 3,
            nav: true,
            loop: false,
            margin: 20
        }
    };

    var aboutUS = {
        0: {
            items: 1,
            nav: true,
            margin: 20,
            loop: true,

        },
        600: {
            items: 2,
            nav: true,
            margin: 20
        },
        768: {
            items: 2,
            nav: true,
            margin: 20
        },
        1000: {
            items: 4,
            nav: true,
            loop: true,
            margin: 20
        },
        1400: {
            items: 5,
            nav: true,
            loop: true,
            margin: 20
        }
    };

    $('#owl-3').owlCarousel({
        margin: 10,
        responsiveClass: true,
        responsive: setting,
        dots: false
    })
    $('#owl-4').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: setting,
        dots: false
    })
    $('#owl-5').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: setting,
        dots: false
    })
    $('#owl-12').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: aboutUS,
        dots: false
    })
    $('#owl-6').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: setting,
        dots: false
    })
    $('#owl-7').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: setting,
        dots: false
    })

    $('#owl-9').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    })


    $('#music-video').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    })

    
    $('#sclive').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 3.5,
                nav: true,
                margin:15,
            },
            600: {
                items: 2,
                nav: true,
                // margin: 20
            },
            768: {
                items: 7,
                nav: true,
                margin: 20,
            },
        },
        dots: false,
    });

    $('#owl-mobile-slide-show').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: true,
                // margin: 20
            },
            768: {
                items: 2,
                nav: true,
                // margin: 20
            },
        },
        dots: false,
    });

    $('#owl-filmDirectorRecentlyAddVideo').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-filmDirectorTrendingNow').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-filmDirectorBestSellers').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-filmDirectorPitchVideo').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });


    $('#owl-film-merchandise').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });


    $('#owl-fasionDesignerGarment').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        //responsive: artistPitch,
        dots: false,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: true,
                // margin: 20
            },
            768: {
                items: 5,
                nav: true,
                // margin: 20
            },
        }
    });

    $('#music-video-FilmDirector').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#music-video-FilmDirector').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });


    $('#owl-filmDirector-music').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
        responsive: setting,
    });

    
    $('#owl-filmDirector-Fashion').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#owl-filmDirectorSclOriginals').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#owl-filmDirectorHightestSclScore').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-fasionDesignerTrendingNow').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-fasionDesignerBestSeller').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-fasionDesignerSclOriginal').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#owl-fasionDesignerheigstScl').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-fashionDesignerPitchVideo').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#owl-fashionMusic').owlCarousel({
        margin: 10,
        responsiveClass: true,
        responsive: setting,
        dots: false
    });

    $('#owl-fashionDesignerFilm').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-fashion-merchandise').owlCarousel({
        // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#music-video-FashionDesigner').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#owl-musicFilms').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('#owl-musicfashion').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        dots: false,
    });

    $('#owl-hightscl').owlCarousel({
    // loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: artistPitch,
        responsive: setting,
        dots: false,
    });

    $('.audio').on('click', function() {
        $('#single-song-player').removeClass('d-none');
        let songObj = {}
        let data = $(this).attr('data-url');

        songObj = JSON.parse(atob(data));
        addSong(songObj);

        // $('#play-song').modal('show');
    });

    $('.close-player').on('click', function(event) {
        pauseSong();
        $('#single-song-player').addClass('d-none');
    })

})();
