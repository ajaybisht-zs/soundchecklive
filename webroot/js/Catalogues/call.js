var session;
var connectionCount = 0;
var mute = false;
var videoPublisher;
var screenPublisher;
var videoPublisherElement = 'videoPublisher';
var screenPublisherElement = 'screenPublisher';
var videoSubscriber;
var screenSubscriber;
var videoSubscriberElement = 'videoSubscriber';
var screenSubscriberElement = 'screenSubscriber';
// Handling all of our errors here by alerting them
function handleError(error) {
    if (error) {
        switch (error.name) {
            case "OT_NOT_CONNECTED":
                alert("Publishing your video failed. You are not connected to the internet.");
                break;
            case "OT_NO_DEVICES_FOUND":
                alert("No voice or video input devices are available on this machine.");
                break;
            case "OT_CREATE_PEER_CONNECTION_FAILED":
                alert("Publishing your video failed. This could be due to a restrictive firewall.");
                break;
            case "OT_USER_MEDIA_ACCESS_DENIED":
                alert('Please allow access to the Camera, Microphone and Screen sharing and try publishing again.');
                break;
            case "OT_SCREEN_SHARING_EXTENSION_NOT_REGISTERED":
                alert('Screen Sharing support in this browser requires an extension, but one has not been registered.');
                break;
            default:
                alert(error.message);
        }
    }
}
function handleVideoPublishError(error) {
    if (error) {
        handleError(error)
    } else {
        publishVideo();
    }
}
// Handling all of our errors here by alerting them
function handleScreenPublishError(error) {
    if (error) {
        handleError(error);
    } else {
        publishScreen();
    }
}
function connect() {
    session = OT.initSession(apiKey, sessionId);
    session.on({
        connectionCreated: function (event) {
            connectionCount++;
            console.log(connectionCount + ' connections.');
            if(connectionCount == 2) {
                $('#rm-ringing').addClass('d-none');
                timeCountDown();
            }
            //$('#rm-ringing').addClass('d-none');
            if (event.connection.connectionId == session.connection.connectionId) {
                if(connectionCount == 2) {
                    $('#rm-ringing').addClass('d-none');
                    //timeCountDown();

                   // alert('call connected');
                }
                if(connectionCount > 2) {
                    alert('To many connections');
                    session.destroy();
                }else {
                    document.querySelector('.action-button').style.display = 'block';
                    initVideo();
                }
            }
        },
        connectionDestroyed: function (event) {
            connectionCount--;
            console.log(connectionCount + ' connections.');
        },
        sessionDisconnected: function sessionDisconnectHandler(event) {
            // The event is defined by the SessionDisconnectEvent class
            console.log('Disconnected from the session.');
            if (event.reason == "networkDisconnected") {
                alert("Your network connection terminated.")
            }
            // document.getElementById('disconnectBtn').style.display = 'none';
        },
        streamCreated: function(event) {
            console.log('stream created -->',event.stream)            
            if(event.stream.videoType === 'screen') {
                subscribeScreen(event);
            } else {
                subscribeVideo(event);
            }            
        },
        streamDestroyed: function(event) {
            console.log('stream destroyed', event.stream)
            if (event.reason === 'networkDisconnected') {
                alert('Your publisher lost its connection. Please check your internet connection and try publishing again.');
            }
            if(event.stream.videoType === 'screen') {
                document.getElementById(videoSubscriberElement).style.display = "block";
                document.getElementById(screenSubscriberElement).style.display = "none";                
                document.getElementById('ss').style.display = "inline-block";
                console.log(videoPublisher)
                if(videoPublisher) {
                    videoPublisher.publishVideo(true);
                }
            } else {                
                document.getElementById(videoSubscriberElement).style.display = "none";
                document.getElementById(screenSubscriberElement).style.display = "block";
            }         
        },
        streamPropertyChanged: function (event) {
            // console.log('streamPropertyChanged:', event.stream);
            // stream changed and user is subscribed to that event then
        }
    });
    // append token with your own value:
    session.connect(token, function(error) {
        if (error) {
            handleError(error);
        } else {
           console.log('Connected to the session.');
                // initVideo();
        }    
    });
}
connect();
function publishVideo() {
    session.publish(videoPublisher, function(err){
        if(err) {
            handleError(err)
        }else {
        }
    });
}
function publishScreen() {
    session.publish(screenPublisher, function(err){
        if (err) {
            handleError(err)
        }else {            
            document.getElementById('ss').style.display='none';
            //stopping video publishing if screen publish 
            if(videoPublisher) {
                videoPublisher.publishVideo(false);
            }
        }
    });
}
function initVideo() {    
    videoPublisher = OT.initPublisher(videoPublisherElement, {
        insertMode: 'append',
        width:150,
        height:150
    }, handleVideoPublishError);
    videoPublisher.on('mediaStopped', function(event) {
        // The user clicked stop.
    });
    videoPublisher.on('streamDestroyed', function(event) {
        event.preventDefault();
        if (event.reason === 'mediaStopped') {
            // User clicked stop sharing
            // publisher.destroy();
            // // publishScreen();
        } else if (event.reason === 'forceUnpublished') {
            // A moderator forced the user to stop sharing.
            // publisher.destroy();
            // publishScreen();
        }
    });
}
function initScreen() { 
    screenPublisher = OT.initPublisher(screenPublisherElement, {
        videoSource:'screen',
        insertMode: 'append',
        // publishAudio:true,
    }, handleScreenPublishError);
    screenPublisher.on('mediaStopped', function(event) {
    });
    screenPublisher.on('streamDestroyed', function(event) {
        if (event.reason === 'mediaStopped') {
            document.getElementById('ss').style.display='inline-block';
            // User clicked stop sharing
        } else if (event.reason === 'forceUnpublished') {
            // A moderator forced the user to stop sharing.
            alert('moderator forced the user to stop sharing');
        }
        if(videoPublisher) {
            videoPublisher.publishVideo(true);
        }
    });
}
function subscribeScreen(event) {
    // session.subscribe(event.stream, 'subscriber', subOptions);
    screenSubscriber = session.subscribe(event.stream, screenSubscriberElement, {
        insertMode: 'append',
        width: '100%',
        height: '100%'
    }, handleError);
    document.getElementById(videoSubscriberElement).style.display = "none";
    document.getElementById(screenSubscriberElement).style.display = "block";
    // removing screen share icon on subscribing screen because only one can share screen
    document.getElementById('ss').style.display = "none";
    if(videoPublisher) {
        videoPublisher.publishVideo(false);
    }
}
function subscribeVideo(event){
    // session.subscribe(event.stream, 'people', subOptions);
    videoSubscriber = session.subscribe(event.stream, videoSubscriberElement, {
        insertMode: 'append',
        width: '100%',
        height: '100%'
    }, handleError);
    document.getElementById(videoSubscriberElement).style.display = "block";
    document.getElementById(screenSubscriberElement).style.display = "none";
}
function disconnect(location='/catalog') {
    session.disconnect();
    window.location = location;
}
// publisher.destroy();
// session.unpublish(publisher);
/*document.getElementById('ss').addEventListener('click', function(){
    initScreen();
});*/
document.getElementById('stop').addEventListener('click', function(){
    if(confirm('Are you sure want to leave the session')){
        disconnect(this.getAttribute('data-url'));
        // window.location=
    }
});
document.getElementById('mute').addEventListener('click', function(){
    if(videoPublisher){
        videoPublisher.publishAudio(mute);
        if(mute) {            
            let microphone = document.getElementById('mute');
            microphone.classList.remove('microphone');
            microphone.setAttribute('title', 'mute audio')
        }else {
            let microphone = document.getElementById('mute');
            microphone.classList.add('microphone');
            microphone.setAttribute('title', 'unmute audio');
        }
    }
    mute = !mute;
});

function timeCountDown() {

    var timer2 = "3:01";
    //var timer2 = "0:15";
    var interval = setInterval(function() {


      var timer = timer2.split(':');
      //by parsing integer, I avoid all extra string processing
      var minutes = parseInt(timer[0], 10);
      var seconds = parseInt(timer[1], 10);
      --seconds;
      minutes = (seconds < 0) ? --minutes : minutes;
      if (minutes < 0) clearInterval(interval);
      seconds = (seconds < 0) ? 59 : seconds;
      seconds = (seconds < 10) ? '0' + seconds : seconds;
      //minutes = (minutes < 10) ?  minutes : minutes;
      $('.countdown').html(minutes + ':' + seconds);
      timer2 = minutes + ':' + seconds;
      console.log(timer2);
      if(timer2 == '0:00') {
        //alert('your call has been ended. Thanks for calling.');
        disconnect();
         //alert('ji');
      }
    }, 1000);
}