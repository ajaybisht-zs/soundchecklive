Amplitude.init({
    "bindings": {
        37: 'prev',
        39: 'next',
        32: 'play_pause'
    },
    "songs": [
      
    ]
});

(function(){    
    window.onkeydown = function(e) {
        return !(e.keyCode == 32);
    };

      /*
        Handles a click on the song played progress bar.
      */
    document.getElementById('song-played-progress').addEventListener('click', function( e ){
        var offset = this.getBoundingClientRect();
        var x = e.pageX - offset.left;
        Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
    });
})();

function addSong(song_object) {
    // Amplitude.addSong( song_object );
    Amplitude.playNow( song_object );
}

function removeSong(indexOfSong = 0) {
    Amplitude.removeSong( indexOfSong )
}

function pauseSong() {
    Amplitude.pause()
}

function playSong() {
    Amplitude.play()
}