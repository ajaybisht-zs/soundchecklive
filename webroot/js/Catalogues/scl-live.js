$(function () {
    var style = {
    base: {
     // Add your base input styles here. For example:
     fontSize: '16px',
     color: "#969696",
     
    }
    };
    var stripe = Stripe(window.stipePublickey);
    var elements = stripe.elements();
    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-livechat');

    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors-chat');
        if (event.error) {
           displayError.textContent = event.error.message;
        } else {
           displayError.textContent = '';
        }
    });

    var form = document.getElementById('scl-chat-form');
        form.addEventListener('submit', function(event) {
         event.preventDefault();
            $("#submit-btn-chatpayment").attr("disabled", true);

        stripe.createToken(card).then(function(result) {
           if (result.error) {
             // Inform the customer that there was an error.
             var errorElement = document.getElementById('card-errors-chat');
             errorElement.textContent = result.error.message;
            } else {
             // Send the token to your server.
             stripeTokenHandler(result.token);
            }
        });
    });

    function stripeTokenHandler(token) {
     // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('scl-chat-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        let formData = new FormData(form);
         // Submit the form
        sclLiveChatePayment(formData);
        //form.submit();
    }

    function sclLiveChatePayment(data) {
        var btnText = $('#submit-btn-chatpayment').text();
         $.ajax({
                url: window.url +'catalogues/chatPayment/',
                type:'post',
                processData: false,
                contentType: false,
                dataType: 'json',
                data :data,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', window.csrf);
                    $('#submit-btn-chatpayment').text('Your Payment is inprogess Please Wait....');
                },   
                success: function(data) {
                $("#submit-btn-chatpayment").attr("disabled", false);
                  if(data.status == true){
                    //$('#rm-ringing').removeClass('d-none');
                    $('#submit-btn-chatpayment').text(btnText);
                    $('#submit-btn-chatpayment').hide();
                    $("#scl-chat-form")[0].reset();
                    $('#payementsucceschat').html('<div class="alert alert-success"></div>');
                    $("#payementsucceschat>div").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>'+data.message+'</strong>');
                    setTimeout(function(){
                            window.location.href = window.url +'catalogues/videoChat/'+data.user_id;
  
                    },3000);
                } else {
                $("#submit-btn-chatpayment").attr("disabled", false);
                $('#submit-btn-chatpayment').text(btnText);
                $('#submit-btn-chatpayment').show();
                $("#scl-chat-form")[0].reset();
                $('#carderrorchat').html('<div class="alert alert-danger"></div>');
                $("#carderrorchat>div").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>Failed!</strong>'+data.message);
                /*setTimeout(function(){
                            window.location.href = window.url +'catalogues/index';
  
                    },3000);*/
                }
            }
        });
    }
});