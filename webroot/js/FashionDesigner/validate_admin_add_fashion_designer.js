var iti;

var inputPhoneNo = document.querySelector("#phone"),
	errorMsg = document.querySelector("#error-msg"),
	validMsg = document.querySelector("#valid-msg");

var errorMap = [
	"Invalid number",
	"Invalid country code",
	"Too short",
	"Too long",
	"Invalid number"
];
$(document).ready(function() {
	validateSignUp("#add-designer");
	$(document).on('keydown', '#password, #confirm-password, #email', function(e) {
	    if (e.keyCode == 32) return false;
	});

	iti = window.intlTelInput(inputPhoneNo, {		
		separateDialCode: true,
		initialCountry: "us",
		hiddenInput: "full_phone",
		utilsScript: "/js/IntelJs/utils.js"
	});

	$('input:radio[name="fashion_designer_profile[is_licensed_brand_or_independent_line]"]').change(function () {
        if ($(this).val() == '0') {

            $('#parent_company').removeClass('d-none')
        }
        if ($(this).val() == '1') {
           $('#parent_company').addClass('d-none');
            $('#parent_company').val('');
        }
    });

    $('input:radio[name="fashion_designer_profile[have_distributor]"]').change(function () {
        if ($(this).val() == '1') {
            
            $('#distributor-name').removeClass('d-none')
        }
        if ($(this).val() == '0') {
           $('#distributor-name').addClass('d-none');
            $('#distributor-name').val('');
        }
    });
});

jQuery.validator.addMethod("intlTelNumber", function(value, element) {
		if (element.value.trim()) {
			if (iti.isValidNumber()) {
				return true;
			} else {
				return false;
			}
		}
}, 'Please enter a valid International Phone Number');	

jQuery.validator.addMethod("noSpace", function(value, element) {
	console.log(value);
	return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");


jQuery.validator.addMethod("emailValid", function(value, element) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return $.trim(value).match(pattern) ? true : false;
}, "Please enter a valid email address");

// validate signup form on keyup and submit
var validateSignUp = function(element) {
	$(element).validate({
		rules: {
		   'fashion_designer_profile[first_name]': {
				required: true,
				noSpace: true,
				minlength: 3,
			},
			'fashion_designer_profile[last_name]': {
				required: true,
				noSpace: true,
				minlength: 3,
			},
			'fashion_designer_profile[production_company]': {
				required: true,
				//noSpace: true,
				minlength: 3,
			},   
			'email': {
				required: true,
				email: true,
				noSpace: true,
				emailValid: true,
				remote:{
					url: window.url+'api/Users/checkEmailSignup.json',
				},
			},
		   	'password': {
				required: true,
				minlength:6,
			},
			'confirm_password': {
				required: true,
				minlength:6,
				equalTo:"#password",
		  	},
		  	'fashion_designer_profile[zipcode]': {
				required: true,
				noSpace: true,
				maxlength:10
			},
			'fashion_designer_profile[phone_number]': {
				//number: true,
				noSpace: true,
				maxlength:14
			}
		},
		messages: {
			'email': {
				required: "Please enter a valid email address",
				remote: "The email address you have entered is already registered"
			},
			'fashion_designer_profile[first_name]': {
				required: "Please enter your first name",
				minlength: "Your firstname must be at least 3 characters long"
			},
			'fashion_designer_profile[last_name]': {
				required: "Please enter your last name",
				minlength: "Your lastname must be at least 3 characters long"
			},
			'password': {
				required: "Please enter a password",
				minlength: "Your password must be at least 6 characters long"
			},
			'confirm_password': {
				equalTo: "Please enter the same password."
			},
			'fashion_designer_profile[zipcode]': {
				required: "Please enter the zipcode."
			},
			'fashion_designer_profile[phone_number]': {
				required: "Please enter a phone number",
			}
		}
	});
};

