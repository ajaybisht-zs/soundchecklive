$(function () {
    $(document).on('change', "input[name='user_profile[is_signed]']", function () {
        checkSigned()
    });

    /*$(document).on('change', "input[name='user_profile[is_performance_rights_affiliated]']", function () {
        var inputCheckBoxValue = $(this).val();
        if (inputCheckBoxValue == 2) {
            $('#agency-name').removeClass('d-none');
        } else {
            $('#agency-name').addClass('d-none');
            $('#agency-name').val('');
        }
    });*/

   /* $(document).on('click', "input[name='fashion_designer_profile[is_licensed_brand_or_independent_line]']", function () {
        var inputCheckBoxValue = $(this).val();
        if (inputCheckBoxValue == 1) {
            $('#parent_company').addClass('d-none');
        } else {
            $('#parent_company').removeClass('d-none');
            $('#agency-name').val('');
        }
    });*/

    $('input:radio[name="fashion_designer_profile[is_licensed_brand_or_independent_line]"]').change(function () {
        if ($(this).val() == '1') {
            $('#parent_company').addClass('d-none');
            $('#parent_company').val('');
             $('#parent_company').removeClass('required');
             $('#parent_company-error').remove();
        }
        if ($(this).val() == '0') {
           $('#parent_company').removeClass('d-none');
            $('#parent_company').addClass('required');
        }
    });

    $(document).on('change', "input[name='user_profile[is_ready_for_release]']", function () {
        console.log(this.value)
        setRelaseReady();
    });

    $('input:radio[name="radio12"]').change(function () {
        if ($(this).val() == '1') {
            $(document).find('#merchandiseContainer').addClass('d-none');
        }
        if ($(this).val() == '0') {
            $(document).find('#merchandiseContainer').removeClass('d-none');
        }
    });
    $('input:radio[name="radio11"]').change(function () {
        if ($(this).val() == '1') {
            $(document).find('#upcomingShowsContainer').addClass('d-none');
        }
        if ($(this).val() == '0') {
            $(document).find('#upcomingShowsContainer').removeClass('d-none');
        }
    });

    /*$(document).on('click', ".next", function () {
        var index = $('.step-from').index($(this).closest('.step-from'));
        $('.step-from').addClass('d-none');
        $(this).closest('.step-from').next('.step-from').removeClass('d-none');
        window.location.hash = index;
    });*/

    $(document).on('click', ".previous", function () {
        var index = $('.step-from').index($(this).closest('.step-from'));
        console.log('>>>',index)
        $('.step-from').addClass('d-none');
        $(this).closest('.step-from').prev('.step-from').removeClass('d-none');
        window.location.hash = index;
    });

    $(document).on('click', ".previousSpec", function () {
        var index = $('.step-from').index($(this).closest('.step-from'));
        $('.step-from').addClass('d-none');
         $(this).closest('.step-from').prev('.step-from').removeClass('d-none');
            window.location.hash = index;
        // let checkvalue = $("input[name='user_profile[is_ready_for_release]']:checked").val()
        // if (checkvalue == 1) {
        //     $(this).closest('.step-from').prev('.step-from').removeClass('d-none');
        //     window.location.hash = index;
        // } else {
        //     $('.step-from:eq(9)').removeClass('d-none')
        //     window.location.hash = 9;
        // }

    });

    $(document).on('click', '.plan-box', function () {
        $('.plan-box').removeClass('active');
        $('.plan-box').addClass('bg-white rounded');
        $(this).addClass('active');
        $(this).removeClass('bg-white rounded');
        $('#selectedPlan').val($(this).attr('data-id'));
    });

    checkSigned();
    currentStep();
    setRelaseReady();
});

function checkSigned() {
    if ($("input[name='user_profile[is_signed]']:checked").val() == 0) {
        $(document).find('.signed-input').removeClass('d-none');
    } else {
        $(document).find('.signed-input').addClass('d-none');
    }

}

function checkAffilated() {
    if ($("input[name='user_profile[is_performance_rights_affiliated]']:checked").val() == 1) {
        $(document).find('.signed-input').removeClass('d-none');
    } else {
        $(document).find('.signed-input').addClass('d-none');
    }

}

function setRelaseReady() {
    let checkvalue = $("input[name='user_profile[is_ready_for_release]']:checked").val()
    if (checkvalue == 1) {
        $('#releaseNext').val(11)
    } else {
        $('#releaseNext').val(16)
    }
}

jQuery.validator.addMethod("validateDate", function (value, element) {
    if ($('#birthMonth').val() != '') {
        //Checks for mm/dd/yyyy format.
        dtMonth = $('#birthMonth').val();
        dtDay = $('#birthDay').val();
        dtYear = $('#birthYear').val();

        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    } else {
        return true;
    }
}, "Entered date does not exist in the month.");

function currentStep() {
    let step = window.location.hash.substr(1);
    if (step == "" || step == 0) {
        step = 1;
    }
    let percent = (step / 17) * 100;
    console.log(percent)
    $('.progress-bar').css('width', percent + '%');
    $('.step-from').addClass('d-none');
    $(".step-from:eq(" + (step - 1) + ")").removeClass('d-none');
}

// $('form').on('submit', function(e){
//     e.preventDefault();
//     console.log($(this).closest('.step-from'))
//     $(this).closest('.step-from').addClass('d-none');
//     $(this).closest('.step-from').next('.step-from').removeClass('d-none')
//     console.log($(this).closest('.step-from').next('.step-from'))
// });


function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}


function updateImageDisplay(input, imagepreview) {
    var curFiles = input.files;
    console.log(curFiles)
    if (curFiles.length === 0) {
        var para = document.createElement('p');
        para.textContent = 'No files currently selected for upload';
        preview.appendChild(para);
    } else {

        for (var i = 0; i < curFiles.length; i++) {
            // if(validFileType(curFiles[i])) {
            // para.textContent = 'File name ' + curFiles[i].name + ', file size ' + returnFileSize(curFiles[i].size) + '.';
            var image = document.getElementById(imagepreview);
            image.src = window.URL.createObjectURL(curFiles[i]);

            // } else {
            //     para.textContent = 'File name ' + curFiles[i].name + ': Not a valid file type. Update your selection.';
            //     listItem.appendChild(para);
            // }

            // list.appendChild(listItem);
            // }
        }
    }
}


function updateImageName(input, target) {
    var curFiles = input.files;
    console.log(curFiles)
    if (curFiles.length === 0) {
        var para = document.createElement('p');
        para.textContent = 'No files currently selected for upload';
        preview.appendChild(para);
    } else {

        for (var i = 0; i < curFiles.length; i++) {
            // if(validFileType(curFiles[i])) {
            // para.textContent = 'File name ' + curFiles[i].name + ', file size ' + returnFileSize(curFiles[i].size) + '.';
            $(target).attr('placeholder', curFiles[i].name)
            // var image = document.getElementById(imagepreview);

            // image.src = window.URL.createObjectURL();

            // } else {
            //     para.textContent = 'File name ' + curFiles[i].name + ': Not a valid file type. Update your selection.';
            //     listItem.appendChild(para);
            // }

            // list.appendChild(listItem);
            // }
        }
    }
}
